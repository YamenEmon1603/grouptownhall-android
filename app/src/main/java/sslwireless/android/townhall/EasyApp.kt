package sslwireless.android.townhall

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.utils.SharedPrefHeaderSingleton
import net.danlew.android.joda.JodaTimeAndroid

class EasyApp : Application() {


    private lateinit var dataManager: DataManager

   // lateinit var context: Context

    companion object {
        lateinit var context: Context
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        context = base
        setDataManager()
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        JodaTimeAndroid.init(this)

        SharedPrefHeaderSingleton.instance.init(this)
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    fun getDataManager(): DataManager {
        return dataManager
    }

    fun setDataManager() {
        dataManager = DataManager(this)
    }

}