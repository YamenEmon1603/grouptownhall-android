package sslwireless.android.townhall.data

import android.content.Context
import sslwireless.android.townhall.data.local_db.RoomHelper
import sslwireless.android.townhall.data.network.ApiHelper
import sslwireless.android.townhall.data.network.RetrofitFactory
import sslwireless.android.townhall.data.prefence.PreferencesHelper

class DataManager(
    context: Context
) {
    private val context = context

    val mContext = context
    val mPref = PreferencesHelper(mContext)
    val roomHelper = RoomHelper(context)
    val apiHelper = ApiHelper(RetrofitFactory.providePostApi(mPref))

}