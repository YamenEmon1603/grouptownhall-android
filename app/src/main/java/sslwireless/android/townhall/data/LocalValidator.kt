package sslwireless.android.townhall.data

import android.util.Patterns
import com.google.i18n.phonenumbers.PhoneNumberUtil
import java.util.regex.Pattern


class LocalValidator {
    companion object {

      //  val regex = "/(^(\\+88|0088)?(01){1}[3456789]{1}(\\d){8})\$/\n"
        val regex2 = "/(^(\\+8801|8801|01|008801))[1|3-9]{1}(\\d){8}\$/\n"
        val regex3 = "(^([+]{1}[8]{2}|0088)?(01){1}[3-9]{1}\\d{8})\$\n"
        private val phonePattern = "^01[1-9]\\d{8}\$"

        fun isEmailValid(email: String): Boolean {
            return Patterns.EMAIL_ADDRESS.toRegex().matches(email);
        }
        fun isPhoneValid(phone: String): Boolean {
            val phoneUtil = PhoneNumberUtil.getInstance()
            try {
                val numberProto = phoneUtil.parse(phone, "BD")
                return phoneUtil.isValidNumber(numberProto)
            } catch (e: Exception) {
                System.err.println("NumberParseException was thrown: $e")
            }
            return false
        }

        fun isNewPhnValid(phone:String):Boolean {
            val pattern = Pattern.compile(
                phonePattern
            )
            val matcher = pattern.matcher(phone)
            return matcher.matches()
        }
    }
}