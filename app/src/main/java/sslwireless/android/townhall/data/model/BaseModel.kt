package sslwireless.android.townhall.data.model

class BaseModel<z>() {
    lateinit var status:String
    lateinit var message: String
    var token: String? = null
    var ui: String? = null
    var code = -1
    var data: z? = null
}