package sslwireless.android.townhall.data.model

class DeviceInfo {

    //lateinit var DEVICE_ID: String
    //lateinit var MODEL: String
    //lateinit var MANUFACTURER: String
    //lateinit var BRAND: String
    lateinit var KEY_BROWSER: String
    lateinit var KEY_BROWSER_VERSION: String
    lateinit var KEY_DEVICE: String
    lateinit var KEY_DEVICE_KEY: String
    lateinit var KEY_OS: String
    lateinit var KEY_OS_VERSION: String
    lateinit var KEY_USER_AGENT: String
    lateinit var IP_ADDRESS: String
    var OTP_KEY: String?=null
    lateinit var FCM_TOKEN: String
    var app_android_version: Int = 0

}
