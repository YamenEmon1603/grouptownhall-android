package sslwireless.android.townhall.data.model

import com.google.gson.annotations.SerializedName

data class DeviceLocation(

	@field:SerializedName("zip")
	val zip: String,

	@field:SerializedName("country")
	val country: String,

	@field:SerializedName("city")
	val city: String,

	@field:SerializedName("org")
	val org: String,

	@field:SerializedName("timezone")
	val timezone: String,

	@field:SerializedName("regionName")
	val regionName: String,

	@field:SerializedName("isp")
	val isp: String,

	@field:SerializedName("query")
	val query: String,

	@field:SerializedName("lon")
	val lon: Double,


	@field:SerializedName("countryCode")
	val countryCode: String,

	@field:SerializedName("region")
	val region: String,

	@field:SerializedName("lat")
	val lat: Double,

	@field:SerializedName("status")
	val status: String
)
