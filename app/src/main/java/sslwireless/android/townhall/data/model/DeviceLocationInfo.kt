package sslwireless.android.townhall.data.model

data class DeviceLocationInfo (
        val status:String?,
        val city:String?,
        val country:String?
)
