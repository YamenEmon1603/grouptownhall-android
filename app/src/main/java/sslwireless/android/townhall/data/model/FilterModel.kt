package sslwireless.android.townhall.data.model

import java.io.Serializable

data class FilterModel(var categoryName: String, var isSelected: Boolean = false) : Serializable