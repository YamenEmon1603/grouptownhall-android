package sslwireless.android.townhall.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GameResponse(
    @SerializedName("game_info") val game_info: GameInfo? = null,
    @SerializedName("is_eligible") val is_eligible: Boolean? = null
) : Serializable

data class GameInfo(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("game_title") val game_title: String? = null,
    @SerializedName("game_code") val game_code: String? = null,
    @SerializedName("description") val description: String? = null,
    @SerializedName("image_url") val image_url: String? = null,
    @SerializedName("game_image") val game_image: String? = null,
    @SerializedName("status") val status: Int? = null,
    @SerializedName("created_at") val created_at: String? = null
) : Serializable