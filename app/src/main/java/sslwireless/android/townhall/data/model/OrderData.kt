package sslwireless.android.townhall.data.model

import com.google.gson.annotations.SerializedName

data class OrderData(

	@field:SerializedName("transaction_id")
	val transactionId: String,

	@field:SerializedName("amount")
	val amount: Int,

	@field:SerializedName("phone")
	val phone: String,

	@field:SerializedName("bonus_percentage")
	val bonusPercentage: Int,

	@field:SerializedName("bonus_amount")
	val bonusAmount: Int,

	@field:SerializedName("type")
	val type: String,

	@field:SerializedName("operator")
	val operator: Int,

	@field:SerializedName("status")
	val status: String
)
