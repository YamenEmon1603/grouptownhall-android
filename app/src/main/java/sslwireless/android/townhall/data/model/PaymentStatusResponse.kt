package sslwireless.android.townhall.data.model

import com.squareup.moshi.Json

data class PaymentStatusResponse(

	@field:Json(name="order_data")
	val orderData: String? = null,

	@field:Json(name="status")
	val status: String? = null
)
