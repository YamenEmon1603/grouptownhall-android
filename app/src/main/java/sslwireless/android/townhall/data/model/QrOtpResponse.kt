package sslwireless.android.townhall.data.model

import com.google.gson.annotations.SerializedName

data class QrOtpResponse (

    @field:SerializedName("bangla_qr_pin_key")
    var bangla_qr_pin_key: String? = null

)