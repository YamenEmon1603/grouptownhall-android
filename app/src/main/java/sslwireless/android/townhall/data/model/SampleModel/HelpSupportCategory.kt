package sslwireless.android.townhall.data.model.SampleModel

import java.io.Serializable

class HelpSupportCategory(category_name:String, category_icon:Int, category_details:String) : Serializable{
    var category_name = category_name
    var category_icon = category_icon
    var category_details = category_details
}