package sslwireless.android.townhall.data.model.SampleModel

import java.io.Serializable

class HelpSupportItem(title:String, shortDescription:String, longDescription:String,category_icon: Int): Serializable {
    var title = title
    var shortDescription = shortDescription
    var longDescription = longDescription
    var category_icon = category_icon
}