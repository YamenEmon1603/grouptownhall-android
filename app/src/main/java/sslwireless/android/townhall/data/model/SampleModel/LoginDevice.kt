package sslwireless.android.townhall.data.model.SampleModel

import com.squareup.moshi.Json

data class LoginDevice(

	@Json(name="os")
	val os: String,

	@Json(name="browser")
	val browser: String,

	@field:Json(name="os_version")
	val osVersion: String,

	@field:Json(name="session_key")
	val sessionKey: String,

	@field:Json(name="device_location")
	val deviceLocation: String,

	@Json(name="userAgent")
	val userAgent: String,

	@field:Json(name="logged_time")
	val loggedTime: String,

	@field:Json(name="browser_version")
	val browserVersion: String,

	@Json(name="device")
	val device: String
)
