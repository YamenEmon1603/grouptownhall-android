package sslwireless.android.townhall.data.model.SampleModel

class LoginSession(device_type:String, device_name:String, app_name:String,login_time:String) {
    var device_type : String = device_type
    var device_name : String = device_name
    var app_name : String = app_name
    var login_time : String = login_time
}