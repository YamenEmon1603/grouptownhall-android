package sslwireless.android.townhall.data.model.SampleModel

import com.squareup.moshi.Json

data class Notification (

    @field:Json(name="icon_url")
    val iconUrl: String,

    @Json(name="link")
    val link: String,

    @Json(name="id")
    val id: Int,

    @Json(name="title")
    val title: String,

    @Json(name="message")
    val message: String,

    @Json(name="status")
    val status: Int,

    @field:Json(name="notified_at")
    val notifiedAt: String
)