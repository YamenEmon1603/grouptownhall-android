package sslwireless.android.townhall.data.model.SampleModel

class Promo(promo_code:String,promo_details:String,promo_validity:String) {
    var promo_code : String = promo_code
    var promo_details : String = promo_details
    var promo_validity : String = promo_validity
}