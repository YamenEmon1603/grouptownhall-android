package sslwireless.android.townhall.data.model

import java.io.Serializable

data class StoreUtilityData(
    var level: String, var bnLevel: String,
    var dataValue: String
) : Serializable