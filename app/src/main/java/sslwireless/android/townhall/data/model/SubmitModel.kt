package sslwireless.android.townhall.data.model

import java.io.Serializable

data class SubmitModel(
    var data: String,
    var requiredCheck: Boolean,
    var type: String,
    var dataType: String,
    var level: String,
    var bnLevel: String
) : Serializable