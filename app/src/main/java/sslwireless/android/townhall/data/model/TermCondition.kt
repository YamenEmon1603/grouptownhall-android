package sslwireless.android.townhall.data.model

data class TermCondition(
    val created_at: String,
    val description: String,
    val description_bn: String,
    val id: Int,
    val subtitle: String,
    val subtitle_bn: String,
    val title: String,
    val title_bn: String,
    val title_desc: String,
    val title_desc_bn: String,
    val updated_at: String
)