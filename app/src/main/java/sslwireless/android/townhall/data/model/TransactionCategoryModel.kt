package sslwireless.android.townhall.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransactionCategoryModel(
    var user: String? = null,
    var categoryName: String? = null,
    var amount: String? = null,
    var transactionId: String? = null,
    var paymentMethod: String? = null,
    var status: String? = null,
    var statusEnum: Int? = null,
    var date: String? = null,
    var token: String? = null,
    var type: Int,
    var rating: Int=0,
    var sub_bill_status: String=""
) : Parcelable