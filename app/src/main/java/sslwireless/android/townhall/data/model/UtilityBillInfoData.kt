package sslwireless.android.townhall.data.model

import java.io.Serializable

data class UtilityBillInfoData(
    var parameter: String,
    var dataValue: String?,
    var isAmount : Boolean = false
) : Serializable