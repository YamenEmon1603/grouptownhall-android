package sslwireless.android.townhall.data.model.bill_api

import com.google.gson.annotations.SerializedName

data class BillApiResponse(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("ui")
	val ui: Any? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Data(

	@field:SerializedName("recharge_completed_at")
	val rechargeCompletedAt: String? = null,

	@field:SerializedName("service_completed_at")
	val serviceCompletedAt: String? = null,

	@field:SerializedName("order_data")
	val orderData: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("service_status")
	val serviceStatus: Any? = null,

	@field:SerializedName("paid_amount")
	val paidAmount: Int? = null,

	@field:SerializedName("service_schedule_at")
	val serviceScheduleAt: Any? = null,

	@field:SerializedName("rating")
	val rating: Int? = null,

	@field:SerializedName("bonus_amount")
	val bonusAmount: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
