package sslwireless.android.townhall.data.model.bill_info

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BillInfoModel(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("ui")
	val ui: Any? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Serializable