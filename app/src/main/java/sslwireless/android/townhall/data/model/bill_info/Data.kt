package sslwireless.android.townhall.data.model.bill_info

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Data(

	@field:SerializedName("status_title")
	val statusTitle: String? = null,

	@field:SerializedName("transaction_id")
	val transactionId: String? = null,

	@field:SerializedName("status_code")
	val statusCode: String? = null,

	@field:SerializedName("data")
	val data: Map<String,String>?=null,

	@field:SerializedName("bn_title")
	val bnTitle: String? = null,

	@field:SerializedName("lid")
	val lid: String? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("account_number")
	val accountNumber: String? = null,

	@field:SerializedName("bill_amount")
	val billAmount: String? = null,

	@field:SerializedName("stamp_amount")
	val stampAmount: String? = null,

	@field:SerializedName("due_date")
	val dueDate: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("organization_code")
	val organizationCode: Int? = null,

	@field:SerializedName("zone_code")
	val zoneCode: String? = null,

	@field:SerializedName("settlement_account")
	val settlementAccount: String? = null,

	@field:SerializedName("lpc_amount")
	val lpcAmount: String? = null,

	@field:SerializedName("bill_number")
	val billNumber: String? = null,

	@field:SerializedName("vat_amount")
	val vatAmount: String? = null,

	@field:SerializedName("total_amount")
	val totalAmount: String? = null,

	@field:SerializedName("deducted_amount")
	val deductedAmount: String? = null,

	@field:SerializedName("tariff")
	val tariff: String? = null,

	@field:SerializedName("note")
	val note: String? = null
): Serializable