package sslwireless.android.townhall.data.model.bill_info

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataItem(

    @field:SerializedName("account_number")
    val Account_Number: String? = null,

    @field:SerializedName("bill_number")
    val Bill_Number: String? = null,

    @field:SerializedName("zone_code")
    val Zone_Code: String? = null,

    @field:SerializedName("due_date")
    val Due_Date: String? = null,

    @field:SerializedName("organization_code")
    val Organization_Code: Int? = null,

    @field:SerializedName("tariff")
    val Tariff: String? = null,

    @field:SerializedName("settlement_account")
    val Settlement_Account: String? = null,

    @field:SerializedName("bill_amount")
    val Bill_Amount: String? = null,

    @field:SerializedName("vat_amount")
    val Vat_Amount: String? = null,

    @field:SerializedName("lpc_amount")
    val Lpc_Amount: String? = null,

    @field:SerializedName("stamp_amount")
    val Stamp_Amount: String? = null,

    @field:SerializedName("total_amount")
    val Total_Amount: String? = null,

    @field:SerializedName("deducted_amount")
    val Deducted_Amount: String? = null,

    @field:SerializedName("discount")
    val Discount: String? = null,

    @field:SerializedName("convenience_fee")
    val Convenience_Fee: String? = null
): Serializable