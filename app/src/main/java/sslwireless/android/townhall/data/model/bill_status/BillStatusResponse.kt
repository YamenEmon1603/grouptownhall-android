package sslwireless.android.townhall.data.model.bill_status

import com.google.gson.annotations.SerializedName

data class BillStatusResponse(

	@field:SerializedName("data")
	val data: Data? = null
)

data class Data(

	@field:SerializedName("note")
	val note: String? = null,

	@field:SerializedName("total_amount")
	val totalAmount: String? = null,

	@field:SerializedName("mobile_no")
	val mobileNo: String? = null,

	@field:SerializedName("deducted_amount")
	val deductedAmount: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("meter_no")
	val meterNo: String? = null,

	@field:SerializedName("message")
	val message: String? = null
)
