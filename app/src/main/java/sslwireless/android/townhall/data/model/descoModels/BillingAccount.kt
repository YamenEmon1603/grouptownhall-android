package sslwireless.android.townhall.data.model.descoModels

data class BillingAccount(
    var type: String,
    var account_no: String,
    var account_title: Any,
    var account_type: String,
    var monthly_bill_alert: Int
)