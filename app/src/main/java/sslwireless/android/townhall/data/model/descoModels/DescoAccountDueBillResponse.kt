package sslwireless.android.townhall.data.model.descoModels

data class DescoAccountDueBillResponse(
    val data: List<DescoBillItem>,
    val monthly_bill_enable: Int
)