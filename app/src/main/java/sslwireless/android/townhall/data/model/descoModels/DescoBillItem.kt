package sslwireless.android.townhall.data.model.descoModels

import java.io.Serializable

data class DescoBillItem(
    val account_number: String,
    val bill_amount: String,
    val bill_number: String,
    val due_date: String,
    val lpc_amount: String,
    val organization_code: String,
    val settlement_account: String,
    val tariff: String,
    val total_amount: String,
    val vat_amount: String,
    val zone_code: String,
    var is_selected:Boolean = false
):Serializable