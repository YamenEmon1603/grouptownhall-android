package sslwireless.android.townhall.data.model.descoModels

data class DescoBillPaymentItem(
    val account_no: String,
    val account_type: String,
    val amount: String,
    val bill_no: String
)