package sslwireless.android.townhall.data.model.descoModels

data class DescoBillPaymentRequestModel(
    val data: List<DescoBillPaymentItem>,
    val monthly_bill_alert: Int,
    val token: String,
    val is_mobile: Boolean = true
)