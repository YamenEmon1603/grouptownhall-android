package sslwireless.android.townhall.data.model.experience

import com.squareup.moshi.Json

data class ExperienceResponse(

	@Json(name="townhall")
	val townhall: Townhall? = null,

	@Json(name="image_upload_status")
	val imageUploadStatus: Int? = null
)

data class ItemsItem(

	@field:Json(name="short_description")
	val shortDescription: String? = null,

	@Json(name="background")
	val background: String? = null,

	@Json(name="icon")
	val icon: String? = null,

	@Json(name="link")
	val link: String? = null,

	@Json(name="title")
	val title: String? = null
)

data class Townhall(

	@Json(name="items")
	val items: List<ItemsItem>? = null
)
