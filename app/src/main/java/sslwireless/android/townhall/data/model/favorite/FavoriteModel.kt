package sslwireless.android.townhall.data.model.favorite

import java.io.Serializable

data class FavoriteModel(
    val connection_type: String,
    val name: String,
    val operator_id: Int,
    val phone: String
):Serializable