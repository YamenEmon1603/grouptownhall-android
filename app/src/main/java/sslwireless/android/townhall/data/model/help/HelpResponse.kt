package sslwireless.android.townhall.data.model.help

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HelpResponse(

	@field:Json(name="active_popular_topics")
	val activePopularTopics: List<ActivePopularTopicsItem?>? = null,

	@field:Json(name="active_child")
	val activeChild: List<ActivePopularTopicsItem?>? = null,

	@field:Json(name="support_email")
	val supportEmail: String? = null,

	@field:Json(name="updated_at")
	val updatedAt: String? = null,

	@field:Json(name="support_phone")
	val supportPhone: String? = null,

	@field:Json(name="created_at")
	val createdAt: String? = null,

	@field:Json(name="support_phone_bn")
	val supportPhoneBn: String? = null,

	@field:Json(name="title")
	val title: String? = null,

	@field:Json(name="title_bn")
	val titleBn: String? = null,

	@field:Json(name="id")
	val id: Int? = null,

	@field:Json(name="support_email_bn")
	val supportEmailBn: String? = null,

	@field:Json(name="active_categories")
	val activeCategories: List<ActiveCategoriesItem?>? = null
): Parcelable