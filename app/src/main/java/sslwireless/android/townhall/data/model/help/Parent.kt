package sslwireless.android.townhall.data.model.help

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Parent(

	@field:Json(name="description_bn")
	val descriptionBn: String? = null,

	@field:Json(name="banner")
	val banner: String? = null,

	@field:Json(name="description")
	val description: String? = null,

	@field:Json(name="created_at")
	val createdAt: String? = null,

	@field:Json(name="link_title")
	val linkTitle: String? = null,

	@field:Json(name="contact_id")
	val contactId: Int? = null,

	@field:Json(name="type")
	val type: String? = null,

	@field:Json(name="title")
	val title: String? = null,

	@field:Json(name="priority")
	val priority: Int? = null,

	@field:Json(name="link_title_bn")
	val linkTitleBn: String? = null,

	@field:Json(name="updated_at")
	val updatedAt: String? = null,

	@field:Json(name="parent_id")
	val parentId: String? = null,

	@field:Json(name="subtitle")
	val subtitle: String? = null,

	@field:Json(name="title_bn")
	val titleBn: String? = null,

	@field:Json(name="link_url")
	val linkUrl: String? = null,

	@field:Json(name="subtitle_bn")
	val subtitleBn: String? = null,

	@field:Json(name="fav")
	val fav: Int? = null,

	@field:Json(name="id")
	val id: Int? = null,

	@field:Json(name="status")
	val status: Int? = null
): Parcelable