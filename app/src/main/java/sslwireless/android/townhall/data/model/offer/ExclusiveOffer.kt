package sslwireless.android.townhall.data.model.offer

data class ExclusiveOffer(
    val banner: String,
    val code: String,
    val code_title: String,
    val code_title_bn: String,
    val created_at: String,
    val description: String,
    val description_bn: String,
    val exclusive_offer_type_id: Int,
    val id: Int,
    val priority: Int,
    val status: Int,
    val title: String,
    val title_bn: String,
    val updated_at: String
)