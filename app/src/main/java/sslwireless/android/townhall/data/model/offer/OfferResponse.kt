package sslwireless.android.townhall.data.model.offer

data class OfferResponse(
    val created_at: String,
    val exclusive_offers: List<ExclusiveOffer>,
    val id: Int,
    val priority: Int,
    val status: Int,
    val title: String,
    val title_bn: String,
    val title_slug: String,
    val updated_at: String
)