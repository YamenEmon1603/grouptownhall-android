package sslwireless.android.townhall.data.model.operator

import com.squareup.moshi.Json

data class OperatorModel(

	@field:Json(name="operator_name")
	val operatorName: String,

	@field:Json(name="operator_name_bn")
	val operatorNameBn: String,

	@field:Json(name="postpaid_high_limit")
	val postpaidHighLimit: Int,

	@field:Json(name="operator_short_name")
	val operatorShortName: String,

	@field:Json(name="operator_id")
	val operatorId: Int,

	@field:Json(name="image_url")
	val imageUrl: String,

	@field:Json(name="prefix")
	val prefix: String,

	@field:Json(name="postpaid_low_limit")
	val postpaidLowLimit: Int,

	@field:Json(name="prepaid_high_limit")
	val prepaidHighLimit: Int,

	@field:Json(name="prepaid_low_limit")
	val prepaidLowLimit: Int
)
