package sslwireless.android.townhall.data.model.populerPlan

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PopularPlan(
    val amount_max: Int,
    val amount_min: Int,
    val created_at: String?,
    val description: String,
    val description_bn: String,
    val id: Int,
    val is_for_postpaid: Int,
    val is_for_prepaid: Int,
    val log_time: String,
    val offer_details_url: String,
    val offer_expire_time: String,
    val offer_title: String,
    val offer_type: String,
    val operator_name: String,
    val operator_short_name: String,
    val priority: Int?,
    val rank: Int,
    val is_popular: Int,
    val usage_cnt: Int,
    val updated_at: String?,
    val validity: String?,
    val validity_bn: String?,
    val ribbon: String?,
    val ribbon_bn: String?
): Parcelable