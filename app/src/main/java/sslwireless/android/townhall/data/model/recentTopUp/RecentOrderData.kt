package sslwireless.android.townhall.data.model.recentTopUp

import com.google.gson.annotations.SerializedName

data class RecentOrderData (
    @field:SerializedName("amount")
    val amount: Int? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("operator")
    val operator: Int? = null,

    var time:String?=null


)