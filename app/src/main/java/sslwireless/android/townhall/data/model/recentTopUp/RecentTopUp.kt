package sslwireless.android.townhall.data.model.recentTopUp

import com.squareup.moshi.Json

data class RecentTopUp(

	@field:Json(name="service_title")
	val serviceTitle: String? = null,

	@field:Json(name="order_data")
	val orderData: String? = null,

	@field:Json(name="requested_at")
	val requestedAt: String? = null
)
