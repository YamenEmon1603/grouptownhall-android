package sslwireless.android.townhall.data.model.recharge_request

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("paginator") val paginator: Paginator? = null,
    @SerializedName("pagination_last_page") val pagination_last_page: Int? = null,
    @SerializedName("total_count") val total_count: Int? = null,
    @SerializedName("data") val data: List<RequestListItem>? = null
)