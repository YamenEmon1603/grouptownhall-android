package sslwireless.android.townhall.data.model.recharge_request

import com.google.gson.annotations.SerializedName

data class Paginator(
    @SerializedName("current_page") val current_page: Int? = null,
    @SerializedName("total_pages") val total_pages: Int? = null,
    @SerializedName("previous_page_url") val previous_page_url: String? = null,
    @SerializedName("next_page_url") val next_page_url: String? = null,
    @SerializedName("record_per_page") val record_per_page: Int? = null
)