package sslwireless.android.townhall.data.model.recharge_request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestBody(
    @SerializedName("amount") val amount: Double? = null,
    @SerializedName("recharge_amount") val rechargeAmount: String? = null,
    @SerializedName("operator") val operator: String? = null,
    @SerializedName("connection_type") val connection_type: String? = null,
    @SerializedName("operator_id") val operator_id: Int? = null,
    @SerializedName("requester_identifier") val requester_identifier: String? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("requested_identifier") val requested_identifier: String? = null,
    @SerializedName("msisdn") val msisdn: String? = null
) : Parcelable