package sslwireless.android.townhall.data.model.recharge_request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestDetails(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("requested") val requested: Requested? = null,
    @SerializedName("requester") val requester: Requester? = null,
    @SerializedName("ruid") val ruid: String? = null,
    @SerializedName("transaction_id") val transaction_id: String? = null,
    @SerializedName("notification_method") val notification_method: String? = null,
    @SerializedName("is_recharge_create") val is_recharge_create: Int? = null,
    @SerializedName("is_payment_done") val is_payment_done: Int? = null,
    @SerializedName("is_canceled") val is_canceled: Int? = null,
    @SerializedName("request_body") val request_body: RequestBody? = null
) : Parcelable
