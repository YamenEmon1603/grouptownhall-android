package sslwireless.android.townhall.data.model.recharge_request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestListItem(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("ruid") val ruid: String? = null,
    @SerializedName("transaction_id") val transaction_id: String? = null,
    @SerializedName("notification_method") val notification_method: String? = null,
    @SerializedName("status") val status: Int? = null,
    @SerializedName("status_value") val status_value: String? = null,
    @SerializedName("created_at") val created_at: String? = null,
    @SerializedName("request_body") val request_body: RequestBody? = null
) : Parcelable
