package sslwireless.android.townhall.data.model.recharge_request

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Requested(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("mobile_no") val mobile_no: String? = null,
    @SerializedName("address") val address: String? = null,
    @SerializedName("occupation") val occupation: String? = null,
    @SerializedName("organization") val organization: String? = null,
    @SerializedName("gender") val gender: String? = null,
    @SerializedName("date_of_birth") val date_of_birth: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("profile_picture_url") val profile_picture_url: String? = null,
    @SerializedName("email_verified") val email_verified: Int? = null,
    @SerializedName("mobile_verified") val mobile_verified: Int? = null,
    @SerializedName("status") val status: Int? = null,
    @SerializedName("google_id") val google_id: String? = null,
    @SerializedName("facebook_id") val facebook_id: String? = null,
    @SerializedName("apple_id") val apple_id: String? = null,
    @SerializedName("sign_up_source") val sign_up_source: String? = null,
    @SerializedName("social_media_token") val social_media_token: String? = null,
    @SerializedName("loyalty_point") val loyalty_point: Int? = null,
    @SerializedName("password") val password: String? = null,
    @SerializedName("qr_pin") val qr_pin: String? = null,
    @SerializedName("w_pin_count") val w_pin_count: Int? = null,
    @SerializedName("is_pin_block") val is_pin_block: Int? = null,
    @SerializedName("sslcommerz_reference_id") val sslcommerz_reference_id: String? = null,
    @SerializedName("remember_token") val remember_token: String? = null,
    @SerializedName("created_at") val created_at: String? = null,
    @SerializedName("updated_at") val updated_at: String? = null
): Parcelable