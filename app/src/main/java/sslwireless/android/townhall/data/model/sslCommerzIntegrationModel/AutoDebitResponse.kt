package sslwireless.android.townhall.data.model.sslCommerzIntegrationModel

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AutoDebitResponse(

	@Json(name="is_redirect_required")
	val is_redirect_required: Boolean,

	@Json(name="session_key")
	val session_key: String,

	@Json(name="redirect_gateway_url")
	val redirect_gateway_url: String,

	@Json(name="cancel_url")
	val cancel_url: String,

	@Json(name="fail_url")
	val fail_url: String,

	@Json(name="success_url")
	val success_url: String
): Parcelable
