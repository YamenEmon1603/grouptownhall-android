package sslwireless.android.townhall.data.model.sslCommerzIntegrationModel

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardsItem(

	@Json(name="card_index")
	val card_index: String? = null,

	@Json(name="card_no")
	val card_no: String? = null,

	@Json(name="is_default_card")
	val is_default_card: Int? = null,

	@Json(name="bank_name")
	val bank_name: String? = null,

	@Json(name="logo")
	val logo: String? = null,

	@Json(name="is_moto")
	val is_moto: Int? = null,

	@Json(name="channel")
	val channel: String? = null,

	@Json(name="channel_type")
	val channel_type: String? = null,

	@Json(name="expiry_date")
	val expiry_date: String? = null
): Parcelable