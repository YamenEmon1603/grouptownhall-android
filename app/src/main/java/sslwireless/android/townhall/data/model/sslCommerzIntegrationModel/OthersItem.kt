package sslwireless.android.townhall.data.model.sslCommerzIntegrationModel

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OthersItem(

	@Json(name="channel_type")
	val channel_type: String? = null,

	@Json(name="logo_url")
	val logo_url: String? = null,

	@Json(name="channel")
	val channel: String? = null,

	@Json(name="channel_name")
	val channel_name: String? = null
): Parcelable