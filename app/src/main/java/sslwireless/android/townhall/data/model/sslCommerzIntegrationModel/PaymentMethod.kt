package sslwireless.android.townhall.data.model.sslCommerzIntegrationModel

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PaymentMethod(

	@Json(name="cards")
	val cards: List<CardsItem>? = null,

	@Json(name="others")
	val others: List<OthersItem>? = null
): Parcelable