package sslwireless.android.townhall.data.model.sslCommerzIntegrationModel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SslCommerzIntegrationModel(
    val amount: Double,
    val ipn_url: String,
    val service_title: String,
    val store_id: String,
    val store_password: String,
    val transaction_id: String,
    val value_b: String,
    val value_c: String,
    val user_refer: String?,
    val pay_with_save_card: Boolean=false,
    val payment_methods : PaymentMethod?
): Parcelable