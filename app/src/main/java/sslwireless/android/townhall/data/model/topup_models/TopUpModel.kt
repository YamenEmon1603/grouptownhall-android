package sslwireless.android.townhall.data.model.topup_models

import java.io.Serializable

data class TopUpModel(
    var amount: Int,
    val operator: Int,
    val phone: String,
    val type: String,
    var isAdded:Boolean? = null
):Serializable