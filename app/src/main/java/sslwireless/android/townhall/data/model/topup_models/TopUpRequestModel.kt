package sslwireless.android.townhall.data.model.topup_models

data class TopUpRequestModel(
    val data : List<TopUpModel>,
    val schedule_recharge: String,
    val token: String,
    val is_mobile: Boolean = true,
    var is_recharge_request:Boolean? = null,
    var ruid:String?=null
)