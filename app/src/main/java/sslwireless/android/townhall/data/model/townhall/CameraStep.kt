package sslwireless.android.townhall.data.model.townhall

import java.io.File

data class CameraStep(
    var id: Int,
    var isCaptured: Boolean = false,
    var type: String,
    var image: File? = null
)