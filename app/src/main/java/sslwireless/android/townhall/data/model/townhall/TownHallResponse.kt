package sslwireless.android.townhall.data.model.townhall

import com.squareup.moshi.Json

data class TownHallResponse(

	@Json(name="townhall")
	val townhall: Townhall? = null,

	@field:Json(name="image_upload_status")
	val imageUploadStatus: Int? = null
)

data class Bot(

	@Json(name="whatsapp")
	val whatsapp: String? = null,

	@Json(name="messenger")
	val messenger: String? = null,

	@Json(name="telegram")
	val telegram: String? = null
)

data class InstructionsItem(

	@Json(name="button")
	val button: String? = null,

	@Json(name="description")
	val description: String? = null,

	@Json(name="title")
	val title: String? = null
)

data class ScheduleItem(

	@Json(name="time")
	val time: String? = null,

	@Json(name="title")
	val title: String? = null
)

data class Townhall(

	@Json(name="date")
	val date: String? = null,

	@Json(name="schedule")
	val schedule: List<ScheduleItem>? = null,

	@Json(name="instructions")
	val instructions: List<InstructionsItem>? = null,

	@Json(name="address")
	val address: String? = null,

	@field:Json(name="max_image_size")
	val max_image_size: Int? = null,

	@Json(name="phone")
	val phone: List<String>? = null,

	@Json(name="bot")
	val bot: Bot? = null,

	@field:Json(name="Vanue")
	val vanue: String? = null,

	@Json(name="time")
	val time: String? = null,

	@Json(name="title")
	val title: String? = null
)
