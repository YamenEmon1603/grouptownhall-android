package sslwireless.android.townhall.data.model.townhall

import com.google.gson.annotations.SerializedName

data class UserCheckResponse(
    @SerializedName("image_upload_status") val image_upload_status: Boolean? = null,
    @SerializedName("user_exist") val user_exist: Boolean? = null
)