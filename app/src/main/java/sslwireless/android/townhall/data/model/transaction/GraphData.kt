package sslwireless.android.townhall.data.model.transaction
import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class GraphData(
    @SerializedName("amount")
    val amount: String? = null,
    @SerializedName("month")
    val month: String? = null,
    @SerializedName("day")
    val day: String? = null
) : Serializable