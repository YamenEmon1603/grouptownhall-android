package sslwireless.android.townhall.data.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OrderData(
	@SerializedName("dada")
	val dada: List<OrderDataDetails>?
) : Serializable