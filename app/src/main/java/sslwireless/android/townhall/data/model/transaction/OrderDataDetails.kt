package sslwireless.android.townhall.data.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class OrderDataDetails(
    @SerializedName("Label")
    val label: String,
    @SerializedName("value")
    val value: String
) : Serializable