package sslwireless.android.townhall.data.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Transaction(
	/*@SerializedName("graph_data")
	val graph_data: List<GraphData>,*/
	@SerializedName("sum_amount")
	val sum_amount: String?,
	@SerializedName("transaction_data")
	val transaction_data: List<TransactionData>
): Serializable