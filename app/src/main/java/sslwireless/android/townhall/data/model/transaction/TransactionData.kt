package sslwireless.android.townhall.data.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class TransactionData(
    @SerializedName("amount")
    val amount: String,
    @SerializedName("transaction_id")
    val transaction_id: String,
    @SerializedName("payment_method")
    val payment_method: String,
    @SerializedName("service_title")
    val service_title: String,
    @SerializedName("order_data")
    val order_data: OrderData,
    @SerializedName("transaction_at")
    val transaction_at: String,
    @SerializedName("payment_status")
    val payment_status: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("status_enum")
    val status_enum: Int,
    @SerializedName("bill_status")
    val bill_status: String,
    @SerializedName("token")
    val token: String?=null,
    @SerializedName("rating")
    val rating: Int,
    @SerializedName("sub_bill_status")
    val sub_bill_status: String
) : Serializable