package sslwireless.android.townhall.data.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeviceKey {

    @SerializedName("device_key")
    @Expose
    var deviceKey: String? = null
    @SerializedName("app_version")
    @Expose
    var appVersion: String? = null
    @SerializedName("version_code")
    @Expose
    var versionCode: String? = null
}