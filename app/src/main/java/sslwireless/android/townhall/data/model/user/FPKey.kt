package sslwireless.android.townhall.data.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FPKey {

    @SerializedName("fpKey")
    @Expose
    var fpKey: String? = null

}