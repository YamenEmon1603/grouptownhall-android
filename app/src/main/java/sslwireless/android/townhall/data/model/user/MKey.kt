package sslwireless.android.townhall.data.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MKey {

    @SerializedName("mKey")
    @Expose
    var mKey: String? = null

}