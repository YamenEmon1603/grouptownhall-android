package sslwireless.android.townhall.data.model.user

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OTP {

    @SerializedName("otp")
    @Expose
    var otp: Int? = null

}