package sslwireless.android.townhall.data.model.user

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegistrationInfo(
    var name: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var referal_key: String? = null,
    var device_key: String? = null,
    var password: String? = null,
    var pass_confirmation: String? = null,
    var isSocial: Boolean = false,
    var social_login_id: String? = null ,
    var provider: String = ""
) : Parcelable