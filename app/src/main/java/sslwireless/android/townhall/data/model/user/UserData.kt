package sslwireless.android.townhall.data.model.user

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserData (
    @SerializedName("name")
    @Expose
    var name: String? = null,
    @SerializedName("email")
    @Expose
    var email: String = "",
    @SerializedName("mobile")
    @Expose
    var mobile: String = "",
    @SerializedName("member_since")
    @Expose
    var member_since: String? = null,
    @SerializedName("address")
    @Expose
    var address: String = "",
    @SerializedName("occupation")
    @Expose
    var occupation: String? = null,
    @SerializedName("organization")
    @Expose
    var organization: String? = null,
    @SerializedName("gender")
    @Expose
    var gender: String = "",
    @SerializedName("date_of_birth")
    @Expose
    var date_of_birth: String="",
    @SerializedName("loyalty_point")
    @Expose
    var loyalty_point: Int? = null,
    @SerializedName("profile_picture_url")
    @Expose
    var profile_picture_url: String ="",
    @SerializedName("social_login_id")
    @Expose
    var social_login_id: String? =null,
    @SerializedName("qr_pin")
    @Expose
    var qr_pin: Boolean =false

): Parcelable