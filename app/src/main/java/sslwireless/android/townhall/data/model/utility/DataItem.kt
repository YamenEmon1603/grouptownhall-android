package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataItem(

	@field:SerializedName("sequence")
	val sequence: Any? = null,

	@field:SerializedName("bn_title")
	val bnTitle: String? = null,

	@field:SerializedName("service_lists")
	val serviceLists: List<ServiceListsItem?>? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("commission")
	val commission: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("type")
	val type: String? = null
): Serializable