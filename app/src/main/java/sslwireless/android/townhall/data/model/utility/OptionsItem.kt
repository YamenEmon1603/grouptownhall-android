package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class OptionsItem(
    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("value")
    val value: String? = null,

    var lastPositionTrack: Int? = -1
) : Serializable