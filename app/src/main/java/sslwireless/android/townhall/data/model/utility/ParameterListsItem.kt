package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ParameterListsItem(

	@field:SerializedName("level")
	val level: String? = null,

	@field:SerializedName("utility_bill_id")
	val utilityBillId: Int? = null,

	@field:SerializedName("utility")
	val utility: Utility? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("required")
	val required: String? = null,

	@field:SerializedName("field_name")
	val fieldName: String? = null,

	@field:SerializedName("format")
	val format: String? = null,

	@field:SerializedName("bn_level")
	val bnLevel: String? = null,

	@field:SerializedName("sequence")
	val sequence: String? = null,

	@field:SerializedName("default")
	val jsonMemberDefault: Any? = null,

	@field:SerializedName("data_type")
	val dataType: String? = null,

	@field:SerializedName("options")
	val options: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: String? = null,

	var itemChecker: Boolean = false,
	var currentPosition: Int = -1
): Serializable