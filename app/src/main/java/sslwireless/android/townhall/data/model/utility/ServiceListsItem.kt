package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServiceListsItem(

	@field:SerializedName("service_category_id")
	val serviceCategoryId: Int? = null,

	@field:SerializedName("sequence")
	val sequence: Any? = null,

	@field:SerializedName("bn_title")
	val bnTitle: String? = null,

	@field:SerializedName("service_category")
	val serviceCategory: ServiceCategory? = null,

	@field:SerializedName("utility_bill_types")
	val utilityBillTypes: List<UtilityBillTypesItem?>? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
): Serializable