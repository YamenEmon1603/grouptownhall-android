package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Utility(

	@field:SerializedName("bn_title")
	val bnTitle: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
): Serializable