package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UtilityBillTypesItem(

	@field:SerializedName("sequence")
	val sequence: Any? = null,

	@field:SerializedName("bn_title")
	val bnTitle: String? = null,

	@field:SerializedName("parameter_lists")
	val parameterLists: List<ParameterListsItem?>? = null,

	@field:SerializedName("service_list_id")
	val serviceListId: Int? = null,

	@field:SerializedName("service_list")
	val serviceList: ServiceList? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("title")
	val title: String? = null
): Serializable