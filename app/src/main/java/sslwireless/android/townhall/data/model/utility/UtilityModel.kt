package sslwireless.android.townhall.data.model.utility

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UtilityModel(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("ui")
	val ui: Any? = null,

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Serializable