package sslwireless.android.townhall.data.model.utility_payment

import com.google.gson.annotations.SerializedName

data class Data(

	@field:SerializedName("store_id")
	val storeId: String? = null,

	@field:SerializedName("transaction_id")
	val transactionId: String? = null,

	@field:SerializedName("service_title")
	val serviceTitle: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("store_password")
	val storePassword: String? = null,

	@field:SerializedName("ipn_url")
	val ipnUrl: String? = null,

	@field:SerializedName("value_b")
	val valueB: String? = null,

	@field:SerializedName("value_c")
	val valueC: String? = null
)