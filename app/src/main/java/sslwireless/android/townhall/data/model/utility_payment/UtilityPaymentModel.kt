package sslwireless.android.townhall.data.model.utility_payment

import com.google.gson.annotations.SerializedName

data class UtilityPaymentModel(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("ui")
	val ui: Any? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)