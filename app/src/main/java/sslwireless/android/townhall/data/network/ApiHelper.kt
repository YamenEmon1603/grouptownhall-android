package sslwireless.android.townhall.data.network

import com.google.gson.Gson
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.DeviceInfo
import sslwireless.android.townhall.data.model.SubmitModel
import sslwireless.android.townhall.data.model.descoModels.DescoBillPaymentRequestModel
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.data.model.user.RegistrationInfo
import sslwireless.android.townhall.data.network.api_call_factory.ApiGetCall
import sslwireless.android.townhall.data.network.api_call_factory.ApiPostCall
import sslwireless.android.townhall.data.network.api_call_factory.ApiPostCallWithDocument
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import io.reactivex.Maybe
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import sslwireless.android.townhall.view.activity.qrPay.model.QRResponse

class ApiHelper(val apiService: IApiService) {

    companion object {
        //call type
        const val CALL_TYPE_GET = "get"
        const val CALL_TYPE_POST = "post"
        const val CALL_TYPE_POST_WITH_DOCUMENT = "post with document"

        //endpoint
        const val ENDPOINT_LOGIN = "user-login"
        const val ENDPOINT_GET_DEVICE_KEY = "townhall/app-release-version"
        const val ENDPOINT_USER_STATUS = "user-status"
        const val ENDPOINT_SOCIAL_LOGIN = "user-social-login"
        const val ENDPOINT_SOCIAL_LOGIN_FB = "social-login"
        const val ENDPOINT_USER_REG = "registration"
        const val ENDPOINT_USER_REG_VERIFY = "otp-verification"
        const val ENDPOINT_USER_LOGIN = "user-login"
        const val ENDPOINT_FORGOT_PASS = "forgot-password-otp"
        const val ENDPOINT_UOLOAD_IMAGE_STATUS = "townhall/image-upload-status"
        const val ENDPOINT_CHANGE_PASS = "forgot-password-restore"
        const val ENDPOINT_UPDATE_PASS = "password-update"
        const val ENDPOINT_UPDATE_PHONE = "old-user-otp-for-mobile-restore"
        const val ENDPOINT_GET_PLANS = "vr/get-plans"
        const val ENDPOINT_TOP_UP = "vr/top-up"

        const val ENDPOINT_ADD_TO_FAV_CONTACT = "vr/add-favourite-number"
        const val ENDPOINT_UPDATE_TO_FAV_CONTACT = "vr/update-favourite-number"
        const val ENDPOINT_PROFILE_PICTURE_UPLOAD = "profile-picture-upload"
        const val ENDPOINT_PROFILE = "profile-update"
        const val ENDPOINT_GET_FAV_CONTACT = "vr/get-favorite-number"
        const val ENDPOINT_GET_HELP_SUPPORT = "easy/fm/get-contact"
        const val ENDPOINT_GET_TOPIC_HELP_SUPPORT = "easy/fm/get-topics"
        const val ENDPOINT_GET_OPERATORS = "vr/operator-list"
        const val ENDPOINT_DELETE_FAV_CONTACT = "vr/delete-favourite-number"
        const val ENDPOINT_GET_LOGIN_DEVICES = "get-logged-in-devices"
        const val ENDPOINT_LOGOUT_SINGLE_DEVICE = "logout"
        const val ENDPOINT_LOGOUT_ALL_DEVICE = "logout-from-all-devices"
        const val ENDPOINT_GET_USE_PROFILE = "get-user-profile"
        const val ENDPOINT_GET_TOWNHALL_CONTENT_ = "townhall/content"
        const val ENDPOINT_DESCO_POSTPAID_DUE_BILLS = "desco/postpaid-due-bills"
        const val ENDPOINT_DESCO_BILL_PAYMENT = "desco/bill-payment"
        const val ENDPOINT_DESCO_ADD_ACOUNT = "desco/add-account"
        const val ENDPOINT_WASA_ADD_ACOUNT = "wasa/add-account"
        const val ENDPOINT_DESCO_GET_ACOUNTS = "desco/get-accounts"
        const val ENDPOINT_WASA_GET_ACOUNTS = "wasa/get-accounts"
        const val ENDPOINT_DESCO_DELETE_ACOUNT = "desco/delete-account"
        const val ENDPOINT_WASA_DELETE_ACOUNT = "wasa/delete-account"
        const val ENDPOINT_TRANSACTION_HISTORY = "report/dynamic-recent-transaction"
        const val ENDPOINT_RECENT_TRANSACTION = "report/recent-transaction"
        const val ENDPOINT_RERSEND_OTP = "resend-otp"
        const val ENDPOINT_RATING = "user-feedback/transaction-rating"
        const val ENDPOINT_INITIATE_HOLD_USER = "vr/initiate-hold-recharge-user"
        const val ENDPOINT_PAYMENT_STATUS = "payment-status"

        const val ENDPOINT_QR_PAYLOAD = "qr/payment-create"
        const val ENDPOINT_TRANSACTION_VERIFY = "qr/payment-init"
        const val ENDPOINT_QR_OTP_REQUEST = "qr/otp-send"
        const val ENDPOINT_QR_OTP_VERIFY = "qr/otp-verify"
        const val ENDPOINT_QR_SET_PIN = "qr/set-pin"
        const val ENDPOINT_QR_CHANGE_PIN = "qr/pin-change"
        const val ENDPOINT_CARD_DELETE = "payment/delete-card"
        const val ENDPOINT_AUTO_DEBIT = "payment/auto-debit"

        const val ENDPOINT_RECHARGE_REQUEST_CREATE = "recharge-request/create"
        const val ENDPOINT_RECHARGE_REQUEST_LIST = "recharge-request/list"
        const val ENDPOINT_RECHARGE_REQUESTED_LIST = "recharge-request/requested-list"
        const val ENDPOINT_RECHARGE_REQUEST_HISTORY = "recharge-request/receive-history"
        const val ENDPOINT_RECHARGE_REQUEST_REJECT = "recharge-request/reject"
        const val ENDPOINT_RECHARGE_REQUEST_INFO = "recharge-request/info"

        const val ENDPOINT_IMAGE_UPLOAD = "townhall/image-upload"
        const val ENDPOINT_TOWNHALL_SUBMIT_GAME_QR = "townhall/game-qr-user"
        const val ENDPOINT_TOWNHALL_USER_INFO = "townhall/townhall-user-check"

        //api method field key
        const val KEY_USER_NAME = "username"
        const val KEY_PASSWORD = "password"
        const val KEY_BROWSER = "browser"
        const val KEY_BROWSER_VERSION = "browser_version"
        const val KEY_DEVICE = "device"
        const val KEY_DEVICE_KEY = "device_key"
        const val KEY_OS = "os"
        const val KEY_OS_VERSION = "os_version"
        const val KEY_USER_AGENT = "userAgent"
        const val IP_ADDRESS = "ip_address"
    }


    //var token = EasyApp.context.getDataManager.mPref.prefGetCustomerToken().toString()
    fun utilityService(
        deviceKey: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap[KEY_DEVICE_KEY] = deviceKey
        getApiCallObservable(CALL_TYPE_POST, "dynamic/services", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun transactionHistoryService(
        apiCallbackHelper: ApiCallbackHelper,
        token: String,
        fromDate: String,
        toDate: String,
        isMonthWise: String
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] = token
        hashMap["from_date"] = fromDate
        hashMap["to_date"] = toDate
        hashMap["is_month_wise"] = isMonthWise

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_TRANSACTION_HISTORY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun recentTransactionService(
        apiCallbackHelper: ApiCallbackHelper,
        token: String
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECENT_TRANSACTION, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun billInfo(
        utility_bill_id: String,
        mHashmap: HashMap<String, SubmitModel>,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("utility_bill_id", utility_bill_id)

        for ((key, value) in mHashmap) {
            hashMap.put(key, value.data)
        }

        getApiCallObservable(CALL_TYPE_POST, "dynamic/bill-info", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun billPayment(
        utility_bill_id: String,
        transaction_id: String,
        is_mobile: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("utility_bill_id", utility_bill_id)
        hashMap.put("transaction_id", transaction_id)
        hashMap.put("is_mobile", is_mobile)
        hashMap.put("token", token)
        getApiCallObservable(CALL_TYPE_POST, "dynamic/bill-payment", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiLogin(username: String, password: String, apiCallbackHelper: ApiCallbackHelper) {

        val hashMap = HashMap<String, String>()
        hashMap.put(KEY_USER_NAME, username)
        hashMap.put(KEY_PASSWORD, password)
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_LOGIN, hashMap).subscribe(apiCallbackHelper)
    }

    fun apiGetDeviceKey(deviceInfo: DeviceInfo, apiCallbackHelper: ApiCallbackHelper) {

        val hashMap = HashMap<String, String>()
        hashMap.put("fcm_token", deviceInfo.FCM_TOKEN)
        hashMap.put(KEY_BROWSER, deviceInfo.KEY_BROWSER)
        hashMap.put(KEY_BROWSER_VERSION, deviceInfo.KEY_BROWSER_VERSION)
        hashMap.put(KEY_DEVICE, deviceInfo.KEY_DEVICE)
        hashMap.put(KEY_DEVICE_KEY, deviceInfo.KEY_DEVICE_KEY)
        hashMap.put(KEY_OS, deviceInfo.KEY_OS)
        hashMap.put(KEY_OS_VERSION, deviceInfo.KEY_OS_VERSION)
        hashMap.put(KEY_USER_AGENT, deviceInfo.KEY_USER_AGENT)
        hashMap.put(IP_ADDRESS, deviceInfo.IP_ADDRESS)
        deviceInfo.OTP_KEY?.let {
            hashMap.put("otp_key", it)
        }
        hashMap.put("app_android_version", deviceInfo.app_android_version.toString())

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_DEVICE_KEY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUserStatus(data: String, apiCallbackHelper: ApiCallbackHelper) {

        val hashMap = HashMap<String, String>()


        if (data.contains("@")) {
            hashMap.put("email", data)
            // hashMap.put("device_key", deviceKey!!.deviceKey.toString())
        } else {
            hashMap.put("mobile", data)
            // hashMap.put("device_key", deviceKey!!.deviceKey.toString())
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_STATUS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiSocialLogin(data: String, apiCallbackHelper: ApiCallbackHelper) {
        val hashMap = HashMap<String, String>()
        hashMap.put("email", data)
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_SOCIAL_LOGIN, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun onSocialLoginFb(
        provider: String,
        access_token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, Any>()
        hashMap.put("provider", provider)
        hashMap.put("access_token", access_token)
        hashMap.put("is_mobile", true)
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_SOCIAL_LOGIN_FB, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUserReg(data: RegistrationInfo, apiCallbackHelper: ApiCallbackHelper) {

        val hashMap = HashMap<String, String>()
        hashMap.put("name", data.name.toString())
        hashMap.put("email", data.email.toString())
        if (!data.isSocial) {
            hashMap.put("password", data.password.toString())
            hashMap.put("password_confirmation", data.pass_confirmation.toString())
        } else {
            data.social_login_id?.let {
                hashMap.put("social_login_id", it)
                hashMap.put("provider", data.provider)
            }
        }
        hashMap.put("referrer_key", data.referal_key.toString())
        hashMap.put("mobile", data.phone.toString())

        getApiCallObservable(
            CALL_TYPE_POST,
            ENDPOINT_USER_REG,
            hashMap
        ).subscribe(apiCallbackHelper)

    }

    fun apiVerifyRegistrationOTP(
        data: RegistrationInfo,
        otp: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["otp"] = otp
        hashMap["name"] = data.name ?: ""
        hashMap["email"] = data.email ?: ""
        if (!data.isSocial) {
            hashMap.put("password", data.password.toString())
            hashMap.put("password_confirmation", data.pass_confirmation.toString())
        } else {
            data.social_login_id?.let {
                hashMap.put("social_login_id", it)
                hashMap.put("provider", data.provider)
            }
        }
        hashMap["referrer_key"] = data.referal_key ?: ""
        hashMap["mobile"] = data.phone ?: ""

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_REG_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiVerifyOldAccOTP(
        email: String,
        phone: String,
        otp: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["otp"] = otp

        if (email.isNotEmpty()) {
            hashMap["email"] = email
        } else if (phone.isNotEmpty()) {
            hashMap["mobile"] = phone
        }
        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_REG_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiVerifyForgetPassOTP(
        email: String,
        mobile: String,
        otp: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["otp"] = otp

        if (email.isNotEmpty()) {
            hashMap["email"] = email
        } else if (mobile.isNotEmpty()) {
            hashMap["mobile"] = mobile
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_REG_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }


    fun apiVerifyOTP(
        email: String,
        mKey: String,
        mobile: String,
        otp: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap.put("email", email)
        hashMap.put("mKey", mKey)
        hashMap.put("mobile", mobile)
        hashMap.put("otp", otp)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_REG_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiVerifyQROTP(
        token: String,
        otp: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("otp", otp)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_QR_OTP_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUserLogin(
        userId: String,
        userPass: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        if (userId.contains("@")) {
            hashMap.put("password", userPass)
            hashMap.put("email", userId)
        } else {
            hashMap.put("password", userPass)
            hashMap.put("mobile", userId)
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_USER_LOGIN, hashMap).subscribe(
            apiCallbackHelper
        )
    }


    fun apiAddToFavContact(
        token: String,
        phone: String,
        name: String,
        connection_type: String,
        operator_id: Int,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("phone", phone)
        hashMap.put("name", name)
        hashMap.put("connection_type", connection_type)
        hashMap.put("operator_id", operator_id.toString())

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_ADD_TO_FAV_CONTACT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUpdateToFavContact(
        token: String,
        phone: String,
        name: String,
        connection_type: String,
        operator_id: Int,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("phone", phone)
        hashMap.put("name", name)
        hashMap.put("connection_type", connection_type)
        hashMap.put("operator_id", operator_id.toString())

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_UPDATE_TO_FAV_CONTACT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUpdateProfile(
        token: String,
        name: String,
        gender: String,
        dob: String,
        address: String,
        mobile: String,
        email: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("name", name)
        if (gender.isNotEmpty()) {
            hashMap.put("gender", gender)
        }
        hashMap.put("dateOfBirth", dob)
        hashMap.put("address", address)
        hashMap.put("mobile", mobile)
        hashMap.put("email", email)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_PROFILE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiAddProfilePicture(
        token: RequestBody,
        picture: MultipartBody.Part,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, RequestBody>()
        hashMap.put("token", token)

        apiService.sendDocumentsMultipart(ENDPOINT_PROFILE_PICTURE_UPLOAD, hashMap, picture)
            .subscribe(apiCallbackHelper)

    }

    fun apiGetFavContact(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_FAV_CONTACT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetHelpSupport(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_HELP_SUPPORT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetTopicHelpSupport(
        topic: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("link_url", topic)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_TOPIC_HELP_SUPPORT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetOperator(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_OPERATORS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRequestOTP(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_QR_OTP_REQUEST, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiDeleteFavContact(
        token: String,
        phone: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("phone", phone)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_DELETE_FAV_CONTACT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetLoginDevice(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_LOGIN_DEVICES, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiLogoutSingleDevice(
        token: String,
        sessionKey: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("session_key", sessionKey)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_LOGOUT_SINGLE_DEVICE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiLogoutAllDevice(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_LOGOUT_ALL_DEVICE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetDescoPostpaidBillings(
        account_no: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("account_no", account_no)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_DESCO_POSTPAID_DUE_BILLS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun getNotifications(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, "user-activity-notification", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun onDeleteNotifications(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, "clear-user-activity-notification", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun getOffers(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, "easy/get-exclusive-offers", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun getProfile(
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_USE_PROFILE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun getTownhallContent(
        token: String,
        contentId: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        val qMap = HashMap<String, String>()
        qMap.put("content_id", contentId)

        getApiCallObservable(
            CALL_TYPE_GET,
            ENDPOINT_GET_TOWNHALL_CONTENT_,
            hashMap,
            qMap
        ).subscribe(
            apiCallbackHelper
        )
    }

    fun apiAddDescoAccount(
        token: String,
        account_no: String,
        monthly_bill_alert: Int,
        account_type: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("account_no", account_no)
        hashMap.put("monthly_bill_alert", monthly_bill_alert.toString())
        hashMap.put("account_type", account_type)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_DESCO_ADD_ACOUNT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetDescoAccounts(
        token: String,
        limit: Int?,
        account_type: String?,
        monthly_bill_alert: Int?,
        user_id: Int?,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        limit?.let {
            hashMap.put("limit", limit.toString())
        }
        account_type?.let {
            hashMap.put("account_type", account_type)
        }
        monthly_bill_alert?.let {
            hashMap.put("monthly_bill_alert", monthly_bill_alert.toString())
        }
        user_id?.let {
            hashMap.put("user_id", user_id.toString())
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_DESCO_GET_ACOUNTS, hashMap).subscribe(
            apiCallbackHelper
        )
    }


    fun apiAddWasaAccount(
        token: String,
        account_no: String,
        account_title: String,
        monthly_bill_alert: Int,
        account_type: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("account_no", account_no)
        hashMap.put("account_title", account_title)
        hashMap.put("monthly_bill_alert", monthly_bill_alert.toString())
        hashMap.put("account_type", account_type)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_WASA_ADD_ACOUNT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetWasaAccounts(
        token: String,
        limit: Int?,
        account_type: String?,
        monthly_bill_alert: Int?,
        user_id: Int?,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)

        limit?.let {
            hashMap.put("limit", limit.toString())
        }
        account_type?.let {
            hashMap.put("account_type", account_type)
        }
        monthly_bill_alert?.let {
            hashMap.put("monthly_bill_alert", monthly_bill_alert.toString())
        }
        user_id?.let {
            hashMap.put("user_id", user_id.toString())
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_WASA_GET_ACOUNTS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiDeleteDescoAccount(
        token: String,
        account_no: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("account_no", account_no)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_DESCO_DELETE_ACOUNT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiDeleteWasaAccount(
        token: String,
        account_no: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap.put("token", token)
        hashMap.put("account_no", account_no)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_WASA_DELETE_ACOUNT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiDescoBillPayment(
        descoBillPaymentRequestModel: DescoBillPaymentRequestModel,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val raw = Gson().toJson(descoBillPaymentRequestModel).toString()

        apiService.postRequestForRaw(
            ENDPOINT_DESCO_BILL_PAYMENT,
            RequestBody.create(MediaType.parse("text/plain"), raw)
        ).subscribe(
            apiCallbackHelper
        )
    }

    fun apiForgetPassword(
        mobile: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        if (mobile.contains("@")) {
            hashMap.put("email", mobile)
        } else {
            hashMap.put("mobile", mobile)
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_FORGOT_PASS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiCheckImageUploadStatus(
        mobile: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        if (mobile.contains("@")) {
            hashMap.put("email", mobile)
        } else {
            hashMap.put("mobile_number", mobile)
        }
        hashMap.put("token", token)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_UOLOAD_IMAGE_STATUS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun resendOTP(
        mobile: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        if (mobile.contains("@")) {
            hashMap.put("email", mobile)
        } else {
            hashMap.put("mobile", mobile)
        }

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RERSEND_OTP, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiGetPlans(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_GET_PLANS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiTopUpInit(
        topUpRequestModel: TopUpRequestModel,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val raw = Gson().toJson(topUpRequestModel).toString()

        apiService.postRequestForRaw(
            ENDPOINT_TOP_UP,
            RequestBody.create(MediaType.parse("text/plain"), raw)
        ).subscribe(
            apiCallbackHelper
        )
    }

    fun apiQRInit(
        qrResponse: QRResponse,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val raw = Gson().toJson(qrResponse).toString()

        apiService.postRequestForRaw(
            ENDPOINT_QR_PAYLOAD,
            RequestBody.create(MediaType.parse("text/plain"), raw)
        ).subscribe(
            apiCallbackHelper
        )
    }

    fun apiValidateTransaction(
        transactionId: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["transaction_id"] = transactionId
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_TRANSACTION_VERIFY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiDeleteCard(
        user_refer: String,
        card_index: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["user_refer"] = user_refer
        hashMap["card_index"] = card_index
        hashMap["token"] = token
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_CARD_DELETE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiAutoDebit(
        channel_type: String,
        channel: String,
        card_index: String,
        cvv: String,
        total_amount: String,
        tran_id: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["channel_type"] = channel_type
        hashMap["channel"] = channel
        if (card_index.isNotEmpty()) {
            hashMap["card_index"] = card_index
        }
        if (cvv.isNotEmpty()) {
            hashMap["cvv"] = cvv
        }
        hashMap["total_amount"] = total_amount
        hashMap["tran_id"] = tran_id
        hashMap["token"] = token
        hashMap["service_title"] = "bangla-qr"
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_AUTO_DEBIT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRating(
        rating: String,
        transactionId: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["transaction_id"] = transactionId
        hashMap["rating"] = rating
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RATING, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiInitiateHoldUser(
        allow_offer: String,
        transactionId: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["transaction_id"] = transactionId
        hashMap["allow_offer"] = allow_offer
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_INITIATE_HOLD_USER, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiPaymentStatus(
        transactionId: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["tran_id"] = transactionId
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_PAYMENT_STATUS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequest(
        name: String,
        requester_identifier: String,
        amount: String,
        msisdn: String,
        operator: String,
        operator_id: String,
        connection_type: String,
        requested_identifier: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val hashMap = HashMap<String, String>()
        hashMap["name"] = name
        hashMap["requester_identifier"] = requester_identifier
        hashMap["amount"] = amount
        hashMap["msisdn"] = msisdn
        hashMap["operator"] = operator.toLowerCase()
        hashMap["operator_id"] = operator_id
        hashMap["connection_type"] = connection_type
        hashMap["requested_identifier"] = requested_identifier

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUEST_CREATE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequestList(
        page: Int,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["page"] = page.toString()
        hashMap["token"] = token
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUEST_LIST, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequestedList(
        page: Int,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["page"] = page.toString()
        hashMap["token"] = token
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUESTED_LIST, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequestHistory(
        page: Int,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["page"] = page.toString()
        hashMap["token"] = token
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUEST_HISTORY, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequestReject(
        requestId: Int,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["request_id"] = requestId.toString()
        hashMap["token"] = token

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUEST_REJECT, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiRechargeRequestInfo(
        ruid: String,
        token: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()
        hashMap["ruid"] = ruid
        hashMap["token"] = token
        hashMap["is_mobile"] = "1"

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_RECHARGE_REQUEST_INFO, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun fetchSearchData(
        topUpRequestModel: TopUpRequestModel,
        apiCallbackHelper: ApiCallbackHelper
    ) {

        val raw = Gson().toJson(topUpRequestModel).toString()

        apiService.postRequestForRaw(
            ENDPOINT_TOP_UP,
            RequestBody.create(MediaType.parse("text/plain"), raw)
        ).subscribe(
            apiCallbackHelper
        )
    }


    fun getTerms(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, "easy/fm/get-terms", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun getPrivacy(
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        getApiCallObservable(CALL_TYPE_POST, "easy/fm/get-privacy", hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUpdatePhone(
        email: String,
        mobile: String,
        mkey: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("email", email)
        hashMap.put("mKey", mkey)
        hashMap.put("mobile", mobile)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_UPDATE_PHONE, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiChangePassword(
        fpKey: String,
        mobile: String,
        password: String,
        password_confirmation: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("fpKey", fpKey)
        if (mobile.contains("@")) {
            hashMap.put("email", mobile)
        } else {
            hashMap.put("mobile", mobile)
        }
        hashMap.put("password", password)
        hashMap.put("password_confirmation", password_confirmation)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_CHANGE_PASS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUpdatePassword(
        token: String,
        oldPassword: String,
        password: String,
        password_confirmation: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("old_password", oldPassword)
        hashMap.put("password", password)
        hashMap.put("password_confirmation", password_confirmation)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_UPDATE_PASS, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiSetPin(
        token: String,
        qrKey: String,
        newPin: String,
        retypePin: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("bangla_qr_pin_key", qrKey)
        hashMap.put("qr_pin", newPin)
        hashMap.put("confirm_qr_pin", retypePin)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_QR_SET_PIN, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiChangePin(
        token: String,
        old_qr_pin: String,
        qr_pin: String,
        confirm_qr_pin: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, String>()

        hashMap.put("token", token)
        hashMap.put("old_qr_pin", old_qr_pin)
        hashMap.put("qr_pin", qr_pin)
        hashMap.put("confirm_qr_pin", confirm_qr_pin)

        getApiCallObservable(CALL_TYPE_POST, ENDPOINT_QR_CHANGE_PIN, hashMap).subscribe(
            apiCallbackHelper
        )
    }

    fun apiUploadPicture(
        token: RequestBody,
        mobile_no: RequestBody,
        image_serial: RequestBody,
        user_image: MultipartBody.Part,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val hashMap = HashMap<String, RequestBody>()
        hashMap["token"] = token
        hashMap["mobile_no"] = mobile_no
        hashMap["image_serial"] = image_serial

        apiService.sendDocumentsMultipart(ENDPOINT_IMAGE_UPLOAD, hashMap, user_image)
            .subscribe(apiCallbackHelper)

    }

    fun apiGetUserInfo(
        token: String,
        mobile: String,
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val body = HashMap<String, String>()
        body["token"] = token

        val query = HashMap<String, String>()
        query["mobile"] = mobile

        getApiCallObservable(CALL_TYPE_GET, ENDPOINT_TOWNHALL_USER_INFO, body, query).subscribe(
            apiCallbackHelper
        )
    }

    fun <T> getApiCallObservable(
        callType: String,
        path: String,
        hashMap: HashMap<String, T>,
        qMap: HashMap<String, String>? = null
    ): Maybe<BaseModel<Any>> {
        return when {
            callType.equals(CALL_TYPE_GET) -> {
                ApiGetCall().getMaybeObserVable(apiService, path, hashMap!!, qMap)
            }
            callType.equals(CALL_TYPE_POST) -> {
                ApiPostCall().getMaybeObserVable(apiService, path, hashMap!!)

            }
            else -> {
                ApiPostCallWithDocument().getMaybeObserVable(apiService, path, hashMap!!)

            }
        }
    }

    fun submitGameQR(
        token: String,
        game_code: String,
        check_eligible: String = "no",
        apiCallbackHelper: ApiCallbackHelper
    ) {
        val map = HashMap<String, String>()
        map["token"] = token
        map["game_code"] = game_code
        map["check_eligible"] = check_eligible

        getApiCallObservable(
            CALL_TYPE_POST,
            ENDPOINT_TOWNHALL_SUBMIT_GAME_QR,
            map
        ).subscribe(
            apiCallbackHelper
        )
    }
}