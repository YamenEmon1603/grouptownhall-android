package sslwireless.android.townhall.data.network

import sslwireless.android.townhall.data.model.BaseModel
import io.reactivex.Maybe
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import sslwireless.android.townhall.data.model.transaction.Transaction

interface IApiService {

    @GET("{url}")
    abstract fun getRequest(
        @Path(value = "url", encoded = true) path: String,
        @HeaderMap hashMap: Map<String, String>,
        @QueryMap qMap: Map<String, String>?
    ): Maybe<BaseModel<Any>>

    @FormUrlEncoded
    @POST("{url}")
    abstract fun postRequest(
        @Path(value = "url", encoded = true) path: String,
        @FieldMap hashMap: Map<String, String>
    ): Maybe<BaseModel<Any>>

    @FormUrlEncoded
    @POST("report/dynamic-recent-transaction")
    fun transactionHistoryService(
        @FieldMap hashMap: Map<String, String>
    ): Single<BaseModel<Transaction>>

    @Multipart
    @POST("{url}")
    abstract fun sendDocuments(
        @Path(value = "url", encoded = true) path: String,
        @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>
    ): Maybe<BaseModel<Any>>

    @Multipart
    @POST("{url}")
    abstract fun sendDocumentsMultipart(
        @Path(value = "url", encoded = true) path: String,
        @PartMap partMap: Map<String, @JvmSuppressWildcards RequestBody>,
        @Part profile: MultipartBody.Part
    ): Maybe<BaseModel<Any>>


    @Headers("Content-Type: application/json")
    @POST("{url}")
    abstract fun postRequestForRaw(
        @Path(value = "url", encoded = true) path: String,
        @Body requestBody: RequestBody
    ): Maybe<BaseModel<Any>>

}