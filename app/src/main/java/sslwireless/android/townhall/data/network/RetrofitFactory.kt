package sslwireless.android.townhall.data.network

import com.facebook.stetho.okhttp3.StethoInterceptor
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.data.prefence.PreferencesHelper
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitFactory private constructor(){


    companion object {
        lateinit var iApiHelper:IApiService
        internal fun providePostApi(pref: PreferencesHelper): IApiService {
            val retrofit: Retrofit =
                provideRetrofitInterface(pref)
            iApiHelper =  retrofit.create(IApiService::class.java)
            return iApiHelper;
        }

        internal fun provideRetrofitInterface(pref:PreferencesHelper): Retrofit {

            var client: OkHttpClient.Builder = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .addInterceptor { chain ->
                    val request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("device-key", pref.prefGetDeviceKey()?.deviceKey?:"")
                        .addHeader("lang", pref.prefGetLanguage())
                        .addHeader("is-town-hall", "1")


                    chain.proceed(request.build())
                }
                .addInterceptor(DefaultInterceptors().httpBodyLoggingInterceptor)
                .addNetworkInterceptor(StethoInterceptor())

            return Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(client.build())
                .build()
        }

        class DefaultInterceptors {

            val httpBodyLoggingInterceptor: HttpLoggingInterceptor
                get() {
                    val interceptor = HttpLoggingInterceptor()
                    interceptor.level = HttpLoggingInterceptor.Level.BODY
                    return interceptor
                }

        }


    }

}