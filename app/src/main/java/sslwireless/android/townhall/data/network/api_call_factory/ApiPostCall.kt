package sslwireless.android.townhall.data.network.api_call_factory

import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.network.IApiService
import io.reactivex.Maybe

class ApiPostCall() : IApiCall {


    override fun <T> getMaybeObserVable(
        apiService: IApiService,
        path: String,
        hashMap: HashMap<String, T>,
        qMap: HashMap<String, String>?
    ): Maybe<BaseModel<Any>> {
        return apiService.postRequest(path,hashMap as HashMap<String,String>)
    }
}