package sslwireless.android.townhall.data.network.api_call_factory

import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.network.IApiService
import io.reactivex.Maybe
import okhttp3.RequestBody

class ApiPostCallWithDocument() : IApiCall {


    override fun <T> getMaybeObserVable(
        apiService: IApiService,
        path: String,
        hashMap: HashMap<String, T>,
        qMap: HashMap<String, String>?
    ): Maybe<BaseModel<Any>> {
        return apiService.sendDocuments(path,hashMap as HashMap<String, RequestBody>)
    }
}