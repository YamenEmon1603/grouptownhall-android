package sslwireless.android.townhall.data.prefence

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.google.gson.Gson
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.data.model.operator.OperatorModel
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.data.model.user.DeviceKey
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.view.activity.login.LoginStep1Activity
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.fromJson

class PreferencesHelper(context: Context) {
    private val PREF_KEY_IS_LOGIN = "PREF_KEY_IS_LOGIN"
    private val PREF_KEY_IS_FAVORITE = "PREF_KEY_IS_FAVORITE"
    private val PREF_KEY_OPERATOR = "PREF_KEY_OPERATOR"
    private val PREF_KEY_TOKEN = "PREF_KEY_TOKEN"
    private val PREF_KEY_USER_INFO = "PREF_KEY_USER_INFO"
    private val PREF_KEY_DEVICE_KEY = "PREF_KEY_DEVICE_KEY"
    private val PREF_KEY_TOWNHALL_KEY = "PREF_KEY_TOWNHALL_KEY"
    private val PREF_KEY_LANGUAGE = "PREF_KEY_LANGUAGE"
    private val PREF_KEY_CUSTOMER_TOKEN = "PREF_KEY_CUSTOMER_TOKEN"
    private val PREF_KEY_FCM_TOKEN = "PREF_KEY_FCM_TOKEN"
    private val PREF_KEY_USER_OPERATOR_NAME = "PREF_KEY_USER_OPERATOR_NAME"
    private val PREF_KEY_USER_OPERATOR_TYPE = "PREF_KEY_USER_OPERATOR_TYPE"
    private val PREF_KEY_IS_SOCIAL = "PREF_KEY_IS_SOCIAL"
    private val PREF_KEY_IS_BOT_OPENED = "PREF_KEY_IS_BOT_OPENED"

    private val mPrefs: SharedPreferences =
        context.getSharedPreferences("preference_name", Context.MODE_PRIVATE)

    fun prefGetCurrentUser(): UserData {
        return Gson().fromJson(mPrefs.getString(PREF_KEY_USER_INFO, null), UserData::class.java)
    }

    fun prefLogin(user: UserData) {
        mPrefs.edit().putString(PREF_KEY_USER_INFO, Gson().toJson(user)).apply()
    }

    fun prefLoginIsValid() {
        mPrefs.edit().putBoolean(PREF_KEY_IS_LOGIN, true).apply()
    }

    fun prefUserData(user: UserData) {
        mPrefs.edit().putString(PREF_KEY_USER_INFO, Gson().toJson(user)).apply()
    }

    fun prefFavorite(favoriteList: ArrayList<FavoriteModel>) {
        mPrefs.edit().putString(PREF_KEY_IS_FAVORITE, Gson().toJson(favoriteList)).apply()
    }

    fun prefGetFavoriteList(): ArrayList<FavoriteModel> {
        return if (mPrefs.getString(PREF_KEY_IS_FAVORITE, "")!!.isEmpty()) {
            ArrayList()
        } else {
            Gson().fromJson(mPrefs.getString(PREF_KEY_IS_FAVORITE, "")!!)
        }
    }

    fun prefOperator(operatorList: ArrayList<OperatorModel>) {
        mPrefs.edit().putString(PREF_KEY_OPERATOR, Gson().toJson(operatorList)).apply()
    }

    fun prefGetOperatorList(): ArrayList<OperatorModel> {
        return if (mPrefs.getString(PREF_KEY_OPERATOR, "")!!.isEmpty()) {
            ArrayList()
        } else {
            Gson().fromJson(mPrefs.getString(PREF_KEY_OPERATOR, "")!!)
        }
    }

    fun prefSetDeviceKey(deviceKey: DeviceKey) {
        mPrefs.edit().putString(PREF_KEY_DEVICE_KEY, Gson().toJson(deviceKey)).apply()
    }

    fun prefGetDeviceKey(): DeviceKey? {
        return Gson().fromJson(mPrefs.getString(PREF_KEY_DEVICE_KEY, ""), DeviceKey::class.java)
    }

    fun prefSetTownHallResponseKey(townHallResponse: TownHallResponse) {
        mPrefs.edit().putString(PREF_KEY_TOWNHALL_KEY, Gson().toJson(townHallResponse)).apply()
    }

    fun prefGetTownhallKey(): TownHallResponse? {
        return Gson().fromJson(
            mPrefs.getString(PREF_KEY_TOWNHALL_KEY, ""),
            TownHallResponse::class.java
        )
    }


    fun prefSetCustomerToken(token: String) {
        mPrefs.edit().putString(PREF_KEY_CUSTOMER_TOKEN, token).apply()
    }

    fun prefGetCustomerToken(): String? {
        return mPrefs.getString(PREF_KEY_CUSTOMER_TOKEN, "")
    }

    fun prefSetFCMToken(token: String) {
        mPrefs.edit().putString(PREF_KEY_FCM_TOKEN, token).apply()
    }

    fun prefGetFCMToken(): String? {
        return mPrefs.getString(PREF_KEY_FCM_TOKEN, "")
    }

    fun prefLogout(activity: Context) {
        mPrefs.edit().putString(PREF_KEY_USER_INFO, null).apply()
        mPrefs.edit().putBoolean(PREF_KEY_IS_LOGIN, false).apply()
        mPrefs.edit().putString(PREF_KEY_CUSTOMER_TOKEN, "").apply()
        mPrefs.edit().putString(PREF_KEY_USER_OPERATOR_NAME, "").apply()
        mPrefs.edit().putString(PREF_KEY_USER_OPERATOR_TYPE, "").apply()
        mPrefs.edit().putBoolean(PREF_KEY_IS_BOT_OPENED, false).apply()

        activity.startActivity(
            Intent(
                activity,
                LoginStep1Activity::class.java
            ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        )
    }

    fun prefGetLoginMode(): Boolean {
        return mPrefs.getBoolean(PREF_KEY_IS_LOGIN, false)
    }

    fun prefGetToken(): String {
        return mPrefs.getString(PREF_KEY_TOKEN, "")!!
    }

    private fun prefSetToken(token: String) {
        mPrefs.edit().putString(PREF_KEY_TOKEN, token).apply()
    }

    fun prefSetOperatorName(name: String) {
        mPrefs.edit().putString(PREF_KEY_USER_OPERATOR_NAME, name).apply()
    }

    fun prefSetOperatorType(type: String) {
        mPrefs.edit().putString(PREF_KEY_USER_OPERATOR_TYPE, type).apply()
    }

    fun prefGetOperatorName(): String {
        return mPrefs.getString(PREF_KEY_USER_OPERATOR_NAME, "").toString()
    }

    fun prefGetOperatorType(): String {
        return mPrefs.getString(PREF_KEY_USER_OPERATOR_TYPE, "").toString()
    }

    fun prefSetLanguage(language: String) {
        AppConstants.Language.CURRENT_LANGUAGE = language
        mPrefs.edit().putString(PREF_KEY_LANGUAGE, language).apply()
    }

    fun prefGetLanguage(): String {
        val lang = mPrefs.getString(PREF_KEY_LANGUAGE, AppConstants.Language.ENGLISH).toString()
        AppConstants.Language.CURRENT_LANGUAGE = lang
        return lang
    }

    fun prefSetSocial(check: Boolean) {
        mPrefs.edit().putBoolean(PREF_KEY_IS_SOCIAL, check).apply()
    }

    fun isSocial(): Boolean {
        return mPrefs.getBoolean(PREF_KEY_IS_SOCIAL, false)
    }

    fun prefSetOTPHash(check: String) {
        mPrefs.edit().putString("OTPHashKey", check).apply()
    }

    fun prefGetOTPHashKey(): String {
        return mPrefs.getString("OTPHashKey", "").toString()
    }

    fun prefSetIsBotOpened(isOpened: Boolean) {
        mPrefs.edit().putBoolean(PREF_KEY_IS_BOT_OPENED, isOpened).apply()
    }

    fun prefIsBotOpened() = mPrefs.getBoolean(PREF_KEY_IS_BOT_OPENED, false)
}