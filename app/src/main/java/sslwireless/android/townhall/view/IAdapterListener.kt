package sslwireless.android.townhall.view

import android.view.View
import android.view.ViewGroup
import sslwireless.android.townhall.view.base.BaseViewHolder2

interface IAdapterListener {
    fun <T> clickListener(position: Int, model: T, view: View)
    fun  getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2
}