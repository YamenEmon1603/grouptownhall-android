package sslwireless.android.townhall.view.activity.BillingAccount.AddBillingAccount

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProviders
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.descoModels.BillingAccount
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA_OR_DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountList.BillingAccountListActivity
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_add_new_billing_account.*
import kotlinx.android.synthetic.main.activity_notifications.title_tv
import java.lang.reflect.Type

class AddNewBillingAccountActivity : BaseActivity() {

    private lateinit var billingAccount: BillingAccount

    private lateinit var viewModel: BillingAccountViewModel

    companion object {
        const val POSTPAID = "postpaid"
        const val PREPAID = "prepaid"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_billing_account)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(BillingAccountViewModel(getDataManager()))
            ).get(BillingAccountViewModel::class.java)


    }

    override fun viewRelatedTask() {
        var connectionType = POSTPAID
        radio_group.setOnCheckedChangeListener { _, id ->
            connectionType = when (id) {
                R.id.perPaid_rb -> {
                    PREPAID
                }
                R.id.postPaid_rb -> {
                    POSTPAID
                }
                else -> {
                    POSTPAID
                }
            }
        }

        account_number_et.doOnTextChanged { text, start, before, count ->
            if (text.isNullOrEmpty()) {
                add_account_btn.setBackgroundResource(R.drawable.bg_bottom_button)
            } else {
                add_account_btn.setBackgroundResource(R.drawable.bg_bottom_button_2)
            }
        }

        when (intent.getStringExtra(WASA_OR_DESCO)) {
            DESCO -> {
                inputLayoutTitle.visibility = View.GONE
                title_tv.text = getString(R.string.desco_account)

                add_account_btn.setOnClickListener {
                    if (account_number_et.text.toString().isNotEmpty()) {

                        viewModel.apiAddDescoAccount(
                            getDataManager().mPref.prefGetCustomerToken()!!,
                            account_number_et.text.toString(),
                            0,
                            connectionType,
                            this,
                            this
                        )

                    } else {
                        account_number_et.error = getString(R.string.error_empty_billing_account_no)
                    }
                }

            }
            WASA -> {
                account_type_title_tv.visibility = GONE
                radio_group.visibility = GONE
                title_tv.text = getString(R.string.wasa_account)
                inputLayoutTitle.visibility = View.VISIBLE


                add_account_btn.setOnClickListener {

                    if (account_title_et.text.toString().isEmpty()) {
                        account_title_et.error =
                            getString(R.string.error_empty_billing_account_title)
                        return@setOnClickListener
                    }

                    if (account_number_et.text.toString().isEmpty()) {
                        account_number_et.error = getString(R.string.error_empty_billing_account_no)
                        return@setOnClickListener
                    }

                    viewModel.apiAddWasaAccount(
                        getDataManager().mPref.prefGetCustomerToken()!!,
                        account_number_et.text.toString(),
                        account_title_et.text.toString(),
                        0,
                        POSTPAID,
                        this,
                        this
                    )
                }
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

        if (result.data!!.code == 200) {

            val rawResponse = result.data.data!!.toString()
            Log.e("RawResponse:", "---->  " + rawResponse)

            when (key) {
                "apiAddDescoAccount" -> {

                    val listType: Type = object : TypeToken<BillingAccount>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<BillingAccount> = moshi.adapter(listType)

                    billingAccount = adapter.fromJsonValue(result.data.data) as BillingAccount

                    startActivity(
                        Intent(
                            this,
                            BillingAccountListActivity::class.java
                        ).putExtra(
                            WASA_OR_DESCO,
                            DESCO
                        )
                    )
                    finish()


                }

                "apiAddWasaAccount" -> {
                    val listType: Type = object : TypeToken<BillingAccount>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<BillingAccount> = moshi.adapter(listType)

                    billingAccount = adapter.fromJsonValue(result.data.data) as BillingAccount

                    startActivity(
                        Intent(
                            this,
                            BillingAccountListActivity::class.java
                        ).putExtra(
                            WASA_OR_DESCO,
                            WASA
                        )
                    )
                    finish()

                }

            }

        } else {
            showToast(this, result.data.message)
        }

    }

}
