package sslwireless.android.townhall.view.activity.BillingAccount

import android.content.Intent
import android.os.Bundle
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountList.BillingAccountListActivity
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_billing_account.*

class BillingAccountActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    companion object{
        val WASA_OR_DESCO = "WASA_OR_DESCO"
        val WASA = "WASA"
        val DESCO = "DESCO"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_billing_account)

    }

    override fun viewRelatedTask() {
        desco_billing_account_layout.setOnClickListener {
            startActivity(Intent(this,
                BillingAccountListActivity::class.java).putExtra(
                WASA_OR_DESCO,
                DESCO
            ))
        }

        wasa_billing_account_layout.setOnClickListener {
            startActivity(Intent(this,
                BillingAccountListActivity::class.java).putExtra(
                WASA_OR_DESCO,
                WASA
            ))
        }
    }

}
