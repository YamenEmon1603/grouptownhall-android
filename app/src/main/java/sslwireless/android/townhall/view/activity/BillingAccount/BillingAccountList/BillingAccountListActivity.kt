package sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountList

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.descoModels.BillingAccount
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.AddBillingAccount.AddNewBillingAccountActivity
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA_OR_DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountViewModel
import sslwireless.android.townhall.view.activity.desco.descoBillPayment.DescoBillPamentActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.adapter.BillingAccountAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_billing_account_list.*
import kotlinx.android.synthetic.main.activity_notifications.title_tv
import java.lang.reflect.Type

class BillingAccountListActivity : BaseActivity(),BillingAccountAdapter.BillingAccountActionListerner {

    private lateinit var viewModel: BillingAccountViewModel
    var billingAccounts: ArrayList<BillingAccount> = ArrayList()
    var billingAccountAdapter : BillingAccountAdapter ?= null
    private lateinit var type : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_billing_account_list)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(BillingAccountViewModel(getDataManager()))
            ).get(BillingAccountViewModel::class.java)

    }


    override fun viewRelatedTask() {
        billingAccounts.clear()

        when (intent.getStringExtra(WASA_OR_DESCO)) {
            DESCO -> {
                type = DESCO
                title_tv.text = "DESCO BILLING ACCOUTS"

                viewModel.apiGetDescoAccounts(getDataManager().mPref.prefGetCustomerToken()!!,
                    null,
                    null,
                    null,
                    null,
                    this,
                    this
                    )

                add_new_account_btn.setOnClickListener {
                    startActivity(Intent(this,
                        AddNewBillingAccountActivity::class.java).putExtra(
                        WASA_OR_DESCO, DESCO))
                }

            }


            WASA -> {
                type = WASA
                title_tv.text = "WASA BILLING ACCOUTS"

                viewModel.apiGetWasaAccounts(getDataManager().mPref.prefGetCustomerToken()!!,
                    null,
                    null,
                    null,
                    null,
                    this,
                    this
                )

                add_new_account_btn.setOnClickListener {
                    startActivity(Intent(this,
                        AddNewBillingAccountActivity::class.java).putExtra(
                        WASA_OR_DESCO, WASA))
                }
            }
        }


    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        if (result.data!!.code == 200) {

            val rawResponse = result.data!!.data!!.toString()

            Log.e("RawResponse:", "---->  " + rawResponse)

            when(key){
                "apiGetDescoAccounts"->{

                    val listType: Type = object : TypeToken<List<BillingAccount>>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<List<BillingAccount>> = moshi.adapter(listType)


                    billingAccounts = adapter.fromJsonValue(result.data.data) as ArrayList<BillingAccount>

                    if(billingAccounts.size>0){
                        billingAccounts.forEach {
                            it.type = DESCO
                        }

                        billing_account_rev_view.layoutManager = LinearLayoutManager(this)
                        billing_account_rev_view.setHasFixedSize(true)
                        billingAccountAdapter = BillingAccountAdapter(this,billingAccounts,this)
                        billing_account_rev_view.adapter = billingAccountAdapter
                        empty_view.visibility = View.GONE
                    }else{
                        empty_view.visibility = View.VISIBLE
                    }

                }

                "apiGetWasaAccounts"->{

                    val listType: Type = object : TypeToken<List<BillingAccount>>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<List<BillingAccount>> = moshi.adapter(listType)

                    billingAccounts = adapter.fromJsonValue(result.data.data) as ArrayList<BillingAccount>

                    if(billingAccounts.size>0){
                        billingAccounts.forEach {
                            it.type = WASA
                        }

                        billing_account_rev_view.layoutManager = LinearLayoutManager(this)
                        billing_account_rev_view.setHasFixedSize(true)
                        billingAccountAdapter = BillingAccountAdapter(this,billingAccounts,this)
                        billing_account_rev_view.adapter = billingAccountAdapter
                        empty_view.visibility = View.GONE
                    }else{
                        empty_view.visibility = View.VISIBLE
                    }

                }

                "apiDeleteDescoAccount"->{
                    showToast(this, result.data.message)
                    viewModel.apiGetDescoAccounts(getDataManager().mPref.prefGetCustomerToken()!!,
                        null,
                        null,
                        null,
                        null,
                        this,
                        this
                    )

                }


                "apiDeleteWasaAccount"->{
                    showToast(this, result.data.message)
                    viewModel.apiGetWasaAccounts(getDataManager().mPref.prefGetCustomerToken()!!,
                        null,
                        null,
                        null,
                        null,
                        this,
                        this
                    )
                }
            }

        } else {
            showToast(this, result.data.message)
        }
    }




    override fun onDeleteAccount(billingAccount: BillingAccount) {
        when(billingAccount.type){
            DESCO->{
                viewModel.apiDeleteDescoAccount(getDataManager().mPref.prefGetCustomerToken()!!,
                    billingAccount.account_no,
                    this,
                    this)
            }
            WASA->{
                viewModel.apiDeleteWasaAccount(getDataManager().mPref.prefGetCustomerToken()!!,
                    billingAccount.account_no,
                    this,
                    this)
            }
        }
    }

    override fun onEditAccount(billingAccount: BillingAccount) {
    }

    override fun onItemClicked(billingAccount: BillingAccount) {
        var intent =
            Intent(this, DescoBillPamentActivity::class.java)
        intent.putExtra(Enums.parameter.is_prepaid.toString(), false)
        intent.putExtra("account_no", billingAccount.account_no)
        startActivity(intent)
    }
}
