package sslwireless.android.townhall.view.activity.BillingAccount

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class BillingAccountViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun apiAddDescoAccount(
        token: String,
        account_no: String,
        monthly_bill_alert: Int,
        account_type: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiAddDescoAccount(
            token,
            account_no,
            monthly_bill_alert,
            account_type,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiAddDescoAccount"))
        )
    }

    fun apiAddWasaAccount(
        token: String,
        account_no: String,
        account_title: String,
        monthly_bill_alert: Int,
        account_type: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiAddWasaAccount(
            token,
            account_no,
            account_title,
            monthly_bill_alert,
            account_type,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiAddWasaAccount"))
        )
    }

    fun apiGetDescoAccounts(
        token: String,
        limit: Int?,
        account_type: String?,
        monthly_bill_alert: Int?,
        user_id: Int?,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetDescoAccounts(
            token,
            limit,
            account_type,
            monthly_bill_alert,
            user_id,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiGetDescoAccounts"))
        )
    }

    fun apiGetWasaAccounts(
        token: String,
        limit: Int?,
        account_type: String?,
        monthly_bill_alert: Int?,
        user_id: Int?,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetWasaAccounts(
            token,
            limit,
            account_type,
            monthly_bill_alert,
            user_id,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiGetWasaAccounts"))
        )
    }

    fun apiDeleteDescoAccount(
        token: String,
        account_no: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiDeleteDescoAccount(
            token,
            account_no,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiDeleteDescoAccount"))
        )
    }

    fun apiDeleteWasaAccount(
        token: String,
        account_no: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiDeleteWasaAccount(
            token,
            account_no,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiDeleteWasaAccount"))
        )
    }




}