package sslwireless.android.townhall.view.activity.ChangePassword

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_change_password.*
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.activity.login.LoginStep1Activity
import sslwireless.android.townhall.view.activity.qrPay.QrActivity
import sslwireless.android.townhall.view.activity.webview.WebViewActivity
import sslwireless.android.townhall.view.utils.*

class ChangePasswordActivity : BaseActivity() {
    private lateinit var fpkey: String
    private lateinit var phone: String
    private var qrKey=""
    var currentPassRequired: Boolean = true
    private var isSetPin: Boolean = false
    private var isChangePin: Boolean = false

    private lateinit var viewModel: ChangePasswordViewModel


    companion object {
        val FROM_FORGET_PASS = "FROM_FORGET_PASS"
        val FROM_CHANGE_PASS = "FROM_CHANGE_PASS"
        val FROM_SET_PIN = "FROM_SET_PIN"
        val FROM_CHANGE_PIN = "FROM_CHANGE_PIN"
        val FROM_ACTION = "FROM_ACTION"
        val PIN_KEY = "PIN_KEY"
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            when(key) {
                "forgot-password-restore" -> {
                    showToast(this, getString(R.string.password_update_change))
                    /*val userData = result.data.data.fromResponse<UserData>()
                    val token = result.data.token
                    getDataManager().mPref.prefSetCustomerToken(token!!)
                    getDataManager().mPref.prefLogin(userData!!)*/
                    startActivity(Intent(this, LoginStep1Activity::class.java))
                    finishAffinity()
                }
                "password-update"->{
                    showToastMsg(result.data.message)
                    onBackPressed()
                }
                "set_pin"->{
                    val user = getDataManager().mPref.prefGetCurrentUser()
                    user.qr_pin = true
                    getDataManager().mPref.prefUserData(user)
                    showEditDialog(true,resources.getString(R.string.pin_set_successful))
                }
                "change_pin" ->{
                    showToastMsg(result.data.message)
                    onBackPressed()
                }
            }
        } else {
            showToast(this, result.data.message)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
    }

    override fun viewRelatedTask() {

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(ChangePasswordViewModel(getDataManager()))
            )
                .get(ChangePasswordViewModel::class.java)

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }

        old_password_et.doOnTextChanged { text, start, before, count ->
            old_password_et.error=null
            old_password_layout.isEndIconVisible= true
        }
        new_password_et.doOnTextChanged { text, start, before, count ->
            new_password_et.error=null
            if (!isSetPin) {
                new_password_layout.isEndIconVisible = true
            }
        }
        confirm_password_et.doOnTextChanged { text, start, before, count ->
            confirm_password_et.error=null
            if (!isSetPin) {
                confirm_password_layout.isEndIconVisible = true
            }
        }


        if (intent.getStringExtra(FROM_ACTION) == FROM_FORGET_PASS) {
            fpkey = intent.getStringExtra("fpKey")?:""
            phone = intent.getStringExtra("phone")?:""
            currentPassRequired = false
            forgetPasswordGroup.makeVisible()
            chancePasswordGroup.makeGone()
            title_change_password.makeGone()

        } else if (intent.getStringExtra(FROM_ACTION) == FROM_CHANGE_PASS) {
            currentPassRequired = true
            forgetPasswordGroup.makeGone()
            chancePasswordGroup.makeVisible()
            title_change_password.makeVisible()
        } else if (intent.getStringExtra(FROM_ACTION) == FROM_SET_PIN) {
            isSetPin = true
            forgetPasswordGroup.makeVisible()
            chancePasswordGroup.makeGone()
            title_change_password.makeGone()
            sixDigitHints.makeVisible()
            tvTerms.visibility = View.VISIBLE
            tvPrivacy.visibility = View.VISIBLE
            divider.visibility = View.VISIBLE

            title_tv.text = resources.getText(R.string.set_pin)
            subTitle_tv.text = resources.getText(R.string.set_new_pin)

            new_password_layout.hint = resources.getText(R.string.enter_new_pin)
            confirm_password_layout.hint = resources.getText(R.string.retype_new_pin)

            new_password_et.filters = arrayOf(InputFilter.LengthFilter(6))
            confirm_password_et.filters = arrayOf(InputFilter.LengthFilter(6))
            confirm_password_et.inputType = InputType.TYPE_CLASS_NUMBER
            new_password_et.inputType = InputType.TYPE_CLASS_NUMBER


            new_password_layout.isPasswordVisibilityToggleEnabled = false
            confirm_password_layout.isPasswordVisibilityToggleEnabled = false

            qrKey = intent.getStringExtra(PIN_KEY)?:""

        } else if (intent.getStringExtra(FROM_ACTION) == FROM_CHANGE_PIN) {
            isChangePin = true
            forgetPasswordGroup.makeVisible()
            chancePasswordGroup.makeVisible()
            title_change_password.makeGone()
            sixDigitHints2.makeVisible()
            tvTerms.visibility = View.VISIBLE
            tvPrivacy.visibility = View.VISIBLE
            divider.visibility = View.VISIBLE

            title_tv.text = resources.getText(R.string.change_pin)
            subTitle_tv.text = resources.getText(R.string.set_new_pin)

            new_password_layout.hint = resources.getText(R.string.enter_new_pin)
            confirm_password_layout.hint = resources.getText(R.string.retype_new_pin)
            old_password_layout.hint = resources.getText(R.string.enter_old_pin)

            new_password_et.filters = arrayOf(InputFilter.LengthFilter(6))
            confirm_password_et.filters = arrayOf(InputFilter.LengthFilter(6))
            old_password_et.filters = arrayOf(InputFilter.LengthFilter(6))

            confirm_password_et.inputType = InputType.TYPE_NUMBER_VARIATION_PASSWORD
            new_password_et.inputType = InputType.TYPE_NUMBER_VARIATION_PASSWORD
            old_password_et.inputType = InputType.TYPE_NUMBER_VARIATION_PASSWORD


            new_password_layout.isPasswordVisibilityToggleEnabled = false
            confirm_password_layout.isPasswordVisibilityToggleEnabled = false
            old_password_layout.isPasswordVisibilityToggleEnabled = false
        }

        change_password_btn.setOnClickListener {
            if (validate()) {
                if (!currentPassRequired) {
                    viewModel.apiChangePassword(
                        fpkey,
                        phone,
                        new_password_et.text.toString(),
                        confirm_password_et.text.toString(),
                        this,
                        this
                    )
                } else if (isSetPin) {
                    viewModel.apiSetPin(qrKey,new_password_et.text.toString(),
                        confirm_password_et.text.toString(),this,this)
                } else if (isChangePin) {
                    viewModel.apiChangePin(old_password_et.text.toString(),new_password_et.text.toString(),
                        confirm_password_et.text.toString(),this,this)
                } else {
                    viewModel.apiUpdatePassword(
                        getDataManager().mPref.prefGetCustomerToken()!!,
                        old_password_et.text.toString(),
                        new_password_et.text.toString(),
                        confirm_password_et.text.toString(),
                        this,
                        this
                    )
                }
            }
        }

        tvTerms.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.terms.ordinal)
            startActivity(intent)
        }
        tvPrivacy.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.privacy.ordinal)
            startActivity(intent)
        }
    }

    private fun validate(): Boolean {
        if (new_password_et.text.isEmpty()) {
            new_password_et.error = getString(R.string.enter_new_password)
            new_password_layout.isEndIconVisible= false
            return false
        }

        if (confirm_password_et.text.isEmpty()) {
            confirm_password_et.error = getString(R.string.confirm_new_password)
            confirm_password_layout.isEndIconVisible= false
            return false
        }

        if (old_password_layout.visibility == View.VISIBLE && old_password_et.text.isEmpty()) {
            old_password_et.error = getString(R.string.enter_old_password)
            old_password_layout.isEndIconVisible= false
            return false
        }

        if (new_password_et.text.toString() != confirm_password_et.text.toString()) {

            confirm_password_et.error = getString(R.string.confirm_password_not_matched)
            confirm_password_layout.isEndIconVisible= false

            return false
        }

        if (isSetPin && new_password_et.text.toString().trim().length<6) {
            showToast(this,"Pin Must Be 6 digit")
            return false
        }
        return true
    }

    private fun showEditDialog(status: Boolean, returnMessage: String) {
        val newDialog = Dialog(this, R.style.customDialog)
        newDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )

        customDialogBinding.ratingText.makeGone()
        customDialogBinding.ratingBar.makeGone()
        if (status) {
            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)
            customDialogBinding.textView85.makeVisible()

            customDialogBinding.textView82.text = getString(R.string.pin_set_successful)
            customDialogBinding.textView85.text = getString(R.string.proceed_scan)
            customDialogBinding.textView83.text = returnMessage

            customDialogBinding.textView84.background =
                ContextCompat.getDrawable(this, R.drawable.bg_white_outlined_silver_2)
            customDialogBinding.textView85.background =
                ContextCompat.getDrawable(this, R.drawable.bg_bottom_button_3)
            customDialogBinding.textView84.setColor(R.color.colorAccent)

        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.textView83.makeGone()

            customDialogBinding.textView84.text = getString(R.string.failed_text)
            customDialogBinding.textView84.background =
                ContextCompat.getDrawable(this, R.drawable.failed_drawable)

            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            if (status) {
                newDialog.dismiss()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            } else {
                newDialog.dismiss()
            }
        }

        customDialogBinding.textView85.setOnClickListener {
            if (status) {
                newDialog.dismiss()
                val intent = Intent(this, QrActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            } else {
                newDialog.dismiss()
            }
        }

        newDialog.setContentView(customDialogBinding.root)

        newDialog.show()
        newDialog.setCancelable(false)
    }
}
