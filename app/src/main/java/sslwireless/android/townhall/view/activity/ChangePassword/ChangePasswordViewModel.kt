package sslwireless.android.townhall.view.activity.ChangePassword

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class ChangePasswordViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager


    fun apiChangePassword(
        fpKey: String,
        mobile: String,
        password: String,
        password_confirmation: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiChangePassword(
            fpKey,
            mobile,
            password,
            password_confirmation,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "forgot-password-restore"))
        )
    }

    fun apiUpdatePassword(
        token: String,
        oldPassword: String,
        password: String,
        password_confirmation: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUpdatePassword(
            token,
            oldPassword,
            password,
            password_confirmation,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "password-update"))
        )
    }

    fun apiSetPin(
        qrKey: String,
        newPin: String,
        retypePin: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiSetPin(
            dataManager.mPref.prefGetCustomerToken()?:"",
            qrKey,
            newPin,
            retypePin,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "set_pin"))
        )
    }

    fun apiChangePin(
        old_qr_pin: String,
        qr_pin: String,
        confirm_qr_pin: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiChangePin(
            dataManager.mPref.prefGetCustomerToken()?:"",
            old_qr_pin,
            qr_pin,
            confirm_qr_pin,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "change_pin"))
        )
    }
}