package sslwireless.android.townhall.view.activity

import android.os.Build
import android.os.Bundle
import android.text.Html
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_help_and_support_item_details.*
import kotlinx.android.synthetic.main.item_help_and_support.help_support_item_title_tv
import sslwireless.android.townhall.data.model.help.ActivePopularTopicsItem
import sslwireless.android.townhall.view.utils.AppConstants

class HelpAndSupportItemDetailsActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private var helpSupportItem: ActivePopularTopicsItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_and_support_item_details)
    }

    override fun viewRelatedTask() {
        helpSupportItem = intent.getParcelableExtra("help_support_item")
        help_support_item_title_tv.text =
            if (getDataManager().mPref.prefGetLanguage() == AppConstants.Language.ENGLISH)
                helpSupportItem!!.title else
                helpSupportItem!!.titleBn
        val description =
            if (getDataManager().mPref.prefGetLanguage() == AppConstants.Language.ENGLISH)
                helpSupportItem!!.description else
                helpSupportItem!!.descriptionBn
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            help_support_item_long_desc_tv.text = Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT);
        } else {
            help_support_item_long_desc_tv.text = Html.fromHtml(description);
        }
    }
}
