package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.HelpSupportItemListRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_help_support_items_via_category.*
import sslwireless.android.townhall.data.model.help.ActivePopularTopicsItem
import sslwireless.android.townhall.data.model.help.HelpResponse
import sslwireless.android.townhall.view.activity.help.HelpAndSupportViewModel
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.fromResponse

class HelpSupportItemsViaCategoryActivity : BaseActivity(),HelpSupportItemListRecyclerAdapter.HelpItemClickListener {
    var helpSupportCategory: String ?= null
    private val viewModel: HelpAndSupportViewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(HelpAndSupportViewModel(getDataManager()))
        ).get(HelpAndSupportViewModel::class.java)
    }
    private  var helpResponse: HelpResponse?=null
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        if (result.data?.code==200) {
            helpResponse = result.data.data?.fromResponse()
            category_name_tv.text = if (getDataManager().mPref.prefGetLanguage()==AppConstants.Language.ENGLISH) helpResponse?.title else helpResponse?.titleBn
            help_support_item_rec_view.setHasFixedSize(true)
            val helpsupportitemadapter =
                HelpSupportItemListRecyclerAdapter(this, getDataManager().mPref.prefGetLanguage())
            help_support_item_rec_view.adapter = helpsupportitemadapter
            helpsupportitemadapter.setData(helpResponse!!.activeChild as ArrayList<ActivePopularTopicsItem>)
        } else {
            showToast(this,result.data?.message?:"")
        }
    }



    override fun onHelpItemClickListener(position: Int) {
        startActivity(Intent(this,HelpAndSupportItemDetailsActivity::class.java)
            .putExtra("help_support_item", helpResponse?.activeChild?.get(position)))
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_support_items_via_category)
    }

    override fun viewRelatedTask() {
        helpSupportCategory = intent.getStringExtra("help_support_category")
        viewModel.apiGetTopicHelpSupport(helpSupportCategory?:"",this,this)
    }
}
