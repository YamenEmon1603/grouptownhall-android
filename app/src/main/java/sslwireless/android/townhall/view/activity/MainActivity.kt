package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomnavigation.BottomNavigationView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.data.model.operator.OperatorModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.fragment.MoreFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_update_number.*
import kotlinx.android.synthetic.main.toolbar.*
import sslwireless.android.townhall.view.activity.otp.OtpActivity
import sslwireless.android.townhall.view.activity.programScheduling.ProgramSchedulingFragment
import sslwireless.android.townhall.view.activity.qrPay.QrActivity
import sslwireless.android.townhall.view.activity.qrPay.bottomDialog.PinRequiredBottomSheet
import sslwireless.android.townhall.view.activity.venue.VenueFragment
import sslwireless.android.townhall.view.fragment.home.HomeFragmentNew
import sslwireless.android.townhall.view.utils.*


class MainActivity : BaseActivity(), PinRequiredBottomSheet.PinRequiredBottomSheetListener {
    private var lastBackPressTime: Long = 0
    private var isHome = true
    private var isNeedLoading = false
    private val cViewModel: CommunicationViewModel by viewModels()
    private val viewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(MainActivityViewModel(getDataManager())))
            .get(MainActivityViewModel::class.java)
    }

    private val homeFragment: HomeFragmentNew by lazy { HomeFragmentNew() }
    private val offerFragment: VenueFragment by lazy { VenueFragment() }
    //private val transactionHistoryActivity: TransactionHistoryActivity by lazy { TransactionHistoryActivity() }
    private val programSchedulingFragment: ProgramSchedulingFragment by lazy { ProgramSchedulingFragment() }
    private val moreFragment: MoreFragment by lazy { MoreFragment() }
    private var activeFragment:Fragment = homeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppConstants.Operators.OPERATOR_LIST.clear()
        AppConstants.Operators.OPERATOR_LIST.addAll(getDataManager().mPref.prefGetOperatorList())

        /*viewModel.apiGetOperator(
            this, this
        )

        viewModel.apiGetFavContact(
            getDataManager().mPref.prefGetCustomerToken() ?: "", this, this
        )*/
    }

    override fun viewRelatedTask() {
        initFragment()
        toolbar.isVisible = false
        //bottomNavigation.menu.getItem(2).isEnabled = false

        setupBottomNavigationView()

        fabOffer.setOnClickListener {
            if (getDataManager().mPref.prefGetCurrentUser().qr_pin) {
                startActivity(Intent(this, QrActivity::class.java))
                finish()
            } else {
                PinRequiredBottomSheet(this).show(supportFragmentManager, "1234")
            }
        }
        cViewModel.onLanguageChange.observe(this, Observer {
            startActivityLanguage(Intent(intent).apply {
                putExtra("lang", true)
            })

        })

        val lang = intent.extras?.getBoolean("lang", false) ?: false
        if (lang) {
            bottomNavigation.findViewById<View>(R.id.menu_more).performClick()
        }
    }

    private fun initFragment() {
        addFragmentNew(false, R.id.container,offerFragment)
        addFragmentNew(false, R.id.container,programSchedulingFragment)
        addFragmentNew(false, R.id.container,moreFragment)
        addFragmentNew(true, R.id.container,homeFragment)
    }


    private fun setupBottomNavigationView() {
        bottomNavigation.setOnNavigationItemSelectedListener(BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val previousItem = bottomNavigation.selectedItemId;
            val nextItem = item.itemId
            if (previousItem != nextItem) {
                when (item.itemId) {
                    R.id.menu_home -> {
                        isHome = true
                        fragmentShowHide(homeFragment,activeFragment)
                        activeFragment = homeFragment
                        cViewModel.onHomeScreenShow.value = true
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.menu_offer -> {
                        isHome = false
                        fragmentShowHide(offerFragment,activeFragment)
                        activeFragment = offerFragment
                        return@OnNavigationItemSelectedListener true
                    }
                   /* R.id.menu_qr -> {
                        // isHome = false
                        //addFragment(true, R.id.container, QRPayFragment())
                        return@OnNavigationItemSelectedListener true
                    }*/
                    R.id.menu_history -> {
                        isHome = false
                        fragmentShowHide(programSchedulingFragment,activeFragment)
                        activeFragment = programSchedulingFragment
                        return@OnNavigationItemSelectedListener true
                    }
                    R.id.menu_more -> {
                        isHome = false
                        fragmentShowHide(moreFragment,activeFragment)
                        activeFragment = moreFragment
                        return@OnNavigationItemSelectedListener true
                    }
                    else -> {
                        isHome = false
                        fragmentShowHide(homeFragment,activeFragment)
                        activeFragment = homeFragment
                        return@OnNavigationItemSelectedListener true
                    }
                }
            }
            false
        })
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        val baseData = result.data ?: return
        if (baseData.code == 200) {
            when (key) {
                "Operators" -> {
                    val operatorModels = result.data.data.fromResponse<List<OperatorModel>>()
                    AppConstants.Operators.OPERATOR_LIST.clear()
                    AppConstants.Operators.OPERATOR_LIST.addAll(operatorModels!!)
                    getDataManager().mPref.prefOperator(operatorModels as ArrayList<OperatorModel>)
                }
                "requestOTP" -> {
                    val intent = Intent(this, OtpActivity::class.java)
                    intent.putExtra(OtpActivity.BUNDLE_IS_PIN, true)
                    intent.putExtra(
                        OtpActivity.BUNDLE_PHONE,
                        getDataManager().mPref.prefGetCurrentUser().mobile
                    )
                    startActivity(intent)
                }
                else -> {
                    val favList = result.data.data.fromResponse<List<FavoriteModel>>()
                    getDataManager().mPref.prefFavorite(favList as ArrayList<FavoriteModel>)
                }
            }
        } else if (baseData.code == 401) {
            showErrorToast(baseData.message)
            getDataManager().mPref.prefLogout(this)
        } else {
            showErrorToast(baseData.message)
        }
    }

    override fun onBackPressed() {
        if (!isHome) {
            bottomNavigation.findViewById<View>(R.id.menu_home).performClick()
            isHome = true
        } else {
            if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
                Toast.makeText(this, getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT)
                    .show()
                this.lastBackPressTime = System.currentTimeMillis()
            } else {
                super.onBackPressed()
            }
        }
    }


    override fun onLoading(isLoader: Boolean) {
        super.onLoading(isNeedLoading)
        isNeedLoading = false
    }

    override fun onProceedPin() {
        isNeedLoading = true
        viewModel.apiRequestOTP(this, this)
    }
}
