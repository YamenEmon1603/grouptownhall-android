package sslwireless.android.townhall.view.activity

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class MainActivityViewModel(private val dataManager: DataManager): BaseViewmodel(dataManager) {

    fun apiGetFavContact(
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetFavContact(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "getFav"))
        )
    }

    fun apiGetOperator(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetOperator(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "Operators"))
        )
    }

    fun apiRequestOTP(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRequestOTP(dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "requestOTP"))
        )
    }
}