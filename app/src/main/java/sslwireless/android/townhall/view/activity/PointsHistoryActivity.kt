package sslwireless.android.townhall.view.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.data.model.SampleModel.PointHistory
import sslwireless.android.townhall.view.adapter.PointHistoryAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.custom.ListItemDecoration
import kotlinx.android.synthetic.main.activity_reward_point_history.*

class PointsHistoryActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private var pointHistoryAdapter: PointHistoryAdapter? = null
    private var pointHistorys: ArrayList<PointHistory> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reward_point_history)
    }

    override fun viewRelatedTask() {

        var pointHistory = PointHistory(
            "You earned 56 Points for 500 Tk at Mobile Recharge",
            "MON, 6 NOV ’19",
            "56",
            "reward"
        )

        pointHistorys.add(pointHistory)

        pointHistory = PointHistory(
            "You have paid november wasa bill using 325 points",
            "THU, 1 SEP ’19",
            "325",
            "redeem"
        )

        pointHistorys.add(pointHistory)

        pointHistory = PointHistory(
            "You have paid january DTH bill using 400 points",
            "THE, 31 JAN ’19",
            "400",
            "reward"
        )

        pointHistorys.add(pointHistory)

        pointHistory = PointHistory(
            "You earned 75 Points for 1000 Tk at Desco Bill Paymet",
            "WED, 25 OCT ’19",
            "75",
            "redeem"
        )

        pointHistorys.add(pointHistory)


        point_history_item_recycler_view.layoutManager = LinearLayoutManager(this)
        point_history_item_recycler_view.setHasFixedSize(true)
        point_history_item_recycler_view.addItemDecoration(ListItemDecoration(this))
        pointHistoryAdapter = PointHistoryAdapter(this, pointHistorys)
        point_history_item_recycler_view.adapter = pointHistoryAdapter

    }


}
