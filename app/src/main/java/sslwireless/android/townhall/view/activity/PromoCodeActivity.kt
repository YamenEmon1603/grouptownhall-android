package sslwireless.android.townhall.view.activity

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.data.model.SampleModel.Promo
import sslwireless.android.townhall.view.adapter.AppliedPromoAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_promo_code.*

class PromoCodeActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var promos : ArrayList<Promo> = ArrayList()


    var adapter : AppliedPromoAdapter ? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promo_code)

    }

    override fun viewRelatedTask() {
        var  promo = Promo("EASY50","Enjoy flat 50 percent discount on any type of transaction, offer valid only for the first transaction.","MON, 6 NOV ’19")

        promos.add(promo)

        promo = Promo("DESCO20","Pay your desco bill with 20 taka discount, offer valid for the first time transaction.","THU, 1 SEP ’19")

        promos.add(promo)

        promo = Promo("BLOCKBASTER33","Enjoy upto 33 percent discount on any movie ticket purchase from blockbaster","THE, 31 JAN ’19")

        promos.add(promo)

        applied_promo_recycler_view.layoutManager = LinearLayoutManager(this)
        applied_promo_recycler_view.setHasFixedSize(true)
        adapter = AppliedPromoAdapter(this,promos)
        applied_promo_recycler_view.adapter = adapter
    }
}
