package sslwireless.android.townhall.view.activity

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.TopUp.RechargeActivity
import sslwireless.android.townhall.view.activity.favorites.OperatorBottomSheet
import sslwireless.android.townhall.view.adapter.RechargeFavoriteNumberAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.*
import kotlinx.android.synthetic.main.recharge_number_select.*
import sslwireless.android.townhall.data.model.recentTopUp.RecentOrderData
import sslwireless.android.townhall.data.model.recentTopUp.RecentTopUp
import sslwireless.android.townhall.view.activity.TopUp.RechargeViewModel
import sslwireless.android.townhall.view.adapter.RecentTransactionAdapter
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import java.util.regex.Pattern

class RechargeNumberSelectionActivity : BaseActivity(),
    OperatorBottomSheet.BottomSheetListener,
    RechargeFavoriteNumberAdapter.NumberListener, RecentTransactionAdapter.RecentNumberListener {
    private var isFromRechargeRequest: Boolean = false
    private lateinit var mobile_number: String
    private var loadingCount = 0
    private val PERMISSION_REQUEST_CODE: Int = 100
    private val PICK_CONTACT: Int = 200
    private val phonePattern = "(^(01){1}[3-9]{1}\\d{8})$"
    private val pref by lazy { getDataManager().mPref }
    private val viewModel: RechargeViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(RechargeViewModel(getDataManager())))
            .get(RechargeViewModel::class.java)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (key == "recent") {
            recentShimmer.stopShimmer()
            recentShimmer.makeGone()
            val recentTopUp =
                result.data?.data.fromResponse<List<RecentTopUp>>()
                    ?.filter { it.serviceTitle == "top-up" }
            val orderList = ArrayList<RecentOrderData>()
            recentTopUp?.forEach { recentItem ->
                val orderItems: ArrayList<RecentOrderData> =
                    Gson().fromJson(recentItem.orderData?.replace("&quot;", "\""))
                orderItems.forEach { item ->
                    item.time = recentItem.requestedAt
                    orderList.add(item)
                }
            }

            if (orderList.isNotEmpty()) {
                recentGroup.makeVisible()
                val distinctList = orderList.distinctBy { it.phone }
                recentRecycler.adapter =
                    RecentTransactionAdapter(
                        if (distinctList.size > 5) distinctList.take(5) as ArrayList<RecentOrderData>
                        else distinctList as ArrayList<RecentOrderData>,
                        this
                    )
            } else {
                recentGroup.makeGone()
            }
        } else if (key == "getFav") {
            favShimmer.stopShimmer()
            favShimmer.makeGone()
            val favList = result.data?.data.fromResponse<List<FavoriteModel>>()
            favList?.let {
                if (favList.isNotEmpty()) {
                    favGroup.makeVisible()
                    favRecycler.adapter =
                        RechargeFavoriteNumberAdapter(favList as ArrayList<FavoriteModel>, this)
                } else {
                    favGroup.makeGone()
                }
            }

        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recharge_number_selection)
    }

    override fun viewRelatedTask() {
        isFromRechargeRequest =
            intent.getBooleanExtra(RechargeActivity.KEY_FROM_RECHARGE_REQUEST, false)

        if (isFromRechargeRequest) {
            customTextView16.visibility = View.GONE
            view25.visibility = View.GONE
        } else {
            customTextView16.visibility = View.VISIBLE
            view25.visibility = View.VISIBLE
        }

        mobNumber.requestFocus()

        view19.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))

        if (!isFromRechargeRequest) {
            customTextView17.text = pref.prefGetCurrentUser().mobile

            if (pref.prefGetOperatorName().isNotEmpty() && pref.prefGetOperatorType()
                    .isNotEmpty()
            ) {
                imageView17.operatorIcon(pref.prefGetOperatorName())
            }
            recentShimmer.startShimmer()
            recentShimmer.makeVisible()
            favShimmer.startShimmer()
            favShimmer.makeVisible()
            viewModel.recentTransactionService(this, this)
            viewModel.apiGetFavContact(this, this)

            buttonClickGroup.setOnClickListener {
                if (pref.prefGetOperatorName().isNotEmpty() && pref.prefGetOperatorType()
                        .isNotEmpty()
                ) {
                    if (isValidNumber(customTextView17.text.toString())) {
                        val intent =
                            Intent(
                                this@RechargeNumberSelectionActivity,
                                RechargeActivity::class.java
                            )
                        intent.putExtra("operatorName", pref.prefGetOperatorName())
                        intent.putExtra("operatorType", pref.prefGetOperatorType())
                        intent.putExtra("mobile_number", mobile_number)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                } else {
                    isValidNumber(customTextView17.text.toString())
                }
            }
        }

        intent?.extras?.apply {
            mobNumber.setText(getString("number"))
            continueBtn.visibility = VISIBLE
        }
        backGroup.setOnClickListener {
            finish()
        }



        mobNumber.setOnFocusChangeListener { view, b ->
            if (b) {
                view19.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
            }
        }

        mobNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().trim().length == 11) {
                    /* val intent = Intent(this@RechargeNumberSelectionActivity , SelectOperatorActivity::class.java)
                    intent.putExtra("num",mobNumber.text.toString())
                    startActivity(intent)*/
                    continueBtn.visibility = VISIBLE
                } else {
                    if (continueBtn.visibility == VISIBLE) {
                        continueBtn.visibility = GONE
                    }
                }
            }
        })

        continueBtn.setOnClickListener {
            if (isValidNumber(mobNumber.text.toString().trim())) {
                mobile_number = mobNumber.text.toString().trim()
                val operators = OperatorBottomSheet("")
                operators.show(supportFragmentManager, "1234")
            } else {
                continueBtn.visibility = GONE
            }
        }

        imageView16.setOnClickListener {
            /*if (isPermissionGranted(Manifest.permission.READ_CONTACTS, PERMISSION_REQUEST_CODE)) {
                callContactIntent()
            }*/
            callContactIntent()
        }
    }

    private fun callContactIntent() {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        pickContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(pickContact, PICK_CONTACT)
    }

    private fun pickContact(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                var c: Cursor? = null
                try {
                    c = contentResolver.query(
                        uri, arrayOf(
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                        ),
                        null, null, null
                    )
                    if (c != null && c.moveToFirst()) {
                        isValidNumber(c.getString(0))
                    }
                } finally {
                    c?.close()
                }
            }
        }
    }

    private fun isValidNumber(number: String): Boolean {
        try {
            number.replace("+88", "").replace("\\D+".toRegex(), "")
            if (number.length >= 11) {
                var finalNumber =
                    number.replace("-".toRegex(), "").replace(" ".toRegex(), "")
                finalNumber = finalNumber.substring(finalNumber.length - 11)
                val pattern = Pattern.compile(
                    phonePattern,
                    Pattern.CASE_INSENSITIVE
                )
                val matcher = pattern.matcher(finalNumber)
                return if (matcher.matches()) {
                    mobile_number = finalNumber
                    mobNumber.setText(finalNumber)
                    mobNumber.setSelection(mobNumber.length())
                    true
                } else {
                    showToastMsg(getString(R.string.msg_invalid_number))
                    false
                }
            } else {
                showToastMsg(getString(R.string.msg_invalid_number))
                return false
            }
        }catch (e:Exception) {
            showToastMsg(getString(R.string.msg_invalid_number))
            return false
        }
    }

    override fun onClicked(operatorName: String, operatorType: String) {
        if (isFromRechargeRequest) {
            pref.prefSetOperatorName(operatorName)
            pref.prefSetOperatorType(operatorType)
        } else {
            val userdata = pref.prefGetCurrentUser()
            if (mobile_number == userdata.mobile) {
                pref.prefSetOperatorName(operatorName)
                pref.prefSetOperatorType(operatorType)
            }
        }

        val intent = Intent(this@RechargeNumberSelectionActivity, RechargeActivity::class.java)
        intent.putExtra("operatorName", operatorName)
        intent.putExtra("operatorType", operatorType)
        intent.putExtra("mobile_number", mobile_number)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (requestCode == PERMISSION_REQUEST_CODE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callContactIntent()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_CONTACT) {
            pickContact(data)
        }
    }

    override fun onNumberListener(item: FavoriteModel) {
        if (isValidNumber(item.phone)) {
            val intent = Intent(this@RechargeNumberSelectionActivity, RechargeActivity::class.java)
            intent.putExtra("operatorName", item.operator_id.operatorName())
            intent.putExtra("operatorType", item.connection_type)
            intent.putExtra("mobile_number", mobile_number)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onRecentNumberListener(item: RecentOrderData) {
        if (isValidNumber(item.phone ?: "")) {
            val intent = Intent(this@RechargeNumberSelectionActivity, RechargeActivity::class.java)
            intent.putExtra("operatorName", item.operator?.operatorName())
            intent.putExtra("operatorType", item.type)
            intent.putExtra("mobile_number", mobile_number)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onLoading(isLoader: Boolean) {
        /*if (isLoader) {
            progressBarHandler.show()
        } else {
            loadingCount++
            if (loadingCount == 2) {
                progressBarHandler.hide()
            }
        }*/
    }
}