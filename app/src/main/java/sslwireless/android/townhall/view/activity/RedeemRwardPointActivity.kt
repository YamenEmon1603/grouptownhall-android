package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.data.model.SampleModel.RedeemPointOffer
import sslwireless.android.townhall.view.adapter.RedeemPointOfferAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.dialog.CommonDialogFragment
import sslwireless.android.townhall.view.dialog.RedeemOfferBottomSheet
import kotlinx.android.synthetic.main.activity_redeem_reward_point.*

class RedeemRwardPointActivity : BaseActivity(), RedeemPointOfferAdapter.ClickListener,
    RedeemOfferBottomSheet.IBottomSheetDialogClicked, CommonDialogFragment.OnItemClickListener {
    lateinit var redeemDialog: CommonDialogFragment

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCancelButtonClicked() {
        redeemRedeemOfferBottomSheet!!.dismiss()
    }

    override fun onRedeemButtonClicked() {
        redeemDialog.show(this)
        redeemRedeemOfferBottomSheet!!.dismiss()
    }


    override fun itemClicked(position: Int) {
        redeemRedeemOfferBottomSheet = RedeemOfferBottomSheet(redeemPointOffers.get(position))
        redeemRedeemOfferBottomSheet!!.show(
            supportFragmentManager,
            redeemRedeemOfferBottomSheet!!.tag
        )
        redeemRedeemOfferBottomSheet!!.setBottomDialogListener(this)
    }


    var redeemPointOfferAdapter: RedeemPointOfferAdapter? = null
    var redeemPointOffers: ArrayList<RedeemPointOffer> = ArrayList()
    var redeemRedeemOfferBottomSheet: RedeemOfferBottomSheet? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeem_reward_point)
        initStatusBar()
        redeemDialog = CommonDialogFragment(CommonDialogFragment.REDEEM_SUCCESS, this)
    }


    override fun viewRelatedTask() {
     /*   var offer = RedeemPointOffer(
            "Unlimited + Data : 2 GB per day SMS : 100 per day",
            R.drawable.ic_robi,
            "Validity: 07 Days",
            "150"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "150 MB 4G internet + 1 GB facebook pack + 100 local min",
            R.drawable.ic_gp,
            "Validity: 07 Days",
            "100"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "300 min on any local number + SMS : 1000",
            R.drawable.ic_bl,
            "Validity: 60 Days",
            "325"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "7 GB 4G data with 500 SMS",
            R.drawable.ic_tt,
            "Validity: 07 Days",
            "244"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "Unlimited + Data : 4 GB per day SMS : 200 per day",
            R.drawable.airtel_img,
            "Validity: 30 Days",
            "230"
        )
        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "150 MB 4G internet + 1 GB facebook pack + 100 local min",
            R.drawable.ic_gp,
            "Validity: 07 Days",
            "100"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "Unlimited + Data : 2 GB per day SMS : 100 per day",
            R.drawable.ic_robi,
            "Validity: 07 Days",
            "150"
        )

        redeemPointOffers.add(offer)

        offer = RedeemPointOffer(
            "7 GB 4G data with 500 SMS",
            R.drawable.ic_tt,
            "Validity: 07 Days",
            "244"
        )

        redeemPointOffers.add(offer)*/

        recharge_offer_recycler_view.layoutManager = LinearLayoutManager(this)
        recharge_offer_recycler_view.setHasFixedSize(true)
        redeemPointOfferAdapter = RedeemPointOfferAdapter(this, redeemPointOffers)
        redeemPointOfferAdapter!!.setClickListener(this)
        recharge_offer_recycler_view.adapter = redeemPointOfferAdapter


        history_icon_iv.setOnClickListener {
            startActivity(Intent(this, PointsHistoryActivity::class.java))
        }

    }

    private fun initStatusBar() {
        val window: Window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.trans_black_1)

        val decorView = window.decorView
        decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
    }

    override fun onDoneClick() {
        redeemDialog.dismiss()
        showToast(this, getString(R.string.offer_redeemed))
    }

    override fun onFavClick() {

    }

}
