package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.ReferredFriendsListAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_refer_and_earn.*

class ReferAndEarnActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var adapter: ReferredFriendsListAdapter? = null
    var names: ArrayList<String> = ArrayList()


    override fun viewRelatedTask() {

        names.add("Sarwar Hosen")
        names.add("Ruhul Azam")
        names.add("Mushfique Hasan")
        names.add("Jamil Hasnain")
        names.add("Fasi Uddin Rayham")

        referred_friends_list_recycler_view.layoutManager = LinearLayoutManager(this)
        referred_friends_list_recycler_view.setHasFixedSize(true)
        adapter = ReferredFriendsListAdapter(this, names)
        referred_friends_list_recycler_view.adapter = adapter

        referCoupon.text = "BESTYEAR2020"

        share.setOnClickListener {
            val coupon = referCoupon.text
            if (coupon.isNotEmpty()) {
                val sendIntent: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(Intent.EXTRA_TEXT, coupon)
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(sendIntent, null)
                startActivity(shareIntent)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refer_and_earn)
    }
}
