package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.ChangePassword.ChangePasswordActivity
import sslwireless.android.townhall.view.activity.loginDevice.WhereYouAreLoggedInActivity
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_security_and_login.*

class SecurityAndLoginActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_and_login)
    }

    override fun viewRelatedTask() {
        change_password_layout.setOnClickListener {
            startActivity(
                Intent(this,
                    ChangePasswordActivity::class.java).apply {
                    intent.putExtra(
                        ChangePasswordActivity.FROM_ACTION,
                        ChangePasswordActivity.FROM_CHANGE_PASS
                    )
                })
        }


        where_your_logged_in_layout.setOnClickListener {
            startActivity(
                Intent(this, WhereYouAreLoggedInActivity::class.java))
        }


    }

}
