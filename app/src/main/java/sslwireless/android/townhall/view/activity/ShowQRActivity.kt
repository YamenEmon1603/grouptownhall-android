package sslwireless.android.townhall.view.activity

import android.graphics.Bitmap
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.android.synthetic.main.activity_show_qr.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.databinding.ActivityShowQrBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.getQrCodeBitmap
import sslwireless.android.townhall.view.utils.loadImage

class ShowQRActivity : BaseActivity() {

    private val binding: ActivityShowQrBinding by lazy {
        ActivityShowQrBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }

    override fun viewRelatedTask() {
        binding.backIV.setOnClickListener {
            onBackPressed()
        }

        generateQRCode()
    }

    private fun generateQRCode() {
        val phoneNumber = getDataManager().mPref.prefGetCurrentUser().mobile

        if (phoneNumber.isEmpty()) {
            showToast(this, "Something went wrong!")
            return
        }

        try {
            binding.qrIV.loadImage(phoneNumber.getQrCodeBitmap())
        } catch (exception: Exception) {
            showToast(this, "Something went wrong!")
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}