package sslwireless.android.townhall.view.activity.TopUp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.activity.favorites.OperatorBottomSheet
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible
import kotlinx.android.synthetic.main.connection_type_bottomsheet_layout.*


class ConnectionTypeBottomSheet(private val listener: OperatorBottomSheet.BottomSheetListener?,private val operationName:String) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.connection_type_bottomsheet_layout, container)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (operationName==AppConstants.Operators.GRAMEENPHONE) {
            skittoLayout.makeVisible()
        } else {
            skittoLayout.makeGone()
        }
        prepaidLayout.setOnClickListener {
            dismiss()
            listener?.onClicked(operationName,AppConstants.Operators.PREPAID)
        }

        postpaidLayout.setOnClickListener {
            dismiss()
            listener?.onClicked(operationName,AppConstants.Operators.POSTPAID)
        }
        skittoLayout.setOnClickListener {
            dismiss()
            listener?.onClicked(operationName,AppConstants.Operators.SKITTO)
        }

        imageView43.setOnClickListener { dismiss() }

    }




}
