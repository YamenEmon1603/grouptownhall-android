package sslwireless.android.townhall.view.activity.TopUp

import android.content.Context
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.databinding.CustomMostPopularOffersBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.makeInvisible
import sslwireless.android.townhall.view.utils.makeVisible
import sslwireless.android.townhall.view.utils.operatorIconByShotName

class PopularPlanViewHolder(itemView: ViewDataBinding, context: Context) :
    BaseViewHolder2(itemView.root) {

    var binding = itemView as CustomMostPopularOffersBinding
    var mContext: Context = context


    override fun <T> onBind(position: Int, model: T, mCallback: IAdapterListener) {
        model as PopularPlan

        binding.offerDetailsTv.text = model.description.trim()
        binding.validityText.text = model.validity?.trim()
        binding.offerPriceTv.text = model.amount_max.toString()
        if (model.ribbon == null || model.ribbon.isEmpty()) {
            binding.ribbonGroup.makeInvisible()
        } else {
            binding.ribbonText.text = model.ribbon
            binding.ribbonGroup.makeVisible()
        }

        binding.operatorImageIv.operatorIconByShotName(model.operator_short_name)

        binding.root.setOnClickListener {
            mCallback.clickListener(position, model, binding.root)
        }
    }

}