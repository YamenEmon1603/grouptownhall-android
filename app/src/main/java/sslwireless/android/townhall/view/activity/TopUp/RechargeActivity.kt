package sslwireless.android.townhall.view.activity.TopUp

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.go_back_layout.*
import kotlinx.android.synthetic.main.layout_recharge.*
import org.joda.time.DateTime
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.OrderData
import sslwireless.android.townhall.data.model.PaymentStatusResponse
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.RechargeNumberSelectionActivity
import sslwireless.android.townhall.view.activity.desco.TransactionSuccessfulDF
import sslwireless.android.townhall.view.activity.favorites.OperatorBottomSheet
import sslwireless.android.townhall.view.activity.quickie.QuickieActivity
import sslwireless.android.townhall.view.activity.rechargeOffer.RechargeOfferActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.ArrayList


class RechargeActivity : BaseActivity(), OperatorBottomSheet.BottomSheetListener,
    SSLCTransactionResponseListener, RechargeOfferAdapter.ClickListener {
    private lateinit var sslCommerzIntegrationModel: SslCommerzIntegrationModel
    private var operatorId: Int = 0
    private var mobile_number: String? = null
    private val topUpModels = ArrayList<TopUpModel>()
    private var myAmount = 0
    private var isInSdk = false
    private var isShowOfferFilter = true
    private var isShowSpecialOffer = true
    private var isOfferLoading=true
    private val dialogFragment by lazy { TransactionSuccessfulDF() }
    private val operators: OperatorBottomSheet by lazy {
        OperatorBottomSheet("collapseAfterClicked")
    }
    private val language: String by lazy {
        getDataManager().mPref.prefGetLanguage()
    }
    private lateinit var viewModel: RechargeViewModel
    private val offerPlan: ArrayList<PopularPlan> = ArrayList()
    private val offerAdapter: RechargeOfferAdapter by lazy {
        RechargeOfferAdapter(offerPlan).apply { setClickListener(this@RechargeActivity) }
    }
    private var typeSelection: String? = null
    private var operatorName = ""

    private var popularPlans: ArrayList<PopularPlan> = ArrayList()
    private val popularPlansLiveData = MutableLiveData<List<PopularPlan>>()
    private val offerLiveData = MutableLiveData<PopularPlan>()

    companion object {
        const val KEY_FROM_RECHARGE_REQUEST = "_from_recharge_request"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recharge)

        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(RechargeViewModel(getDataManager())))
                .get(RechargeViewModel::class.java)
    }

    override fun viewRelatedTask() {
        isOfferLoading= true
        shimmer.startShimmer()
        shimmer.makeVisible()
        viewModel.apiGetPlans(this, this)

        val isFromRechargeRequest = intent.getBooleanExtra(KEY_FROM_RECHARGE_REQUEST, false)

        if (isFromRechargeRequest) {
            quickieNTolbarGroup.visibility = View.GONE
            scheduleAndNumberGroup.visibility = View.GONE
        } else {
            quickieNTolbarGroup.visibility = View.VISIBLE
            scheduleAndNumberGroup.visibility = View.VISIBLE
        }

        backGroup.setOnClickListener {
            onBackPressed()
        }

        operatorChangeGroup.setOnClickListener {
            if (!operators.isAdded) {
                operators.show(supportFragmentManager, "1234")
            }
        }


        mobNumber.setOnFocusChangeListener { view, b ->
            if (b) {
                view19.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
                view22.setBackgroundDrawable(resources.getDrawable(R.drawable.edittext_layout))
            }
        }

        amount.setOnFocusChangeListener { view, b ->
            if (b) {
                view22.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
                view19.setBackgroundDrawable(resources.getDrawable(R.drawable.edittext_layout))
            }
        }


        numberGroup.setOnClickListener {
            val intent = Intent(this, RechargeNumberSelectionActivity::class.java)
            intent.putExtra(KEY_FROM_RECHARGE_REQUEST, isFromRechargeRequest)
            startActivityForResult(intent, 200)
        }

        selected_mobile_number_tv.setOnClickListener {
            val intent = Intent(this, RechargeNumberSelectionActivity::class.java)
            intent.putExtra("number", selected_mobile_number_tv.text.toString())
            intent.putExtra(KEY_FROM_RECHARGE_REQUEST, isFromRechargeRequest)
            startActivityForResult(intent, 200)
        }


        top_up_offer_rec_view.layoutManager = LinearLayoutManager(this)

        viewAll.setOnClickListener {
            val intent = Intent(this, RechargeOfferActivity::class.java)
            intent.putExtra("isFromRecharge", true)
            intent.putExtra(
                "operator",
                if (selectedNumberLayout.visibility == GONE) "All" else operatorNameTv.text.toString()
            )
            intent.putExtra(
                "type",
                if (selectedNumberLayout.visibility == GONE) "All" else if (typeSelection == AppConstants.Operators.SKITTO) AppConstants.Operators.PREPAID else typeSelection
            )
            startActivityForResult(intent, 300)
        }

        offerLiveData.observe(this, androidx.lifecycle.Observer {
            it?.let {
                if (isShowSpecialOffer) {
                    offerRechargeGroup.makeVisible()
                    val addAmount = it.amount_max - amount.text.toString().toInt()
                    specialOffer.text = "Add $addAmount TK more to get ${it.offer_title}"
                } else {
                    offerRechargeGroup.makeGone()
                    isShowSpecialOffer = true
                }
            }
        })

        offerApplyBtn.setOnClickListener {
            isShowOfferFilter = false
            isShowSpecialOffer = false
            offerAdapter.setSelection(offerLiveData.value)
            offerAdapter.notifyDataSetChanged()
            amount.setText(offerLiveData.value?.amount_max.toString())

        }

        amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0.toString().trim().isEmpty()) {
                    proceedToRecharge.isEnabled = false
                    addAnotherNumber.isEnabled = false
                    scheduleText.isEnabled = false
                    addAnotherNumber.setTextColor(resources.getColor(R.color.schedule_text_color))
                    scheduleText.setTextColor(resources.getColor(R.color.schedule_text_color))
                    return
                }
                offerShowLayout(p0.toString().trim())
                if (isShowOfferFilter) {
                    myPlanFilter()
                } else {
                    isShowOfferFilter = true
                }
            }
        })


        quick_amount_50.setOnClickListener {
            amount.setText("50")
        }
        quick_amount_100.setOnClickListener {
            amount.setText("100")
        }
        quick_amount_200.setOnClickListener {
            amount.setText("200")
        }
        quick_amount_500.setOnClickListener {
            amount.setText("500")
        }

        addAnotherNumber.setOnClickListener {
            if (!processTopUpModel()) {
                return@setOnClickListener
            }
            numberAddedGroup.visibility = View.VISIBLE
            customTextView46.text =
                topUpModels.size.toString() + " " + getString(R.string.number_added)
            showToastMsg(getString(R.string.new_number_added))
            clearView()
        }
        scheduleText.setOnClickListener { initDatePicker() }
        selectedTime.setOnClickListener { initDatePicker() }

        quickieGroup.setOnClickListener {
            val intent = Intent(this, QuickieActivity::class.java)
            startActivity(intent)
        }

        proceedToRecharge.setOnClickListener {
            if (!isFromRechargeRequest) {
                if (!isInSdk) {
                    if (!processTopUpModel()) {
                        return@setOnClickListener
                    }
                    val intent = Intent(this, RechargeSummaryActivity::class.java).apply {
                        putExtra("topModels", Gson().toJson(topUpModels))
                        putExtra(
                            "scheduleText",
                            if (scheduleTimerGroup.visibility == GONE) "None" else selectedTime.text.toString()
                        )
                    }
                    startActivityForResult(intent, 400)
                }
            } else {
                val number = mobile_number ?: return@setOnClickListener
                val type = typeSelection ?: return@setOnClickListener

                val intent = Intent(this, RechargeRequestActivity::class.java)
                intent.putExtra(
                    RechargeRequestActivity.KEY_RECHARGE_REQUEST_DATA,
                    TopUpModel(
                        amount.text.toString().toInt(),
                        operatorId,
                        number,
                        type.toLowerCase()
                    )
                )
                startActivity(intent)
            }
        }

        numberAddedGroup.setOnClickListener {
            val intent = Intent(this, RechargeSummaryActivity::class.java).apply {
                putExtra("topModels", Gson().toJson(topUpModels))
                putExtra(
                    "scheduleText",
                    if (scheduleTimerGroup.visibility == GONE) "None" else selectedTime.text.toString()
                )
            }
            startActivityForResult(intent, 500)
        }

        stayView.setOnClickListener {
            goBackMainLayout.visibility = View.GONE
        }

        yesView.setOnClickListener {
            super.onBackPressed()
        }
        top_up_offer_rec_view.apply {
            adapter = offerAdapter
        }

        popularPlansLiveData.observe(this, androidx.lifecycle.Observer {
            offerPlan.clear()
            offerPlan.addAll(it)
            offerAdapter.setSelection(null)
            offerAdapter.notifyDataSetChanged()
        })

        imageView26.setOnClickListener {
            scheduleTimerGroup.visibility = GONE
            scheduleText.text = getString(R.string.schedule_recharge)
        }

        val selectedPlan = intent?.getParcelableExtra<PopularPlan>("selectedPlan")
        selectedPlan?.let {
            amount.setText(it.amount_max.toString())
        }
    }

    private fun processTopUpModel(): Boolean {
        if (amount.text.toString().trim().isEmpty()) {
            showToast(this, getString(R.string.enter_amount))
            return false
        }
        val topUpModel =
            TopUpModel(
                amount.text.toString().toInt(),
                operatorId,
                mobile_number ?: "",
                typeSelection ?: "".toLowerCase()
            )
        if (topUpModel.amount < topUpModel.operator.getMinRechargeAmount(topUpModel.type) ||
            topUpModel.amount > topUpModel.operator.getMaxRechargeAmount(topUpModel.type)
        ) {

            val minAmount = topUpModel.operator.getMinRechargeAmount(topUpModel.type).toString()
                .digitChangeToBangla(language)
            val maxAmount = topUpModel.operator.getMaxRechargeAmount(topUpModel.type).toString()
                .digitChangeToBangla(language)
            val msg = getString(
                R.string.recharge_range,
                minAmount,
                maxAmount,
                topUpModel.type.operatorChangeToBangla(language)
            )
            showToast(this, msg)
            return false
        }
        if (topUpModels.isNotEmpty()) {
            val findItem =
                topUpModels.find {
                    it.phone == topUpModel.phone
                            && it.type == topUpModel.type
                            && it.operator == topUpModel.operator
                }
            findItem?.let {
                val restList =
                    topUpModels.filterNot {
                        it.phone == topUpModel.phone
                                && it.type == topUpModel.type
                                && it.operator == topUpModel.operator
                    }
                topUpModel.amount = topUpModel.amount + findItem.amount
                topUpModels.retainAll(restList)
            }
        }
        topUpModels.add(topUpModel)
        return true
    }

    private fun clearView() {
        quickieNTolbarGroup.visibility = View.GONE
        numberGroup.visibility = View.VISIBLE
        selectedNumberLayout.visibility = View.GONE
        offerRechargeGroup.makeGone()
        amount.setText("")
        operatorId = 0
        mobile_number = ""
        typeSelection = ""
        scheduleText.isEnabled = false
        addAnotherNumber.isEnabled = false
        proceedToRecharge.isEnabled = false
        scheduleText.setTextColor(resources.getColor(R.color.schedule_text_color))
        addAnotherNumber.setTextColor(resources.getColor(R.color.schedule_text_color))
        myAmount = 0
        myPlanFilter()
    }

    override fun onBackPressed() {
        hideKeyboard()
        if (selectedNumberLayout.visibility == View.VISIBLE || numberAddedGroup.visibility == View.VISIBLE) {
            goBackMainLayout.visibility = View.VISIBLE
        } else {
            super.onBackPressed()
        }
    }

    private fun initDatePicker() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd =
            DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    //selectedTime.text = "$dayOfMonth/$monthOfYear/$year"
                    val dayOfMonthString = if (dayOfMonth < 10) "0$dayOfMonth" else "" + dayOfMonth
                    val monthOfYearString =
                        if (monthOfYear + 1 < 10) "0${monthOfYear + 1}" else "" + (monthOfYear + 1)
                    initTimePicker("" + year, monthOfYearString, dayOfMonthString)
                },
                year,
                month,
                day
            )
        dpd.datePicker.minDate = System.currentTimeMillis() - 1000
        dpd.datePicker.maxDate = DateTime().plusDays(30).toDate().time
        dpd.show()

    }

    private fun initTimePicker(year: String, month: String, date: String) {
        val mcurrentTime = Calendar.getInstance()
        val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute =
            if ("$date-$month-$year".isToday("dd-MM-yyyy")) mcurrentTime.get(Calendar.MINUTE) + 5 else mcurrentTime.get(
                Calendar.MINUTE
            )
        val mTimePicker: RangeTimePickerDialog
        mTimePicker = RangeTimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { timePicker, selectedHour, selectedMinute ->
                //time.text = "$selectedHour:$selectedMinute:00"
                scheduleTimerGroup.visibility = VISIBLE
                scheduleText.text = ""
                selectedTime.text = "$date-$month-$year $selectedHour:$selectedMinute".dateChangeTo(
                    "dd-MM-yyyy HH:mm",
                    "dd-MMM-yyyy | hh:mm aa"
                )

            }, hour, minute, false
        )//Yes 24 hour time
        if ("$date-$month-$year".isToday("dd-MM-yyyy")) {
            mTimePicker.setMin(hour, minute)
        }
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    @ExperimentalStdlibApi
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        hideKeyboard()
        if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            numberGroup.visibility = View.GONE
            selectedNumberLayout.visibility = View.VISIBLE

            operatorName = data?.getStringExtra("operatorName") ?: ""
            typeSelection = data?.getStringExtra("operatorType") ?: ""
            mobile_number = data?.getStringExtra("mobile_number") ?: ""
            myPlanFilter()
            selected_mobile_number_tv.text = mobile_number

            type.text =
                if (typeSelection == AppConstants.Operators.SKITTO) typeSelection else typeSelection?.toUpperCase()

            operatorNameTv.text = operatorName

            operatorImage.operatorIcon(operatorName)
            operatorId = operatorName.operatorID(typeSelection ?: "")


            offerShowLayout(amount.text.toString().trim())
            //addAnotherNumber.setTextColor(resources.getColor(R.color.login_backborder))
        } else if (requestCode == 300 && resultCode == Activity.RESULT_OK) {
            val selectedPlan = data?.getParcelableExtra<PopularPlan>("selectedPlan")
            selectedPlan?.let {
                amount.setText(it.amount_max.toString())
            }
        } else if (requestCode == 400 && resultCode == Activity.RESULT_CANCELED) {
            val topModelListData = data?.getStringExtra("TopUpModelList") ?: ""
            val list: ArrayList<TopUpModel> = Gson().fromJson(topModelListData)
            topUpModels.clear()
            topUpModels.addAll(list)
            isShowSpecialOffer = false
            processLastItemInUi()

        } else if (requestCode == 500 && resultCode == Activity.RESULT_CANCELED) {
            val topModelListData = data?.getStringExtra("TopUpModelList") ?: ""
            val list: ArrayList<TopUpModel> = Gson().fromJson(topModelListData)
            topUpModels.clear()
            topUpModels.addAll(list)
            customTextView46.text =
                topUpModels.size.toString() + " " + getString(R.string.number_added)
        } else if ((requestCode == 400 || requestCode == 500) && resultCode == Activity.RESULT_OK) {
            when (data?.getSerializableExtra("TopUpType") as TopUpType) {
                TopUpType.ADD_NEW -> {
                    val list: ArrayList<TopUpModel> =
                        Gson().fromJson(data.getStringExtra("TopUpModelList"))
                    topUpModels.clear()
                    topUpModels.addAll(list)
                    numberAddedGroup.visibility = VISIBLE
                    clearView()
                    customTextView46.text =
                        topUpModels.size.toString() + " " + getString(R.string.number_added)
                }
                TopUpType.CLEAR_ALL -> {
                    numberAddedGroup.visibility = GONE
                    topUpModels.clear()
                    scheduleTimerGroup.visibility = GONE
                    scheduleText.text = getString(R.string.schedule_recharge)
                    clearView()
                    quickieNTolbarGroup.visibility = VISIBLE
                    customTextView46.text =
                        topUpModels.size.toString() + " " + getString(R.string.number_added)
                }
                TopUpType.PROCEED -> {
                    val list: ArrayList<TopUpModel> =
                        Gson().fromJson(data.getStringExtra("TopUpModelList"))
                    topUpModels.clear()
                    topUpModels.addAll(list)
                    sslCommerzIntegrationModel =
                        Gson().fromJson(data.getStringExtra("sslCommerzIntegrationModel"))
                    viewModel.apiPaymentStatus(
                        sslCommerzIntegrationModel.transaction_id,
                        this,
                        this
                    )
                }
            }

        }
    }

    private fun offerShowLayout(amountLocal: String) {
        offerRechargeGroup.visibility = GONE
        if (selectedNumberLayout.visibility == View.VISIBLE && amountLocal.length > 1) {
            myAmount = amountLocal.toInt()
            scheduleText.isEnabled = true
            quickieNTolbarGroup.makeGone()
            val isContains =
                topUpModels.any { it.phone == mobile_number && it.operator == operatorId && it.type == typeSelection }
            if (topUpModels.size <= 1 || isContains) {
                addAnotherNumber.isEnabled = true
                addAnotherNumber.setTextColor(resources.getColor(R.color.colorAccent))
            }
            proceedToRecharge.isEnabled = true
            scheduleText.setTextColor(resources.getColor(R.color.colorAccent))
            offerLiveData.value = popularPlans.firstOrNull {
                it.amount_max > amountLocal.toInt() && it.operator_name.contains(
                    operatorNameTv.text.toString(),
                    true
                ) && (when (typeSelection?.toLowerCase()) {
                    AppConstants.Operators.POSTPAID -> it.is_for_postpaid == 1
                    AppConstants.Operators.PREPAID -> it.is_for_prepaid == 1
                    AppConstants.Operators.SKITTO -> it.is_for_prepaid == 1
                    else -> true
                })
            }
        } else if (amountLocal.isEmpty()) {
            myAmount = 0
            scheduleText.setTextColor(resources.getColor(R.color.schedule_text_color))
        } else {
            myAmount = amountLocal.toInt()
        }
    }

    private fun myPlanFilter() {
        val operatorPlan = if (selectedNumberLayout.visibility == View.VISIBLE) {
            popularPlans.filter {
                it.operator_name.contains(
                    operatorName,
                    true
                ) && (when (typeSelection?.toLowerCase(Locale.ROOT)) {
                    AppConstants.Operators.POSTPAID -> it.is_for_postpaid == 1
                    AppConstants.Operators.PREPAID -> it.is_for_prepaid == 1
                    AppConstants.Operators.SKITTO -> it.is_for_prepaid == 1
                    else -> true
                }) && it.amount_max > myAmount
            }
        } else {
            popularPlans.filter { it.amount_max > myAmount }
        }
        val filteredItem = operatorPlan.filter { it.is_popular == 1 } as ArrayList<PopularPlan>

        if (filteredItem.size < 4) {
            filteredItem.addAll(operatorPlan.filter { it.is_popular == 0 }
                .sortedByDescending { it.usage_cnt }.toList())
        }
        popularPlansLiveData.value = if (filteredItem.size > 4) filteredItem.take(4)
            .sortedBy { it.priority }
            .toList() as ArrayList<PopularPlan> else filteredItem

    }

    override fun onClicked(operatorName: String, operatorType: String) {
        operators.dismiss()

        typeSelection = operatorType
        operatorNameTv.text = operatorName
        this.operatorName = operatorName
        type.text =
            if (typeSelection == AppConstants.Operators.SKITTO) typeSelection else typeSelection?.toUpperCase()

        myPlanFilter()

        operatorImage.operatorIcon(operatorName)
        operatorId = operatorName.operatorID(typeSelection ?: "")

        offerShowLayout(amount.text.toString().trim())
    }


    @ExperimentalStdlibApi
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        result.data?.let {
            when (key) {
                "get_plans" -> {
                    isOfferLoading=false
                    shimmer.stopShimmer()
                    shimmer.makeGone()
                    if (result.data.code == 200) {
                        popularPlans =
                            result.data.data.fromResponse<List<PopularPlan>>() as ArrayList<PopularPlan>
                        myPlanFilter()

                    } else {
                        showErrorToast(result.data.message)
                    }
                }
                "top_up" -> {
                    if (result.data.code == 200) {
                        val listType: Type =
                            object : TypeToken<SslCommerzIntegrationModel>() {}.type

                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<SslCommerzIntegrationModel> =
                            moshi.adapter(listType)

                        sslCommerzIntegrationModel =
                            adapter.fromJsonValue(result.data.data) as SslCommerzIntegrationModel
                        isInSdk = true
                        initSdk(this, sslCommerzIntegrationModel, getDataManager(), false)

                    } else {
                        topUpModels.removeLastOrNull()
                        showErrorToast(result.data.message)
                    }
                }
                "rating" -> {
                    finish()
                }
                "payment_status" -> {
                    if (result.data.code == 200) {
                        val paymentStatusResponse =
                            result.data.data.fromResponse<PaymentStatusResponse>()
                        val orderData: ArrayList<OrderData> =
                            Gson().fromJson(paymentStatusResponse?.orderData)
                        val holdData = orderData.firstOrNull { it.status == "hold" }
                        handleSuccessDialog(holdData)

                    } else {
                        topUpModels.removeLastOrNull()
                        showErrorToast(result.data.message)
                    }
                }
                "hold_user" -> {
                    if (result.data.code == 200) {
                        dialogFragment.showPrimaryLayout()
                    } else {
                        topUpModels.removeLastOrNull()
                        showErrorToast(result.data.message)
                    }
                }
            }
        }
    }

    @ExperimentalStdlibApi
    override fun onError(err: Throwable) {
        super.onError(err)
        topUpModels.removeLastOrNull()
    }

    override fun setTopUpOfferData(model: PopularPlan) {
        isShowOfferFilter = false
        isShowSpecialOffer = false
        amount.setText(model.amount_max.toString())
        operatorNameTv.text = model.operator_name.replace(" Bangladesh", "").replace(" Telecom", "")
        operatorImage.operatorIcon(operatorName)
        typeSelection?.let { operatorName.operatorID(it) }
    }


    @ExperimentalStdlibApi
    override fun transactionFail(p0: String?) {
        isInSdk = false
        topUpModels.removeLastOrNull()
        p0?.let { showToast(this, it) }

    }

    override fun closed(message: String?) {
        showToast(this, message ?: "ok")
    }

    @ExperimentalStdlibApi
    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            if (isInSdk) {
                isInSdk = false
                processLastItemInUi()
            }
        }, 1000)
    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        viewModel.apiPaymentStatus(sslCommerzIntegrationModel.transaction_id, this, this)
    }

    private fun handleSuccessDialog(holdData: OrderData?) {
        isInSdk = false
        val args = Bundle()
        args.putBoolean("isSuccess", true)
        args.putBoolean("recharge", true)
        args.putBoolean("favorite", true)
        args.putSerializable("numbers", topUpModels)
        args.putSerializable("favNumbers", getDataManager().mPref.prefGetFavoriteList())
        args.putString("title", getString(R.string.transaction_successful))
        holdData?.let {
            args.putString(
                "holdText", getString(
                    R.string.bonus_text,
                    it.bonusAmount.toString(),
                    (it.amount - it.bonusAmount).toString(),
                    it.amount.toString(),
                    it.phone
                )
            )
            args.putString("bonus", it.bonusAmount.toString())
        }
        dialogFragment.setArguments(args)
        dialogFragment.show(supportFragmentManager, "dialog_submit_confirmation")
        dialogFragment.setOkClick(
            object : TransactionSuccessfulDF.OkClickListener {
                override fun onOkClick(rating: String) {
                    dialogFragment.dismiss()
                    viewModel.apiRating(
                        rating,
                        sslCommerzIntegrationModel.transaction_id,
                        this@RechargeActivity,
                        this@RechargeActivity
                    )
                    //finish()
                }

            }
        )
        dialogFragment.setTryAgainClick(
            object : TransactionSuccessfulDF.TryAgainClickListener {
                override fun onTryAgainClick() {
                    dialogFragment.dismiss()
                    finish()
                }

            }
        )

        dialogFragment.setOnProceed(
            object : TransactionSuccessfulDF.ProceedListener {
                override fun onProceed(isChecked: Boolean) {
                    viewModel.apiInitiateHoldUser(
                        if (isChecked) "1" else "0",
                        sslCommerzIntegrationModel.transaction_id,
                        this@RechargeActivity,
                        this@RechargeActivity
                    )
                }

            }

        )

    }

    override fun onLoading(isLoader: Boolean) {
        if (!isOfferLoading) {
            super.onLoading(isLoader)
        }

    }

    @ExperimentalStdlibApi
    private fun processLastItemInUi() {
        val item = topUpModels.lastOrNull()
        item?.let {
            amount.setText(it.amount.toString())
            operatorId = it.operator
            operatorImage.operatorIconByID(operatorId)
            mobile_number = it.phone
            selected_mobile_number_tv.text = mobile_number
            operatorName = operatorId.operatorName()
            operatorNameTv.text = operatorName
            typeSelection = it.type
            type.text =
                if (typeSelection == AppConstants.Operators.SKITTO) typeSelection else typeSelection?.toUpperCase()
            myPlanFilter()
            topUpModels.removeLastOrNull()
            if (topUpModels.size > 0) {
                numberAddedGroup.visibility = VISIBLE
            } else {
                numberAddedGroup.visibility = GONE
            }
            if (topUpModels.size == 2) {
                addAnotherNumber.isEnabled = false
                addAnotherNumber.setTextColor(resources.getColor(R.color.schedule_text_color))
            } else {
                addAnotherNumber.isEnabled = true
                addAnotherNumber.setTextColor(resources.getColor(R.color.colorAccent))
            }
        }
        customTextView46.text =
            topUpModels.size.toString() + " " + getString(R.string.number_added)
    }
}