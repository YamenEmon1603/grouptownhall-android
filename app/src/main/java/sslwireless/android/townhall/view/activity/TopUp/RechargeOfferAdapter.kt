package sslwireless.android.townhall.view.activity.TopUp

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.databinding.CustomMostPopularOffersBinding
import sslwireless.android.townhall.view.utils.*

class RechargeOfferAdapter (
    private val mTaskInfo:
    ArrayList<PopularPlan>
):
    RecyclerView.Adapter<RechargeOfferAdapter.ViewHolder>() {
    private var clickListener: ClickListener? = null
    private var selectedPosition =-1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context)
            , R.layout.custom_most_popular_offers
            , parent, false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mTaskInfo[position]

        holder.binding.offerDetailsTv.text = model.description.trim()
        holder.binding.validityText.text = model.validity
        holder.binding.offerPriceTv.text = model.amount_max.toString()
        holder.binding.operatorImageIv.operatorIconByShotName(model.operator_short_name)
        if (model.ribbon==null || model.ribbon.isEmpty()) {
            holder.binding.ribbonGroup.makeInvisible()
        } else {
            holder.binding.ribbonText.text = model.ribbon
            holder.binding.ribbonGroup.makeVisible()
        }
        if (model.is_for_postpaid==1 && model.is_for_prepaid==0) {
            holder.binding.postPaidView.makeVisible()
        } else {
            holder.binding.postPaidView.makeGone()
        }

        holder.binding.rootLayout.isSelected = position ==selectedPosition
        holder.binding.rootLayout.setOnClickListener {
            selectedPosition = position
            notifyDataSetChanged()
            clickListener?.setTopUpOfferData(model)
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return mTaskInfo.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun setSelection(selectedPlan: PopularPlan?) {
        selectedPosition = try {
            mTaskInfo.indexOf(selectedPlan)
        } catch (e:Exception) {
            -1
        }
    }

    interface ClickListener {
        fun setTopUpOfferData(model: PopularPlan)
    }

    class ViewHolder(val binding: CustomMostPopularOffersBinding) : RecyclerView.ViewHolder(binding.root) {

    }
}