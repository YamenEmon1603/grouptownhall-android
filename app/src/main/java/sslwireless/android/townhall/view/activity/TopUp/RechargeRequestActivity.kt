package sslwireless.android.townhall.view.activity.TopUp

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_login_step_2.*
import kotlinx.android.synthetic.main.activity_recharge_request.*
import kotlinx.android.synthetic.main.activity_recharge_request.back
import kotlinx.android.synthetic.main.activity_recharge_request.proceed_btn
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.desco.TransactionSuccessfulDF
import sslwireless.android.townhall.view.activity.login.LoginStep1Activity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import sslwireless.android.townhall.view.utils.AppConstants.Operators.PREPAID

class RechargeRequestActivity : BaseActivity() {
    private lateinit var data: TopUpModel

    companion object {
        const val KEY_RECHARGE_REQUEST_DATA = "_recharge_request_data"
    }

    private val viewModel: RechargeViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(RechargeViewModel(getDataManager())))
            .get(RechargeViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recharge_request)
    }

    override fun viewRelatedTask() {
        data = intent.getSerializableExtra(KEY_RECHARGE_REQUEST_DATA) as TopUpModel
        if (getDataManager().mPref.prefGetLoginMode()) {
            try {
                val user = getDataManager().mPref.prefGetCurrentUser()

                if (!user.name.isNullOrEmpty()) {
                    nameET.setText(user.name)
                }

                if (user.mobile.isNotEmpty()) {
                    emailOrPhoneET.setText(user.mobile)
                } else if (user.email.isNotEmpty()) {
                    emailOrPhoneET.setText(user.email)
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }

        simTypeTV.text = data.type
        simTypeTV.isAllCaps = data.operator!=AppConstants.Operators.SKITTO_ID
        forNumberTV.text = data.phone
        amountTV.text = data.amount.toString()
        iconIV.operatorIcon(data.operator.operatorName())

        back.setOnClickListener {
            onBackPressed()
        }

        proceed_btn.setOnClickListener {
            submitRechargeRequest()
        }
    }

    private fun submitRechargeRequest() {
        val name = nameET.text.toString().trim()
        val requesterIdentifier = emailOrPhoneET.text.toString().trim()
        val amount = amountTV.text.toString().trim()
        val msisdn = forNumberTV.text.toString().trim()
        val operator = data.operator.operatorName()
        val operatorId = data.operator.toString()
        val connectionType = if (data.operator== AppConstants.Operators.SKITTO_ID) PREPAID else data.type
        val requestedIdentifier = toEmailOrPhoneET.text.toString().trim()

        var isFormValid = true
        if (name.isEmpty()) {
            isFormValid = false
        }

        if (requesterIdentifier.isEmpty()) {
            isFormValid = false
        }

        if (amount.isEmpty()) {
            isFormValid = false
        }

        if (msisdn.isEmpty()) {
            isFormValid = false
        }

        if (operator.isEmpty()) {
            isFormValid = false
        }

        if (operatorId.isEmpty()) {
            isFormValid = false
        }

        if (connectionType.isEmpty()) {
            isFormValid = false
        }

        if (requestedIdentifier.isEmpty()) {
            isFormValid = false
        }

        if (!requesterIdentifier.isEmailOrMobileValid()) {
            isFormValid = false
        }

        if (!requestedIdentifier.isEmailOrMobileValid()) {
            isFormValid = false
        }

        if (!isFormValid) {
            showToast(this, getString(R.string.invalid_input))
            return
        }

        viewModel.sendRechargeRequest(
            name = name,
            requested_identifier = requestedIdentifier,
            requester_identifier = requesterIdentifier,
            msisdn = msisdn,
            operator = operator,
            operator_id = operatorId,
            amount = amount,
            connection_type = connectionType,
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        result.data?.let {
            when (key) {
                RechargeViewModel.CALL_RECHARGE_REQUEST -> {
                    if (result.data.code == 200) {
                        handleSuccessDialog(result.data.message)
                    } else {
                        showErrorToast(result.data.message)
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        hideKeyboard()
        super.onBackPressed()
    }

    private val dialogFragment by lazy { TransactionSuccessfulDF() }
    private val topUpModels = ArrayList<TopUpModel>()
    private fun handleSuccessDialog(subtitle: String) {
        topUpModels.clear()
        val topUpModel =
            TopUpModel(
                data.amount,
                data.operator,
                data.phone!!,
                data.type.toLowerCase()
            )
        topUpModels.add(topUpModel)

        val args = Bundle()
        args.putBoolean("isSuccess", true)
        args.putBoolean("recharge", false)
        args.putBoolean("rechargeRequest", true)
        args.putBoolean("favorite", false)
        args.putSerializable("numbers", topUpModels)
        args.putSerializable("favNumbers", getDataManager().mPref.prefGetFavoriteList())
        args.putString("title", getString(R.string.request_successful))
        args.putString("message", subtitle)

        /* args.putString(
             "holdText", getString(
                 R.string.bonus_text,
                 it.bonusAmount.toString(),
                 (it.amount - it.bonusAmount).toString(),
                 it.amount.toString(),
                 it.phone
             )
         )
         args.putString("bonus", it.bonusAmount.toString())*/
        dialogFragment.setArguments(args)
        dialogFragment.show(supportFragmentManager, "dialog_submit_confirmation")
        dialogFragment.setOkClick(
            object : TransactionSuccessfulDF.OkClickListener {
                override fun onOkClick(rating: String) {
                    dialogFragment.dismiss()
                    if (getDataManager().mPref.prefGetLoginMode()) {
                        val intent = Intent(this@RechargeRequestActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                        finish()
                    } else {
                        val intent =
                            Intent(this@RechargeRequestActivity, LoginStep1Activity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent)
                        finish()
                    }

                }

            }
        )
        dialogFragment.setTryAgainClick(
            object : TransactionSuccessfulDF.TryAgainClickListener {
                override fun onTryAgainClick() {
                    dialogFragment.dismiss()
                    val intent = Intent(this@RechargeRequestActivity, RechargeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent)
                    finish()
                }

            }
        )

        dialogFragment.setOnProceed(
            object : TransactionSuccessfulDF.ProceedListener {
                override fun onProceed(isChecked: Boolean) {
                }

            }

        )

    }
}