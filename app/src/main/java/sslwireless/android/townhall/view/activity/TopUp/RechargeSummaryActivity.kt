package sslwireless.android.townhall.view.activity.TopUp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCCustomerInfoInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.adapter.PaymentSummaryAdapter
import kotlinx.android.synthetic.main.activity_recharge_summary.*
import kotlinx.android.synthetic.main.activity_recharge_summary.proceedToRecharge
import kotlinx.android.synthetic.main.layout_recharge.*
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet
import sslwireless.android.townhall.view.utils.*

class RechargeSummaryActivity : BaseActivity(),
    PaymentSummaryAdapter.SummaryListener,
    SessionLogoutBottomSheet.IBottomSheetDialogClicked, SSLCTransactionResponseListener {
    private var topUpModelList = ArrayList<TopUpModel>()
    private var sslCommerzIntegrationModel: SslCommerzIntegrationModel? = null
    private lateinit var scheduleString: String
    private val adapter: PaymentSummaryAdapter by lazy {
        PaymentSummaryAdapter(
            topUpModelList,
            this
        )
    }
    private val viewModel: RechargeViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(RechargeViewModel(getDataManager())))
            .get(RechargeViewModel::class.java)
    }

    private val sessionLogoutBottomSheet: SessionLogoutBottomSheet by lazy {
        SessionLogoutBottomSheet().apply {
            arguments = Bundle().apply {
                putString(
                    "title",
                    this@RechargeSummaryActivity.getString(R.string.do_you_want_to_clear_all)
                )
                putString(
                    "subText",
                    this@RechargeSummaryActivity.getString(R.string.number_phone_text)
                )
                putString("btn1", this@RechargeSummaryActivity.getString(R.string.yes))
                putString("btn2", this@RechargeSummaryActivity.getString(R.string.stay))
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        result.data?.let {
            if (result.data.code == 200) {
                when (key) {
                    "top_up" -> {
                        sslCommerzIntegrationModel = result.data.data.fromResponse()

                        val SSLCCustomerInfoInitializer =
                            SSLCCustomerInfoInitializer(
                                getDataManager().mPref.prefGetCurrentUser().name,
                                getDataManager().mPref.prefGetCurrentUser().email,
                                getDataManager().mPref.prefGetCurrentUser().address,
                                "",
                                "",
                                "Bangladesh",
                                getDataManager().mPref.prefGetCurrentUser().mobile
                            )

                        val sslCommerzInitialization = SSLCommerzInitialization(
                            sslCommerzIntegrationModel?.store_id,
                            sslCommerzIntegrationModel?.store_password,
                            sslCommerzIntegrationModel?.amount.toString().toDouble(),
                            SSLCCurrencyType.BDT,
                            sslCommerzIntegrationModel?.transaction_id,
                            "easy consumer",
                            if (BuildConfig.FLAVOR=="demo") SSLCSdkType.TESTBOX else SSLCSdkType.LIVE
                        )

                        sslCommerzInitialization.ipn_url = sslCommerzIntegrationModel?.ipn_url
                        val additionalInitializer = SSLCAdditionalInitializer()
                        additionalInitializer.valueB = sslCommerzIntegrationModel?.value_b
                        additionalInitializer.valueC = sslCommerzIntegrationModel?.value_c
                        IntegrateSSLCommerz
                            .getInstance(this)
                            .addSSLCommerzInitialization(sslCommerzInitialization)
                            .addCustomerInfoInitializer(SSLCCustomerInfoInitializer)
                            .addAdditionalInitializer(additionalInitializer)
                            .buildApiCall(this);
                    }
                }

            } else {
                showErrorToast(result.data.message)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recharge_summary)
    }

    override fun viewRelatedTask() {
        val topModels = intent.extras?.getString("topModels")
        scheduleString = intent.extras?.getString("scheduleText", "None") ?: "None"
        topModels?.let {
            topUpModelList = Gson().fromJson(it)
        }

        summaryRecycler.adapter = adapter
        conditionCheck()

        addAnotherNumberTV.setOnClickListener {
            onAddNewView()
        }

        imageView3.setOnClickListener {
            onBackPressed()
        }

        customTextView44.setOnClickListener {
            sessionLogoutBottomSheet.show(supportFragmentManager, "1234")
            sessionLogoutBottomSheet.setBottomDialogListener(this)
        }

        proceedToRecharge.setOnClickListener {
            initSDK()
        }

        requestRechargeFullTV.setOnClickListener {
            gotoRequestRechargeScreen()
        }

        requestRechargeHalfTV.setOnClickListener {
            gotoRequestRechargeScreen()
        }
    }

    private fun gotoRequestRechargeScreen() {
        if (topUpModelList.isEmpty()) return

        val intent = Intent(this, RechargeRequestActivity::class.java)
        intent.putExtra(
            RechargeRequestActivity.KEY_RECHARGE_REQUEST_DATA,
            topUpModelList[0]
        )
        startActivity(intent)
    }

    private fun initSDK() {
        val topUpRequestModel = TopUpRequestModel(
            topUpModelList,
            if (scheduleGroup.visibility == GONE) "" else scheduleTime.text.toString()
                .dateChangeTo("dd-MMM-yyyy | hh:mm aa", "dd-MM-yyyy HH:mm"),
            getDataManager().mPref.prefGetCustomerToken()!!
        )
        viewModel.apiTopUpInit(topUpRequestModel, this, this)
    }

    private fun onAddNewView() {
        if (topUpModelList.size < 3) {
            val intent = Intent().apply {
                putExtra("TopUpType", TopUpType.ADD_NEW)
                putExtra("TopUpModelList", Gson().toJson(topUpModelList))
            }
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun onClearView() {
        val intent = Intent().apply {
            putExtra("TopUpType", TopUpType.CLEAR_ALL)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onBackPressed() {
        val intent = Intent().apply {
            putExtra("TopUpModelList", Gson().toJson(topUpModelList))
        }
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun conditionCheck() {
        if (topUpModelList.size == 3) {
            addAnotherNumberTV.setTextColor(ContextCompat.getColor(this, R.color.grey))
        } else {
            addAnotherNumberTV.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
        }

        if (scheduleString == "None") {
            scheduleGroup.visibility = GONE
            if (topUpModelList.size == 1) {
                requestRechargeFullGroup.visibility = VISIBLE
            } else {
                requestRechargeFullGroup.visibility = GONE
            }
        } else {
            scheduleGroup.visibility = VISIBLE

            if (topUpModelList.size == 1) {
                requestRechargeHalfTV.isEnabled = true
                requestRechargeHalfTV.text = getString(R.string.request_recharge)
            } else {
                requestRechargeHalfTV.isEnabled = false
                requestRechargeHalfTV.text = ""
            }

            requestRechargeFullGroup.visibility = GONE
            scheduleTime.text = scheduleString
        }
    }

    override fun onItemDelete(position: Int) {
        topUpModelList.removeAt(position)
        adapter.notifyDataSetChanged()
        conditionCheck()
        if (topUpModelList.isEmpty()) {
            onClearView()
        }
    }

    override fun onCancelButtonClicked() {
        onClearView()
    }

    override fun onSessionLogoutButtonClicked(tag: String) {

    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        val intent = Intent().apply {
            putExtra("TopUpType", TopUpType.PROCEED)
            putExtra("TopUpModelList", Gson().toJson(topUpModelList))
            putExtra("sslCommerzIntegrationModel", Gson().toJson(sslCommerzIntegrationModel))
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun transactionFail(p0: String?) {

    }

    override fun closed(message: String?) {

    }
}
