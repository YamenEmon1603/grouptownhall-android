package sslwireless.android.townhall.view.activity.TopUp

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class RechargeViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun recentTransactionService(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.recentTransactionService(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "recent")),
            dataManager.mPref.prefGetCustomerToken() ?: ""
        )
    }

    fun apiGetPlans(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetPlans(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "get_plans"))
        )
    }

    fun apiGetFavContact(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetFavContact(
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "getFav"))
        )
    }

    fun apiTopUpInit(
        topUpRequestModel: TopUpRequestModel,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiTopUpInit(
            topUpRequestModel,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "top_up"))
        )
    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating, transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }

    fun apiInitiateHoldUser(
        allow_offer: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiInitiateHoldUser(
            allow_offer, transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "hold_user"))
        )
    }

    fun apiPaymentStatus(
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiPaymentStatus(
            transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "payment_status"))
        )
    }

    fun sendRechargeRequest(
        name: String,
        requester_identifier: String,
        amount: String,
        msisdn: String,
        operator: String,
        operator_id: String,
        connection_type: String,
        requested_identifier: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequest(
            name = name,
            requester_identifier = requester_identifier,
            amount = amount,
            msisdn = msisdn,
            operator = operator,
            operator_id = operator_id,
            connection_type = connection_type,
            requested_identifier = requested_identifier,
            apiCallbackHelper = ApiCallbackHelper(
                livedata(
                    lifecycleOwner,
                    iObserverCallBack,
                    CALL_RECHARGE_REQUEST
                )
            )
        )
    }

    companion object {
        const val CALL_RECHARGE_REQUEST = "_recharge_request"
    }
}