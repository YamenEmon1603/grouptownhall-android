package sslwireless.android.townhall.view.activity.TopUp

enum class TopUpType {
    ADD_NEW, CLEAR_ALL, PROCEED
}