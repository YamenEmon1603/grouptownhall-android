package sslwireless.android.townhall.view.activity.UpdatePhone

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.LocalValidator
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.DeviceKey
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.otp.OtpActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_update_number.*

class UpdateNumberActivity : BaseActivity() {

    private lateinit var mKey: String
    private lateinit var deviceKey: DeviceKey
    var viewModel: UpdatePhoneViewModel? = null
    lateinit var email: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_number)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(UpdatePhoneViewModel(getDataManager()))
            )
                .get(UpdatePhoneViewModel::class.java)


        email = intent.getStringExtra("email")?:""
        mKey = intent.getStringExtra("mKey")?:""
        deviceKey = getDataManager().mPref.prefGetDeviceKey()!!
    }

    override fun viewRelatedTask() {
        proceed_btn.setOnClickListener {

            if (!LocalValidator.isPhoneValid(phone_number_et.text.toString())) {
                phone_number_et.error = "Enter a valid mobile number"
                return@setOnClickListener
            }
            viewModel!!.apiUpdatePhone(
                email,
                phone_number_et.text.toString(),
                mKey,
                this,
                this
            )
        }
        back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        showToast(this, result.data?.message?:getString(R.string.something_went_wrong))
        if (result.data!!.ui.equals("otp")) {
            val intent = Intent(this, OtpActivity::class.java)
            intent.putExtra("mKey", mKey)
            intent.putExtra("email", email)
            intent.putExtra("phone", phone_number_et.text.toString())
            startActivity(intent)
        }
    }

}