package sslwireless.android.townhall.view.activity.UpdatePhone

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class UpdatePhoneViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun apiUpdatePhone(
        email: String,
        mobile: String,
        mkey: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUpdatePhone(
            email,
            mobile,
            mkey,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "old-user-otp-for-mobile-restore"))
        )
    }
}