package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.utility.DataItem
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import sslwireless.android.townhall.data.model.utility.UtilityModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.otherServices.OtherServicesViewModel
import sslwireless.android.townhall.view.activity.otherServices.dynamic_services_form_box.DynamicCommonApiActivity
import sslwireless.android.townhall.view.adapter.UtilityBillsListAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.custom.GridItemDecoration
import kotlinx.android.synthetic.main.activity_utility_bills.*

class UtilityBillsActivity : BaseActivity(), UtilityBillsListAdapter.ItemListener {
    private lateinit var viewModel: OtherServicesViewModel
    private lateinit var adapter: UtilityBillsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_utility_bills)

        initUtilityList()

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(OtherServicesViewModel(getDataManager()))
            ).get(OtherServicesViewModel::class.java)


        viewModel.utilityService(this, this)
    }

    private fun initUtilityList() {
        adapter = UtilityBillsListAdapter(this,getDataManager().mPref.prefGetLanguage())
        utilityRecycler.layoutManager = GridLayoutManager(this, 2)
        utilityRecycler.addItemDecoration(
            GridItemDecoration(
                this, GridItemDecoration.ALL
            )
        )
        utilityRecycler.adapter = adapter
    }

    override fun viewRelatedTask() {
        backIconImage.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            "utilityService" -> {
                val type = object : TypeToken<UtilityModel>() {}.type

                val baseModel = Gson().fromJson<UtilityModel>(
                    Gson().toJson(result.data),
                    UtilityModel::class.java
                )

                when (baseModel.code) {
                    200 -> {
                        showUtilityList(baseModel.data)
                    }
                    422 -> {
                        showToast(this, baseModel.message.toString())
                    }
                    401 -> {
                        showToast(this, baseModel.message.toString())

                        getDataManager().mPref.prefLogout(this)
                    }
                }
            }
        }
    }

    private fun showUtilityList(data: List<DataItem?>?) {
        if (data == null || data.isEmpty()) {
            showToast(this, getString(R.string.something_went_wrong))
            return
        }

        adapter.list = data[0]?.serviceLists
        adapter.notifyDataSetChanged()
    }

    override fun itemClicked(item: ServiceListsItem) {
        val intent = Intent(this, DynamicCommonApiActivity::class.java)
        intent.putExtra("modelData", item)
        startActivity(intent)
    }
}