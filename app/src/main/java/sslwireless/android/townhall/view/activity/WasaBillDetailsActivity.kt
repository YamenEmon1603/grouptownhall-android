package sslwireless.android.townhall.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.sslwireless.crm.view.adapter.SpinnerAdapterSource
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.databinding.ActivityWasaBillDetailsBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity

class WasaBillDetailsActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    lateinit var adapterSource: SpinnerAdapterSource
    lateinit var binding: ActivityWasaBillDetailsBinding
    var dates = ArrayList<String>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_wasa_bill_details)
    }

    override fun viewRelatedTask() {
        dates.add("15.July")
        dates.add("15.July")
        dates.add("15.July")
        dates.add("15.July")

        adapterSource = SpinnerAdapterSource(applicationContext,dates)
        binding.codeSpinner.adapter = adapterSource.getAdapter()



    }
}
