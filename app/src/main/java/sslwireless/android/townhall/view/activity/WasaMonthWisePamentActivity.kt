package sslwireless.android.townhall.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import sslwireless.android.townhall.view.adapter.WasaMonthWiseBillAdapter
import sslwireless.android.townhall.view.adapter.WasaOffersAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_wasa_month_wise_pament.*

class WasaMonthWisePamentActivity : BaseActivity(), IAdapterCallback {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    var offers = ArrayList<Int>()
    var bills = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wasa_month_wise_pament)
    }

    override fun viewRelatedTask() {

        rvOffers.layoutManager = GridLayoutManager(this,1)
        rvMonthWisePament.layoutManager = GridLayoutManager(this,1)
        offers.add(R.drawable.ic_offers2)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)

        bills.add("")
        bills.add("")
        bills.add("")
        bills.add("")

        rvOffers.adapter = WasaOffersAdapter(offers).setCallback(this)
        val adapter = WasaMonthWiseBillAdapter(bills)
        adapter.setCallBack(this)
        rvMonthWisePament.adapter = adapter
    }


    public fun goWasaBillDetailsActivity(view: View){
        var intent = Intent(this@WasaMonthWisePamentActivity, WasaBillDetailsActivity::class.java)
        startActivity(intent)
    }



    override fun <T> clickListener(position: Int, model: T, view: View) {
        goWasaBillDetailsActivity(view)

    }

    override fun onRemoved(position: Int) {
    }


}
