package sslwireless.android.townhall.view.activity.bill_alert

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.databinding.ActivityBillingAlertsBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.activity.bill_alert.adapter.DescoBillAlertAdapter
import sslwireless.android.townhall.view.activity.bill_alert.adapter.WasaBillAlertAdapter
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import kotlinx.android.synthetic.main.activity_recurring_payments.*

class BillingAlertsActivity : BaseActivity(), IAdapterCallback {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    lateinit var binding: ActivityBillingAlertsBinding
    var accounts = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_billing_alerts)
    }

    override fun viewRelatedTask() {

        rvDescoBill.layoutManager = GridLayoutManager(this,1)
        rvWasaBill.layoutManager = GridLayoutManager(this,1)
        accounts.add("")
        accounts.add("")
        accounts.add("")

        rvDescoBill.adapter = DescoBillAlertAdapter(accounts).setCallback(this)
        rvWasaBill.adapter = WasaBillAlertAdapter(accounts).setCallback(this)

    }

    override fun <T> clickListener(position: Int, model: T, view: View) {

    }

    override fun onRemoved(position: Int) {
    }
}
