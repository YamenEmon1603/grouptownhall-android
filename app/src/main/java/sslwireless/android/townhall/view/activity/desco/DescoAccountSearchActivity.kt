package sslwireless.android.townhall.view.activity.desco

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.activity.desco.adapters.DescoAccountSearchAdapter
import sslwireless.android.townhall.view.activity.desco.descoBillPayment.DescoBillPamentActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import kotlinx.android.synthetic.main.activity_desco_account_search.*

class DescoAccountSearchActivity : BaseActivity(),IAdapterCallback {

    var accounts = ArrayList<String>()
    var is_desco_prepaid: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desco_account_search)

        if (intent.hasExtra(Enums.parameter.is_prepaid.toString())) {
            is_desco_prepaid = intent.getBooleanExtra(Enums.parameter.is_prepaid.toString(), false)
        }
    }

    override fun viewRelatedTask() {

        accounts.add("")
        accounts.add("")
        accounts.add("")
        accounts.add("")
        accounts.add("")
        rvAccountSearch.layoutManager = GridLayoutManager(this, 1)
        rvAccountSearch.adapter = DescoAccountSearchAdapter(accounts).setCallBack(this)

        etAccountNumber.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (etAccountNumber.length() > 2) {
                    var intent =
                        Intent(this@DescoAccountSearchActivity, DescoBillPamentActivity::class.java)
                    intent.putExtra(Enums.parameter.is_prepaid.toString(), is_desco_prepaid)
                    startActivity(intent)
                } else {
                    Toast.makeText(this@DescoAccountSearchActivity,"Enter Account Number",Toast.LENGTH_SHORT).show()
                }
                return@setOnEditorActionListener true
            }
            false
        }

        proceed_icon_iv.setOnClickListener {
            if (etAccountNumber.length() > 2) {
                var intent =
                    Intent(this@DescoAccountSearchActivity, DescoBillPamentActivity::class.java)
                intent.putExtra(Enums.parameter.is_prepaid.toString(), is_desco_prepaid)
                intent.putExtra("account_no", etAccountNumber.text.toString())
                startActivity(intent)
            } else {
                Toast.makeText(this@DescoAccountSearchActivity,"Enter Account Number",Toast.LENGTH_SHORT).show()
            }
        }

        etAccountNumber.requestFocus()
        etAccountNumber.isCursorVisible = true
        etAccountNumber.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    override fun <T> clickListener(position: Int, model: T, view: View) {

    }

    override fun onRemoved(position: Int) {
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}
