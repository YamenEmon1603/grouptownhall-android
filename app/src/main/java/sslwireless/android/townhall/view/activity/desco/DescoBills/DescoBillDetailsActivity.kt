package sslwireless.android.townhall.view.activity.desco.DescoBills

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.descoModels.DescoBillItem
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountViewModel
import sslwireless.android.townhall.view.activity.desco.adapters.DescoMonthWiseBillAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_desco_bill_details.*

class DescoBillDetailsActivity : BaseActivity() {

    private lateinit var viewModel: BillingAccountViewModel
    private var position: Int = 0
    private lateinit var dueBillItems: ArrayList<DescoBillItem>
    private var dueBillItemsBillingDates: ArrayList<String> = ArrayList()

    override fun viewRelatedTask() {

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("Not yet implemented")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desco_bill_details)



        dueBillItems = intent.getSerializableExtra("bills") as ArrayList<DescoBillItem>
        position = intent.getIntExtra("position",0)

        dueBillItems.forEach {
            dueBillItemsBillingDates.add(DescoMonthWiseBillAdapter.getBillingMonth(it.bill_number))
        }

        val adapter = ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,dueBillItemsBillingDates)
        bill_spinner.adapter = adapter



        bill_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                loadBillDetails(dueBillItems[p2])
            }

        }


        bill_spinner.setSelection(position)

        imageView7.setOnClickListener {
            onBackPressed()
        }

    }

    private fun loadBillDetails(descoBillItem: DescoBillItem) {
        bill_no_title_tv.text = "Electricity Bill No - ${descoBillItem.bill_number}"
        account_no_tv.text = descoBillItem.account_number
        bill_no_tv.text = descoBillItem.bill_number
        billing_amount_tv.text = "${descoBillItem.bill_amount} TK"
        vat_amount_tv.text = "${descoBillItem.vat_amount} TK"
        lpc_amount_tv.text = "${descoBillItem.lpc_amount} TK"
        total_payable_bill_tv.text = "${descoBillItem.total_amount} TK"
    }


}
