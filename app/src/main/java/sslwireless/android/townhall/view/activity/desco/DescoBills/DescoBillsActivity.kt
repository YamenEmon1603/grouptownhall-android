package sslwireless.android.townhall.view.activity.desco.DescoBills

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.descoModels.BillingAccount
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountList.BillingAccountListActivity
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountViewModel
import sslwireless.android.townhall.view.activity.desco.DescoAccountSearchActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.activity.desco.adapters.SavedAccountsAdapter
import sslwireless.android.townhall.view.activity.desco.descoBillPayment.DescoBillPamentActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_desco_bills.*
import java.lang.reflect.Type

class DescoBillsActivity : BaseActivity(), IAdapterCallback {


    private lateinit var viewModel: BillingAccountViewModel
    //lateinit var binding: ActivityDescoBillsBinding
    var offers = ArrayList<Int>()

    var billingAccounts = ArrayList<BillingAccount>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desco_bills)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(BillingAccountViewModel(getDataManager()))
            ).get(BillingAccountViewModel::class.java)

        viewModel.apiGetDescoAccounts(getDataManager().mPref.prefGetCustomerToken()!!,
            null,
            null,
            null,
            null,
            this,
            this
        )

    }

    override fun viewRelatedTask() {

        saved_accounts_rv.layoutManager = LinearLayoutManager(this)
        rvOffers.layoutManager = GridLayoutManager(this, 1)

        offers.add(R.drawable.ic_offers2)
        offers.add(R.drawable.ic_offers1)

        tvDescoAccountNumber.setOnClickListener() {
            var intent = Intent(this@DescoBillsActivity, DescoAccountSearchActivity::class.java)
            if (radio_prepaid.isChecked) {
                intent.putExtra(Enums.parameter.is_prepaid.toString(), true)
            } else {
                intent.putExtra(Enums.parameter.is_prepaid.toString(), false)
            }

            startActivity(intent)
        }

        see_all_tv.setOnClickListener {
            startActivity(Intent(this,
                BillingAccountListActivity::class.java).putExtra(
                BillingAccountActivity.WASA_OR_DESCO,
                BillingAccountActivity.DESCO
            ))
        }

    }

    override fun <T> clickListener(position: Int, model: T, view: View) {
        var intent =
            Intent(this, DescoBillPamentActivity::class.java)
        intent.putExtra(Enums.parameter.is_prepaid.toString(), false)
        intent.putExtra("account_no", billingAccounts[position].account_no)
        startActivity(intent)
    }

    override fun onRemoved(position: Int) {
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if(result.data!!.code == 200){
            val rawResponse = result.data!!.data!!.toString()

            Log.e("RawResponse:", "$key ----> $rawResponse")
            when(key){
                "apiGetDescoAccounts"->{

                    val listType: Type = object : TypeToken<List<BillingAccount>>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<List<BillingAccount>> = moshi.adapter(listType)

                    billingAccounts = adapter.fromJsonValue(result.data!!.data) as ArrayList<BillingAccount>

                    saved_accounts_rv.adapter = SavedAccountsAdapter(billingAccounts).setCallback(this)
                }
            }
        }else{
            showToast(this, result.data.message)
        }
    }




}
