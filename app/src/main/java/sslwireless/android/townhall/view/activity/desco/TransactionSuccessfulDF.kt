package sslwireless.android.townhall.view.activity.desco

import android.content.Intent
import android.graphics.Color
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.databinding.DialogDescoTransactionSuccessfulBinding
import sslwireless.android.townhall.view.activity.favorites.AddToFavListActivity
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible
import sslwireless.android.townhall.view.utils.setColor


class TransactionSuccessfulDF : DialogFragment() {
    internal var isSuccess: Boolean = false
    internal var isFavorite: Boolean = false
    internal var recharge: Boolean = false
    internal var rechargeRequest: Boolean = false
    internal var textDialogTitle: String? = null
    internal var textMessage: String? = null
    internal var holdText: String? = null
    internal lateinit var mOkClickListener: OkClickListener
    internal lateinit var mTryAgainlickListener: TryAgainClickListener
    internal lateinit var mProceedListener: ProceedListener
    private var topUpModels = ArrayList<TopUpModel>()
    private var favList = ArrayList<FavoriteModel>()
    private lateinit var binding: DialogDescoTransactionSuccessfulBinding


    override fun onResume() {
        super.onResume()

        val window = dialog?.window
        val size = Point()

        val display = window!!.windowManager.defaultDisplay
        display.getSize(size)

        val width = size.x
        val height = size.y

        window.setLayout((width * 0.75).toInt(), (height * 0.9).toInt())
        window.setGravity(Gravity.CENTER)
    }

    private fun processData() {
        if (requireArguments().containsKey("isSuccess")) {
            isSuccess = requireArguments().getBoolean("isSuccess")
        }
        if (requireArguments().containsKey("favorite")) {
            isFavorite = requireArguments().getBoolean("favorite")
        }
        if (requireArguments().containsKey("title")) {
            textDialogTitle = requireArguments().getString("title")
        }
        if (requireArguments().containsKey("message")) {
            textMessage = requireArguments().getString("message")
        }

        if (requireArguments().containsKey("holdText")) {
            holdText = requireArguments().getString("holdText")
            binding.ratingLayout.makeGone()
            binding.primaryLayout.makeGone()
            binding.secondaryLayout.makeVisible()
            binding.checkbox.text = holdText
        }

        if (requireArguments().containsKey("bonus")) {
            val bonusText = requireArguments().getString("bonus")
            binding.textSubtitle.text = getString(R.string.earn_bonus_tk, bonusText)
            binding.textSubtitle.setColor(R.color.colorAccent)
        }

        if (requireArguments().containsKey("recharge")) {
            recharge = requireArguments().getBoolean("recharge")

            if (requireArguments().containsKey("numbers")) {
                topUpModels =
                    requireArguments().getSerializable("numbers") as ArrayList<TopUpModel>

                favList =
                    requireArguments().getSerializable("favNumbers") as ArrayList<FavoriteModel>
            }
        }
        if (requireArguments().containsKey("rechargeRequest")) {
            rechargeRequest = requireArguments().getBoolean("rechargeRequest")
            binding.ratingLayout.makeGone()
            binding.layoutOk.makeGone()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_desco_transaction_successful,
            container,
            false
        )
        processData()
        //getDialog().getWindow()!!.requestFeature(Window.FEATURE_NO_TITLE)

        if (dialog != null && dialog?.window != null) {
            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog?.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }

        binding.lottieAnim.setAnimation("new_success_lottie.json")
        binding.lottieAnim.playAnimation()
        binding.lottieAnim.speed = 0.5F

        if (recharge) {
            binding.textMessage.text = getString(R.string.done)
            binding.layoutOk.text = getString(R.string.add_to_favorite)
            if (topUpModels.size > 1) {
                binding.rechargeLay.visibility = View.VISIBLE
                binding.textSubtitle.text =
                    getString(R.string.following_phone_numbers_recharged_successfully)
            } else {
                binding.rechargeLay.visibility = View.GONE
                binding.textSubtitle.text =
                    getString(R.string.successfully_recharged_amp_add_number_to_favorite_contacts)
            }
            if (isFavorite) {
                binding.layoutOk.makeVisible()
            } else {
                binding.layoutOk.makeGone()
            }

            topUpModels.forEach {
                val itemLayout = inflater.inflate(R.layout.transaction_number_layout, null, true)
                val numberText: TextView = itemLayout.findViewById(R.id.recharged_number_tv)
                val amountText: TextView = itemLayout.findViewById(R.id.recharged_amount_tv)
                numberText.text = it.phone
                amountText.text = "${it.amount} TK"
                binding.rechargeLay.addView(itemLayout)
            }
        } else if (rechargeRequest) {
            binding.textMessage.text = getString(R.string.done)
            binding.textSubtitle.text = textMessage
        }

        binding.textDialogTitle.text = textDialogTitle
        //textViewMessage.setText(textMessage)

        binding.layoutTryAgain.setOnClickListener {
            mOkClickListener.onOkClick(binding.ratingBar.rating.toInt().toString())
        }
        binding.layoutOk.setOnClickListener {
            //  mOkClickListener.onOkClick()
            if (recharge) {
                requireContext().startActivity(
                    Intent(
                        context,
                        AddToFavListActivity::class.java
                    ).apply {
                        putExtra("top_up_models", topUpModels)
                        putStringArrayListExtra("favList",
                            favList.map { it.phone } as java.util.ArrayList<String>?)
                    }
                )
                requireActivity().finish()
                dismiss()
            } else if (rechargeRequest) {
                mTryAgainlickListener.onTryAgainClick()
            }
        }

        binding.secondaryLayout.setOnClickListener {
            binding.proceedBtn.text = ""
            binding.proceedBtn.isClickable = false
            binding.progressBar.makeVisible()
            mProceedListener.onProceed(binding.checkbox.isChecked)
        }

        return binding.root
    }

    fun showPrimaryLayout() {
        binding.secondaryLayout.makeGone()
        binding.ratingLayout.makeVisible()
        binding.primaryLayout.makeVisible()
        binding.textSubtitle.text =
            getString(R.string.successfully_recharged_amp_add_number_to_favorite_contacts)
        binding.textSubtitle.setColor(R.color.get_started_text)
    }

    fun setTryAgainClick(dialogResult: TryAgainClickListener) {
        mTryAgainlickListener = dialogResult
    }

    fun setOkClick(dialogResult: OkClickListener) {
        mOkClickListener = dialogResult
    }

    fun setOnProceed(dialogResult: ProceedListener) {
        mProceedListener = dialogResult
    }

    override fun onStart() {
        super.onStart()
        val dialog = getDialog()
        if (dialog != null) {
            dialog!!.getWindow()!!.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )

            dialog!!.setCancelable(false)

        }
    }

    public interface TryAgainClickListener {
        fun onTryAgainClick()
    }

    public interface OkClickListener {
        fun onOkClick(rating: String)
    }

    public interface ProceedListener {
        fun onProceed(isChecked: Boolean)
    }

}

