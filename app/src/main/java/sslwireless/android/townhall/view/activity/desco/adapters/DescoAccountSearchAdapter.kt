package sslwireless.android.townhall.view.activity.desco.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.adapter.BaseViewHolder
import sslwireless.android.townhall.view.adapter.IAdapterCallback

class DescoAccountSearchAdapter (accounts: ArrayList<String>) : RecyclerView.Adapter<BaseViewHolder>() {

    lateinit var iAdapterListener: IAdapterCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_desco_account_search, parent, false
            ))
    }

    var accounts = accounts




    override fun getItemCount(): Int {
        return accounts.size
    }

    public fun setCallBack(iAdapterListener: IAdapterCallback):DescoAccountSearchAdapter{
        this.iAdapterListener = iAdapterListener
        return this

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, accounts.get(position),iAdapterListener)

        holder as ViewHolder

        holder.itemView.setOnClickListener {
            iAdapterListener.clickListener(position,accounts.get(position),holder.itemView)

        }
    }



    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {



        override fun<T> onBind(position: Int, model:T, iAdapterListener: IAdapterCallback) {

            model as String


        }


    }
}