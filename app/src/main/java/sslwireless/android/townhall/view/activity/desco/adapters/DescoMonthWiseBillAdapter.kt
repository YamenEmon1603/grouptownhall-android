package sslwireless.android.townhall.view.activity.desco.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.descoModels.DescoBillItem
import sslwireless.android.townhall.databinding.ItemDescoMonthWiseBillBinding
import sslwireless.android.townhall.view.adapter.BaseViewHolder
import sslwireless.android.townhall.view.adapter.IAdapterCallback

class DescoMonthWiseBillAdapter (bills: ArrayList<DescoBillItem>) : RecyclerView.Adapter<BaseViewHolder>() {

    var bills = bills
    lateinit var iAdapterListener: IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_desco_month_wise_bill, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return bills.size
    }

    public fun setCallBack(iAdapterListener: IAdapterCallback){
        this.iAdapterListener = iAdapterListener

    }


    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, bills.get(position),iAdapterListener)
        holder as ViewHolder

        holder.binding.billingMonthTv.text = getBillingMonth(bills[position].bill_number)
        holder.binding.billAmountTv.text = "${bills[position].total_amount.toDouble()} TK"
        holder.binding.billingNoTv.text = "Bill No. - ${bills[position].bill_number}"

        holder.binding.selectBillCv.isChecked = bills[position].is_selected

        holder.binding.billAmountTv.setOnClickListener{
            iAdapterListener.clickListener(position,bills.get(position),holder.binding.billAmountTv)
        }
        holder.binding.billDetailsTv.setOnClickListener{
            iAdapterListener.clickListener(position,bills.get(position),holder.binding.billDetailsTv)
        }
        holder.binding.ivWasaBillNextStep.setOnClickListener{
            iAdapterListener.clickListener(position,bills.get(position),holder.binding.ivWasaBillNextStep)
        }
        holder.binding.selectBillCv.setOnClickListener {

            bills.get(position).is_selected = !bills.get(position).is_selected
            iAdapterListener.clickListener(position,bills.get(position),holder.binding.selectBillCv)
        }


    }


    companion object{
        fun getBillingMonth(billNumber: String): String {
            when (billNumber.substring(0, 2)) {
                "01" -> {
                    return "JANUARY'${billNumber.substring(2,4)}"
                }
                "02" -> {
                    return "FEBRUARY'${billNumber.substring(2,4)}"
                }
                "03" -> {
                    return "MARCH'${billNumber.substring(2,4)}"
                }
                "04" -> {
                    return "APRIL'${billNumber.substring(2,4)}"
                }
                "05" -> {
                    return "MAY'${billNumber.substring(2,4)}"
                }
                "06" -> {
                    return "JUNE'${billNumber.substring(2,4)}"
                }
                "07" -> {
                    return "JULY'${billNumber.substring(2,4)}"
                }
                "08" -> {
                    return "AUGUST'${billNumber.substring(2,4)}"
                }
                "09" -> {
                    return "SEPTEMBER'${billNumber.substring(2,4)}"
                }
                "10" -> {
                    return "OCTOBER'${billNumber.substring(2,4)}"
                }
                "11" -> {
                    return "NOVEMBER'${billNumber.substring(2,4)}"
                }
                else -> {
                    return "DECEMBER'${billNumber.substring(2,4)}"
                }
            }
        }
    }


    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        var binding = itemView  as ItemDescoMonthWiseBillBinding

        override fun<T> onBind(position: Int, model:T, iAdapterListener: IAdapterCallback) {
            model as DescoBillItem

        }


    }

}
