package sslwireless.android.townhall.view.activity.desco.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.adapter.BaseViewHolder
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import kotlinx.android.synthetic.main.item_desco_offer.view.*

class DescoOffersAdapter (offers: ArrayList<Int>) : RecyclerView.Adapter<BaseViewHolder>() {

    var offers = offers
    lateinit var mCallback: IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_desco_offer, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return offers.size
    }
    fun setCallback(mCallback: IAdapterCallback):DescoOffersAdapter{
        this.mCallback = mCallback
        return this
    }


    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind<Int>(position, offers.get(position), mCallback)

    }



    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {


        override fun<T> onBind(position: Int, model : T, mCallback : IAdapterCallback) {

            model as Int
            itemView.ivOffer.setImageResource(model)
        }


    }

}
