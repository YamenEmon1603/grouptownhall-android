package sslwireless.android.townhall.view.activity.desco.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.descoModels.BillingAccount
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA
import sslwireless.android.townhall.view.adapter.BaseViewHolder
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import kotlinx.android.synthetic.main.item_saved_billing_account.view.*

class SavedAccountsAdapter(accounts: ArrayList<BillingAccount>) : RecyclerView.Adapter<BaseViewHolder>() {

    var accounts = accounts
    lateinit var iAdapterCallback: IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_saved_billing_account, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return accounts.size
    }

    public fun setCallback(iAdapterCallback: IAdapterCallback):SavedAccountsAdapter{
        this.iAdapterCallback = iAdapterCallback
        return this
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, accounts.get(position),iAdapterCallback)


    }

    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        override fun <T> onBind(position: Int, model: T, iAdapterCallback: IAdapterCallback) {

            model as BillingAccount

            itemView.saved_account_number_tv.text  = model.account_no

            when (model.type) {
                DESCO -> {
                    itemView.saved_account_image_iv.setImageResource(R.drawable.ic_desco_bill)
                    itemView.saved_account_type_tv.text = model.account_type
                    itemView.saved_account_type_tv.visibility = View.VISIBLE
                }
                WASA -> {
                    itemView.saved_account_image_iv.setImageResource(R.drawable.ic_wasa_drop)
                    itemView.saved_account_type_tv.visibility = View.GONE
                }
            }


            itemView.rootView.setOnClickListener {
                iAdapterCallback.clickListener(position,model,itemView.rootView)
            }

        }
    }
}
