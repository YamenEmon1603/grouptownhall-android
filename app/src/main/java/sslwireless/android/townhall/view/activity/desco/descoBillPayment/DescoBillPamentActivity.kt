package sslwireless.android.townhall.view.activity.desco.descoBillPayment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.reflect.TypeToken
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCCustomerInfoInitializer
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.descoModels.DescoAccountDueBillResponse
import sslwireless.android.townhall.data.model.descoModels.DescoBillItem
import sslwireless.android.townhall.data.model.descoModels.DescoBillPaymentItem
import sslwireless.android.townhall.data.model.descoModels.DescoBillPaymentRequestModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.desco.DescoBills.DescoBillDetailsActivity
import sslwireless.android.townhall.view.activity.desco.TransactionSuccessfulDF
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.activity.desco.adapters.DescoMonthWiseBillAdapter
import sslwireless.android.townhall.view.activity.desco.adapters.DescoOffersAdapter
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.activity_desco_bill_payment.*
import sslwireless.android.townhall.BuildConfig
import java.lang.reflect.Type

class DescoBillPamentActivity : BaseActivity(), IAdapterCallback, SSLCTransactionResponseListener {

    private lateinit var sslCommerzIntegrationModel: SslCommerzIntegrationModel
    private var total_bill: Double = 0.00
    private lateinit var descoAccountDueBillResponse: DescoAccountDueBillResponse
    private lateinit var dueBillItems: ArrayList<DescoBillItem>
    private lateinit var account_no: String
    private lateinit var viewModel: DescoBillPaymentViewModel
    var offers = ArrayList<Int>()
    var is_desco_prepaid: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desco_bill_payment)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(DescoBillPaymentViewModel(getDataManager()))
            ).get(DescoBillPaymentViewModel::class.java)


        if (intent.hasExtra(Enums.parameter.is_prepaid.toString())) {
            is_desco_prepaid = intent.getBooleanExtra(Enums.parameter.is_prepaid.toString(), false)
            account_no = intent.getStringExtra("account_no")?:""

            desco_account_no_tv.text = account_no

            viewModel.apiGetDescoPostpaidBillings(account_no,this,this)
        }

    }

    override fun viewRelatedTask() {

        rvOffers.layoutManager = GridLayoutManager(this@DescoBillPamentActivity, 1)
        rvMonthWisePament.layoutManager = GridLayoutManager(this, 1)
        offers.add(R.drawable.ic_offers2)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)
        offers.add(R.drawable.ic_offers1)


        rvOffers.adapter = DescoOffersAdapter(offers).setCallback(this)

        if (is_desco_prepaid) {
            inputLayoutAmount.visibility = View.VISIBLE
            layoutMonthlyBill.visibility = View.GONE

            etAmount.requestFocus()
            etAmount.isCursorVisible = true
        } else {
            inputLayoutAmount.visibility = View.GONE
            layoutMonthlyBill.visibility = View.VISIBLE
        }

        btnBillPayment.setOnClickListener {

            val descoBillPaymentItems : ArrayList<DescoBillPaymentItem> = ArrayList()

            dueBillItems.forEach{
                if(it.is_selected){
                    val descoBillPaymentItem = DescoBillPaymentItem(it.account_number,
                        "postpaid",
                        it.total_amount,
                        it.bill_number)
                    descoBillPaymentItems.add(descoBillPaymentItem)
                }
            }

            val descoBillPaymentRequestModel = DescoBillPaymentRequestModel(descoBillPaymentItems,
               0,
                getDataManager().mPref.prefGetCustomerToken()!!)

            viewModel.apiDescoBillPayment(descoBillPaymentRequestModel,this,this)


        }

        cbSelectAllBill.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                total_bill = 0.00
                dueBillItems.forEach {
                    it.is_selected = p1
                    if(p1){
                        total_bill += it.total_amount.toDouble()
                    }
                }
                updateTotalTv(total_bill)
                rvMonthWisePament.adapter!!.notifyDataSetChanged()
            }

        })

    }

    override fun <T> clickListener(position: Int, model: T, view: View) {
        if(view.id == R.id.select_bill_cv){
            if((view as CheckBox).isChecked){
                total_bill += dueBillItems[position].total_amount.toDouble()
            }else{
                if(total_bill>0){
                    total_bill -= dueBillItems[position].total_amount.toDouble()
                }
            }
            updateTotalTv(total_bill)
        }else{
            var intent =
                Intent(this@DescoBillPamentActivity, DescoBillDetailsActivity::class.java)
            intent.putExtra(Enums.parameter.is_prepaid.toString(), is_desco_prepaid)
            intent.putExtra("bills",dueBillItems)
            intent.putExtra("position",position)
            startActivity(intent)
        }
    }

    private fun updateTotalTv(totalBill: Double) {
        if(totalBill>0){
            total_bill_tv.visibility = View.VISIBLE
            total_bill_tv.text = "Total: $totalBill Tk"

            btnBillPayment.background = ContextCompat.getDrawable(
                this,
                R.drawable.bg_bottom_button
            )
            btnBillPayment.isEnabled = true
        }else{
            total_bill_tv.text = "Total: $totalBill Tk"
            total_bill_tv.visibility = View.GONE

            btnBillPayment.background = ContextCompat.getDrawable(
                this,
                R.drawable.bg_bottom_button_disabled
            )
            btnBillPayment.isEnabled = false
        }
    }

    override fun onRemoved(position: Int) {
    }



    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        val rawResponse = result.data!!.data!!.toString()

        Log.e("RawResponse:", "$key ----> $rawResponse")

        when(key){
            "apiGetDescoPostpaidBillings"->{

                if(result.data.code == 200){

                    val listType: Type = object : TypeToken<DescoAccountDueBillResponse>() {}.type
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<DescoAccountDueBillResponse> = moshi.adapter(listType)

                    descoAccountDueBillResponse = adapter.fromJsonValue(result.data.data) as DescoAccountDueBillResponse

                    dueBillItems = descoAccountDueBillResponse.data as ArrayList<DescoBillItem>

                    if(dueBillItems.isNotEmpty()){
                        cbSelectAllBill.visibility = View.VISIBLE
                        empty_view.visibility = View.GONE
                        var descoBillAdapter = DescoMonthWiseBillAdapter(dueBillItems)
                        descoBillAdapter.setCallBack(this)
                        rvMonthWisePament.adapter = descoBillAdapter
                    }else{
                        empty_view.visibility = View.VISIBLE
                        cbSelectAllBill.visibility = View.GONE
                    }

                }else{
                    showToast(this,result.data.message)
                }
            }

            "apiDescoBillPayment"->{

                if(result.data.code == 200){
                    val listType: Type = object : TypeToken<SslCommerzIntegrationModel>() {}.type

                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<SslCommerzIntegrationModel> = moshi.adapter(listType)

                    sslCommerzIntegrationModel = adapter.fromJsonValue(result.data.data) as SslCommerzIntegrationModel

                    val SSLCCustomerInfoInitializer =
                        SSLCCustomerInfoInitializer(
                            getDataManager().mPref.prefGetCurrentUser().name, getDataManager().mPref.prefGetCurrentUser().email,
                            getDataManager().mPref.prefGetCurrentUser().address, "", "", "Bangladesh", getDataManager().mPref.prefGetCurrentUser().mobile
                        )

                    val sslCommerzInitialization = SSLCommerzInitialization(
                        sslCommerzIntegrationModel.store_id,
                        sslCommerzIntegrationModel.store_password,
                        sslCommerzIntegrationModel.amount.toString().toDouble(),
                        SSLCCurrencyType.BDT,
                        sslCommerzIntegrationModel.transaction_id,
                        "easy consumer",
                        if (BuildConfig.FLAVOR=="demo") SSLCSdkType.TESTBOX else SSLCSdkType.LIVE
                    )

                    sslCommerzInitialization.ipn_url = sslCommerzIntegrationModel.ipn_url
                    val additionalInitializer = SSLCAdditionalInitializer()
                    additionalInitializer.valueB = sslCommerzIntegrationModel.value_b
                    additionalInitializer.valueC = sslCommerzIntegrationModel.value_c

                    IntegrateSSLCommerz
                        .getInstance(this)
                        .addSSLCommerzInitialization(sslCommerzInitialization)
                        .addCustomerInfoInitializer(SSLCCustomerInfoInitializer)
                        .addAdditionalInitializer(additionalInitializer)
                        .buildApiCall(this);
                }else{
                    showToast(this, result.data.message)
                }
            }


        }

    }


    override fun transactionFail(p0: String?) {}
    override fun closed(message: String?) {

    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        Handler().postDelayed(object : Runnable {
            override fun run() {
                val dialogFragment = TransactionSuccessfulDF()
                val args = Bundle()
                args.putBoolean("isSuccess", true)
                args.putBoolean("recharge", false)
                args.putString("title", "Transaction Successful!")
                dialogFragment.setArguments(args)
                dialogFragment.show(supportFragmentManager, "dialog_submit_confirmation")
                dialogFragment.setOkClick(
                    object : TransactionSuccessfulDF.OkClickListener {
                        override fun onOkClick(rating:String) {
                            dialogFragment.dismiss()
                        }

                    }
                )
                dialogFragment.setTryAgainClick(
                    object : TransactionSuccessfulDF.TryAgainClickListener {
                        override fun onTryAgainClick() {
                            dialogFragment.dismiss()
                            finish()
                        }

                    }

                )
            }

        }, 500)
    }
}
