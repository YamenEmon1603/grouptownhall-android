package sslwireless.android.townhall.view.activity.desco.descoBillPayment

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.descoModels.DescoBillPaymentRequestModel
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class DescoBillPaymentViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun apiGetDescoPostpaidBillings(
        account_no: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetDescoPostpaidBillings(
            account_no,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiGetDescoPostpaidBillings"))
        )
    }


    fun apiDescoBillPayment(
        descoBillPaymentRequestModel: DescoBillPaymentRequestModel,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiDescoBillPayment(
            descoBillPaymentRequestModel,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "apiDescoBillPayment"))
        )
    }


}