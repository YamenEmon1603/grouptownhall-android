package sslwireless.android.townhall.view.activity.desco.utils

class Enums {
    enum class Common {
        value,
        terms,
        privacy,
        condition,
        Gallery_Picture,
        Camera_Request,
        Location_Request,
        CALL_PHONE,
        SEND_SMS,
        Activity1,
        FragmentContacts,
        FragmentCompanies,
        Activity2,
        itemDeal,
        itemContact
    }

    enum class parameter {
        is_prepaid
    }
}
