package sslwireless.android.townhall.view.activity.experience_zone

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.experience.ItemsItem
import sslwireless.android.townhall.databinding.ExperienceZoneItemLayoutBinding
import sslwireless.android.townhall.view.utils.*

class ExperienceAdapter(
    private val list: ArrayList<ItemsItem>,
    private val listener: (title: String, link: String) -> Unit
) :
    RecyclerView.Adapter<ExperienceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.experience_zone_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        holder.binding.bg.loadImage(item.background ?: "")
        holder.binding.roundIcon.loadImageWithoutCache(item.icon ?: "")
        holder.binding.title.text = item.title
        holder.binding.subTitle.text = item.shortDescription
        holder.binding.root.setOnClickListener {
            listener(item.title ?: "", item.link ?: "")
        }

    }


    override fun getItemCount(): Int = list.size


    class ViewHolder(val binding: ExperienceZoneItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }
}