package sslwireless.android.townhall.view.activity.experience_zone

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_experience_details.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity

class ExperienceDetailsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_experience_details)
    }

    override fun viewRelatedTask() {
        val title = intent.getStringExtra("title") ?: ""
        val link = intent.getStringExtra("link") ?: ""
        toolbar_title.text = title
        webView.loadUrl(link)

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}