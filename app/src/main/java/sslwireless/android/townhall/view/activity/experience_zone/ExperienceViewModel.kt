package sslwireless.android.townhall.view.activity.experience_zone

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class ExperienceViewModel @Inject constructor(private val dataManager: DataManager) : BaseViewmodel(dataManager) {



    fun getTownhallContent(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getTownhallContent(dataManager.mPref.prefGetCustomerToken()?:"","2",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "townhall_content"))
        )
    }
}