package sslwireless.android.townhall.view.activity.experience_zone

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_experience_zone.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.experience.ExperienceResponse
import sslwireless.android.townhall.data.model.experience.ItemsItem
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse

class ExperienceZoneActivity : BaseActivity() {
    private lateinit var experienceViewModel: ExperienceViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_experience_zone)
        experienceViewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(ExperienceViewModel(getDataManager())))
                .get(ExperienceViewModel::class.java)
        experienceViewModel.getTownhallContent(this, this)
    }

    override fun viewRelatedTask() {
        back_icon_iv.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (result.data?.code) {
            200 -> {
                val deviceKey = result.data.data?.fromResponse<ExperienceResponse>()
                val list = deviceKey?.townhall?.items ?: arrayListOf()
                experienceRecycler.adapter = ExperienceAdapter(
                    list = list as ArrayList<ItemsItem>,
                    listener = { title, link ->
                        val intent = Intent(this, ExperienceDetailsActivity::class.java).apply {
                            putExtra("title", title)
                            putExtra("link", link)
                        }
                        startActivity(intent)
                    }

                )
            }
            401 -> {
                showToast(this, result.data.message)
                getDataManager().mPref.prefLogout(this)
            }
            else -> {
                showToast(this, result.data!!.message)
            }
        }
    }
}