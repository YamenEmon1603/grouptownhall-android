package sslwireless.android.townhall.view.activity.favorites

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Bundle
import android.provider.ContactsContract
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.LocalValidator
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.isPermissionGranted
import sslwireless.android.townhall.view.utils.showErrorToast
import sslwireless.android.townhall.view.utils.showToastMsg
import kotlinx.android.synthetic.main.activity_add_to_fav.*
import java.util.regex.Pattern

class AddToFavActivity : BaseActivity(), OperatorBottomSheet.BottomSheetListener {


    private val operators: OperatorBottomSheet by lazy {
        OperatorBottomSheet("fav")
    }
    private val viewModel: AddToFavActivityViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(AddToFavActivityViewModel(getDataManager())))
            .get(AddToFavActivityViewModel::class.java)
    }
    private var finalNumber = ""
    private var isAddRequest = true
    private var connectionType = ""
    private val PERMISSION_REQUEST_CODE: Int = 100
    private val PICK_CONTACT: Int = 200
    private val phonePattern = "(^(01){1}[3-9]{1}\\d{8})$"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_to_fav)

    }

    override fun viewRelatedTask() {
        number_et.requestFocus()
        first_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        number_et.setOnFocusChangeListener { view, b ->
            if (b) {
                first_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
            } else {
                first_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.edittext_layout))
            }
        }

        operator_text.setOnFocusChangeListener { view, b ->
            if (b) {
                second_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
            } else {
                second_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.edittext_layout))
            }
        }

        nickName_et.setOnFocusChangeListener { view, b ->
            if (b) {
                third_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.selected_edit_layout))
            } else {
                third_holder.setBackgroundDrawable(resources.getDrawable(R.drawable.edittext_layout))
            }
        }

        try {
            val favoriteModel = intent.getSerializableExtra("favList") as FavoriteModel

            number_et.setText(favoriteModel.phone)
            nickName_et.setText(favoriteModel.name)
            connectionType = favoriteModel.connection_type

            operator_text.setText(
                AppConstants.Operators.OPERATOR_LIST.find { it.operatorId == favoriteModel.operator_id }?.operatorName
                    ?: if (favoriteModel.operator_id==AppConstants.Operators.SKITTO_ID) AppConstants.Operators.GRAMEENPHONE else ""
            )

            isAddRequest = false
            textView9.text = getString(R.string.edit_favourite_contact)
            submitFavBtn.text = getString(R.string.save_updates)
        } catch (e: Exception) {

        }

        back.setOnClickListener {
            onBackPressed()
        }
        number_et.doAfterTextChanged {
            number_et.error = null
        }

        submitFavBtn.setOnClickListener {
            val phoneNumber = number_et.text.toString()
            val operatorName = operator_text.text.toString()
            val nickName = nickName_et.text.toString()

            when {
                phoneNumber.isEmpty() -> {
                    number_et.error = getString(R.string.please_enter_contact_number)
                }
                operatorName.isEmpty() -> {
                    operator_text.error = getString(R.string.please_select_operator)
                    showToastMsg(getString(R.string.select_operator))
                }
                nickName.isEmpty() -> {
                    nickName_et.error = getString(R.string.please_enter_contact_nick_name)
                }
                !LocalValidator.isPhoneValid(phoneNumber.trim()) -> {
                    number_et.error = getString(R.string.msg_invalid_number)
                }
                else -> {
                    val operatorID =
                        if (operatorName == AppConstants.Operators.GRAMEENPHONE && connectionType == AppConstants.Operators.SKITTO) {
                            13
                        } else {
                            AppConstants.Operators.OPERATOR_LIST.find { it.operatorName == operatorName }?.operatorId ?: 0
                        }

                    if (isAddRequest) {
                        viewModel.apiAddToFavContact(
                            phoneNumber,
                            nickName,
                            connectionType,
                            operatorID.toString().toInt(),
                            this,
                            this
                        )
                    } else {
                        viewModel.apiUpdateToFavContact(
                            phoneNumber,
                            nickName,
                            connectionType,
                            operatorID.toString().toInt(),
                            this,
                            this
                        )
                    }
                }
            }
        }

        second_holder.setOnClickListener {
            hideKeyboard()
            val im = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            im.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            operators.show(supportFragmentManager, "dialog")
        }

        operator_text.setOnClickListener {
            val im = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            im.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
            operators.show(supportFragmentManager, "dialog")
        }
        contacts.setOnClickListener {
            if (isPermissionGranted(Manifest.permission.READ_CONTACTS, PERMISSION_REQUEST_CODE)) {
                callContactIntent()
            }
        }
    }

    private fun callContactIntent() {
        val pickContact = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI)
        pickContact.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(pickContact, PICK_CONTACT)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (requestCode == PERMISSION_REQUEST_CODE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callContactIntent()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_CONTACT) {
            pickContact(data)
        }
    }

    private fun pickContact(data: Intent?) {
        if (data != null) {
            val uri = data.data
            if (uri != null) {
                var c: Cursor? = null
                try {
                    c = contentResolver.query(
                        uri, arrayOf(
                            ContactsContract.CommonDataKinds.Phone.NUMBER
                        ),
                        null, null, null
                    )
                    if (c != null && c.moveToFirst()) {
                        if (isValidNumber(c.getString(0))) {
                            number_et.setText(finalNumber)
                        } else {
                            number_et.error = getString(R.string.msg_invalid_number)
                        }
                    }
                } finally {
                    c?.close()
                }
            }
        }
    }


    private fun isValidNumber(number: String): Boolean {
        number.replace("+88", "").replace("\\D+".toRegex(), "")
        return if (number.length >= 11) {
            finalNumber =
                number.replace("-".toRegex(), "").replace(" ".toRegex(), "")
            finalNumber = finalNumber.substring(finalNumber.length - 11)
            val pattern = Pattern.compile(
                phonePattern,
                Pattern.CASE_INSENSITIVE
            )
            val matcher = pattern.matcher(finalNumber)
            matcher.matches()
        } else {
            false
        }
    }

    override fun onClicked(operatorName: String, operatorType: String) {
        operator_text.setText(operatorName)
        connectionType = operatorType
        operators.dismiss()
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            showToastMsg(result.data.message)
            if (key == "updateFavContact") {
                val operatorID = AppConstants.Operators.OPERATOR_LIST.find { it.operatorName == operator_text.text.toString()}?.operatorId ?: 0

                val favoriteModel = FavoriteModel(
                    connectionType,
                    nickName_et.text.toString(),
                    operatorID.toString().toInt(),
                    number_et.text.toString()
                )
                val intent = Intent().apply {
                    putExtra("favList", favoriteModel)
                }
                setResult(RESULT_OK, intent)
                finish()
            } else {
                onBackPressed()
            }
        } else {
            showErrorToast(result.data.message)
        }

    }

}
