package sslwireless.android.townhall.view.activity.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.custom.CustomEdittext
import sslwireless.android.townhall.view.utils.showErrorToast
import sslwireless.android.townhall.view.utils.showToastMsg
import kotlinx.android.synthetic.main.activity_add_to_fav_list.*

class AddToFavListActivity : BaseActivity() {


    private var topUpModels = ArrayList<TopUpModel>()
    private val topUpListLiveData = MutableLiveData<List<TopUpModel>>()
    private var position = -1
    private val viewModel: AddToFavActivityViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(AddToFavActivityViewModel(getDataManager())))
            .get(AddToFavActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_to_fav_list)
    }

    override fun viewRelatedTask() {

        back.setOnClickListener {
            onBackPressed()
        }

        topUpModels = intent.getSerializableExtra("top_up_models") as ArrayList<TopUpModel>
        val favNumbers = intent.getStringArrayListExtra("favList")

        topUpListLiveData.value = topUpModels.map {
            it.isAdded = favNumbers?.contains(it.phone)
            return@map it
        }


        topUpListLiveData.observe(this, Observer {
            contacts_recview.adapter = BaseRecyclerAdapter(this, object : IAdapterListener {
                override fun <T> clickListener(position: Int, model: T, view: View) {
                    val nameET: CustomEdittext = view as CustomEdittext
                    if (nameET.text.toString().trim().isEmpty()) {
                        showToastMsg(getString(R.string.name_empty))
                        return
                    }
                    gotoAddToFavActivity(
                        position,
                        model as TopUpModel,
                        nameET.text.toString().trim()
                    )
                }

                override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {
                    return ContactsViewHolder(
                        DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context)
                            , R.layout.fav_list_items
                            , parent, false
                        )
                        , this@AddToFavListActivity
                    )

                }

            }, it as ArrayList<TopUpModel>)
        })
    }

    private fun gotoAddToFavActivity(position: Int, model: TopUpModel, name: String) {
        this.position = position
        /*val intent = Intent(this, AddToFavActivity::class.java)
        intent.putExtra("top_up_model", topUpModels[position])
        startActivityForResult(intent,200)*/
        viewModel.apiAddToFavContact(
            model.phone, name, model.type, model.operator, this, this
        )
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            val list: ArrayList<TopUpModel> = topUpListLiveData.value as ArrayList<TopUpModel>
            list[position].isAdded = true
            topUpListLiveData.value = list
            showToastMsg(getString(R.string.added))
        } else {
            showErrorToast(result.data.message)
        }
    }

}
