package sslwireless.android.townhall.view.activity.favorites

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class AddToFavActivityViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun apiAddToFavContact(
        phone: String,
        name: String,
        connection_type: String,
        operator_id: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        val token = dataManager.mPref.prefGetCustomerToken() ?: return

        dataManager.apiHelper.apiAddToFavContact(
            token,
            phone,
            name,
            connection_type,
            operator_id,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "addFavContact"))
        )
    }

    fun apiUpdateToFavContact(
        phone: String,
        name: String,
        connection_type: String,
        operator_id: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        val token = dataManager.mPref.prefGetCustomerToken() ?: return

        dataManager.apiHelper.apiUpdateToFavContact(
            token,
            phone,
            name,
            connection_type,
            operator_id,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "updateFavContact"))
        )
    }


}