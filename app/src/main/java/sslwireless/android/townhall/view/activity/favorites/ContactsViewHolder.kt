package sslwireless.android.townhall.view.activity.favorites

import android.content.Context
import android.view.View
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.databinding.FavListItemsBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.operatorIconByID

class ContactsViewHolder(itemView: ViewDataBinding, context: Context) :
    BaseViewHolder2(itemView.root) {

    var binding = itemView as FavListItemsBinding
    var mContext: Context = context


    override fun <T> onBind(position: Int, model: T, mCallback: IAdapterListener) {
        model as TopUpModel

        binding.number.text = model.phone
        binding.type.text = model.type
        binding.icon.operatorIconByID(model.operator)
        binding.enterNameGroup.visibility = View.GONE

        if (!model.isAdded!!) {
            binding.addToFavBtn.visibility = View.VISIBLE
            binding.addedBtn.visibility = View.GONE
        } else {
            binding.addToFavBtn.visibility = View.GONE
            binding.addedBtn.visibility = View.VISIBLE
        }

        binding.addToFavBtn.setOnClickListener {
            binding.enterNameGroup.visibility = View.VISIBLE
            binding.addToFavBtn.visibility = View.GONE
            binding.addedBtn.visibility = View.GONE

        }
        binding.cross.setOnClickListener {
            mCallback.clickListener(position,model,binding.customEdittext)
        }
    }

//    interface OnAddToFavBtnClickedListener{
//        fun onAddToFavBtnClicked(model:String)
//    }
}