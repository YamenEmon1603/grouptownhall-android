package sslwireless.android.townhall.view.activity.favorites

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.FavoriteContactAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse
import sslwireless.android.townhall.view.utils.showToastMsg
import kotlinx.android.synthetic.main.activity_favorite_contacts.*
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet

class FavoriteContactsActivity : BaseActivity(), FavoriteContactAdapter.FavoriteListener,SessionLogoutBottomSheet.IBottomSheetDialogClicked {
    private lateinit var viewModel: FavoriteContactsViewModel
    private var favList = ArrayList<FavoriteModel>()
    private lateinit var adapter: FavoriteContactAdapter
    private var position = -1
    private var phone =""
    private val sessionLogoutBottomSheet: SessionLogoutBottomSheet by lazy {
        SessionLogoutBottomSheet().apply {
            arguments = Bundle().apply {
                putString("title",this@FavoriteContactsActivity.getString(R.string.remove_number_title))
                putString("subText","")
                putString("btn1",this@FavoriteContactsActivity.getString(R.string.no))
                putString("btn2",this@FavoriteContactsActivity.getString(R.string.yes))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite_contacts)

        viewModel = ViewModelProviders.of(
            this,
            BaseViewmodelFactory(FavoriteContactsViewModel(getDataManager()))
        ).get(FavoriteContactsViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        hideKeyboard()
        viewModel.apiGetFavContact(
            this, this
        )

        initList()
    }

    private fun initList() {
        adapter = FavoriteContactAdapter(this)
        contacts_recview.layoutManager = LinearLayoutManager(this)
        contacts_recview.adapter = adapter
    }

    override fun viewRelatedTask() {
        addNewContactBtn.setOnClickListener {
            startActivity(Intent(this, AddToFavActivity::class.java))
        }
        back.setOnClickListener { onBackPressed() }

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200) {
            when (key) {
                "getFav" -> {
                    favList =
                        result.data.data?.fromResponse<List<FavoriteModel>>() as ArrayList<FavoriteModel>
                    getDataManager().mPref.prefFavorite(favList)
                    adapter.favList = favList
                    adapter.notifyDataSetChanged()
                }
                "deleteFavContact" -> {
                    showToastMsg(result.data.message)
                    favList.removeAt(position)
                    getDataManager().mPref.prefFavorite(favList)
                    adapter.notifyDataSetChanged()
                }
            }

        } else {
            result.data?.message?.let { showToastMsg(it) }
        }

    }

    override fun onNumberDelete(position: Int, phone: String) {
        this.position = position
        this.phone = phone
        sessionLogoutBottomSheet.show(supportFragmentManager, sessionLogoutBottomSheet.tag)
        sessionLogoutBottomSheet.setBottomDialogListener(this)
    }

    override fun onNumberUpdate(position: Int, item: FavoriteModel) {
        this.position = position
        val intent = Intent(this, AddToFavActivity::class.java).apply {
            putExtra("favList", item)
        }
        startActivityForResult(intent, 200)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 200 && resultCode == RESULT_OK) {
            val item = data!!.extras!!.getSerializable("favList") as FavoriteModel
            favList[position] = item
            adapter.notifyDataSetChanged()
        }
    }

    override fun onCancelButtonClicked() {

    }

    override fun onSessionLogoutButtonClicked(tag: String) {
        viewModel.apiDeleteFavContact(
            phone, this, this
        )
    }
}