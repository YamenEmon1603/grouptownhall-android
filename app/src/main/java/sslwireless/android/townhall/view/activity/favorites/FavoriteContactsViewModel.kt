package sslwireless.android.townhall.view.activity.favorites

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class FavoriteContactsViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun apiGetFavContact(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        val token = dataManager.mPref.prefGetCustomerToken() ?: return

        dataManager.apiHelper.apiGetFavContact(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "getFav"))
        )
    }

    fun apiDeleteFavContact(
        phone:String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        val token = dataManager.mPref.prefGetCustomerToken() ?: return

        dataManager.apiHelper.apiDeleteFavContact(
            token,
            phone,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "deleteFavContact"))
        )
    }


}