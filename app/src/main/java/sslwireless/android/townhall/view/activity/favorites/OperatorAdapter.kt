package sslwireless.android.townhall.view.activity.favorites

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.operator.OperatorModel
import sslwireless.android.townhall.databinding.OperatorItemLayoutBinding
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImage
import sslwireless.android.townhall.view.utils.setBackgroundExtension

class OperatorAdapter(
    private val listener: OperatorListener
) :
    RecyclerView.Adapter<OperatorAdapter.ViewHolder>() {
    private var selected = -1
    var opList: ArrayList<OperatorModel> = AppConstants.Operators.OPERATOR_LIST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.operator_item_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = opList[position]
        holder.binding.name.text =  if (AppConstants.Language.CURRENT_LANGUAGE==AppConstants.Language.ENGLISH) item.operatorName else item.operatorNameBn
        holder.binding.icon.loadImage(item.imageUrl)
        if (position==selected) {
            holder.binding.radio.loadImage(R.drawable.ic_circle_colored)
            holder.binding.rootLayout.setBackgroundExtension(R.drawable.layout_backgroud_field_operator_select)
        } else {
            holder.binding.radio.loadImage(R.drawable.ic_circle_not_colored)
            holder.binding.rootLayout.setBackgroundExtension(R.drawable.layout_backgroud_field_operator)
        }
        holder.binding.rootLayout.setOnClickListener {
            selected = position
            notifyDataSetChanged()
            listener.onOperatorClick(item.operatorName)
        }

    }

    override fun getItemCount(): Int {
        return opList.size
    }


    interface OperatorListener {
        fun onOperatorClick(name:String)

    }


    class ViewHolder(val binding: OperatorItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)
}