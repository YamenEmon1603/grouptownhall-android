package sslwireless.android.townhall.view.activity.favorites

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.activity.TopUp.ConnectionTypeBottomSheet
import kotlinx.android.synthetic.main.operator_bottomsheet_layout.*


class OperatorBottomSheet(s: String) : BottomSheetDialogFragment(),OperatorAdapter.OperatorListener {

    private var bottomSheetListener: BottomSheetListener? = null
    var id = s

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.operator_bottomsheet_layout, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerOperator.adapter = OperatorAdapter(this)
        imageView43.setOnClickListener { dismiss() }
    }


    interface BottomSheetListener {
        fun onClicked(operatorName: String,operatorType: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        bottomSheetListener = context as BottomSheetListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog = super.onCreateDialog(savedInstanceState)
        if (!id.equals("fav")) {
            bottomSheetDialog.setOnShowListener { dialog ->
                val d = dialog as BottomSheetDialog
                val operator_bottomsheet = d.findViewById<View>(R.id.operator_bottomsheet)
                operator_bottomsheet!!.visibility = View.VISIBLE
                val height = requireActivity().resources.displayMetrics
                val bottomSheet =
                    d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
                bottomSheetBehavior.peekHeight = height.heightPixels
                //  bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        } else {
            bottomSheetDialog.setOnShowListener { dialog ->
                val d = dialog as BottomSheetDialog
                val operator_bottomsheet = d.findViewById<View>(R.id.operator_bottomsheet)
                operator_bottomsheet!!.visibility = View.VISIBLE

                val bottomSheet =
                    d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout
                val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
                //bottomSheetBehavior.peekHeight = bottomSheet.height
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        return bottomSheetDialog
    }

    override fun onOperatorClick(name: String) {
        val typeDialog = ConnectionTypeBottomSheet(bottomSheetListener,name)
        typeDialog.show(requireActivity().supportFragmentManager, "12347")
    }

}
