package sslwireless.android.townhall.view.activity.forgetPassword

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.password_bottomsheet.*
import sslwireless.android.townhall.R

class ForgetPassBottomSheet : BottomSheetDialogFragment() {

    private var bottomSheetListener: BottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.password_bottomsheet, container)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view14.setOnClickListener {
            bottomSheetListener?.onClicked("proceed")
            dismiss()
        }
        imageView7.setOnClickListener {
            dismiss()
        }

    }

    interface BottomSheetListener {
        fun onClicked(text: String)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        bottomSheetListener = context as BottomSheetListener
    }
}
