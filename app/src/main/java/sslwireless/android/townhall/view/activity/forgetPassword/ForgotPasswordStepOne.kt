package sslwireless.android.townhall.view.activity.forgetPassword

import android.os.Bundle
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_forgot_password_step_one.*

class ForgotPasswordStepOne : BaseActivity() , ForgetPassBottomSheet.BottomSheetListener {

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key:String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password_step_one)
    }

    override fun viewRelatedTask() {
         proceed_btn.setOnClickListener {
             val forgotPasswordConfirmation = ForgetPassBottomSheet()
             forgotPasswordConfirmation.show(supportFragmentManager, "1234")
         }
    }

    override fun onClicked(text: String) {

    }
}
