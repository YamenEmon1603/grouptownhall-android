package sslwireless.android.townhall.view.activity.forgetPassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sslwireless.android.townhall.R

class ForgotPasswordStepTwo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password_step_two)
    }
}
