package sslwireless.android.townhall.view.activity.game

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.budiyev.android.codescanner.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.GameResponse
import sslwireless.android.townhall.databinding.ActivityGameQrBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse

class GameQRActivity : BaseActivity() {
    private lateinit var binding: ActivityGameQrBinding
    private lateinit var codeScanner: CodeScanner
    private lateinit var viewModel: GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_game_qr)
    }

    override fun viewRelatedTask() {
        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(GameViewModel(getDataManager())))
                .get(GameViewModel::class.java)

        codeScanner = CodeScanner(this, binding.scannerView)

        binding.closeBtn.setOnClickListener {
            onBackPressed()
        }

        binding.flashBtn.setOnClickListener {
            codeScanner.isFlashEnabled = !codeScanner.isFlashEnabled
        }

        initScanner()
    }

    private fun attemptSubmitQR(qr: String?) {
        if (qr.isNullOrEmpty()) {
            showToast(this, "Invalid QR code!")
            onBackPressed()
            return
        }

        viewModel.submitGameQR(
            qrCode = qr,
            checkEligible = "yes",
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            GameViewModel.SUBMIT_QR -> {
                when (result.data?.code) {
                    200 -> {
                        val gameResponse = result.data.data.fromResponse<GameResponse>()
                        handleResponse(gameResponse)
                    }
                    422 -> {
                        showToast(this@GameQRActivity, result.data.message)
                        onBackPressed()
                    }
                }
            }
        }

    }

    private fun handleResponse(gameResponse: GameResponse?) {
        if (gameResponse == null) {
            showToast(this@GameQRActivity, "Something went wrong!")
            return
        }

        gotoGameStatus(gameResponse)
    }

    @Deprecated("Play game screen is not needed anymore")
    private fun gotoPlayGame(data: GameResponse) {
        val intent = Intent(this, PlayGameActivity::class.java)
        intent.putExtra("data", data)
        startActivity(intent)
        finish()
    }


    private fun gotoGameStatus(data: GameResponse) {
        val intent = Intent(this, GameStatusActivity::class.java)
        intent.putExtra("data", data)
        startActivity(intent)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                codeScanner.startPreview()
            } else {
                showToast(this, "Camera permission denied")
                onBackPressed()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        stopScanner()
    }

    override fun onStop() {
        super.onStop()
        stopScanner()
    }

    override fun onDestroy() {
        super.onDestroy()
        stopScanner()
    }

    private fun stopScanner() {
        codeScanner.stopPreview()
        codeScanner.releaseResources()
    }


    private fun startScanner() {
        if (!codeScanner.isPreviewActive) {
            codeScanner.startPreview()
        }
    }

    private fun initScanner() {
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,

        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                //showToast(this, it.text)
                attemptSubmitQR(it.text)
            }
        }

        codeScanner.errorCallback = ErrorCallback {
            runOnUiThread {
                Toast.makeText(
                    this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initRuntimePermission()
        } else {
            startScanner()
        }
    }


    private fun initRuntimePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            startScanner()
            return
        }

        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSION_REQUEST_CODE
            )
        } else {
            startScanner()
        }
    }

    companion object {
        private const val PERMISSION_REQUEST_CODE: Int = 100
    }
}