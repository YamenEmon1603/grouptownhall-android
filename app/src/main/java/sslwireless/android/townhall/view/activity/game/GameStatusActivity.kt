package sslwireless.android.townhall.view.activity.game

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.GameResponse
import sslwireless.android.townhall.databinding.ActivityGameStatusBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.loadImage

class GameStatusActivity : BaseActivity() {
    private lateinit var binding: ActivityGameStatusBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_game_status)
    }

    override fun viewRelatedTask() {
        initView()

        binding.continueBtn.setOnClickListener {
            gotoHome(this)
        }
    }

    private fun initView() {
        try {
            val data = intent.getSerializableExtra("data") as GameResponse

            if (data.is_eligible == true) {
                binding.messageTV.text = getString(R.string.congratulations_text)
                binding.continueBtn.text = getString(R.string.done_cap)
                binding.bgLayout.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.jungle_green_two
                    )
                )
                binding.continueBtn.background =
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.bg_game_continue
                    )
            } else {
                binding.messageTV.text = getString(R.string.already_played)
                binding.continueBtn.text = getString(R.string.go_home)
                binding.bgLayout.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.rouge
                    )
                )
                binding.continueBtn.background =
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.bg_game_failed
                    )
            }

            binding.gameIV.loadImage(data.game_info?.image_url ?: R.drawable.ic_dices)
            binding.gameNameTV.text = data.game_info?.game_title
            binding.descTV.text = data.game_info?.description
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}