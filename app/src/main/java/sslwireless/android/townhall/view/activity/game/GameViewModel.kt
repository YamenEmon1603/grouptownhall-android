package sslwireless.android.townhall.view.activity.game

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class GameViewModel @Inject constructor(private val dataManager: DataManager) :
    BaseViewmodel(dataManager) {


    fun submitGameQR(
        qrCode: String,
        checkEligible: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.submitGameQR(
            token = dataManager.mPref.prefGetCustomerToken() ?: "",
            game_code = qrCode,
            check_eligible = checkEligible,
            apiCallbackHelper = ApiCallbackHelper(
                livedata(
                    lifecycleOwner,
                    iObserverCallBack,
                    SUBMIT_QR
                )
            )
        )
    }

    fun playGame(
        qrCode: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.submitGameQR(
            token = dataManager.mPref.prefGetCustomerToken() ?: "",
            game_code = qrCode,
            apiCallbackHelper = ApiCallbackHelper(
                livedata(
                    lifecycleOwner,
                    iObserverCallBack,
                    PLAY_GAME
                )
            )
        )
    }

    companion object {
        const val SUBMIT_QR = "_submit_qr"
        const val PLAY_GAME = "_play_game"
    }
}