package sslwireless.android.townhall.view.activity.game

import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.GameResponse
import sslwireless.android.townhall.databinding.ActivityPlayGameBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse
import sslwireless.android.townhall.view.utils.loadImage
import sslwireless.android.townhall.view.utils.setSpanColor

class PlayGameActivity : BaseActivity() {
    private lateinit var binding: ActivityPlayGameBinding
    private lateinit var viewModel: GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_play_game)
    }

    override fun viewRelatedTask() {
        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(GameViewModel(getDataManager())))
                .get(GameViewModel::class.java)

        initView()

        binding.playGameBTN.setOnClickListener {
            handlePlay()
        }

        binding.drawerNavigationIcon.setOnClickListener {
            onBackPressed()
        }
    }

    private fun handlePlay() {
        try {
            val data = intent.getSerializableExtra("data") as GameResponse
            val gameCode = data.game_info?.game_code ?: return

            viewModel.playGame(gameCode, this, this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun gotoGameStatus(data: GameResponse?, isSuccess: Boolean) {
        data ?: return

        val intent = Intent(this, GameStatusActivity::class.java)
        intent.putExtra("data", data)
        intent.putExtra("isSuccess", isSuccess)
        startActivity(intent)
        finish()
    }

    private fun initView() {
        try {
            val data = intent.getSerializableExtra("data") as GameResponse

            binding.gameIV.loadImage(data.game_info?.game_image ?: R.drawable.ic_dices)

            val gameName = data.game_info?.game_title ?: ""
            val gameTitle = String.format(
                getString(R.string.game_title),
                gameName
            )

            binding.gameTitleTV.text =
                gameTitle.setSpanColor(
                    ContextCompat.getColor(this, R.color.colorAccent),
                    32,
                    gameTitle.length
                )

            val rules = getString(R.string.rules1_game)
            binding.rules1TV.text = rules.setSpanColor(
                ContextCompat.getColor(this, R.color.pumpkin_orange),
                21,
                rules.length
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            GameViewModel.PLAY_GAME -> {
                when (result.data?.code) {
                    200 -> {
                        val gameResponse = result.data.data.fromResponse<GameResponse>()

                        gotoGameStatus(gameResponse, true)
                    }
                    422 -> {
                        val gameResponse = result.data.data.fromResponse<GameResponse>()

                        gotoGameStatus(gameResponse, false)
                    }
                }
            }
        }
    }
}