package sslwireless.android.townhall.view.activity.help

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_help_and_support.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.help.ActiveCategoriesItem
import sslwireless.android.townhall.data.model.help.ActivePopularTopicsItem
import sslwireless.android.townhall.data.model.help.HelpResponse
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.HelpAndSupportItemDetailsActivity
import sslwireless.android.townhall.view.activity.HelpSupportItemsViaCategoryActivity
import sslwireless.android.townhall.view.adapter.HelpSupportCategoryListRecyclerAdapter
import sslwireless.android.townhall.view.adapter.HelpSupportItemListRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse

class HelpAndSupportActivity : BaseActivity(),
    HelpSupportCategoryListRecyclerAdapter.CategoryItemClickListener,
    HelpSupportItemListRecyclerAdapter.HelpItemClickListener {
    private var helpResponse: HelpResponse? = null
    private val PERMISSION_REQUEST_CODE: Int = 200

    private val viewModel: HelpAndSupportViewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(HelpAndSupportViewModel(getDataManager()))
        ).get(HelpAndSupportViewModel::class.java)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200) {
            helpResponse = result.data.data?.fromResponse()
            help_support_category_rec_view.setHasFixedSize(true)
            val helpsupportcategoryadapter = HelpSupportCategoryListRecyclerAdapter(
                this,
                getDataManager().mPref.prefGetLanguage()
            )
            help_support_category_rec_view.adapter = helpsupportcategoryadapter
            helpsupportcategoryadapter!!.setData((helpResponse!!.activeCategories as ArrayList<ActiveCategoriesItem>))

            help_support_popular_item_rec_view.setHasFixedSize(true)
            val helpsupportitemadapter = HelpSupportItemListRecyclerAdapter(
                this,
                getDataManager().mPref.prefGetLanguage()
            )
            help_support_popular_item_rec_view.adapter = helpsupportitemadapter
            helpsupportitemadapter!!.setData(helpResponse!!.activePopularTopics as ArrayList<ActivePopularTopicsItem>)

        } else {
            showToast(this, result.data?.message ?: "")
        }
    }


    override fun onHelpItemClickListener(position: Int) {
        startActivity(
            Intent(
                this,
                HelpAndSupportItemDetailsActivity::class.java
            ).putExtra("help_support_item", helpResponse?.activePopularTopics?.get(position))
        )
    }

    override fun onCategoryItemClickListener(position: Int) {
        startActivity(
            Intent(
                this,
                HelpSupportItemsViaCategoryActivity::class.java
            ).putExtra(
                "help_support_category",
                helpResponse?.activeCategories?.get(position)?.linkUrl ?: ""
            )
        )
    }


    override fun viewRelatedTask() {
        viewModel.apiGetHelpSupport(this, this)
        support_btn.setOnClickListener {
            callContactIntent()
        }
    }



    private fun callContactIntent() {
        helpResponse?.supportPhone?.split(",")?.get(0)?.let {
            val intent = Intent(
                Intent.ACTION_DIAL,
                Uri.fromParts("tel", it, null)
            )
            startActivity(intent)
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_and_support)

    }


}
