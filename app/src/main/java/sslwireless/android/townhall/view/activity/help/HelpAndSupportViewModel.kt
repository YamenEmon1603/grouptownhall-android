package sslwireless.android.townhall.view.activity.help

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class HelpAndSupportViewModel(private val dataManager: DataManager) : BaseViewmodel(dataManager) {

    fun apiGetHelpSupport(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetHelpSupport(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "help"))
        )
    }

    fun apiGetTopicHelpSupport(topic: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetTopicHelpSupport(
            topic, ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "topic"))
        )
    }
}