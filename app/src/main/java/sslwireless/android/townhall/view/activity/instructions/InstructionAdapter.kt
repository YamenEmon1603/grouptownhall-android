package sslwireless.android.townhall.view.activity.instructions

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.townhall.Bot
import sslwireless.android.townhall.databinding.InstructionItemLayoutBinding
import sslwireless.android.townhall.view.utils.*

class InstructionAdapter(
    private val list: ArrayList<String>,
    private val bot: Bot?,
    private val dataManager: DataManager
) : RecyclerView.Adapter<InstructionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.instruction_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        showDataOnCell(holder, position)

        showDoneIconOnFirstTwoItems(holder, position)

        showSocialButtonOnSecondItem(holder, position)

        showDoneForSocialIconOnBotOpened(holder, position)

        holder.binding.whatsapp.setOnClickListener {
            val isOpened = holder.binding.root.context.openSocialUrl(bot?.whatsapp)

            if (isOpened) {
                trackBotClick(holder, position)
            }
        }

        holder.binding.messenger.setOnClickListener {
            val isOpened = holder.binding.root.context.openSocialUrl(bot?.messenger)

            if (isOpened) {
                trackBotClick(holder, position)
            }
        }

        holder.binding.telegram.setOnClickListener {
            val isOpened = holder.binding.root.context.openTelegram(bot?.telegram)

            if (isOpened) {
                trackBotClick(holder, position)
            }
        }
    }

    override fun getItemCount(): Int = list.size

    class ViewHolder(val binding: InstructionItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    private fun trackBotClick(holder: ViewHolder, position: Int) {
        dataManager.mPref.prefSetIsBotOpened(true)

        showDoneForSocialIconOnBotOpened(holder, position)
    }

    private fun showDoneForSocialIconOnBotOpened(holder: ViewHolder, position: Int) {
        val isBotOpened = dataManager.mPref.prefIsBotOpened()

        if (position < 2) {
            holder.binding.tick.makeVisible()
            holder.binding.count.setColor(R.color.newSuccessColor)
            holder.binding.shape.setBackgroundExtension(R.drawable.circle_green)
        } else if (position == 2 && isBotOpened) {
            holder.binding.tick.makeVisible()
            holder.binding.count.setColor(R.color.newSuccessColor)
            holder.binding.shape.setBackgroundExtension(R.drawable.circle_green)
        } else {
            holder.binding.tick.makeGone()
            holder.binding.count.setColor(R.color.buttonBackground)
            holder.binding.shape.setBackgroundExtension(R.drawable.circle_blue)
        }
    }

    private fun showDataOnCell(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.binding.instructionText.text = item
        holder.binding.count.text = "${position + 1}"
    }

    private fun showSocialButtonOnSecondItem(holder: ViewHolder, position: Int) {
        if (position == 2) {
            holder.binding.socialScroll.makeVisible()
        } else {
            holder.binding.socialScroll.makeGone()
        }
    }

    private fun showDoneIconOnFirstTwoItems(holder: ViewHolder, position: Int) {
        if (position < 2) {
            holder.binding.tick.makeVisible()
            holder.binding.count.setColor(R.color.newSuccessColor)
            holder.binding.shape.setBackgroundExtension(R.drawable.circle_green)
        } else {
            holder.binding.tick.makeGone()
            holder.binding.count.setColor(R.color.buttonBackground)
            holder.binding.shape.setBackgroundExtension(R.drawable.circle_blue)
        }
    }

}