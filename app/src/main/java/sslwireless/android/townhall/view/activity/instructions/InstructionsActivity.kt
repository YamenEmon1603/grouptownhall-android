package sslwireless.android.townhall.view.activity.instructions

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_instructions.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity

class InstructionsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instructions)
    }

    override fun viewRelatedTask() {
        initInstructions()

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initInstructions() {
        val result =
            getDataManager().mPref.prefGetTownhallKey()?.townhall?.instructions ?: arrayListOf()

        val bot = getDataManager().mPref.prefGetTownhallKey()?.townhall?.bot

        val instructionList = arrayListOf<String>()
        instructionList.add(getString(R.string.get_registered_sith_easy_app))
        instructionList.add(getString(R.string.please_upload_your_pictures))
        instructionList.add(getString(R.string.connect_with_chatbot))

        val resultList = result.map { it.description ?: "" } as ArrayList
        instructionList.addAll(resultList)

        instructionRecycler.adapter = InstructionAdapter(instructionList, bot, getDataManager())
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}