package sslwireless.android.townhall.view.activity.login

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.activity_login_step_1.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.operator.OperatorModel
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.activity.webview.WebViewActivity
import sslwireless.android.townhall.view.activity.registration.RegistrationActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import java.util.*

class LoginStep1Activity : BaseActivity(), ViewPager.OnPageChangeListener {
    private var mCallbackManager: CallbackManager? = null
    var mGoogleSignInClient: GoogleSignInClient? = null
    private var emailOrPhone: String? = null
    private var provider = ""
    private val cViewModel: CommunicationViewModel by viewModels()
    private var isNeedGoogleSignOut = false

    private val viewModel: LoginViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(LoginViewModel(getDataManager())))
            .get(LoginViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
       /* if (isNeedGoogleSignOut) {
            isNeedGoogleSignOut = false
            googleSignOut()
        }*/
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        isNeedGoogleSignOut = true
        if (result.data?.code == 200) {
            if (key == "Operators") {
                val operatorModels = result.data.data.fromResponse<List<OperatorModel>>() ?: return
                AppConstants.Operators.OPERATOR_LIST.clear()
                AppConstants.Operators.OPERATOR_LIST.addAll(operatorModels)
                getDataManager().mPref.prefOperator(operatorModels as ArrayList<OperatorModel>)
            } else if (result.data.ui == null) {
                if (result.data.status == "success" || result.data.status == "সফল") {
                    getDataManager().mPref.prefSetSocial(true)
                    val userData = result.data.data.fromResponse<UserData>()
                    getDataManager().mPref.prefSetCustomerToken(result.data.token!!)
                    getDataManager().mPref.prefLogin(userData!!)
                    startActivity(Intent(this, MainActivity::class.java))
                    finishAffinity()
                } else {
                    showToast(
                        this,
                        result.data?.message ?: getString(R.string.something_went_wrong)
                    )
                }
            } else if (result.data.ui == "registration") {
                getDataManager().mPref.prefSetSocial(true)
                val userData = result.data.data.fromResponse<UserData>()
                val intent = Intent(this, RegistrationActivity::class.java).putExtra(
                    "emailorphone",
                    userData?.email ?: ""
                ).putExtra("social_login_id", userData?.social_login_id ?: "")
                    .putExtra("name", userData?.name ?: "")
                    .putExtra("provider", provider)
                startActivity(intent)
            } else {
                showToast(this, result.data?.message ?: getString(R.string.something_went_wrong))
            }
        } else {
            showToast(this, result.data?.message ?: getString(R.string.something_went_wrong))
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_step_1)
        getDataManager().mPref.prefSetSocial(false)


       /* viewModel.apiGetOperator(
            this, this
        )*/
    }




    override fun viewRelatedTask() {
        enterLayout.setOnClickListener {
            val intent = Intent(this, LoginStep2Activity::class.java)
            startActivity(intent)
        }
        terms.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.terms.ordinal)
            startActivity(intent)
        }
        privacy.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.privacy.ordinal)
            startActivity(intent)
        }
       /* requestRechargeTV.setOnClickListener {
            val intent = Intent(this, RechargeActivity::class.java)
            intent.putExtra(RechargeActivity.KEY_FROM_RECHARGE_REQUEST, true)
            startActivity(intent)
        }*/

        val gso =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                //.requestIdToken(getString(R.string.default_web_client_id))
                //.requestIdToken("303443237001-3jhaqi72vasqrafh4nmuqitp5s4m9dk2.apps.googleusercontent.com")
                .requestIdToken("303443237001-mtth5ie1cj92763eapdoeuqh79n0qf74.apps.googleusercontent.com")
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        mCallbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    provider = AppConstants.Social.FACEBOOK
                    viewModel.onSocialLogin(
                        AppConstants.Social.FACEBOOK,
                        loginResult.accessToken.token,
                        this@LoginStep1Activity,
                        this@LoginStep1Activity
                    )
                }

                override fun onCancel() {
                }

                override fun onError(e: FacebookException) {
                }
            })

       /* facebookLogin.setOnClickListener {
            //LoginManager.getInstance().logOut()
            LoginManager.getInstance().logInWithReadPermissions(
                this,
                Arrays.asList("public_profile", "email")
            )
        }
        googleLogin.setOnClickListener {
            val signInIntent = mGoogleSignInClient!!.getSignInIntent()
            startActivityForResult(signInIntent, 10)
        }

        googleSignOut()*/
    }

    private fun googleSignOut() {
        val alreadyLoggedAccount = GoogleSignIn.getLastSignedInAccount(this)
        if (alreadyLoggedAccount != null) {
            mGoogleSignInClient?.signOut()?.addOnCompleteListener {
                //showToastMsg("Google Sign out successfully")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mCallbackManager!!.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 10) {
//            hideLoading();
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task?.getResult(ApiException::class.java)
                account?.idToken?.let {
                    provider = AppConstants.Social.GOOGLE
                    viewModel.onSocialLogin(AppConstants.Social.GOOGLE, it, this, this)
                }
                // account.getDisplayName());
                //callFbSigninApi();
            } catch (e: ApiException) {
//                showMessage(e.getMessage());
            }
        } else {

        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

    }

}