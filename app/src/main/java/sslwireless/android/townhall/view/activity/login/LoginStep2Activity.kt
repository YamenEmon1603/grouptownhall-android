package sslwireless.android.townhall.view.activity.login

import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.LocalValidator
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.webview.WebViewActivity
import sslwireless.android.townhall.view.activity.otp.OtpActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_login_step_2.*
import sslwireless.android.townhall.view.activity.desco.utils.Enums

class LoginStep2Activity : BaseActivity() {

    private lateinit var viewModel: LoginViewModel
    private var emailOrPhone: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_step_2)
        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(LoginViewModel(getDataManager())))
                .get(LoginViewModel::class.java)
    }

    override fun viewRelatedTask() {
        back.setOnClickListener {
            finish()
        }
        email_or_phone_et.doAfterTextChanged {
            email_or_phone_et.error=null
        }

        termsText.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.condition.ordinal)
            startActivity(intent)
        }

        proceed_btn.setOnClickListener {
            if (email_or_phone_et.text.isEmpty()) {
                email_or_phone_et.error = getString(R.string.enter_phone_email)
            } else {

                emailOrPhone = email_or_phone_et.text.toString()


                if (emailOrPhone!!.contains("@")) {
                    if (!LocalValidator.isEmailValid(emailOrPhone!!)) {
                        email_or_phone_et.error = getString(R.string.email_not_valid)
                        return@setOnClickListener
                    }
                } else {
                    if (!LocalValidator.isNewPhnValid(emailOrPhone!!)) {
                        email_or_phone_et.error = getString(R.string.phone_not_valid)
                        return@setOnClickListener
                    }
                }

                if (getDataManager().mPref.prefGetDeviceKey() != null) {
                    //showProgressDialog(getString(R.string.loading))
                    viewModel.fetchUserStatus(
                        emailOrPhone!!,
                        this,
                        this
                    )
                }
            }
//           val intent = Intent(this, UpdateNumberActivity::class.java)
//            startActivity(intent)
        }

        editTextFocusEventHandler()

        //initTermsText()
    }

    private fun editTextFocusEventHandler() {
        email_or_phone_et.setOnFocusChangeListener { view, b ->
            if (b) {
                numEmail.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                numEmail.setPadding(30, 18, 30, 10)
            }
        }
    }

    private fun initTermsText() {
        termsText.setText(
            resources.getString(R.string.by_clicking_proceed_you_agree_to_easy_s_terms_conditions),
            TextView.BufferType.SPANNABLE
        )
        val span = termsText.text as Spannable
        span.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.terms_text_color_login)), 0,
            termsText.length(),
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.login_backborder)), 42,
            termsText.length(),
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        //hideProgressDialog()

        if (result.data!!.code == 200) {

            if (result.data.ui.equals("registration")) {
                showToast(this,"User does not exist")
                /*val intent = Intent(this, RegistrationActivity::class.java).putExtra(
                    "emailorphone",
                    emailOrPhone
                ).putExtra("social_login_id","")
                        .putExtra("name","").putExtra("provider","")
                startActivity(intent)*/

            } else if (result.data.ui.equals("password")) {
                val intent =
                    Intent(this, LoginStep3Activity::class.java).putExtra(
                        LoginStep3Activity.BUNDLE_EMAILORPHONE,
                        emailOrPhone
                    )
                startActivity(intent)
            } else if (result.data.ui.equals("update")) {

            } else if (result.data.ui.equals("otp")) {
                val intent = Intent(this, OtpActivity::class.java)
                if (emailOrPhone!!.contains("@")) {
                    intent.putExtra(OtpActivity.BUNDLE_EMAIL, emailOrPhone)
                } else {
                    intent.putExtra(OtpActivity.BUNDLE_PHONE, emailOrPhone)
                }
                startActivity(intent)
            }
        } else {
            showToast(this, result.data.message!!)
        }

    }

}