package sslwireless.android.townhall.view.activity.login

import android.content.Intent
import android.os.Bundle
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProviders
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_login_step3.*
import kotlinx.android.synthetic.main.activity_login_step3.back
import kotlinx.android.synthetic.main.activity_login_step3.proceed_btn
import kotlinx.android.synthetic.main.activity_login_step3.termsText
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.data.model.user.MKey
import sslwireless.android.townhall.data.model.user.OTP
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.UpdatePhone.UpdateNumberActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.activity.forgetPassword.ForgetPassBottomSheet
import sslwireless.android.townhall.view.activity.otp.OtpActivity
import sslwireless.android.townhall.view.activity.webview.WebViewActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse

class LoginStep3Activity : BaseActivity(), ForgetPassBottomSheet.BottomSheetListener {
    private lateinit var emailOrPhone: String
    var viewModel: LoginStep3ViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_step3)
    }

    companion object {
        const val BUNDLE_EMAILORPHONE = "emailorphone"
    }

    override fun viewRelatedTask() {
        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(LoginStep3ViewModel(getDataManager())))
                .get(LoginStep3ViewModel::class.java)

        emailOrPhone = intent.getStringExtra(BUNDLE_EMAILORPHONE) ?: ""

        userId.text = emailOrPhone

        userPass.doOnTextChanged { text, start, before, count ->
            userPass.error = null
            pass.isEndIconVisible = true;
        }

        proceed_btn.setOnClickListener {
            if (userPass.text!!.isEmpty()) {
                userPass.error = getString(R.string.enter_password)
                pass.isEndIconVisible = false;
            } else {
                //showProgressDialog(getString(R.string.loading))
                viewModel?.userLogin(
                    userId.text.toString(),
                    userPass.text.toString(),
                    this,
                    this
                )
            }
        }

        forget_password_tv.setOnClickListener {
            val forgotPasswordConfirmation = ForgetPassBottomSheet()
            forgotPasswordConfirmation.show(supportFragmentManager, "1234")
        }

        back.setOnClickListener {
            onBackPressed()
        }

        termsText.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.condition.ordinal)
            //intent.putExtra(WebViewActivity.BUNDLE_PARAM_URL, WebViewActivity.TERMS_URL)
            startActivity(intent)
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            LoginStep3ViewModel.KEY_USER_LOGIN -> {
                val baseData = result.data ?: return
                if (baseData.code == 200) {
                    if (baseData.ui.equals("otp")) {
                        showToast(this, baseData.message)
                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<OTP> = moshi.adapter(OTP::class.java)
                        val otp = adapter.fromJsonValue(baseData.data)

                        val intent = Intent(this, OtpActivity::class.java)
                        intent.putExtra(OtpActivity.BUNDLE_PHONE, emailOrPhone)
                        startActivity(intent)
                    } else if (baseData.ui.equals("mobile")) {
                        showToast(this, baseData.message)
                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<MKey> = moshi.adapter(MKey::class.java)
                        val mKey = adapter.fromJsonValue(baseData.data)
                        val intent = Intent(this, UpdateNumberActivity::class.java)
                        intent.putExtra(OtpActivity.BUNDLE_MKEY, mKey!!.mKey.toString())
                        intent.putExtra(OtpActivity.BUNDLE_EMAIL, emailOrPhone)
                        startActivity(intent)
                    } else {
                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<UserData> = moshi.adapter(UserData::class.java)
                        val userData = adapter.fromJsonValue(baseData.data!!)
                        val token = baseData.token
                        getDataManager().mPref.prefSetCustomerToken(token!!)
                        getDataManager().mPref.prefLogin(userData!!)
                        viewModel?.apiCheckImageUploadStatus(this, this)
                        //startActivity(Intent(this, MainActivity::class.java))
                        //finishAffinity()
                    }
                } else {
                    showToast(this, baseData.message)
                }
            }
            LoginStep3ViewModel.KEY_FORGOT_PASSWORD -> {
                val baseData = result.data ?: return
                showToast(this, baseData.message)

                if (baseData.ui.equals("otp")) {
                    val intent = Intent(this, OtpActivity::class.java)
                    if (emailOrPhone.contains("@")) {
                        intent.putExtra(OtpActivity.BUNDLE_EMAIL, emailOrPhone)
                    } else {
                        intent.putExtra(OtpActivity.BUNDLE_PHONE, emailOrPhone)
                    }
                    intent.putExtra(OtpActivity.BUNDLE_IS_FORGOT, true)
                    startActivity(intent)
                } else if (baseData.ui.equals("mobile")) {
                    val moshi: Moshi = Moshi.Builder().build()
                    val adapter: JsonAdapter<MKey> = moshi.adapter(MKey::class.java)
                    val mKey = adapter.fromJsonValue(baseData.data)

                    val intent = Intent(this, UpdateNumberActivity::class.java)
                    intent.putExtra(OtpActivity.BUNDLE_MKEY, mKey?.mKey.toString())
                    intent.putExtra(OtpActivity.BUNDLE_EMAIL, emailOrPhone)
                    intent.putExtra(OtpActivity.BUNDLE_IS_FORGOT, true)
                    startActivity(intent)
                }
            }
            else -> {
                val baseData = result.data ?: return
                if (baseData.code == 200) {
                    val deviceKey = result.data.data?.fromResponse<TownHallResponse>() ?: return
                    getDataManager().mPref.prefSetTownHallResponseKey(deviceKey)
                    getDataManager().mPref.prefLoginIsValid()
                    if (deviceKey?.imageUploadStatus == 0) {
                        val mobile = intent.getStringExtra(BUNDLE_EMAILORPHONE) ?: return
                        gotoUploadPicture(this, mobile)
                    } else {
                        startActivity(Intent(this, MainActivity::class.java))
                        finishAffinity()
                    }

                } else {
                    showToast(this, baseData.message)
                    //startActivity(Intent(this, UploadPictureActivity::class.java))
                    //finishAffinity()
                }
            }
        }

    }

    override fun onClicked(text: String) {
        //showProgressDialog(getString(R.string.loading))
        viewModel?.apiForgetPassword(emailOrPhone, this, this)
    }

}
