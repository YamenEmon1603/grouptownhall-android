package sslwireless.android.townhall.view.activity.login

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class LoginStep3ViewModel(val dataManager: DataManager) : BaseViewmodel(dataManager) {
    companion object {
        const val KEY_USER_LOGIN = "userLoginKey"
        const val KEY_FORGOT_PASSWORD = "forgot-password-otp"
        const val KEY_IMAGE_UPLOAD = "KEY_IMAGE_UPLOAD"
    }

    fun userLogin(
        userId: String,
        userPass: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiUserLogin(
            userId,
            userPass,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_USER_LOGIN))
        )
    }

    fun apiForgetPassword(
        mobile: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiForgetPassword(
            mobile,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_FORGOT_PASSWORD))
        )
    }

    fun apiCheckImageUploadStatus(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getTownhallContent(dataManager.mPref.prefGetCustomerToken()?:"","1",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "townhall_content"))
        )
    }
}