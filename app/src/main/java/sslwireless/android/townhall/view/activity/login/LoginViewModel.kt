package sslwireless.android.townhall.view.activity.login

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class LoginViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun fetchUserStatus(
        data: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUserStatus(
            data,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "userStatusKey"))
        )
    }

    fun onSocialLogin(
        provider: String,
        access_token:String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.onSocialLoginFb(
            provider,access_token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "socialLogin"))
        )
    }

    fun apiGetOperator(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetOperator(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "Operators"))
        )
    }
}