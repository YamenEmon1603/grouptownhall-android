package sslwireless.android.townhall.view.activity.login.slide

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.viewpager.SmartFragmentStatePagerAdapter

class SlideAdapter(fragmentManager: FragmentManager) : SmartFragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(mPosition: Int): Fragment {
        return when (mPosition) {
            0 -> SlideFragment.newInstance(R.drawable.slider1,R.string.pay_text)
            1 -> SlideFragment.newInstance(R.drawable.slider1,R.string.pay_text)
            else -> SlideFragment.newInstance(R.drawable.slider1,R.string.pay_text)
        }
    }

    override fun getCount(): Int = 1

}