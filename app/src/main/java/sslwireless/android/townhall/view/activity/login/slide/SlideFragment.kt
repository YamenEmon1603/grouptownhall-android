package sslwireless.android.townhall.view.activity.login.slide

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import sslwireless.android.townhall.R
import kotlinx.android.synthetic.main.fragment_slide.*
import sslwireless.android.townhall.view.utils.CommunicationViewModel
import sslwireless.android.townhall.view.utils.getLocale

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_IMAGE_RES_ID = "param1"
private const val ARG_TITLE_RES_ID = "param2"


class SlideFragment : Fragment() {
    private var mTitleResId:Int?=null
    private val viewModel by activityViewModels<CommunicationViewModel>()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            @DrawableRes
            val mDemographicImageResId = it.getInt(ARG_IMAGE_RES_ID)
            @StringRes
            mTitleResId = it.getInt(ARG_TITLE_RES_ID)
            mTitleResId?.let { it2->textView70.setText(it2) }
            imageView22.setImageResource(mDemographicImageResId)
        }

        viewModel.onLanguageChange.observe(viewLifecycleOwner, Observer {
            val con = requireContext().getLocale(it)
            val value = con.resources.getString(mTitleResId?:R.string.pay_text)
            textView70.text = value
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_slide, container, false)
    }

    companion object {

        @JvmStatic
        fun newInstance(@DrawableRes mDemographicImageResId: Int, @StringRes mTitleResId: Int) =
            SlideFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_IMAGE_RES_ID, mDemographicImageResId)
                    putInt(ARG_TITLE_RES_ID, mTitleResId)
                }
            }
    }
}