package sslwireless.android.townhall.view.activity.loginDevice

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.data.model.SampleModel.LoginDevice
import sslwireless.android.townhall.view.adapter.LoginSessionAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet
import sslwireless.android.townhall.view.utils.fromResponse
import sslwireless.android.townhall.view.utils.showErrorToast
import kotlinx.android.synthetic.main.activity_where_you_are_logged_in.*

class WhereYouAreLoggedInActivity : BaseActivity(), LoginSessionAdapter.LogoutOptionClickListener,
    SessionLogoutBottomSheet.IBottomSheetDialogClicked {

    private val viewModel: WhereYouAreLoggedInViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(WhereYouAreLoggedInViewModel(getDataManager())))
            .get(WhereYouAreLoggedInViewModel::class.java)
    }
    private var loginSessionsList: ArrayList<LoginDevice> = ArrayList()
    private val adapter: LoginSessionAdapter by lazy {
        LoginSessionAdapter(loginSessionsList,this,getDataManager().mPref.prefGetDeviceKey()?.deviceKey?:"")
    }
    private var position =-1
    override fun onCancelButtonClicked() {
    }

    override fun onSessionLogoutButtonClicked(tag:String) {
        if (tag=="All") {
            viewModel.apiLogoutAllDevice(
                getDataManager().mPref.prefGetCustomerToken()!!,
                this,
                this
            )
        } else {
            val sessionKey = loginSessionsList[position].sessionKey
            viewModel.apiLogoutSingleDevice(
                getDataManager().mPref.prefGetCustomerToken()!!,
                sessionKey,
                this,
                this
            )
        }

    }

    override fun onLogoutOptionClick(position: Int) {
        this.position = position
       // try {
        val sessionLogoutBottomSheet =
            SessionLogoutBottomSheet().apply {
                arguments = Bundle().apply {
                    putString(
                        "title",
                        this@WhereYouAreLoggedInActivity.getString(R.string.you_can_log_out_of_this_session)
                    )
                    putString(
                        "subText",
                        this@WhereYouAreLoggedInActivity.getString(R.string.secure_your_account)
                    )
                    putString("btn1", this@WhereYouAreLoggedInActivity.getString(R.string.cancel))
                    putString("btn2", this@WhereYouAreLoggedInActivity.getString(R.string.log_out))
                }
            }
        sessionLogoutBottomSheet.show(supportFragmentManager, sessionLogoutBottomSheet.tag)
        sessionLogoutBottomSheet.setBottomDialogListener(this)
        //} catch (e:Exception) {}
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_where_you_are_logged_in)
    }

    override fun viewRelatedTask() {

        viewModel.apiGetLoginDevices(
            getDataManager().mPref.prefGetCustomerToken()!!,
            this,
            this
        )

        logout_from_all_device.setOnClickListener {
            val sessionLogoutBottomSheet =
                SessionLogoutBottomSheet().apply {
                    arguments = Bundle().apply {
                        putString(
                            "title",
                            this@WhereYouAreLoggedInActivity.getString(R.string.sign_out_all_title)
                        )
                        putString(
                            "subText",
                            ""
                        )
                        putString("btn1", this@WhereYouAreLoggedInActivity.getString(R.string.cancel))
                        putString("btn2", this@WhereYouAreLoggedInActivity.getString(R.string.log_out))
                        putString("tag", "All")
                    }
                }
            sessionLogoutBottomSheet.show(supportFragmentManager, "1234")
            sessionLogoutBottomSheet.setBottomDialogListener(this)
        }

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code==200) {
            when(key) {
                "get-login-devices"->{
                    val list = result.data.data.fromResponse<List<LoginDevice>>() as ArrayList<LoginDevice>
                    getDataManager().mPref.prefGetDeviceKey()?.deviceKey?.let { deviceKey->
                        val activeList = list.filter { it.sessionKey== deviceKey}
                        val  inActiveList = list.filter { it.sessionKey!=deviceKey }
                        val finalList = activeList+inActiveList
                        loginSessionsList.clear()
                        loginSessionsList.addAll(finalList)
                        login_session_rev_view.adapter = adapter
                    }

                }
                "logout_single_device" ->{
                    showToast(this, getString(R.string.logged_out_session))
                    if (loginSessionsList[position].sessionKey==getDataManager().mPref.prefGetDeviceKey()?.deviceKey) {
                        getDataManager().mPref.prefLogout(this)
                    } else {
                        loginSessionsList.removeAt(position)
                        adapter.notifyDataSetChanged()
                    }
                }
                else->{
                    showToast(this, getString(R.string.logged_out_all_session))
                    getDataManager().mPref.prefLogout(this)
                }
            }

        } else {
            showErrorToast(result.data.message)
        }
    }

    override fun onError(err: Throwable) {
        showToast(this, getString(R.string.logged_out_all_session))
        getDataManager().mPref.prefLogout(this)
    }
}
