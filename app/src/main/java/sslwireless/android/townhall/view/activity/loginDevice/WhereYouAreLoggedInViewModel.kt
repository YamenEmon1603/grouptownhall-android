package sslwireless.android.townhall.view.activity.loginDevice

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class WhereYouAreLoggedInViewModel(private val dataManager: DataManager): BaseViewmodel(dataManager) {

    fun apiGetLoginDevices(
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetLoginDevice(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "get-login-devices"))
        )
    }

    fun apiLogoutSingleDevice(
        token: String,
        sessionKey: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiLogoutSingleDevice(
            token,
            sessionKey,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "logout_single_device"))
        )
    }

    fun apiLogoutAllDevice(
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiLogoutAllDevice(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "logout_all_device"))
        )
    }
}