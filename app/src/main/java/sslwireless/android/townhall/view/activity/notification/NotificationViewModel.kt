package sslwireless.android.townhall.view.activity.notification

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class NotificationViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun getNotifications(
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getNotifications(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "notifications"))
        )
    }

    fun onDeleteNotifications(
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.onDeleteNotifications(
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "delete notifications"))
        )
    }
}