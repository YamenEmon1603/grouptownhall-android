package sslwireless.android.townhall.view.activity.notification

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.data.model.SampleModel.Notification
import sslwireless.android.townhall.view.adapter.NotificationRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import kotlinx.android.synthetic.main.activity_notifications.*
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet

class NotificationsActivity : BaseActivity(), SessionLogoutBottomSheet.IBottomSheetDialogClicked {

    private val viewModel: NotificationViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(NotificationViewModel(getDataManager())))
            .get(NotificationViewModel::class.java)
    }
    private val weekNotificationRecyclerAdapter: NotificationRecyclerAdapter by lazy {
        NotificationRecyclerAdapter(thisWeekNotifications,false)
    }
    private val todayNotificationRecyclerAdapter: NotificationRecyclerAdapter by lazy {
        NotificationRecyclerAdapter(todayNotifications,true)
    }
    var todayNotifications: ArrayList<Notification> = ArrayList()
    var thisWeekNotifications = ArrayList<Notification>()
    private val sessionLogoutBottomSheet: SessionLogoutBottomSheet by lazy {
        SessionLogoutBottomSheet().apply {
            arguments = Bundle().apply {
                putString("title",this@NotificationsActivity.getString(R.string.do_you_want_to_clear_all))
                putString("subText",this@NotificationsActivity.getString(R.string.your_all_previous_notification_will))
                putString("btn1",this@NotificationsActivity.getString(R.string.yes))
                putString("btn2",this@NotificationsActivity.getString(R.string.no))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notifications)
    }

    override fun viewRelatedTask() {
        this_week_notification_recycler_view.apply {
            setHasFixedSize(true)
            adapter = weekNotificationRecyclerAdapter
        }
        today_notification_recycler_view.apply {
            setHasFixedSize(true)
            adapter = todayNotificationRecyclerAdapter
        }
        getNotification()
        clear_all_tv.setOnClickListener {
            sessionLogoutBottomSheet.show(supportFragmentManager, sessionLogoutBottomSheet.tag)
            sessionLogoutBottomSheet.setBottomDialogListener(this)
        }
    }

    private fun getNotification() {
        viewModel.getNotifications(
            getDataManager().mPref.prefGetCustomerToken()!!,
            this,
            this
        )
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            if (key == "notifications") {
                val res = ArrayList<Notification>(result.data.data.fromResponse<List<Notification>>()!!)
                todayNotifications.addAll(res.filter { it.notifiedAt.split(" ")[0].isToday("yyyy-MM-dd") })
                thisWeekNotifications.addAll(res.filter {
                    !it.notifiedAt.split(" ")[0].isToday("yyyy-MM-dd")
                })
                if (thisWeekNotifications.isEmpty()) {
                    thisWeekGroup.makeGone()
                } else {
                    thisWeekGroup.makeVisible()
                    weekNotificationRecyclerAdapter.notifyDataSetChanged()
                }
                if (todayNotifications.isEmpty()) {
                    todayGroup.makeGone()
                    new_notification_banner_container_cl.makeGone()
                } else {
                    todayGroup.makeVisible()
                    new_notification_banner_container_cl.makeVisible()
                    new_notification_count_tv.text = ""+todayNotifications.size+" New"
                    todayNotificationRecyclerAdapter.notifyDataSetChanged()
                }

                if (thisWeekNotifications.isEmpty() && todayNotifications.isEmpty()) {
                    noNotification.makeVisible()
                    clear_all_tv.makeGone()
                } else {
                    noNotification.makeGone()
                    clear_all_tv.makeVisible()
                }
            } else if (key=="delete notifications") {
                todayNotifications.clear()
                thisWeekNotifications.clear()
                weekNotificationRecyclerAdapter.notifyDataSetChanged()
                todayNotificationRecyclerAdapter.notifyDataSetChanged()
                todayGroup.makeGone()
                thisWeekGroup.makeGone()
                clear_all_tv.makeGone()
                new_notification_banner_container_cl.makeGone()
                noNotification.makeVisible()
            }
        } else {
            showErrorToast(result.data.message)
        }
    }

    override fun onCancelButtonClicked() {
        viewModel.onDeleteNotifications(
            getDataManager().mPref.prefGetCustomerToken()!!,
            this,
            this
        )
    }

    override fun onSessionLogoutButtonClicked(tag:String) {
    }

}
