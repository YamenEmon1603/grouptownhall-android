package sslwireless.android.townhall.view.activity.offer


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.FilterModel
import sslwireless.android.townhall.databinding.CategoryItemLayoutBinding
import kotlinx.android.synthetic.main.offer_filter_bottom_sheet.*


class OfferBottomSheet(private val listener: OfferFilterListener) : BottomSheetDialogFragment() {

    private var filterCategoryList = ArrayList<FilterModel>()
    private val isSelected = HashMap<String,Boolean>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.offer_filter_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        popularGroup.setOnClickListener {
            popularTick.visibility = View.VISIBLE
            newTick.visibility = View.GONE
        }
        newGroup.setOnClickListener {
            newTick.visibility = View.VISIBLE
            popularTick.visibility = View.GONE
        }
        filterCategoryList =
            requireArguments().getSerializable("categoryList") as ArrayList<FilterModel>

        processLayout()

        textView17.setOnClickListener {
            newTick.visibility = View.GONE
            popularTick.visibility = View.GONE
            categoryLayout.removeAllViews()
            filterCategoryList.forEachIndexed { index, filterModel ->
                val item = filterCategoryList[index]
                item.isSelected = false
                filterCategoryList[index] = item
            }
            processLayout()
        }

        applyBtn.setOnClickListener {
            val list = filterCategoryList.filter { it.isSelected }.map { it.categoryName }
            when {
                popularGroup.visibility == View.VISIBLE -> {
                    listener.onApplyFilter(textView18.text.toString(), list)
                }
                newGroup.visibility == View.VISIBLE -> {
                    listener.onApplyFilter(textView20.text.toString(), list)
                }
                else -> {
                    listener.onApplyFilter("None", list)
                }
            }
            dismissAllowingStateLoss()
        }

    }


    private fun processLayout() {
        filterCategoryList.forEachIndexed { index, value->
            val binding: CategoryItemLayoutBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.category_item_layout, null, false
            )
            binding.categoryName.text = value.categoryName
            binding.checkbox.isChecked = value.isSelected
            binding.rootLayout.setOnClickListener {
                binding.checkbox.isChecked = !binding.checkbox.isChecked
                val item = filterCategoryList[index]
                item.isSelected = binding.checkbox.isChecked
                filterCategoryList[index] = item
            }
            categoryLayout.addView(binding.root)

        }
    }

    interface OfferFilterListener {
        fun onApplyFilter(sortBy: String, filterCategoryList: List<String>)
    }


}
