package sslwireless.android.townhall.view.activity.offer

import android.os.Bundle
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.fragment.offer.OfferFragment


class OffersActivity : BaseActivity() {
    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offers)
    }

    override fun viewRelatedTask() {
        addFragment(true, R.id.container,
            OfferFragment()
        )
    }
}