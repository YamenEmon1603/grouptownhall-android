package sslwireless.android.townhall.view.activity.organization

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_organization.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.organization.OrganizationPojo
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.loadImage
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible

class OrganizationActivity : BaseActivity() {
    private val organizationList = ArrayList<OrganizationPojo>()
    private lateinit var adapter: OrganizationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organization)
    }

    override fun viewRelatedTask() {
        organizationList.add(OrganizationPojo(R.drawable.ic_cgl,"Concorde Garments Lts."))
        organizationList.add(OrganizationPojo(R.drawable.osman,"Osman Interlining Ltd."))
        organizationList.add(OrganizationPojo(R.drawable.ssl,"SSL Wireless"))

        adapter = OrganizationAdapter(
            onItemClick = {data, position, view ->

            },
            list = organizationList
        )
        organizationRecycler.adapter = adapter

        self.setOnClickListener {
            oneTick.loadImage(R.drawable.ic_tickk)
            twoTick.loadImage(R.drawable.ic_non_tick)
            numberInputField.makeGone()
        }
        other.setOnClickListener {
            oneTick.loadImage(R.drawable.ic_non_tick)
            twoTick.loadImage(R.drawable.ic_tickk)
            numberInputField.makeVisible()
        }
    }



    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}