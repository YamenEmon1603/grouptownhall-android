package sslwireless.android.townhall.view.activity.organization

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.organization.OrganizationPojo
import sslwireless.android.townhall.databinding.OrganzationItemLayoutBinding
import sslwireless.android.townhall.view.utils.*

class OrganizationAdapter (val onItemClick: (data: OrganizationPojo?, position: Int, view: View) -> Unit,private val list:ArrayList<OrganizationPojo>):
    RecyclerView.Adapter<OrganizationAdapter.ViewHolder>() {

    private var selectedCount=0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(DataBindingUtil.inflate(
            LayoutInflater.from(parent.context)
            , R.layout.organzation_item_layout
            , parent, false
        ))
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        holder.binding.name.text = item.name
        holder.binding.img.loadImage(item.icon)
        if (position==selectedCount) {
            holder.binding.mRoot.setBackgroundExtension(R.drawable.bg_outlined_purple_2)
            holder.binding.selected.loadImage(R.drawable.ic_tickk)
        } else {
            holder.binding.mRoot.setBackgroundExtension(R.drawable.bg_outlined_gray_2)
            holder.binding.selected.loadImage(R.drawable.ic_non_tick)
        }

        holder.binding.mRoot.setOnClickListener {
            selectedCount = position
            notifyDataSetChanged()
            onItemClick(item,position,holder.binding.mRoot)

        }
    }


    override fun getItemCount(): Int = list.size



    class ViewHolder(val binding: OrganzationItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {

    }
}