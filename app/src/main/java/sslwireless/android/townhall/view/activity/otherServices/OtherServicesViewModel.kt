package sslwireless.android.townhall.view.activity.otherServices

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.SubmitModel
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class OtherServicesViewModel @Inject
constructor(dataManager: DataManager) : BaseViewmodel(dataManager) {

    val dataManager = dataManager

    fun utilityService(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.utilityService(
            dataManager.mPref.prefGetDeviceKey()?.deviceKey ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "utilityService"))
        )
    }

    fun billInfo(
        utility_bill_id: String,
        mHashmap: HashMap<String, SubmitModel>,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.billInfo(
            utility_bill_id,
            mHashmap,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "billInfo"))
        )
    }

    fun billPayment(
        utility_bill_id: String,
        transaction_id: String,
        is_mobile: String,
        token: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.billPayment(
            utility_bill_id,
            transaction_id,
            is_mobile,
            token,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "billPayment"))
        )
    }

    fun validateTransaction(
        transactionId: String, lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiPaymentStatus(
            transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "validation"))
        )

    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating, transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }
}