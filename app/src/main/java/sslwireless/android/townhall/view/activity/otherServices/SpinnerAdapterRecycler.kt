package sslwireless.android.townhall.view.activity.otherServices

import android.content.Context
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.data.model.utility.OptionsItem
import sslwireless.android.townhall.data.model.utility.UtilityBillTypesItem
import sslwireless.android.townhall.databinding.CustomUserSpinnerBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeInvisible
import sslwireless.android.townhall.view.utils.makeVisible

class SpinnerAdapterRecycler(
    itemView: ViewDataBinding,
    context: Context,
    private val type: String,
    private var spinnerMultiList: ArrayList<String>
) :
    BaseViewHolder2(itemView.root) {

    var binding = itemView as CustomUserSpinnerBinding
    var mContext: Context = context

    override fun <T> onBind(
        position: Int,
        itemModel: T,
        listener: IAdapterListener
    ) {
        if (type == "billType") {
            binding.spinnerDataItem.makeVisible()
            binding.spinnerDataItemCheck.makeGone()
            itemModel as UtilityBillTypesItem

//            if (LanguageHelper.getLocale(mContext) == LanguageLocales.BANGLA) {
//                binding.spinnerDataItem.text = itemModel.bnTitle
//            } else {
//                binding.spinnerDataItem.text = itemModel.title
//            }
            binding.spinnerDataItem.text = itemModel.title

            binding.root.setOnClickListener {
                listener.clickListener(position, itemModel, binding.root)
            }
        } else if (type == "itemSpinner") {
            binding.spinnerDataItem.makeVisible()
            binding.spinnerDataItemCheck.makeGone()
            itemModel as OptionsItem

            binding.spinnerDataItem.text = itemModel.value

            binding.root.setOnClickListener {
                listener.clickListener(position, itemModel, binding.root)
            }
        } else if (type == "multiItemSpinner") {
            binding.spinnerDataItem.makeInvisible()
            binding.spinnerDataItemCheck.makeVisible()
            itemModel as OptionsItem
            binding.spinnerDataItemCheck.isChecked= spinnerMultiList.contains(itemModel.value)
            binding.spinnerDataItemCheck.text = itemModel.value
            binding.spinnerDataItemCheck.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    itemModel.value?.let { spinnerMultiList.add(it) }

                } else {
                    itemModel.value?.let { spinnerMultiList.remove(it)}
                }
            }
        }
    }
}