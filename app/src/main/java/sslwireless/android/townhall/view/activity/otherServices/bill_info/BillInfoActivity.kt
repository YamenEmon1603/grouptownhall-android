package sslwireless.android.townhall.view.activity.otherServices.bill_info

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import sslwireless.android.townhall.view.IAdapterListener
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.activity_bill_info.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.SubmitModel
import sslwireless.android.townhall.data.model.UtilityBillInfoData
import sslwireless.android.townhall.data.model.bill_api.BillApiResponse
import sslwireless.android.townhall.data.model.bill_info.BillInfoModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import sslwireless.android.townhall.data.model.utility_payment.UtilityPaymentModel
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.UtilityBillsActivity
import sslwireless.android.townhall.view.activity.otherServices.OtherServicesViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.*
import java.util.*
import kotlin.collections.HashMap


class BillInfoActivity : BaseActivity(), SSLCTransactionResponseListener {
    private var utilityBillInfoId: String? = null
    private var modelData: ServiceListsItem? = null
    private var descoModel: BillInfoModel? = null
    private lateinit var viewModel: OtherServicesViewModel
    private var mHashMap = HashMap<String, SubmitModel>()
    private var mStoreUtilityData2 = ArrayList<UtilityBillInfoData>()
    private var tempList = ArrayList<UtilityBillInfoData>()
    private var newDialog: Dialog? = null
    private var sslCommerzIntegrationModel: SslCommerzIntegrationModel? = null
    private val lang by lazy { getDataManager().mPref.prefGetLanguage() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bill_info)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(OtherServicesViewModel(getDataManager()))
            )
                .get(OtherServicesViewModel::class.java)
    }

    override fun viewRelatedTask() {
        arrowBack.setOnClickListener { finish() }

        modelData = intent.getSerializableExtra("modelData") as ServiceListsItem
        descoModel = intent.getSerializableExtra("billInfoModel") as BillInfoModel
        mHashMap = intent.getSerializableExtra("storeData") as HashMap<String, SubmitModel>

        utilityBillInfoId = intent.getStringExtra("utilityBillInfoId")

        val note = descoModel?.data?.note
        if (note.isNullOrEmpty()) {
            noteTextView.visibility = View.GONE
        } else {
            noteTextView.visibility = View.VISIBLE
            noteTextView.text = descoModel?.data?.note
        }

        initBillInfoData()
        textView13.text=  modelData?.title +" Bill Details"

        Glide.with(this)
            .load(modelData?.logo)
            .into(operatorLogo)

        operatorName.text = modelData?.title

        proceedToPay.setOnClickListener {
            viewModel.billPayment(
                utilityBillInfoId.toString(),
                descoModel?.data?.transactionId.toString(),
                "true",
                getDataManager().mPref.prefGetCustomerToken().toString(),
                this,
                this
            )
        }
    }

    /**
     * iterates through class
     * makes list with parameters and value
     */
    private fun initBillInfoData() {
        val item = descoModel?.data?.data
        item?.forEach {
            tempList.add(
                UtilityBillInfoData(
                    it.key,
                    it.value,
                    false
                )
            )
        }

        initWithAmountData()
    }

    /**
     * make data with amount separated
     * show bill info
     */
    private fun initWithAmountData() {
        val dividerList = resources.getStringArray(R.array.divider_list)
        for (itemData in tempList.iterator()) {
            val isAmount = dividerList.contains(itemData.parameter)
            val words = itemData.parameter.split("_").toMutableList()
            var output = ""

            for (word in words) {
                output += word.capitalize() + " "
            }
            output = output.replace("Vat", "VAT").trim()
            val data = try {
                if (isAmount) {
                    "৳" + itemData.dataValue
                } else {
                    itemData.dataValue ?: ""
                }
            } catch (e: Exception) {
                itemData.dataValue ?: ""
            }

            mStoreUtilityData2.add(
                UtilityBillInfoData(
                    output,
                    data,
                    isAmount
                )
            )
        }

        tempList.clear()
        initbillInfoRecyclerView(mStoreUtilityData2)
    }

    private fun initbillInfoRecyclerView(transactions: ArrayList<UtilityBillInfoData>?) {
        itemRecycler.layoutManager = LinearLayoutManager(this)

        itemRecycler.adapter = BaseRecyclerAdapter(this, object :
            IAdapterListener {

            override fun <T> clickListener(position: Int, model: T, view: View) {

            }

            override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {
                return BillInfoViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.custom_bill_info_recycler,
                        parent,
                        false
                    ), this@BillInfoActivity
                )
            }

        }, transactions as ArrayList<UtilityBillInfoData>)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            "billPayment" -> {
                val type = object : TypeToken<UtilityPaymentModel>() {}.type

                val baseModel = Gson().fromJson<UtilityPaymentModel>(
                    Gson().toJson(result.data),
                    UtilityPaymentModel::class.java
                )
                sslCommerzIntegrationModel = result.data?.data.fromResponse()

                if (baseModel.code == 200) {
                    sslCommerzIntegrationModel?.let {
                        initSdk(this, it, getDataManager(), false)
                    }
                } else if (baseModel.code == 422) {
                    showEditDialog(false, baseModel.message.toString())
                } else if (baseModel.code == 401) {
                    showToast(this, baseModel.message.toString())
                    getDataManager().mPref.prefLogout(this)
                } else {
                    showToast(this, baseModel.message.toString())
                }
            }
            "validation" -> {
                if (result.data?.code == 200) {
                    try {
                        val baseModel = Gson().fromJson<BillApiResponse>(
                            Gson().toJson(result.data),
                            BillApiResponse::class.java)

                       /* val mainData = Gson().fromJson<BillStatusResponse>(
                            Gson().toJson(baseModel.data!!.orderData),
                            BillStatusResponse::class.java)*/
                        val msg = "Your Bill payment of "+baseModel.data!!.paidAmount+" was successful for the following  account"

                    }catch (e:Exception){

                    }
                    showEditDialog(true, result.data.message)
                } else {
                    showEditDialog(
                        false,
                        result.data?.message ?: getString(R.string.something_went_wrong)
                    )
                }

            }
        }
    }


    override fun onError(err: Throwable) {

    }

    private fun showEditDialog(status: Boolean, returnMessage: String) {
        newDialog = Dialog(this, R.style.customDialog)
        newDialog!!.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )

        if (status) {
            customDialogBinding.textView83.makeGone()
            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.pay_successfull)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.newSuccessColor
                )
            )
        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_bill_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()
            customDialogBinding.textView83.makeGone()
            customDialogBinding.textView86.makeVisible()

            customDialogBinding.textView84.text = getString(R.string.try_again)


            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            if (status) {
                newDialog?.dismiss()
                sslCommerzIntegrationModel?.let {
                    if (customDialogBinding.ratingBar.rating.toInt()>0) {
                        viewModel.apiRating(
                            customDialogBinding.ratingBar.rating.toInt().toString(),
                            it.transaction_id,
                            this,
                            this
                        )
                    }
                }
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            } else {
                newDialog?.dismiss()
                val intent = Intent(this, UtilityBillsActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            }
        }

        customDialogBinding.textView86.setOnClickListener {
            newDialog?.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        newDialog!!.setContentView(customDialogBinding.root)

        newDialog!!.show()
        newDialog!!.setCancelable(false)
    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        sslCommerzIntegrationModel?.let {
            viewModel.validateTransaction(it.transaction_id, this, this)
        }
    }

    override fun transactionFail(p0: String?) {
        showEditDialog(false, p0.toString())
    }

    override fun closed(message: String?) {

    }

    override fun onBackPressed() {
        startActivity(Intent(this, MainActivity::class.java))
        customBackPressed()
        finish()
    }
}