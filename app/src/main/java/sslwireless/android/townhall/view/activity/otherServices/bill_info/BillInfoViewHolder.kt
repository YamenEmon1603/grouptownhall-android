package sslwireless.android.townhall.view.activity.otherServices.bill_info

import android.content.Context
import android.view.View
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.data.model.UtilityBillInfoData
import sslwireless.android.townhall.databinding.CustomBillInfoRecyclerBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2

class BillInfoViewHolder(itemView: ViewDataBinding, context: Context) :
    BaseViewHolder2(itemView.root) {

    var binding = itemView as CustomBillInfoRecyclerBinding
    var mContext: Context = context
//    var afterText: String? = ""
    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {
        itemModel as UtilityBillInfoData

//        val mainStirng: String? = itemModel.parameter
//        val dateSplitter1 = mainStirng?.split("_")?.toTypedArray()
//        Log.e(TAG, "onBind: ${dateSplitter1?.size}")
//        if (dateSplitter1!!.size > 1) {
//            afterText = dateSplitter1[1]
//        }
//
//        binding.billNo.text = itemModel.parameter.replace("_", " ")
//
//        if (afterText?.toLowerCase() == "amount") {
//            if (itemModel.dataValue == "null") {
//                binding.billNoValue.text = ""
//            } else {
//                binding.billNoValue.text = "৳${itemModel.dataValue}"
//            }
//        } else {
//            if (itemModel.dataValue == "null") {
//                binding.billNoValue.text = ""
//            } else {
//                binding.billNoValue.text = itemModel.dataValue
//            }
//        }

        binding.billNo.text = itemModel.parameter

        if (itemModel.isAmount){
            binding.imageView31.visibility = View.VISIBLE
        } else {
            binding.imageView31.visibility = View.GONE
        }

        if (itemModel.dataValue == "null") {
            binding.billNoValue.text = ""
        } else {
            binding.billNoValue.text = itemModel.dataValue
        }

        binding.root.setOnClickListener {
            listener.clickListener(position, itemModel, binding.root)
        }
    }
}