package sslwireless.android.townhall.view.activity.otherServices.dynamic_services_form_box

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.SubmitModel
import sslwireless.android.townhall.data.model.bill_info.BillInfoModel
import sslwireless.android.townhall.data.model.utility.OptionsItem
import sslwireless.android.townhall.data.model.utility.ParameterListsItem
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import sslwireless.android.townhall.data.model.utility.UtilityBillTypesItem
import sslwireless.android.townhall.databinding.CustomSpinnerDialogBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.otherServices.OtherServicesViewModel
import sslwireless.android.townhall.view.activity.otherServices.SpinnerAdapterRecycler
import sslwireless.android.townhall.view.activity.otherServices.bill_info.BillInfoActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.SharedPrefHeaderSingleton
import kotlinx.android.synthetic.main.activity_dynamic_common_api.*
import kotlinx.android.synthetic.main.custom_spinner_dialog.*
import sslwireless.android.townhall.view.utils.AppConstants

class DynamicCommonApiActivity : BaseActivity() {

    private var modelData: ServiceListsItem? = null
    private var mOptions: List<OptionsItem?>? = null
    private var newDialog: Dialog? = null
    private var districtId: String? = ""
    private var utilityBillInfoId: String? = null
    private var validationChecker: Boolean = false
    private var mModel = 0
    private var mHashMap = HashMap<String, SubmitModel>()
    private var spinnerHash = HashMap<String, String>()
    private var spinnerMultiList = ArrayList<String>()
    private lateinit var viewModel: OtherServicesViewModel
    private var validatorArrayList = ArrayList<String>()
    private val lang by lazy { getDataManager().mPref.prefGetLanguage() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dynamic_common_api)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(OtherServicesViewModel(getDataManager()))
            )
                .get(OtherServicesViewModel::class.java)
    }

    override fun viewRelatedTask() {
        backGroup.setOnClickListener {
            finish()
        }

        modelData = intent.getSerializableExtra("modelData") as ServiceListsItem

        Glide.with(this)
            .load(modelData?.logo)
            .into(imageView23)

//        if (LanguageHelper.getLocale(this) == LanguageLocales.BANGLA) {
//            textView62.text = modelData?.bnTitle
//            textView79.text = modelData?.bnTitle
//        } else {
//        }
        textView62.text = if (lang== AppConstants.Language.ENGLISH) modelData?.title else modelData?.bnTitle
        textView79.text = if (lang==AppConstants.Language.ENGLISH) modelData?.title else modelData?.bnTitle

        if (modelData?.utilityBillTypes?.size == 1) {
            utilityBillInfoId = modelData?.utilityBillTypes!![0]?.id.toString()
            selectTypeGroup.visibility = View.GONE
            initOtherServicesRecyclerView(modelData?.utilityBillTypes!![0]?.parameterLists)
        }

        viewTypeSelection.setOnClickListener {
            viewTypeSelection.background =
                ContextCompat.getDrawable(this, R.drawable.selected_edittext)
            showSpinner("billType", modelData?.utilityBillTypes, mOptions)
        }

        proceedToPay.setOnClickListener {
            validatorArrayList.clear()

            for ((key, value) in mHashMap) {
               spinnerHash[value.data]?.let {
                   value.data= it
               }
                if (value.requiredCheck && TextUtils.isEmpty(value.data)) {
                    validatorArrayList.add(value.level)
                }
            }


            if (validatorArrayList.size > 0) {
                showToast(this, getString(R.string.all_data_text))
            } else {
                viewModel.billInfo(
                    utilityBillInfoId.toString(),
                    mHashMap,
                    this,
                    this
                )
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            "billInfo" -> {
                val type = object : TypeToken<BillInfoModel>() {}.type

                val baseModel = Gson().fromJson<BillInfoModel>(
                    Gson().toJson(result.data),
                    BillInfoModel::class.java
                )

                if (baseModel.code == 200) {
//                    showToast(this, "Success")
                    val intent = Intent(this, BillInfoActivity::class.java)
                    intent.putExtra("storeData", mHashMap)
                    intent.putExtra("billInfoModel", baseModel)
                    intent.putExtra("modelData", modelData)
                    intent.putExtra("utilityBillInfoId", utilityBillInfoId)
                    startActivity(intent)
                } else if (baseModel.code == 422) {
                    showToast(this, baseModel.message.toString())
                } else if (baseModel.code == 401) {
                    showToast(this, baseModel.message.toString())

                    getDataManager().mPref.prefLogout(this)
                }
            }
        }
    }

    override fun onError(err: Throwable) {

    }

    private fun initOtherServicesRecyclerView(transactions: List<ParameterListsItem?>?) {
        dynamicItemRecycler.layoutManager = LinearLayoutManager(this)

        dynamicItemRecycler.adapter = BaseRecyclerAdapter(this, object :
            IAdapterListener {

            override fun <T> clickListener(position: Int, model: T, view: View) {
                model as ParameterListsItem

                if (view.id == R.id.spinnerText) {
                    val type = object : TypeToken<ArrayList<OptionsItem>>() {}.type

                    val baseModel = Gson().fromJson<ArrayList<OptionsItem>>(
                        model.options,
                        type
                    )

                    mModel = position

                    Log.e("Options tag", "clickListener: ${Gson().toJson(baseModel)}")
                    if (model.type=="select") {
                        showSpinner("itemSpinner", modelData?.utilityBillTypes, baseModel)
                    } else {
                        showSpinner("multiItemSpinner", modelData?.utilityBillTypes, baseModel)
                    }
                }
            }

            override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {
                return DynamicCommonApiViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.custom_dynamic_common_api_layout,
                        parent,
                        false
                    ), this@DynamicCommonApiActivity, mHashMap,lang
                )
            }

        }, transactions as ArrayList<ParameterListsItem>)
    }

    private fun showSpinner(
        type: String,
        utilityBillTypes: List<UtilityBillTypesItem?>?,
        mOptions: List<OptionsItem?>?
    ) {
        hideKeyboard()
        newDialog = Dialog(this, R.style.customDialog)
        newDialog!!.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val customDialogBinding: CustomSpinnerDialogBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.custom_spinner_dialog, null, false
        )

        newDialog!!.setContentView(customDialogBinding.root)

        if (type == "billType") {
//            if (Locale.getDefault().language == LocaleManagerCustom.BANGLA) {
//                newDialog?.textView92?.text = getString(R.string.type_bill)
//            } else {
//                newDialog?.textView92?.text = getString(R.string.type_bill)
//            }

            newDialog?.textView92?.text = getString(R.string.type_bill)

            initRecyclerView(
                customDialogBinding.recyclerView2,
                "billType",
                newDialog!!, utilityBillTypes, mOptions
            )
        } else if (type == "itemSpinner" || type=="multiItemSpinner") {
            newDialog?.textView92?.text = SharedPrefHeaderSingleton.instance.getData(
                SharedPrefHeaderSingleton.spinnerHeaderName,
                null
            )

            initRecyclerView(
                customDialogBinding.recyclerView2,
                type,
                newDialog!!, utilityBillTypes, mOptions
            )
        }

        customDialogBinding.done.setOnClickListener {

        }
        customDialogBinding.cancle.setOnClickListener {
            newDialog!!.dismiss()
            SharedPrefHeaderSingleton.instance.setData(
                SharedPrefHeaderSingleton.recyclerItemNotify,
                spinnerMultiList.toString().replace("[","").replace("]","")
            )

            SharedPrefHeaderSingleton.instance.setIntData(
                SharedPrefHeaderSingleton.savePosition,
                mModel
            )

            dynamicItemRecycler.adapter?.notifyItemChanged(mModel)
        }

        newDialog!!.show()
        newDialog!!.setCancelable(false)
    }

    private fun initRecyclerView(
        ansRecycler: RecyclerView,
        type: String,
        newDialog: Dialog,
        utilityBillTypes: List<UtilityBillTypesItem?>?,
        mOptions: List<OptionsItem?>?
    ) {
        ansRecycler.layoutManager =
            LinearLayoutManager(
                this
            )

        ansRecycler.adapter =
            BaseRecyclerAdapter(
                this,
                object : IAdapterListener {
                    override fun <T> clickListener(position: Int, model: T, view: View) {
                        if (type == "billType") {
                            model as UtilityBillTypesItem
                            districtId = model.id.toString()

//                            if (LanguageHelper.getLocale(this@DynamicCommonApiActivity) == LanguageLocales.BANGLA) {
//                                textView13.text = model.bnTitle
//                            } else {
//                                textView13.text = model.title
//                            }

                            textView13.text = model.title

                            viewTypeSelection.background = ContextCompat.getDrawable(
                                this@DynamicCommonApiActivity,
                                R.drawable.edittext_layout
                            )
                            utilityBillInfoId =
                                modelData?.utilityBillTypes!![position]?.id.toString()
                            initOtherServicesRecyclerView(modelData?.utilityBillTypes!![position]?.parameterLists)
                        } else if (type == "itemSpinner" || type=="multiItemSpinner") {
                            model as OptionsItem

                            if (type=="multiItemSpinner") {
                                return
                            }

                            SharedPrefHeaderSingleton.instance.setData(
                                SharedPrefHeaderSingleton.recyclerItemNotify,
                                model.value
                            )

                            model.value?.let { model.name?.let { it1 -> spinnerHash.put(it, it1) } }

                            SharedPrefHeaderSingleton.instance.setIntData(
                                SharedPrefHeaderSingleton.savePosition,
                                mModel
                            )

                            dynamicItemRecycler.adapter?.notifyItemChanged(mModel)
                        }

                        newDialog.dismiss()
                    }

                    override fun getViewHolder(
                        parent: ViewGroup,
                        viewType: Int
                    ): BaseViewHolder2 {
                        return SpinnerAdapterRecycler(
                            DataBindingUtil.inflate(
                                LayoutInflater.from(parent.context),
                                R.layout.custom_user_spinner,
                                parent,
                                false
                            ), this@DynamicCommonApiActivity, type,spinnerMultiList
                        )
                    }
                },
                if (type == "billType") utilityBillTypes as java.util.ArrayList<UtilityBillTypesItem>
                else mOptions as java.util.ArrayList<OptionsItem>
            )

        //}, if (type == "billType") utilityBillTypes!!.size else mOptions!!.size,
        // if (type == "billType") utilityBillTypes as java.util.ArrayList<UtilityBillTypesItem>
        //  else mOptions as java.util.ArrayList<OptionsItem> )
    }
}