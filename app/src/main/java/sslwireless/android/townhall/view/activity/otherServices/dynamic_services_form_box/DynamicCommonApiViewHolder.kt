package sslwireless.android.townhall.view.activity.otherServices.dynamic_services_form_box

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.SubmitModel
import sslwireless.android.townhall.data.model.utility.ParameterListsItem
import sslwireless.android.townhall.databinding.CustomDynamicCommonApiLayoutBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.SharedPrefHeaderSingleton
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import androidx.core.widget.doOnTextChanged


class DynamicCommonApiViewHolder(
    itemView: ViewDataBinding,
    context: Context,
    var mHashMap: HashMap<String, SubmitModel>?, private val lang: String
) :
    BaseViewHolder2(itemView.root) {

    var binding = itemView as CustomDynamicCommonApiLayoutBinding
    var mContext: Context = context

    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {
        itemModel as ParameterListsItem
        if (itemModel.required == "required") {
            if (mHashMap?.get(itemModel.fieldName.toString()) == null) {
                mHashMap?.put(
                    itemModel.fieldName.toString(),
                    SubmitModel(
                        "",
                        true, "", "",
                        itemModel.level.toString(),
                        itemModel.bnLevel.toString()
                    )
                )
            }
        } else {
            if (mHashMap?.get(itemModel.fieldName.toString()) == null) {
                mHashMap?.put(
                    itemModel.fieldName.toString(),
                    SubmitModel(
                        "",
                        false, "", "",
                        itemModel.level.toString(),
                        itemModel.bnLevel.toString()
                    )
                )
            }
        }

        if (SharedPrefHeaderSingleton.instance.getIntData(
                SharedPrefHeaderSingleton.savePosition,
                -1
            ) == position
        ) {
            if (SharedPrefHeaderSingleton.instance.getData(
                    SharedPrefHeaderSingleton.recyclerItemNotify,
                    null
                ) != null
            ) {
                binding.spinnerText.text = SharedPrefHeaderSingleton.instance.getData(
                    SharedPrefHeaderSingleton.recyclerItemNotify,
                    null
                )

                binding.spinnerViewBack.background =
                    ContextCompat.getDrawable(mContext, R.drawable.edittext_layout)

                if (itemModel.required == "required") {
                    mHashMap?.put(
                        itemModel.fieldName.toString(),
                        SubmitModel(
                            binding.spinnerText.text.toString(),
                            true, itemModel.type.toString(), itemModel.dataType.toString(),
                            itemModel.level.toString(),
                            itemModel.bnLevel.toString()
                        )
                    )
                } else {
                    mHashMap?.put(
                        itemModel.fieldName.toString(),
                        SubmitModel(
                            binding.spinnerText.text.toString(),
                            false, itemModel.type.toString(), itemModel.dataType.toString(),
                            itemModel.level.toString(),
                            itemModel.bnLevel.toString()
                        )
                    )
                }

                SharedPrefHeaderSingleton.instance.setIntData(
                    SharedPrefHeaderSingleton.savePosition,
                    -1
                )

                SharedPrefHeaderSingleton.instance.setData(
                    SharedPrefHeaderSingleton.recyclerItemNotify,
                    null
                )
            }
        }


        if (itemModel.type == "text") {
            binding.dynamicTextGroup.visibility = View.VISIBLE

//            if (LanguageHelper.getLocale(mContext) == LanguageLocales.BANGLA) {
////                binding.customerName.hint = itemModel.bnLevel
//                binding.amountInput.hint = itemModel.bnLevel
//            } else {
////                binding.customerName.hint = itemModel.level
//                binding.amountInput.hint = itemModel.level
//            }

            binding.amountInput.hint =
                if (lang == AppConstants.Language.ENGLISH) itemModel.level else itemModel.bnLevel

            if (itemModel.dataType == "text") {
                binding.customerName.filters = arrayOf<InputFilter>(LengthFilter(100))
                binding.customerName.inputType = InputType.TYPE_CLASS_TEXT
            } else if (itemModel.dataType == "number") {
                binding.customerName.filters = arrayOf<InputFilter>(LengthFilter(11))
                binding.customerName.inputType = InputType.TYPE_CLASS_PHONE
            } else if (itemModel.dataType == "decimal") {
                binding.customerName.filters = arrayOf<InputFilter>(LengthFilter(50))
                binding.customerName.inputType = InputType.TYPE_CLASS_NUMBER
            } else if (itemModel.dataType == "alphanumeric") {
                binding.customerName.filters = arrayOf<InputFilter>(LengthFilter(100))
                binding.customerName.inputType = InputType.TYPE_CLASS_TEXT
            } else {
                binding.customerName.filters = arrayOf<InputFilter>(LengthFilter(100))
                binding.customerName.inputType = InputType.TYPE_CLASS_TEXT
            }

            binding.customerName.onFocusChangeListener = object : OnFocusChangeListener {
                override fun onFocusChange(
                    view: View,
                    hasFocus: Boolean
                ) {
                    if (hasFocus) {
                        binding.view22.background =
                            ContextCompat.getDrawable(mContext, R.drawable.selected_edittext)
                    } else {
                        binding.view22.background =
                            ContextCompat.getDrawable(mContext, R.drawable.edittext_layout)
                    }
                }
            }

            binding.customerName.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (itemModel.required == "required") {
                        mHashMap?.put(
                            itemModel.fieldName.toString(),
                            SubmitModel(
                                binding.customerName.text.toString(),
                                true, itemModel.type.toString(), itemModel.dataType.toString(),
                                itemModel.level.toString(),
                                itemModel.bnLevel.toString()
                            )
                        )
                    } else {
                        mHashMap?.put(
                            itemModel.fieldName.toString(),
                            SubmitModel(
                                binding.customerName.text.toString(),
                                false, itemModel.type.toString(), itemModel.dataType.toString(),
                                itemModel.level.toString(),
                                itemModel.bnLevel.toString()
                            )
                        )
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

            })
        } else if (itemModel.type == "select" || itemModel.type == "multi-select") {
            binding.dynamicSpinnerGroup.visibility = View.VISIBLE

//            if (LanguageHelper.getLocale(mContext) == LanguageLocales.BANGLA) {
//                binding.spinnerText.hint = itemModel.bnLevel
//                SharedPrefHeaderSingleton.instance.setData(
//                    SharedPrefHeaderSingleton.spinnerHeaderName,
//                    itemModel.bnLevel
//                )
//            } else {
//                binding.spinnerText.hint = itemModel.level
//                SharedPrefHeaderSingleton.instance.setData(
//                    SharedPrefHeaderSingleton.spinnerHeaderName,
//                    itemModel.level
//                )
//            }

            binding.spinnerText.hint =
                if (lang == AppConstants.Language.ENGLISH) itemModel.level else itemModel.bnLevel
            SharedPrefHeaderSingleton.instance.setData(
                SharedPrefHeaderSingleton.spinnerHeaderName,
                itemModel.level
            )
        } else if (itemModel.type == "date") {
            binding.dynamicDateGroup.visibility = View.VISIBLE

            binding.dateInput.hint =
                if (lang == AppConstants.Language.ENGLISH) itemModel.level else itemModel.bnLevel
            binding.dateEditText.outputDateFormat(itemModel.format?.replace("DD", "dd"))
            binding.dateEditText.doOnTextChanged { text, start, before, count ->
                if (itemModel.required == "required") {
                    mHashMap?.put(
                        itemModel.fieldName.toString(),
                        SubmitModel(
                            binding.dateEditText.text.toString(),
                            true, itemModel.type.toString(), itemModel.dataType.toString(),
                            itemModel.level.toString(),
                            itemModel.bnLevel.toString()
                        )
                    )
                } else {
                    mHashMap?.put(
                        itemModel.fieldName.toString(),
                        SubmitModel(
                            binding.dateEditText.text.toString(),
                            false, itemModel.type.toString(), itemModel.dataType.toString(),
                            itemModel.level.toString(),
                            itemModel.bnLevel.toString()
                        )
                    )
                }
            }
        }

        binding.spinnerText.setOnClickListener {
            binding.spinnerViewBack.background =
                ContextCompat.getDrawable(mContext, R.drawable.selected_edittext)

            binding.view22.background =
                ContextCompat.getDrawable(mContext, R.drawable.edittext_layout)

//            if (LanguageHelper.getLocale(mContext) == LanguageLocales.BANGLA) {
//                SharedPrefHeaderSingleton.instance.setData(
//                    SharedPrefHeaderSingleton.spinnerHeaderName,
//                    itemModel.bnLevel
//                )
//            } else {
//                SharedPrefHeaderSingleton.instance.setData(
//                    SharedPrefHeaderSingleton.spinnerHeaderName,
//                    itemModel.level
//                )
//            }

            SharedPrefHeaderSingleton.instance.setData(
                SharedPrefHeaderSingleton.spinnerHeaderName,
                if (lang == AppConstants.Language.ENGLISH) itemModel.level else itemModel.bnLevel
            )

            itemModel.itemChecker = true
            itemModel.currentPosition = position
            listener.clickListener(position, itemModel, binding.spinnerText)
        }

        binding.root.setOnClickListener {
            listener.clickListener(position, itemModel, binding.root)
        }
    }
}