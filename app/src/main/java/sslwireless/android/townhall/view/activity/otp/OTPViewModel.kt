package sslwireless.android.townhall.view.activity.otp

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.user.RegistrationInfo
import sslwireless.android.townhall.view.activity.login.LoginStep3ViewModel
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class OTPViewModel(val dataManager: DataManager) : BaseViewmodel(dataManager) {
    companion object {
        const val KEY_VERIFY_OTP = "verifyOTP"
        const val KEY_VERIFY_QR_OTP = "QRverifyOTP"
        const val KEY_RESEND_OTP = "resend-otp"
    }

    fun verifyRegisterOTP(
        data: RegistrationInfo,
        otp: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiVerifyRegistrationOTP(
            data,
            otp,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_VERIFY_OTP))
        )
    }

    fun verifyOldAccountOTP(
        email: String,
        phone: String,
        otp: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiVerifyOldAccOTP(
            email,
            phone,
            otp,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_VERIFY_OTP))
        )
    }

    fun verifyForgetPassOTP(
        email: String,
        mobile: String,
        otp: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiVerifyForgetPassOTP(
            email,
            mobile,
            otp,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_VERIFY_OTP))
        )
    }

    fun apiVerifyQROTP(
        otp: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiVerifyQROTP(
           dataManager.mPref.prefGetCustomerToken()?:"",
            otp,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_VERIFY_QR_OTP))
        )
    }

    fun verifyOTP(
        email: String,
        mkey: String,
        mobile: String,
        otp: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiVerifyOTP(
            email,
            mkey,
            mobile,
            otp,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_VERIFY_OTP))
        )
    }

    fun resendOTP(
        mobile: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.resendOTP(
            mobile,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_RESEND_OTP))
        )
    }

    fun resendOTPForForget(
        mobile: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {

        dataManager.apiHelper.apiForgetPassword(
            mobile,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_RESEND_OTP))
        )
    }

    fun resendOTPForRegister(
        data: RegistrationInfo,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUserReg(
            data,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_RESEND_OTP))
        )
    }

    fun apiCheckImageUploadStatus(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiCheckImageUploadStatus(
            dataManager.mPref.prefGetCurrentUser().mobile,
            dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack,
                LoginStep3ViewModel.KEY_IMAGE_UPLOAD
            ))
        )
    }
}