package sslwireless.android.townhall.view.activity.otp

import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.text.Spannable
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_login_step_2.*
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.activity_otp.back
import kotlinx.android.synthetic.main.activity_otp.otpNumber
import kotlinx.android.synthetic.main.activity_otp.proceed_btn
import kotlinx.android.synthetic.main.activity_otp.termsText
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.QrOtpResponse
import sslwireless.android.townhall.data.model.user.FPKey
import sslwireless.android.townhall.data.model.user.RegistrationInfo
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.ChangePassword.ChangePasswordActivity
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.login.LoginStep2Activity
import sslwireless.android.townhall.view.activity.login.LoginStep3ViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.broadcast.SMSReceiver
import sslwireless.android.townhall.view.utils.fromResponse
import java.util.concurrent.TimeUnit


class OtpActivity : BaseActivity() {
    private lateinit var viewModel: OTPViewModel
    private lateinit var timer: CountDownTimer
    private var i = 0
    private val hdlr = Handler()

    lateinit var userPhone: String

    private lateinit var regInfo: RegistrationInfo
    private var password: String? = null
    var email: String? = null
    var mKey: String? = null
    var otpTxt: String? = null
    private var isForgotPass: Boolean = false
    private var isRegister: Boolean = false
    private var isPin: Boolean = false
    private var intentFilter: IntentFilter? = null
    private var smsReceiver: SMSReceiver? = null


    companion object {
        const val BUNDLE_OTP = "otp"
        const val BUNDLE_PHONE = "phone"
        const val BUNDLE_MKEY = "mKey"
        const val BUNDLE_EMAIL = "email"
        const val BUNDLE_IS_FORGOT = "from_forgot_password"
        const val BUNDLE_IS_REGISTER = "from_register"
        const val BUNDLE_REG_INFO = "reg_info"
        const val BUNDLE_IS_PIN = "from_pin"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        initSmsListener()
        initBroadCast()
    }

    private fun initBroadCast() {
        intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        smsReceiver = SMSReceiver()
        smsReceiver?.setOTPListener(object : SMSReceiver.OTPReceiveListener {
            override fun onOTPReceived(otp: String?) {
                otpEditText.setText(otp)
            }
        })
    }

    private fun initSmsListener() {
        val client = SmsRetriever.getClient(this)
        client.startSmsRetriever()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(smsReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(smsReceiver)
    }

    override fun onDestroy() {
        super.onDestroy()
        smsReceiver = null
    }


    override fun viewRelatedTask() {
        viewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(OTPViewModel(getDataManager())))
                .get(OTPViewModel::class.java)

        otpTxt = ""

        otpEditText.doOnTextChanged { text, start, before, count ->
            if (text.toString().length == 6) {
                otpTxt = text.toString()
                proceed_btn.setBackgroundColor(
                    ContextCompat.getColor(
                        this@OtpActivity,
                        R.color.colorAccent
                    )
                )
            } else {
                otpTxt = ""
                proceed_btn.setBackgroundColor(
                    ContextCompat.getColor(
                        this@OtpActivity,
                        R.color.bluey_grey
                    )
                )
            }
        }

        userPhone = intent.getStringExtra(BUNDLE_PHONE) ?: ""
        email = intent.getStringExtra(BUNDLE_EMAIL)
        mKey = intent.getStringExtra(BUNDLE_MKEY)
        isForgotPass = intent.getBooleanExtra(BUNDLE_IS_FORGOT, false)
        isRegister = intent.getBooleanExtra(BUNDLE_IS_REGISTER, false)
        isPin = intent.getBooleanExtra(BUNDLE_IS_PIN, false)
        regInfo = intent.getParcelableExtra(BUNDLE_REG_INFO) ?: RegistrationInfo(
            phone = userPhone,
            email = email
        )

        /*if (otpTxt != null) {
            otpEditText.setText(otpTxt)
        }*/

        if (isPin) {
            changeNumber.visibility = View.GONE
        }

        otpNumber.text = String.format(
            getString(R.string.otp_sent_number),
            if (userPhone.isNotEmpty()) userPhone else email
        )

        startCountDown()
        //initTermsText()
        //editTextFocusEventHandler()

        back.setOnClickListener { finish() }

        timerCountDown.setOnClickListener {
            if (timerCountDown.text == getString(R.string.resend)) {
                startCountDown()
            }
        }

        proceed_btn.setOnClickListener {
            goToHome()
            /*val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)*/
        }

        changeNumber.setOnClickListener {
            if (isForgotPass) {
                val intent = Intent(this, LoginStep2Activity::class.java)
                startActivity(intent)
            }
            finish()
        }

        resendOTP.setOnClickListener {
            if (isForgotPass) {
                viewModel.resendOTPForForget(
                    if (userPhone.isNotEmpty()) userPhone else email ?: "",
                    this,
                    this
                )
            } else if (isRegister) {
                viewModel.resendOTPForRegister(regInfo, this, this)
            } else {
                viewModel.resendOTP(
                    if (userPhone.isNotEmpty()) userPhone else email ?: "",
                    this,
                    this
                )
            }

        }
    }

    private fun goToHome() {
        if (otpTxt.isNullOrEmpty()) {
            showToast(this, getString(R.string.error_empty_otp))
        } else {
            showProgressDialog(getString(R.string.progress_loading))

            if (isForgotPass) {
                viewModel.verifyForgetPassOTP(
                    email ?: "",
                    userPhone,
                    otpTxt.toString(),
                    this,
                    this
                )
            } else if (mKey == null && isRegister) {
                viewModel.verifyRegisterOTP(
                    regInfo,
                    otpTxt.toString(),
                    this,
                    this
                )
            } else if (isPin) {
                viewModel.apiVerifyQROTP(otpTxt.toString(), this, this)
            } else if (mKey == null) {
                viewModel.verifyOldAccountOTP(
                    email ?: "",
                    userPhone,
                    otpTxt.toString(),
                    this,
                    this
                )
            } else {
                viewModel.verifyOTP(
                    email ?: "",
                    mKey ?: "",
                    userPhone,
                    otpTxt.toString(),
                    this,
                    this
                )
            }
        }
    }

    fun startCountDown() {
        resendOTPGroup.visibility = View.GONE
        countDownGroup.visibility = View.VISIBLE
        view69.visibility = View.GONE
        progressBar.max = 120

        timer = object :
            CountDownTimer(
                120000,
                1000
            ) {
            override fun onTick(millisUntilFinished: Long) {
                val seconds = (millisUntilFinished / 1000)
                val hms = String.format(
                    "%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)
                    ),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(
                            millisUntilFinished
                        )
                    )
                )
                /*val anim = ProgressBarAnimation(
                    progressBar,
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(
                            millisUntilFinished
                        )
                    ).toFloat(),
                    TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(
                            millisUntilFinished
                        )
                    ).toFloat() - 1f
                )
                anim.duration = 1000*/
                //progressBar.startAnimation(anim)
                progressBar.progress = seconds.toInt()

                timerCountDown.text = hms
            }

            override fun onFinish() {
                countDownGroup.visibility = View.GONE
                resendOTPGroup.visibility = View.VISIBLE
            }
        }.start()
    }

    private fun editTextFocusEventHandler() {
        otpEditText.setOnFocusChangeListener { view, b ->
            if (b) {
                numEmail.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                numEmail.setPadding(30, 18, 30, 10)
            }
        }
    }

    private fun initTermsText() {
        termsText.setText(
            resources.getString(R.string.by_clicking_proceed_you_agree_to_easy_s_terms_conditions),
            TextView.BufferType.SPANNABLE
        )
        val span = termsText.text as Spannable
        span.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.terms_text_color_login)), 0,
            termsText.length(),
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
        span.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.login_backborder)), 42,
            termsText.length(),
            Spannable.SPAN_INCLUSIVE_EXCLUSIVE
        )
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        hideProgressDialog()
        if (key == LoginStep3ViewModel.KEY_IMAGE_UPLOAD) {
            val baseData = result.data ?: return
            if (baseData.code == 200) {
                getDataManager().mPref.prefLoginIsValid()
                startActivity(Intent(this, MainActivity::class.java))
                finishAffinity()
            } else if (baseData.message == "User Does Not Exist") {
                gotoUploadPicture(this, userPhone)
            }
        } else {
            val baseData = result.data ?: return

            if (baseData.code == 200) {
                if (key == OTPViewModel.KEY_RESEND_OTP) {
                    startCountDown()

                    Toast.makeText(this, baseData.message, Toast.LENGTH_SHORT).show()
                } else if (key == OTPViewModel.KEY_VERIFY_OTP) {
                    val rawResponse = baseData.data.toString()
                    Log.e("RawResponse:", "$key ----> $rawResponse")

                    if (baseData.ui.equals("reset-password")) {
                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<FPKey> = moshi.adapter(FPKey::class.java)

                        val fpKey = adapter.fromJsonValue(baseData.data) ?: return

                        val intent = Intent(this, ChangePasswordActivity::class.java)
                        intent.putExtra("fpKey", fpKey.fpKey.toString())
                        if (userPhone.isNotEmpty()) {
                            intent.putExtra("phone", userPhone)
                        } else {
                            intent.putExtra("phone", email ?: "")
                        }


                        intent.putExtra(
                            ChangePasswordActivity.FROM_ACTION,
                            ChangePasswordActivity.FROM_FORGET_PASS
                        )
                        startActivity(intent)

                    } else {
                        val moshi: Moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<UserData> = moshi.adapter(UserData::class.java)
                        val userData = adapter.fromJsonValue(baseData.data) ?: return
                        val token = baseData.token ?: return
                        getDataManager().mPref.prefLogin(userData)
                        getDataManager().mPref.prefSetCustomerToken(token)
                        viewModel.apiCheckImageUploadStatus(this, this)
                        //startActivity(Intent(this, MainActivity::class.java))
                        //finishAffinity()
                    }
                } else if (key == OTPViewModel.KEY_VERIFY_QR_OTP) {
                    val qrOtpResponse = result.data.data?.fromResponse<QrOtpResponse>()
                    startActivity(
                        Intent(
                            this,
                            ChangePasswordActivity::class.java
                        ).apply {
                            putExtra(
                                ChangePasswordActivity.FROM_ACTION,
                                ChangePasswordActivity.FROM_SET_PIN
                            )
                            putExtra(
                                ChangePasswordActivity.PIN_KEY,
                                qrOtpResponse?.bangla_qr_pin_key ?: ""
                            )
                        })
                }
            } else {
                Toast.makeText(this, baseData.message, Toast.LENGTH_SHORT).show()
            }

            Log.i(
                "TEST_USER_OTP_DATA",
                baseData.message + " " + result.message
            )
        }

    }

    // 403973 01999043405 required
    // 40397 01999043405 required
}