package sslwireless.android.townhall.view.activity.paymentMethod

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.activity_payment_method.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.AutoDebitResponse
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.CardsItem
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.OthersItem
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.paymentMethod.adapter.CardAdapter
import sslwireless.android.townhall.view.activity.paymentMethod.adapter.MobileBankingAdapter
import sslwireless.android.townhall.view.activity.paymentMethod.bottomSheet.CvvBottomSheet
import sslwireless.android.townhall.view.activity.paymentMethod.viewmodel.PaymentMethodViewModel
import sslwireless.android.townhall.view.activity.qrPay.QrActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet
import sslwireless.android.townhall.view.utils.*

class PaymentMethodActivity : BaseActivity(), CardAdapter.CardAdapterListener,
    SessionLogoutBottomSheet.IBottomSheetDialogClicked, MobileBankingAdapter.MobileBankingListener,
    CvvBottomSheet.CvvBottomSheetListener, SSLCTransactionResponseListener {

    private var cardsItem: CardsItem? = null
    private val REQUEST_CODE = 200
    private val sslCommerzIntegrationModel: SslCommerzIntegrationModel? by lazy {
        intent.extras?.getParcelable<SslCommerzIntegrationModel>(
            AppConstants.IntegrationModel.TAG
        )
    }
    private val cardList: ArrayList<CardsItem> by lazy {
        ArrayList(sslCommerzIntegrationModel?.payment_methods?.cards ?: ArrayList())
    }

    private val mblList: ArrayList<OthersItem> by lazy {
        ArrayList(sslCommerzIntegrationModel?.payment_methods?.others ?: ArrayList())
    }

    private val cardAdapter: CardAdapter by lazy {
        CardAdapter(cardList, this)
    }

    private val mobileBankingAdapter: MobileBankingAdapter by lazy {
        MobileBankingAdapter(mblList, this)
    }

    private val viewModel: PaymentMethodViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(PaymentMethodViewModel(getDataManager())))
            .get(PaymentMethodViewModel::class.java)
    }

    private val sessionLogoutBottomSheet: SessionLogoutBottomSheet by lazy {
        SessionLogoutBottomSheet().apply {
            arguments = Bundle().apply {
                putString(
                    "title",
                    this@PaymentMethodActivity.getString(R.string.remove_card)
                )
                putString("subText", "")
                putString("btn1", this@PaymentMethodActivity.getString(R.string.no))
                putString("btn2", this@PaymentMethodActivity.getString(R.string.yes))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)

    }

    override fun viewRelatedTask() {
        cardsRecycler.adapter = cardAdapter
        mblRecycler.adapter = mobileBankingAdapter

        addNewCardText.setOnClickListener {
            loadSdk()
        }
        viewAllBtn.setOnClickListener {
            loadSdk()
        }
        back_icon_iv.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200) {
            when (key) {
                "card_delete" -> {
                    cardList.remove(cardsItem)
                    cardAdapter.notifyDataSetChanged()
                    showToast(this, result.data.message)
                    cardsItem = null
                }
                "auto_debit" -> {
                    val autoDebitResponse: AutoDebitResponse? = result.data.data.fromResponse()
                    autoDebitResponse?.let {
                        if (it.is_redirect_required) {
                            val intent = Intent(this, PaymentWebViewActivity::class.java)
                            intent.apply {
                                putExtra(AppConstants.IntegrationModel.TAG_AUTO_DEBIT, it)
                            }
                            startActivityForResult(intent, REQUEST_CODE)
                        } else {
                            viewModel.validateTransaction(
                                sslCommerzIntegrationModel?.transaction_id ?: "", this, this
                            )
                        }
                    }
                }
                "validation" -> {
                    showEditDialog(
                        true,
                        result.data.message
                    )
                }

            }
        } else {
            if (key=="validation") {
                showEditDialog(
                    false,
                    result.data?.message?:getString(R.string.something_went_wrong)
                )
            } else {
                showErrorToast(result.data?.message ?: getString(R.string.something_went_wrong))
            }
        }
    }

    override fun onDeleteCard(cardsItem: CardsItem, position: Int) {
        this.cardsItem = cardsItem
        sessionLogoutBottomSheet.show(supportFragmentManager, sessionLogoutBottomSheet.tag)
        sessionLogoutBottomSheet.setBottomDialogListener(this)

    }

    override fun onProceedCardPayment(cardsItem: CardsItem) {
        this.cardsItem = cardsItem
        if (cardsItem.is_moto == 0) {
            CvvBottomSheet(this,cardsItem.channel?.contains("AMERICAN EXPRESS",true)?:false).show(supportFragmentManager, "123456")
        } else {
            viewModel.apiAutoDebit(
                AppConstants.ChannelType.CARD,
                cardsItem.channel ?: "",
                cardsItem.card_index ?: "",
                "",
                sslCommerzIntegrationModel?.amount.toString(),
                sslCommerzIntegrationModel?.transaction_id ?: "",
                this, this
            )
        }
    }

    override fun onCancelButtonClicked() {
    }

    override fun onSessionLogoutButtonClicked(tag: String) {
        viewModel.apiDeleteCard(
            sslCommerzIntegrationModel?.user_refer ?: "",
            cardsItem?.card_index ?: "",
            this,
            this
        )
    }

    override fun onApplyPin(cvv: String) {
        viewModel.apiAutoDebit(
            AppConstants.ChannelType.CARD,
            cardsItem?.channel ?: "",
            cardsItem?.card_index ?: "",
            cvv,
            sslCommerzIntegrationModel?.amount.toString(),
            sslCommerzIntegrationModel?.transaction_id ?: "",
            this, this
        )
    }

    override fun onProceedMobilePayment(othersItem: OthersItem) {
        viewModel.apiAutoDebit(
            AppConstants.ChannelType.OTHERS,
            othersItem.channel ?: "",
            "",
            "",
            sslCommerzIntegrationModel?.amount.toString(),
            sslCommerzIntegrationModel?.transaction_id ?: "",
            this, this
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                val status = data?.getBooleanExtra("status",false)?:false
                if (status) {
                    viewModel.validateTransaction(
                        sslCommerzIntegrationModel?.transaction_id ?: "",
                        this,
                        this
                    )
                } else {
                    showEditDialog(false, "")
                }
            } else {
                onBackPressed()
            }
        }

    }

    private fun showEditDialog(status: Boolean, returnMessage: String) {
        val newDialog = Dialog(this, R.style.customDialog)
        newDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )
        customDialogBinding.textView83.makeVisible()

        if (status) {
            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)
            customDialogBinding.textView82.text = getString(R.string.pay_successfull)
            customDialogBinding.textView83.text = returnMessage

        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()
            customDialogBinding.textView86.makeVisible()

            customDialogBinding.textView84.text = getString(R.string.failed_text)
           /* customDialogBinding.textView84.background =
                ContextCompat.getDrawable(this, R.drawable.failed_drawable)*/

            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            newDialog.dismiss()
            if (status) {
                sslCommerzIntegrationModel?.let {
                    if (customDialogBinding.ratingBar.rating.toInt() > 0) {
                        viewModel.apiRating(
                            customDialogBinding.ratingBar.rating.toInt().toString(),
                            it.transaction_id,
                            this,
                            this
                        )
                    }
                }
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            } else {
                val intent = Intent(this, QrActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            }
        }

        customDialogBinding.textView86.setOnClickListener {
            newDialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        newDialog.setContentView(customDialogBinding.root)

        newDialog.show()
        newDialog.setCancelable(false)
    }

    private fun loadSdk() {
        sslCommerzIntegrationModel?.let {
            initSdk(this, it, getDataManager(),false)
        }

    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        viewModel.validateTransaction(sslCommerzIntegrationModel?.transaction_id ?: "", this, this)
    }

    override fun transactionFail(p0: String?) {
        viewModel.validateTransaction(sslCommerzIntegrationModel?.transaction_id ?: "", this, this)
        //showEditDialog(false, p0.toString())
    }

    override fun closed(message: String?) {

    }

//    override fun merchantValidationError(p0: String?) {
//        viewModel.validateTransaction(sslCommerzIntegrationModel?.transaction_id ?: "", this, this)
//       // showEditDialog(false, p0.toString())
//    }
}