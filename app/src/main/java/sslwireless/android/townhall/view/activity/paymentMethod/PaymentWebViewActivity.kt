package sslwireless.android.townhall.view.activity.paymentMethod

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_payment_web_view.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.AutoDebitResponse
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.AppConstants

class PaymentWebViewActivity : BaseActivity() {

    private val autoDebitResponse: AutoDebitResponse? by lazy {
        intent.extras?.getParcelable<AutoDebitResponse>(
            AppConstants.IntegrationModel.TAG_AUTO_DEBIT
        )
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_web_view)
    }

    override fun viewRelatedTask() {
        webView.settings.javaScriptEnabled = true
        webView.settings.domStorageEnabled = true
        webView.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView.loadUrl(autoDebitResponse?.redirect_gateway_url?:"")

        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progressbar.setProgress(newProgress)
            }
        }
        webView.webViewClient = object :WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView, url: String?): Boolean {

                when (url) {
                    autoDebitResponse!!.success_url -> {
                        val intent = Intent().apply {
                            putExtra("status",true)
                        }
                        setResult(RESULT_OK,intent)
                        finish()
                    }
                    autoDebitResponse!!.fail_url -> {
                        val intent = Intent().apply {
                            putExtra("status",false)
                        }
                        setResult(RESULT_OK,intent)
                        finish()
                    }
                    autoDebitResponse!!.cancel_url -> {
                       // setResult(RESULT_CANCELED)
                        finish()
                    }
                    else ->{
                        view.loadUrl(url!!)
                    }
                }

                return true
            }


            override fun onPageFinished(view: WebView?, url: String) {
                //progressbar.visibility = View.GONE
                when (url) {
                    autoDebitResponse!!.success_url -> {
                        val intent = Intent().apply {
                            putExtra("status",true)
                        }
                        setResult(RESULT_OK,intent)
                        finish()
                    }
                    autoDebitResponse!!.fail_url -> {
                        val intent = Intent().apply {
                            putExtra("status",false)
                        }
                        setResult(RESULT_OK,intent)
                        finish()
                    }
                    autoDebitResponse!!.cancel_url -> {
                       // setResult(RESULT_CANCELED)
                        finish()
                    }
                }
            }


        }

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

}