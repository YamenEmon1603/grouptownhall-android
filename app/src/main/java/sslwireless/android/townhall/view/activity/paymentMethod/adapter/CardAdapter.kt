package sslwireless.android.townhall.view.activity.paymentMethod.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.CardsItem
import sslwireless.android.townhall.databinding.ItemCardLayoutBinding
import sslwireless.android.townhall.view.activity.qrPay.model.BanglaQRFundingSource
import sslwireless.android.townhall.view.utils.loadImage

class CardAdapter(private val cardList: List<CardsItem>,private val listener:CardAdapterListener) :
    RecyclerView.Adapter<CardAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_card_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = cardList[position]
        holder.binding.bankName.text = item.bank_name
        holder.binding.cardNumber.text = item.card_no
        holder.binding.cardNumber.text = item.card_no
        holder.binding.expireDate.text = item.expiry_date
        holder.binding.cardLogo.loadImage(item.logo ?: "")
        when (item.channel) {
            BanglaQRFundingSource.FundingSource.VISA.toString() -> {
                holder.binding.cardBackground.loadImage(R.drawable.ic_card_icon_visa)
            }
            BanglaQRFundingSource.FundingSource.MASTERCARD.toString() -> {
                holder.binding.cardBackground.loadImage(R.drawable.ic_card_icon_master)
            }
            else -> {
                holder.binding.cardBackground.loadImage(R.drawable.ic_card_icon_visa)
            }
        }
        holder.binding.cardDelete.setOnClickListener {
            listener.onDeleteCard(item,position)
        }
        holder.binding.rootLayout.setOnClickListener {
            listener.onProceedCardPayment(item)
        }
    }

    override fun getItemCount(): Int = cardList.size

    class ViewHolder(val binding: ItemCardLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    interface CardAdapterListener {
        fun onDeleteCard(cardsItem: CardsItem, position: Int)

        fun onProceedCardPayment(cardsItem: CardsItem)
    }
}