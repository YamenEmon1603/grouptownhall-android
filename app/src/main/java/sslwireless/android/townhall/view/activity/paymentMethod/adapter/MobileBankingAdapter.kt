package sslwireless.android.townhall.view.activity.paymentMethod.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.OthersItem
import sslwireless.android.townhall.databinding.ItemMblLayoutBinding
import sslwireless.android.townhall.view.utils.loadImage

class MobileBankingAdapter(private val mblBankingListList: List<OthersItem>,private val listener:MobileBankingListener) :
    RecyclerView.Adapter<MobileBankingAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_mbl_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mblBankingListList[position]
        holder.binding.imgIcon.loadImage(item.logo_url?:"")
        holder.binding.rootLayout.setOnClickListener {
            listener.onProceedMobilePayment(item)
        }
    }

    override fun getItemCount(): Int = if (mblBankingListList.size>3) 3 else mblBankingListList.size

    class ViewHolder(val binding: ItemMblLayoutBinding) : RecyclerView.ViewHolder(binding.root)

    interface MobileBankingListener {
        fun onProceedMobilePayment(othersItem: OthersItem)
    }
}