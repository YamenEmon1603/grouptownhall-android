package sslwireless.android.townhall.view.activity.paymentMethod.bottomSheet


import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.cvv_bottom_sheet.*
import sslwireless.android.townhall.R


class CvvBottomSheet(private val listener: CvvBottomSheetListener,private val isAmex:Boolean) : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cvv_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgClose.setOnClickListener {
            dismiss()
        }
        cvv_et.requestFocus()
        cvv_et.filters = arrayOf(InputFilter.LengthFilter(if (isAmex) 4 else 3))
        cvv_et.doOnTextChanged { text, start, before, count ->
            if (isAmex) {
                applyBtn.isEnabled = text.toString().length==4
            } else {
                applyBtn.isEnabled = text.toString().length==3
            }

        }

        applyBtn.setOnClickListener {
            dismiss()
            listener.onApplyPin(cvv_et.text.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }

    private fun hideKeyboard(view: View) {
        val imm =view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    interface CvvBottomSheetListener {
        fun onApplyPin(cvv: String)
    }


}
