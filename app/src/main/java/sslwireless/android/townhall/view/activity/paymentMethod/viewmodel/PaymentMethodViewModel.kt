package sslwireless.android.townhall.view.activity.paymentMethod.viewmodel

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class PaymentMethodViewModel(private val dataManager: DataManager): BaseViewmodel(dataManager) {


    fun apiDeleteCard(user_refer:String, card_index:String, lifecycleOwner: LifecycleOwner,
                      iObserverCallBack: IObserverCallBack){
        dataManager.apiHelper.apiDeleteCard(
            user_refer,card_index,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "card_delete"))
        )
    }

    fun apiAutoDebit( channel_type: String,
                      channel:String,
                      card_index:String,
                      cvv:String,
                      total_amount:String,
                      tran_id:String,
                      lifecycleOwner: LifecycleOwner,
                      iObserverCallBack: IObserverCallBack){
        dataManager.apiHelper.apiAutoDebit(
            channel_type,channel,card_index,cvv,total_amount,tran_id,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "auto_debit"))
        )
    }

    fun validateTransaction(transactionId:String,lifecycleOwner: LifecycleOwner,
                            iObserverCallBack: IObserverCallBack){
        dataManager.apiHelper.apiValidateTransaction(
            transactionId,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "validation"))
        )

    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating,transactionId,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }
}