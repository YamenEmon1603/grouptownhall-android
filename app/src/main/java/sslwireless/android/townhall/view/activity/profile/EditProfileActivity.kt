package sslwireless.android.townhall.view.activity.profile

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.github.dhaval2404.imagepicker.ImagePicker
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.databinding.ActivityEditProfileBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import okhttp3.MultipartBody
import java.util.*
import kotlin.collections.ArrayList


class EditProfileActivity : BaseActivity(), DatePickerDialog.OnDateSetListener {
    private val gender: ArrayList<String> = ArrayList()
    private lateinit var binding: ActivityEditProfileBinding
    private val userdata: UserData by lazy {
        getDataManager().mPref.prefGetCurrentUser()
    }
    private val viewModelEdit: EditProfileViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(EditProfileViewModel(getDataManager())))
                .get(EditProfileViewModel::class.java)
    }
    private var picture: MultipartBody.Part? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)
        binding.userData = userdata
    }

    override fun viewRelatedTask() {
        gender.clear()
        gender.add(getString(R.string.select_gender))
        gender.add(getString(R.string.male))
        gender.add(getString(R.string.female))
        gender.add(getString(R.string.others))

        val arrayAdapter: ArrayAdapter<String> =
                ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, gender)
        binding.selectGenderSp.adapter = arrayAdapter
        if (userdata.gender.isNotEmpty()) {
            if (userdata.gender == "male") {
                binding.selectGenderSp.setSelection(1)
            } else if (userdata.gender == "female") {
                binding.selectGenderSp.setSelection(2)
            } else if (userdata.gender == "others") {
                binding.selectGenderSp.setSelection(3)
            }
        } else {
            userdata.gender = getString(R.string.select_gender)
        }
        binding.userData = userdata
        binding.selectGenderSp.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                    override fun onItemSelected(
                            parent: AdapterView<*>,
                            view: View,
                            position: Int,
                            id: Long
                    ) {
                        binding.genderTv.text = gender[position]
                    }

                }

        val c = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
                this,
                this,
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        )

        if (getDataManager().mPref.isSocial() && userdata.email.isNotEmpty()) {
            binding.emailEt.isFocusable = false
            binding.emailEt.isFocusableInTouchMode = false // user touches widget on phone with touch screen
            binding.emailEt.isClickable = false
        } else {
            binding.emailEt.isFocusable = true
            binding.emailEt.isFocusableInTouchMode = true
            binding.emailEt.isClickable = true
        }

        binding.dateOfBirthContainerCv.setOnClickListener {
            datePickerDialog.show();
        }

        binding.saveBtn.setOnClickListener {
            when {
                userdata.name!!.isEmpty() -> showErrorToast(getString(R.string.enter_name))
                userdata.email.isEmpty() -> showErrorToast(getString(R.string.enter_email))

                else -> {
                    if (picture != null) {
                        val token = getDataManager().mPref.prefGetCustomerToken()!!.getNormalBody()
                        viewModelEdit.apiUpdateProfilePicture(
                                token,
                                picture!!,
                                this,
                                this
                        )
                    } else {
                        callProfileUpdateApi()
                    }
                }
            }
        }

        binding.backIconIv.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }

        binding.profilePictureAddIconIv.setOnClickListener {
            ImagePicker.with(this)
                .cropSquare()
                    .compress(1024)
                    .galleryMimeTypes(
                            mimeTypes = arrayOf(
                                    "image/png",
                                    "image/jpg",
                                    "image/jpeg"
                            )
                    )
                    .start()
        }

        binding.profilePicture.setOnClickListener {
            ImagePicker.with(this)
                .cropSquare()
                    .compress(1024)
                    .galleryMimeTypes(
                            mimeTypes = arrayOf(
                                    "image/png",
                                    "image/jpg",
                                    "image/jpeg"
                            )
                    )
                    .start()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            if (key == "profile_picture") {
                callProfileUpdateApi()
            } else {
                val newData = result.data.data?.fromResponse<UserData>()
                getDataManager().mPref.prefUserData(newData!!)
                showToast(this, getString(R.string.updated))
                setResult(RESULT_OK)
                finish()
            }
        } else {
            showErrorToast(result.data.message)
        }
    }

    private fun callProfileUpdateApi() {
        var genderValue = ""
        genderValue = when (binding.selectGenderSp.selectedItem) {
            getString(R.string.male) -> {
                "male"
            }
            getString(R.string.female) -> {
                "female"
            }
            getString(R.string.others) -> {
                "common"
            }
            else -> {
                ""
            }
        }

        viewModelEdit.apiUpdateProfile(
                getDataManager().mPref.prefGetCustomerToken()!!,
                userdata.name!!,
                genderValue,  //userdata.gender.toLowerCase(),
                userdata.date_of_birth.replace("-", "/"),
                userdata.address,
                userdata.mobile,
                userdata.email!!,
                this,
                this
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            binding.profilePicture.loadImage(ImagePicker.getFile(data)!!)
            picture = ImagePicker.getFile(data)!!.getImageBody("profile_picture")
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        val date = if (p3 < 10) "0$p3" else p3.toString()
        val month = if (p2 + 1 < 10) "0${p2 + 1}" else (p2 + 1).toString()
        binding.dateOfBirthTv.setText("$date-$month-$p1".dateChangeTo("dd-MM-yyyy", "dd-MMM-yyyy"))
        userdata.date_of_birth = "$date-$month-$p1"
    }


}
