package sslwireless.android.townhall.view.activity.profile

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import okhttp3.MultipartBody
import okhttp3.RequestBody

class EditProfileViewModel(private val dataManager: DataManager): BaseViewmodel(dataManager) {

    fun apiUpdateProfilePicture(
        token: RequestBody,
        picture: MultipartBody.Part,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiAddProfilePicture(
            token,
            picture,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "profile_picture"))
        )
    }

    fun apiUpdateProfile(
        token: String,
        name: String,
        gender: String,
        dob: String,
        address: String,
        mobile: String,
        email: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUpdateProfile(
            token,
            name,
            gender,
            dob,
            address,
            mobile,
            email,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "profile_update"))
        )
    }
}