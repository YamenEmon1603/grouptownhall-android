package sslwireless.android.townhall.view.activity.profile

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.databinding.ActivityProfileBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.RedeemRwardPointActivity
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_profile.*
import sslwireless.android.townhall.view.utils.capitalizeFirstCharacter


class ProfileActivity : BaseActivity() {
    private lateinit var userData: UserData
    private lateinit var activityProfileBinding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityProfileBinding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        window.apply {
            clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            statusBarColor = Color.TRANSPARENT
        }
    }

    override fun viewRelatedTask() {
        edit_icon_iv.setOnClickListener {
            startActivityForResult(Intent(this,EditProfileActivity::class.java),200)
        }

        reward_point_layout.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    RedeemRwardPointActivity::class.java
                )
            )
        }
        loadCurrentUserDataOnUI()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            loadCurrentUserDataOnUI()
        }
    }

    private fun loadCurrentUserDataOnUI() {
        userData = getDataManager().mPref.prefGetCurrentUser()
        userData.gender = userData.gender.capitalizeFirstCharacter()
        activityProfileBinding.userData = userData

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
    }
}
