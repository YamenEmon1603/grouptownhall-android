package sslwireless.android.townhall.view.activity.programScheduling

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import org.joda.time.DateTime
import org.joda.time.Interval
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.townhall.ScheduleItem
import sslwireless.android.townhall.databinding.SchedulingItemLayoutRectangelBinding
import sslwireless.android.townhall.view.utils.*

class ProgramAdapter(
    private val list: ArrayList<ScheduleItem>, private val day: String
) :
    RecyclerView.Adapter<ProgramAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.scheduling_item_layout_rectangel,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        holder.binding.eventText.text = item.title ?: ""
        holder.binding.eventDummyTextText.text = item.title ?: ""
        holder.binding.eventTime.text = item.time ?: ""

        try {

            val dates = day.dateChangeTo("dd MMMM yyyy", "dd")
            val months = day.dateChangeTo("dd MMMM yyyy", "MM")
            val years = day.dateChangeTo("dd MMMM yyyy", "yyyy")

            val times = item.time?.split("-") ?: arrayListOf()

            val startTimeHours = times[0].replace(" ", "").dateChangeTo("hh:mma", "HH")
            val startTimeMin = times[0].replace(" ", "").dateChangeTo("hh:mma", "mm")

            val endTimeHour = times[1].dateChangeTo("hh:mma", "HH")
            val endTimeMin = times[1].dateChangeTo("hh:mma", "mm")

            val today = DateTime.now()
            val startDay = DateTime(
                years.toInt(),
                months.toInt(),
                dates.toInt(),
                startTimeHours.toInt(),
                startTimeMin.toInt(),
                0,
                0
            )
            val endDay = DateTime(
                years.toInt(),
                months.toInt(),
                dates.toInt(),
                endTimeHour.toInt(),
                endTimeMin.toInt(),
                0,
                0
            )

            val interval = Interval(startDay, endDay)

            if (interval.contains(today)) {
                holder.binding.timeLine.setBackgroundExtension(R.drawable.circle_2)
            } else {
                holder.binding.timeLine.setBackgroundExtension(R.drawable.circle_1)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        when (position) {
            0 -> {
                holder.binding.lineTop.makeGone()
            }
            list.size - 1 -> {
                holder.binding.lineBottom.makeGone()
            }
            else -> {
                holder.binding.lineTop.makeVisible()
                holder.binding.lineBottom.makeVisible()
            }
        }

        when (position % 4) {
            0 -> {
                holder.binding.eventText.setBackgroundExtension(R.color.schedule_1)
            }
            1 -> {
                holder.binding.eventText.setBackgroundExtension(R.color.schedule_2)
            }
            2 -> {
                holder.binding.eventText.setBackgroundExtension(R.color.schedule_3)
            }
            else -> {
                holder.binding.eventText.setBackgroundExtension(R.color.schedule_4)
            }
        }
    }


    override fun getItemCount(): Int = list.size


    class ViewHolder(val binding: SchedulingItemLayoutRectangelBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }
}