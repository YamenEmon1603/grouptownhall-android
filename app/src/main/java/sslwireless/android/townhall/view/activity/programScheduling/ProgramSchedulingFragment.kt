package sslwireless.android.townhall.view.activity.programScheduling

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_program_scheduling.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.ScheduleItem
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.utils.CommunicationViewModel

class ProgramSchedulingFragment : BaseFragment() {
    private val cViewModel: CommunicationViewModel by activityViewModels()

    override fun viewRelatedTask() {
        cViewModel.townHallResponse.observe(this, Observer {
            it ?: return@Observer

            showData(it)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_program_scheduling, container, false)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

    private fun showData(townHallResponse: TownHallResponse) {
        val scheduleList =
            townHallResponse.townhall?.schedule ?: arrayListOf()

        programRecycler.adapter = ProgramAdapter(
            scheduleList as ArrayList<ScheduleItem>,
            getDataManager().mPref.prefGetTownhallKey()?.townhall?.date ?: ""
        )
    }
}