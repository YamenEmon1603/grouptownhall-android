package sslwireless.android.townhall.view.activity.programScheduling

import android.os.Bundle
import kotlinx.android.synthetic.main.activity_schedule.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.ScheduleItem
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity

class ScheduleActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
    }

    override fun viewRelatedTask() {
        val scheduleList =
            getDataManager().mPref.prefGetTownhallKey()?.townhall?.schedule ?: arrayListOf()
        programRecycler.adapter = ProgramAdapter(
            scheduleList as ArrayList<ScheduleItem>,
            getDataManager().mPref.prefGetTownhallKey()?.townhall?.date ?: ""
        )

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}