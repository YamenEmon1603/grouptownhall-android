package sslwireless.android.townhall.view.activity.qrPay

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.databinding.ActivityBanglaQrScanResultBinding
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.qrPay.bottomDialog.PinBottomSheet
import sslwireless.android.townhall.view.activity.qrPay.model.BanglaQRFundingSource
import sslwireless.android.townhall.view.activity.qrPay.model.BanglaQRInitialization
import sslwireless.android.townhall.view.activity.qrPay.model.QRResponse
import sslwireless.android.townhall.view.activity.qrPay.model.emvParser.EMVParserModel
import sslwireless.android.townhall.view.activity.qrPay.viewmodel.QrPayViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import java.util.*


class BanglaQrScanResultActivity : BaseActivity(), SSLCTransactionResponseListener,
    PinBottomSheet.PinBottomSheetListener {
    private lateinit var binding: ActivityBanglaQrScanResultBinding
    lateinit var emvParserModels: ArrayList<EMVParserModel>
    var merchantName: String? = null
    var merchantPanId: String? = null
    var merchantCity: String? = null
    var payload: String? = null
    private var transactionId:String?=null
    private val viewModel: QrPayViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(QrPayViewModel(getDataManager())))
            .get(QrPayViewModel::class.java)
    }
    private var banglaQRInitialization: BanglaQRInitialization? = null
    private var isTagValueMatched: Boolean = false
    private var qrResponse =
        QRResponse(
            null, null, null, null, null,
            null, null, null, null, null,
            null, null, null, null, null,
            null, null, null
        )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bangla_qr_scan_result)
    }

    override fun viewRelatedTask() {

        payload = intent.getStringExtra("payload")
        qrResponse.payload = payload
        binding.amountEt.filters = arrayOf<InputFilter>(MoneyValueFilter())



        emvParserModels = intent.getSerializableExtra("emv_models") as ArrayList<EMVParserModel>

        initiateResultDialog()

        binding.closeDialogIv.setOnClickListener {
            onBackPressed()
        }

        binding.paymentBtn.setOnClickListener {
            /* AlertDialog.Builder(this)
                 .setTitle("Confirmation!")
                 .setMessage("Are you sure to proceed with this payment?")
                 .setPositiveButton(
                     R.string.yes
                 ) { dialog, which ->
                     dialog.dismiss()
                     //showProgressDialog("Transaction in progress")
                     val intent = Intent(this,BanglaQrSummary::class.java).apply {
                         putExtra("qrResponse", qrResponse)
                     }
                     startActivity(intent)
                 }.setNegativeButton(R.string.no, null)
                 .show()*/
            if (binding.amountEt.isEnabled) {
                val intent = Intent(this, BanglaQrSummary::class.java).apply {
                    putExtra("qrResponse", qrResponse)
                }
                startActivity(intent)
            } else {
                PinBottomSheet(this).show(supportFragmentManager, "12345")
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200) {
            when (key) {
                "qr" -> {
                    val sslCommerzIntegrationModel: SslCommerzIntegrationModel =
                        result.data.data.fromResponse()!!

                    initSdk(this, sslCommerzIntegrationModel, getDataManager(),true)

                }
                "validate" -> {
                    showEditDialog(
                        true,
                        result.data.message
                    )
                }
                "rating"->{
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                    finish()
                }
            }
        } else {
            showErrorToast(result.data?.message ?: getString(R.string.something_went_wrong))
        }

    }

    private fun initiateResultDialog() {

        var amount: Double? = null
        var tipConveyanceAmount: Double? = null
        var tipConveyancePercentage: Double? = null
        var total = 0.00


        binding.tipConveyanceLl.visibility = View.GONE
        binding.tipConveyanceEt.setText("")


        for (emv in emvParserModels) {
            when (emv.tag) {
                "01" -> {
                    when (emv.value) {
                        "11" -> {
                            binding.amountEt.isEnabled = true
                            amount = 0.0
                        }
                        "12" -> {
                            binding.amountTextInput.makeGone()
                            binding.amountTvLayout.makeVisible()
                            binding.amountEt.isEnabled = false
                            binding.amountTextInput.background =
                                ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                            amount = emvParserModels.single { s -> s.tag == "54" }.value.toDouble()
                        }
                    }
                }
                "04" -> {
                    if (BanglaQRFundingSource.FundingSource.MASTERCARD == banglaQRInitialization?.fundingSource) {
                        isTagValueMatched = true
                        val lastBit = calculateCheckDigit(emv.value)
                        merchantPanId = emv.value + lastBit
                        //Log.e("ASIF", "initiateResultDialog: $lastBit")
                    }
                }
                "02" -> {
                    if (BanglaQRFundingSource.FundingSource.VISA == banglaQRInitialization?.fundingSource) {
                        isTagValueMatched = true
                        merchantPanId = emv.value
                    }
                }
                "12" -> {
                    if (BanglaQRFundingSource.FundingSource.AMEX == banglaQRInitialization?.fundingSource) {
                        isTagValueMatched = true

                        //Log.e("ASIF", "initiateResultDialog val: " + emv.value)
                        //Log.e("ASIF", "initiateResultDialog length: " + emv.value.length)

                        if (emv.value.length == 28) {
                            val tempVal = emv.value.substring(emv.value.length - 11)
                            val amexCardVal = tempVal.substring(0, tempVal.length - 1)

                            merchantPanId = amexCardVal
                        } else {
                            isTagValueMatched = false
                        }
                    }
                }
                "15" -> {
                    if (BanglaQRFundingSource.FundingSource.UNIONPAY == banglaQRInitialization?.fundingSource) {
                        isTagValueMatched = true

                        if (emv.value.length == 31) {
                            val unionPayVal = emv.value.substring(emv.value.length - 15)
                            //Log.e("ASIF", "initiateResultDialog union pay: " + unionPayVal)
                            merchantPanId = unionPayVal
                        } else {
                            isTagValueMatched = false
                        }
                    }
                }
                "28" -> {
                    if (BanglaQRFundingSource.FundingSource.CASA == banglaQRInitialization?.fundingSource) {
                        isTagValueMatched = true
                        merchantPanId = emv.value
                    }
                }
                "55" -> {
                    binding.tipConveyanceLl.visibility = View.VISIBLE
                    when (emv.value) {
                        "01" -> {
                            binding.tipConveyanceTil.hint = "Tip"
                            binding.tipConveyanceEt.isEnabled = true
                            tipConveyanceAmount = 0.0
                        }
                        "02" -> {
                            binding.tipConveyanceTil.hint = "Convenience fee"
                            binding.tipConveyanceEt.isEnabled = false
                            binding.tipConveyanceTil.background =
                                ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                            tipConveyanceAmount =
                                emvParserModels.single { s -> s.tag == "56" }.value.toDouble()
                            binding.tipConveyanceTvLayout.makeVisible()
                            binding.tipConveyanceEt.makeGone()

                        }
                        "03" -> {
                            binding.tipConveyanceEt.isEnabled = false
                            binding.tipConveyanceTil.background =
                                ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                            tipConveyancePercentage =
                                emvParserModels.single { s -> s.tag == "57" }.value.toDouble()
                            binding.tipConveyanceTil.hint =
                                "Convenience fee ($tipConveyancePercentage%)"


                        }
                    }
                }
                "52" -> {
                    qrResponse.mcc = emv.value
                }
                "59" -> {
                    merchantName = emv.value
                }
                "60" -> {
                    merchantCity = emv.value
                }
                "62" -> {
                    initAdditionalDataIntoDialog()
                    val qwe = emv.value.indexOfFirst { it == 'T' }
                    qrResponse.terminalId = emv.value.substring(qwe, emv.length)
                }
            }
        }

        /*if (!isTagValueMatched) {
            if (BanglaQRFundingSource.FundingSource.MASTERCARD == banglaQRInitialization?.fundingSource) {
                showAlertToUser("This is not a valid MasterCard QR")
            } else if (BanglaQRFundingSource.FundingSource.VISA == banglaQRInitialization?.fundingSource) {
                showAlertToUser("This is not a valid VISA QR")
            } else if (BanglaQRFundingSource.FundingSource.AMEX == banglaQRInitialization?.fundingSource) {
                showAlertToUser("This is not a valid AMEX QR")
            } else if (BanglaQRFundingSource.FundingSource.UNIONPAY == banglaQRInitialization?.fundingSource) {
                showAlertToUser("This is not a valid UnionPay QR")
            } else if (BanglaQRFundingSource.FundingSource.CASA == banglaQRInitialization?.fundingSource) {
                showAlertToUser("This is not a valid SSLCommerz QR")
            } else {
                showAlertToUser("Not a valid card format")
            }
        }*/

        binding.amountEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0!!.toString().equals("")) {

                    amount = p0.toString().toDouble()
                    when {
                        tipConveyanceAmount != null -> {
                            total = amount!! + tipConveyanceAmount
                            updateTotal(total)
                        }
                        tipConveyancePercentage != null -> {
                            binding.tipConveyanceEt.setText(((amount!! * tipConveyancePercentage) / 100).toString())
                            total = amount!! + ((amount!! * tipConveyancePercentage) / 100)
                            updateTotal(total)
                        }
                        else -> {
                            updateTotal(amount!!)
                        }
                    }


                } else {
                    when {
                        tipConveyanceAmount != null -> {
                            total = tipConveyanceAmount
                            updateTotal(total)
                        }
                        else -> {
                            binding.tipConveyanceEt.setText("0")
                            updateTotal(0.00)
                        }
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        binding.tipConveyanceEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (!p0!!.toString().equals("")) {
                    total = amount!! + p0.toString().toDouble()
                    updateTotal(total!!)
                } else {
                    updateTotal(amount!!)
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        binding.loyalityNoEt.doOnTextChanged { text, start, before, count ->
            qrResponse.loyaltyNumber = text.toString()
        }
        binding.billNoEt.doOnTextChanged { text, start, before, count ->
            qrResponse.billNumber = text.toString()
        }
        binding.mobileEt.doOnTextChanged { text, start, before, count ->
            qrResponse.mobileNumber = text.toString()
        }
        binding.storeIdEt.doOnTextChanged { text, start, before, count ->
            qrResponse.storeId = text.toString()
        }
        binding.referenceIdEt.doOnTextChanged { text, start, before, count ->
            qrResponse.referenceId = text.toString()
        }
        binding.customerIdEt.doOnTextChanged { text, start, before, count ->
            qrResponse.customerId = text.toString()
        }
        binding.terminalIdEt.doOnTextChanged { text, start, before, count ->
            qrResponse.terminalId = text.toString()
        }
        binding.purposeEt.doOnTextChanged { text, start, before, count ->
            qrResponse.purpose = text.toString()
        }

        amount?.let {
            if (it > 0) {
                binding.amountEt.setText((String.format("%.02f", it)))
                binding.amountTv.text = "৳ " + (String.format("%.02f", it))
                qrResponse.amount = binding.amountEt.text.toString()
            } else {
                binding.amountEt.setText("")
                binding.amountTv.text = ""
                qrResponse.amount = binding.amountEt.text.toString()
            }
            updateTotal(it)
        }
        tipConveyanceAmount?.let {
            if (it > 0) {
                binding.tipConveyanceEt.setText((String.format("%.02f", it)))
                binding.tipConveyanceTv.text = (String.format("%.02f", it))
                qrResponse.tipConvenseAmount = binding.tipConveyanceEt.text.toString()
            } else {
                binding.tipConveyanceEt.setText("")
                binding.tipConveyanceTv.text = ""
                qrResponse.tipConvenseAmount = binding.tipConveyanceEt.text.toString()
            }
            total = amount!! + it
            updateTotal(total)
        }
        tipConveyancePercentage?.let {
            if (it > 0) {
                binding.tipConveyanceEt.setText((String.format("%.02f", ((amount!! * it) / 100))))
                qrResponse.tipConvensePercentage = binding.tipConveyanceEt.text.toString()
            } else {
                binding.tipConveyanceEt.setText("")
                qrResponse.tipConvensePercentage = binding.tipConveyanceEt.text.toString()
            }
            total = amount!! + ((amount!! * it) / 100)
            updateTotal(total)
        }
        binding.nameLogo.text = merchantName?.first().toString().toUpperCase()
        binding.merchantTv.text = "$merchantName"
        qrResponse.merchantName = merchantName
        qrResponse.merchantCity = merchantCity

        binding.merchantIdTv.text = "$merchantCity"
        qrResponse.merchantID = merchantPanId

        /*binding.dateTv.text =
            SimpleDateFormat("dd-MM-yyyy | hh:mm a").format(Date(Calendar.getInstance().timeInMillis))
        qrResponse.currentDate = binding.dateTv.text.toString()*/


    }

    private fun showAlertToUser(message: String) {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(getString(R.string.app_name))
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(
                "Exit"
            ) { dialog, id ->
                dialog.dismiss()
                val intent = intent
                setResult(RESULT_CANCELED, intent)
                finish()
            }
//        alertDialogBuilder.setNegativeButton(
//            "Exit"
//        ) { dialog, id ->
//            dialog.dismiss()
//            val intent = intent
//            setResult(RESULT_CANCELED, intent)
//            finish()
//        }
        val alert: AlertDialog = alertDialogBuilder.create()
        alert.show()
    }

    private fun updateTotal(total: Double) {
        binding.paymentBtn.isEnabled = total > 0
        qrResponse.totalAmount = total.toString()
        binding.paymentBtn.text = getString(R.string.proceed_to_pay, String.format("%.02f", total))
    }

    private fun initAdditionalDataIntoDialog() {
        for (parentEmv in emvParserModels) {
            if (parentEmv.tag.equals("62")) { //0311S-E6F9-41310718T41315F1C1908EE503
                for (emv in parentEmv.childEMVParserModel) {
                    when (emv.tag) {
                        "01" -> {
                            binding.billNoLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.billNoEt.isEnabled = true
                                    binding.billNoEt.setText("")
                                    qrResponse.billNumber = binding.billNoEt.text.toString()
                                }
                                else -> {
                                    binding.billNoEt.setText(emv.value)
                                    binding.billNoTv.text = emv.value
                                    qrResponse.billNumber = binding.billNoEt.text.toString()
                                    binding.billNoEt.isEnabled = false
                                    binding.billNoTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.billNoTextInput.makeGone()
                                    binding.billNoTvLayout.makeVisible()
                                }
                            }
                        }
                        "02" -> {
                            binding.mobileLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.mobileEt.isEnabled = true
                                    binding.mobileEt.setText("")
                                    qrResponse.mobileNumber = binding.mobileEt.text.toString()
                                }
                                else -> {
                                    binding.mobileEt.setText(emv.value)
                                    binding.mobileTv.text = emv.value
                                    qrResponse.mobileNumber = binding.mobileEt.text.toString()
                                    binding.mobileEt.isEnabled = false
                                    binding.mobileNoTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.mobileTvLayout.makeVisible()
                                    binding.mobileNoTextInput.makeGone()
                                }
                            }
                        }
                        "03" -> {
                            binding.storeIdLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.storeIdEt.isEnabled = true
                                    binding.storeIdEt.setText("")
                                    qrResponse.storeId = binding.storeIdEt.text.toString()
                                }
                                else -> {
                                    binding.storeIdEt.setText(emv.value)
                                    binding.storeIdTv.text = emv.value
                                    qrResponse.storeId = binding.storeIdEt.text.toString()
                                    binding.storeIdEt.isEnabled = false
                                    binding.storeTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.storeTextInput.makeGone()
                                    binding.storeIdTvLayout.makeVisible()
                                }
                            }
                        }
                        "04" -> {
                            binding.loyalityNoLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.loyalityNoEt.isEnabled = true
                                    binding.loyalityNoEt.setText("")
                                    qrResponse.loyaltyNumber = binding.loyalityNoEt.text.toString()
                                }
                                else -> {
                                    binding.loyalityNoEt.setText(emv.value)
                                    binding.loyalityNoTv.text = emv.value
                                    qrResponse.loyaltyNumber = binding.loyalityNoEt.text.toString()
                                    binding.loyalityNoEt.isEnabled = false
                                    binding.loyaltiTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.storeIdTvLayout.makeVisible()
                                    binding.storeTextInput.makeGone()
                                }
                            }
                        }
                        "05" -> {
                            binding.referenceIdLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.referenceIdEt.isEnabled = true
                                    binding.referenceIdEt.setText("")
                                    qrResponse.referenceId = binding.referenceIdEt.text.toString()
                                }
                                else -> {
                                    binding.referenceIdEt.setText(emv.value)
                                    binding.referenceIdTv.text = emv.value
                                    qrResponse.referenceId = binding.referenceIdEt.text.toString()
                                    binding.referenceIdEt.isEnabled = false
                                    binding.referenceTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.referenceTextInput.makeGone()
                                    binding.referenceIdTvLayout.makeVisible()
                                }
                            }
                        }
                        "06" -> {
                            binding.customerIdLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.customerIdEt.isEnabled = true
                                    binding.customerIdEt.setText("")
                                    qrResponse.customerId = binding.customerIdEt.text.toString()
                                }
                                else -> {
                                    binding.customerIdEt.setText(emv.value)
                                    binding.customerIdTv.text = emv.value
                                    qrResponse.customerId = binding.customerIdEt.text.toString()
                                    binding.customerIdEt.isEnabled = false
                                    binding.customerIdTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.customerIdTextInput.makeGone()
                                    binding.customerIdTvLayout.makeVisible()
                                }
                            }
                        }
                        "07" -> {
                            binding.terminalIdLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.terminalIdEt.isEnabled = true
                                    binding.terminalIdEt.setText("")
                                    qrResponse.terminalId = binding.terminalIdEt.text.toString()
                                }
                                else -> {
                                    binding.terminalIdEt.setText(emv.value)
                                    binding.terminalIdTv.text = emv.value
                                    qrResponse.terminalId = binding.terminalIdEt.text.toString()
                                    binding.terminalIdEt.isEnabled = false
                                    binding.terminalIdTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.terminalIdTextInput.makeGone()
                                    binding.terminalTvLayout.makeVisible()
                                }
                            }
                        }
                        "08" -> {
                            binding.purposeLl.visibility = View.VISIBLE
                            when (emv.value) {
                                "***" -> {
                                    binding.purposeEt.isEnabled = true
                                    binding.purposeEt.setText("")
                                    qrResponse.purpose = binding.purposeEt.text.toString()
                                }
                                else -> {
                                    binding.purposeEt.setText(emv.value)
                                    binding.purposeTv.text = emv.value
                                    qrResponse.purpose = binding.purposeEt.text.toString()
                                    binding.purposeEt.isEnabled = false
                                    binding.purposeTextInput.background =
                                        ContextCompat.getDrawable(this, R.drawable.disable_drawable)
                                    binding.terminalIdTextInput.makeGone()
                                    binding.terminalTvLayout.makeVisible()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun luhnCheck(card: String?): Boolean {
        if (card == null) return false
        val checkDigit = card[card.length - 1]
        val digit = calculateCheckDigit(card.substring(0, card.length - 1))
        return checkDigit == digit!![0]
    }

    fun calculateCheckDigit(card: String?): String? {
        if (card == null) return null

        val digit: String
        /* convert to array of int for simplicity */
        val digits = IntArray(card.length)
        for (i in card.indices) {
            digits[i] = Character.getNumericValue(card[i])
        }

        /* double every other starting from right - jumping from 2 in 2 */
        run {
            var i = digits.size - 1
            while (i >= 0) {
                digits[i] += digits[i]

                /* taking the sum of digits grater than 10 - simple trick by substract 9 */
                if (digits[i] >= 10) {
                    digits[i] = digits[i] - 9
                }
                i -= 2
            }
        }
        var sum = 0
        for (i in digits.indices) {
            sum += digits[i]
        }
        /* multiply by 9 step */
        sum *= 9

        /* convert to string to be easier to take the last digit */
        digit = sum.toString() + ""
        return digit.substring(digit.length - 1)
    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        p0?.run {
            transactionId = tranId
            viewModel.validateTransaction(
                tranId,
                this@BanglaQrScanResultActivity,
                this@BanglaQrScanResultActivity
            )
        }
    }

    override fun transactionFail(p0: String?) {
        showEditDialog(false, p0.toString())
    }

    override fun closed(message: String?) {

    }

//    override fun merchantValidationError(p0: String?) {
//        showEditDialog(false, p0.toString())
//    }

    override fun onApplyPin(pin: String) {
        qrResponse.qrPin = pin
        viewModel.initiatePaymentSdk(qrResponse, this, this)
    }

    private fun showEditDialog(status: Boolean, returnMessage: String) {
        val newDialog = Dialog(this, R.style.customDialog)
        newDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )

        if (status) {
            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.pay_successfull)
            customDialogBinding.textView83.text = returnMessage

        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()
            customDialogBinding.textView86.makeVisible()

            customDialogBinding.textView84.text = getString(R.string.failed_text)
            /*customDialogBinding.textView84.background =
                ContextCompat.getDrawable(this, R.drawable.failed_drawable)*/

            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            if (status) {
                newDialog.dismiss()
                transactionId?.let {
                    if (customDialogBinding.ratingBar.rating.toInt()>0) {
                        viewModel.apiRating(
                            customDialogBinding.ratingBar.rating.toInt().toString(),
                            it,
                            this,
                            this
                        )
                    }
                }
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            } else {
                newDialog.dismiss()
                val intent = Intent(this, QrActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            }
        }

        customDialogBinding.textView86.setOnClickListener {
            newDialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        newDialog.setContentView(customDialogBinding.root)

        newDialog.show()
        newDialog.setCancelable(false)
    }
}