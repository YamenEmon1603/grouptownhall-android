package sslwireless.android.townhall.view.activity.qrPay

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.databinding.ActivityBanglaQrSummaryBinding
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.qrPay.bottomDialog.PinBottomSheet
import sslwireless.android.townhall.view.activity.qrPay.model.QRResponse
import sslwireless.android.townhall.view.activity.qrPay.viewmodel.QrPayViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*

class BanglaQrSummary : BaseActivity(), PinBottomSheet.PinBottomSheetListener,
    SSLCTransactionResponseListener {
    private lateinit var binding: ActivityBanglaQrSummaryBinding
    private var transactionId:String?=null
    private val qrResponse: QRResponse by lazy {
        intent.getSerializableExtra("qrResponse") as QRResponse
    }
    private val viewModel: QrPayViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(QrPayViewModel(getDataManager())))
            .get(QrPayViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bangla_qr_summary)
    }

    override fun viewRelatedTask() {
        qrResponse.run {
            binding.merchantTv.text = merchantName?:""
            binding.merchantIdTv.text = merchantCity?:""
            binding.nameLogo.text = merchantName?.first().toString().toUpperCase()
            binding.amountTv.text = "৳ ${totalAmount?.toDouble()?.toDecimalTwo()}"

            tipConvenseAmount?.let {
                binding.tipConveyanceLl.makeVisible()
                binding.tipConveyanceTv.text = it
            }
            billNumber?.let {
                binding.billNoLl.makeVisible()
                binding.billNoTv.text = it
            }
            mobileNumber?.let {
                binding.mobileLl.makeVisible()
                binding.mobileTv.text = it
            }
            storeId?.let {
                binding.storeIdLl.makeVisible()
                binding.storeIdTv.text = it
            }
            loyaltyNumber?.let {
                binding.loyalityNoLl.makeVisible()
                binding.loyalityNoTv.text = it
            }
            referenceId?.let {
                binding.referenceIdLl.makeVisible()
                binding.referenceIdTv.text = it
            }
            customerId?.let {
                binding.customerIdLl.makeVisible()
                binding.customerIdTv.text = it
            }
            terminalId?.let {
                binding.terminalIdLl.makeVisible()
                binding.terminalIdTv.text = it
            }
            purpose?.let {
                binding.purposeLl.makeVisible()
                binding.purposeTv.text = it
            }
            customerId?.let {
                binding.customerIdLl.makeVisible()
                binding.customerIdTv.text = it
            }
        }

        binding.toolbar.setOnClickListener {
            onBackPressed()
        }

        binding.paymentBtn.setOnClickListener {
            PinBottomSheet(this).show(supportFragmentManager,"123456")
        }
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code==200) {
            when (key){
                "qr"->{

                    val sslCommerzIntegrationModel: SslCommerzIntegrationModel =
                        result.data.data.fromResponse()!!
                    transactionId = sslCommerzIntegrationModel.transaction_id
                    initSdk(this,sslCommerzIntegrationModel,getDataManager(),true)

                }
                "validation" -> {
                    showEditDialog(
                        true,
                        result.data.message
                    )
                }
            }
        } else {
            if (key=="validation") {
                showEditDialog(
                    false,
                    result.data?.message?:getString(R.string.something_went_wrong)
                )
            } else {
                showErrorToast(result.data?.message ?: getString(R.string.something_went_wrong))
            }
        }
    }

    override fun onApplyPin(pin: String) {
        qrResponse.qrPin = pin
        viewModel.initiatePaymentSdk(qrResponse,this,this)
    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        p0?.run {
            transactionId= tranId
            viewModel.validateTransaction(tranId,this@BanglaQrSummary,this@BanglaQrSummary)
        }

    }

    override fun transactionFail(p0: String?) {
        viewModel.validateTransaction(transactionId?:"",this@BanglaQrSummary,this@BanglaQrSummary)
        //showEditDialog(false, p0.toString())
    }

    override fun closed(message: String?) {

    }

//    override fun merchantValidationError(p0: String?) {
//        viewModel.validateTransaction(transactionId?:"",this@BanglaQrSummary,this@BanglaQrSummary)
//    }

    private fun showEditDialog(status: Boolean, returnMessage: String) {
        val newDialog = Dialog(this, R.style.customDialog)
        newDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )

        if (status) {
            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.pay_successfull)
            customDialogBinding.textView83.text = returnMessage

        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.textView83.makeVisible()
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()
            customDialogBinding.textView86.makeVisible()

            customDialogBinding.textView84.text = getString(R.string.failed_text)
            /*customDialogBinding.textView84.background =
                ContextCompat.getDrawable(this, R.drawable.failed_drawable)*/

            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            if (status) {
                newDialog.dismiss()
                transactionId?.let {
                    if (customDialogBinding.ratingBar.rating.toInt()>0) {
                        viewModel.apiRating(
                            customDialogBinding.ratingBar.rating.toInt().toString(),
                            it,
                            this,
                            this
                        )
                    }
                }
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            } else {
                newDialog.dismiss()
                val intent = Intent(this, QrActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
                finish()
            }
        }
        customDialogBinding.textView86.setOnClickListener {
            newDialog.dismiss()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            finish()
        }

        newDialog.setContentView(customDialogBinding.root)

        newDialog.show()
        newDialog.setCancelable(false)
    }
}