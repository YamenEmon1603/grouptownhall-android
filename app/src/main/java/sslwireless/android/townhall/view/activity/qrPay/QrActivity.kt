package sslwireless.android.townhall.view.activity.qrPay

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.budiyev.android.codescanner.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.databinding.ActivityQrBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.qrPay.model.QRResponse
import sslwireless.android.townhall.view.activity.qrPay.model.emvParser.EMVParserModel
import sslwireless.android.townhall.view.base.BaseActivity
import kotlin.experimental.and

class QrActivity : BaseActivity() {
    private lateinit var binding: ActivityQrBinding
    private val PERMISSION_REQUEST_CODE: Int = 100
    private lateinit var codeScanner: CodeScanner
    private val emvParserModels: ArrayList<EMVParserModel> = ArrayList()
    private val childEmvParserModels: ArrayList<EMVParserModel> = ArrayList()



    var payload = ""
    var globalPayload = ""
    private var flagCheck: Boolean = false
    private var flashCheck: Boolean = false

    private val tagObject = arrayOf(
        "26", "27", "29",
        "30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
        "40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
        "50", "51",
        "62", "88"
    )

    private val tagObjectList =
        listOf(*tagObject)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_qr)

        binding.referenceNumberLayout.setOnClickListener {

        }

    }

    override fun onResume() {
        super.onResume()

        if (flagCheck) {
            codeScanner.startPreview()
        }
    }

    override fun onPause() {
        super.onPause()
        codeScanner.stopPreview()
        codeScanner.releaseResources()
    }

    override fun onStop() {
        super.onStop()
        codeScanner.stopPreview()
        codeScanner.releaseResources()
    }

    override fun onDestroy() {
        super.onDestroy()
        codeScanner.stopPreview()
        codeScanner.releaseResources()
    }

    override fun viewRelatedTask() {
        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.qr_color)
        }

        codeScanner = CodeScanner(this, binding.scannerView)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)

        binding.imgBack.setOnClickListener {
            codeScanner.stopPreview()
            codeScanner.releaseResources()
            onBackPressed()
        }
        initiateScanner()

        binding.flashBtn.setOnClickListener {
            flashCheck = !flashCheck
            codeScanner.isFlashEnabled = flashCheck
            codeScanner.startPreview()
        }

        binding.callGroup.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:09612222777")
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        startActivity(Intent(this, MainActivity::class.java))
        customBackPressed()
        finish()
    }

    private fun showAlertToUser(message: String) {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle(getString(R.string.app_name))
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(
                getString(R.string.scan_again)
            ) { dialog, id ->
                //flag = 1;
                dialog.dismiss()
                codeScanner.startPreview()
            }
        alertDialogBuilder.setNegativeButton(
            getString(R.string.exit)
        ) { dialog, id ->
            dialog.dismiss()
            showToast(this, message)
            onBackPressed()
        }
        val alert: AlertDialog = alertDialogBuilder.create()
        alert.show()
    }

    private fun crc16(buffer: ByteArray): Int {
        /* Note the change here */
        var crc = 0xFFFF
        for (j in buffer.indices) {
            crc = crc ushr 8 or (crc shl 8) and 0xffff
            crc = crc xor (buffer[j] and 0xff.toByte()).toInt() //byte to int, trunc sign
            crc = crc xor (crc and 0xff shr 4)
            crc = crc xor (crc shl 12 and 0xffff)
            crc = crc xor (crc and 0xFF shl 5 and 0xffff)
        }
        crc = crc and 0xffff
        return crc
    }

    private fun initiateScanner() {

        emvParserModels.clear()
        childEmvParserModels.clear()

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        /*payload = "00020101021102164020040020000018153141610050005204460000000010005385204533153030505802BD5909RONG_MELA6010LAKSHMIPUR62370311S-E6F9-41310718T41315F1C1908EE50363047758"
        globalPayload = payload
        if (payload.length >= 4) {
            val lastFourDigit = payload.substring(payload.length - 4)
            val dateSplitter1 = payload.split(lastFourDigit).toTypedArray()
            val forCRCCheckPayload = dateSplitter1[0]
            val crcRes: Int = crc16(forCRCCheckPayload.toByteArray())

            if (lastFourDigit == Integer.toHexString(crcRes).toUpperCase()) {
                cutPayload(payload)
            } else {
                showAlertToUser("CRC not matched. Transaction failed")
            }
        }*/

        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                payload = it.text
                globalPayload = payload
                if (payload.length >= 4) {
                    val lastFourDigit = payload.substring(payload.length - 4)
                    val dateSplitter1 = payload.split(lastFourDigit).toTypedArray()
                    val forCRCCheckPayload = dateSplitter1[0]
                    val crcRes: Int = crc16(forCRCCheckPayload.toByteArray())

                    if (lastFourDigit == Integer.toHexString(crcRes).toUpperCase()) {
                        cutPayload(payload)
                    } else {
                        showAlertToUser(getString(R.string.crc_not_match))
                    }
                    return@runOnUiThread
                }
                showAlertToUser(getString(R.string.invalid_qr))

            }
        }
        codeScanner.errorCallback = ErrorCallback {
            // or ErrorCallback.SUPPRESS
            runOnUiThread {
                Toast.makeText(
                    this, "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG
                ).show()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initRuntimePermission()
        } else {
            flagCheck = true
            codeScanner.startPreview()
        }
    }

    private fun cutPayload(payload: String) {

        var payload = payload
        val tag = payload.substring(0, 2)
        val tagLength = payload.substring(2, 4)
        val len = 4 + Integer.valueOf(tagLength)
        val tagValue = payload.substring(4, len)

        val emvParserModel = EMVParserModel(tag, tagLength.toInt(), tagValue, ArrayList())

        println("$tag -> $tagLength -> $tagValue")

        if (tagObjectList.contains(tag)) {
            if (tagValue.isNotEmpty()) {
                childEmvParserModels.clear()
                cutChildPayload(emvParserModel, tagValue)
            }
        } else {
            emvParserModels.add(emvParserModel)
        }

        payload = payload.substring(len)

        if (payload.isNotEmpty()) {
            cutPayload(payload)
        } else {
            codeScanner.stopPreview()
            codeScanner.releaseResources()
            if (emvParserModels.find { it.tag=="02" }?.value?.startsWith("402004") != false) {
                startActivity(
                    Intent(this, BanglaQrScanResultActivity::class.java)
                        .putExtra("emv_models", emvParserModels)
                        // .putExtra("mainModel", banglaQRInitialization)
                        .putExtra("payload", globalPayload)
                )

                emvParserModels.clear()

                var payload = ""
                for (emv in emvParserModels) {
                    payload = "$payload\n${emv.tag}  ->  ${emv.length}  ->  ${emv.value}"
                }
                this.payload = ""
            } else {
                emvParserModels.clear()
                showAlertToUser(getString(R.string.invalid_qr))
            }
        }

    }

    private fun cutChildPayload(parentEmvParserModel: EMVParserModel, payload: String) {
        var payload = payload
        val tag = payload.substring(0, 2)
        val tagLength = payload.substring(2, 4)
        val len = 4 + Integer.valueOf(tagLength)
        val tagValue = payload.substring(4, len)

        val emvParserModel = EMVParserModel(tag, tagLength.toInt(), tagValue, ArrayList())
        childEmvParserModels.add(emvParserModel)

        println("            $tag -> $tagLength -> $tagValue")
        payload = payload.substring(len)
        if (!payload.isEmpty()) {
            cutChildPayload(parentEmvParserModel, payload)
        } else {
            parentEmvParserModel.childEMVParserModel = childEmvParserModels
            emvParserModels.add(parentEmvParserModel)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initRuntimePermission() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.CAMERA),
                PERMISSION_REQUEST_CODE
            )
        } else {
            flagCheck = true
            codeScanner.startPreview()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                flagCheck = true
                codeScanner.startPreview()
            } else {
                showToast(this, "camera permission denied")
                onBackPressed()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1002 && resultCode == RESULT_OK) {
            val qrResponseData = data?.getSerializableExtra("qrResponse") as QRResponse
            //IntegrateBanglaQR.mBanglaQRBaseResponseListener?.banglaQRSuccessResponse(qrResponseData)
            //finish()
        } else if (requestCode == 1002 && resultCode == RESULT_CANCELED) {
            showToast(this, "Transaction Failed")
            onBackPressed()
        } else if (requestCode == PERMISSION_REQUEST_CODE) {
            binding.scannerViewLayout.visibility = View.VISIBLE
            codeScanner.startPreview()
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }
}