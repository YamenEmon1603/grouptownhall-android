package sslwireless.android.townhall.view.activity.qrPay.bottomDialog


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pin_bottom_sheet.*
import sslwireless.android.townhall.R


class PinBottomSheet(private val listener: PinBottomSheetListener) : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pin_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pinEditText.requestFocus()
        imgClose.setOnClickListener {
            dismiss()
        }
        pinEditText.doOnTextChanged { text, start, before, count ->
            applyBtn.isEnabled = text.toString().length==6
        }

        applyBtn.setOnClickListener {
            dismiss()
            listener.onApplyPin(pinEditText.text.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }

    private fun hideKeyboard(view: View) {
        val imm =view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    interface PinBottomSheetListener {
        fun onApplyPin(pin: String)
    }


}
