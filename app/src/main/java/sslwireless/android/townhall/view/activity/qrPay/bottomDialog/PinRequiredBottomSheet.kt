package sslwireless.android.townhall.view.activity.qrPay.bottomDialog


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.pin_required_bottom_sheet.*
import sslwireless.android.townhall.R


class PinRequiredBottomSheet(private val listener: PinRequiredBottomSheetListener) : BottomSheetDialogFragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.pin_required_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        proceedBtn.setOnClickListener {
            dismiss()
            listener.onProceedPin()
        }
        cancelBtn.setOnClickListener {
            dismiss()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }




    interface PinRequiredBottomSheetListener {
        fun onProceedPin()
    }


}
