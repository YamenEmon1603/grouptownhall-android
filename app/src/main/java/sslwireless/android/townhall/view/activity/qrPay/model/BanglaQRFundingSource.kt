package sslwireless.android.townhall.view.activity.qrPay.model

class BanglaQRFundingSource {
    enum class FundingSource {
        VISA, MASTERCARD, AMEX, UNIONPAY, CASA
    }
}