package sslwireless.android.townhall.view.activity.qrPay.model

import java.io.Serializable


/**
 * funding source should be VISA, AMEX, MASTERCARD, UNIONPAY,CASA
 * primary, secondary color means your app primary and secondary color code. like - #000000
 * */
data class BanglaQRInitialization(
    val fundingSource : BanglaQRFundingSource.FundingSource,
    val primaryColor : String,
    val secondaryColor : String
) : Serializable