package sslwireless.android.townhall.view.activity.qrPay.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class QRResponse(
    @SerializedName("merchant_name")
    var merchantName: String? = null,  //done

    @SerializedName("merchant_city")
    var merchantCity: String? = null,  //done

    @SerializedName("merchant_id")
    var merchantID: String? = null, //done

    var currentDate: String? = null,

    var amount: String? = null,

    @SerializedName("tip_convense_amount")
    var tipConvenseAmount: String? = null, //done

    @SerializedName("tip_convense_percentage")
    var tipConvensePercentage: String? = null, //done

    @SerializedName("bill_number")
    var billNumber: String? = null, //done

    var mobileNumber: String? = null,

    @SerializedName("tag_62_first_data")
    var storeId: String? = null, //done

    @SerializedName("loyalty_number")
    var loyaltyNumber: String? = null, //done

    var referenceId: String? = null,

    var customerId: String? = null,

    @SerializedName("tag_62_second_data")
    var terminalId: String? = null,  //done

    @SerializedName("purpose")
    var purpose: String? = null, //done

    @SerializedName("transaction_amount")
    var totalAmount: String? = null,  //done

    @SerializedName("merchant_mcc")
    var mcc: String? = null, //done

    @SerializedName("payload")
    var payload: String? = null, //done

    @SerializedName("token")
    var token: String? = null,

    @SerializedName("qr_pin")
    var qrPin: String? = null,


    @SerializedName("pay_with_save_card")
    var payWithSaveCard: Int = 1,

    val is_mobile: Boolean = true
) : Serializable













