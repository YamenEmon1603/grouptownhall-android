package sslwireless.android.townhall.view.activity.qrPay.model.emvParser

import java.io.Serializable

data class EMVParserModel(
    var tag: String,
    var length: Int,
    var value: String,
    var childEMVParserModel: ArrayList<EMVParserModel>
) : Serializable