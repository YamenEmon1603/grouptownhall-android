package sslwireless.android.townhall.view.activity.qrPay.viewmodel

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.activity.qrPay.model.QRResponse
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class QrPayViewModel(private val dataManager: DataManager) : BaseViewmodel(dataManager) {


    fun initiatePaymentSdk(
        qrResponse: QRResponse,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        qrResponse.token = dataManager.mPref.prefGetCustomerToken()
        dataManager.apiHelper.apiQRInit(
            qrResponse,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "qr"))
        )
    }

    fun validateTransaction(
        transactionId: String, lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiValidateTransaction(
            transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "validation"))
        )

    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating, transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }
}