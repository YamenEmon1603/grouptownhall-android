package sslwireless.android.townhall.view.activity.quickie

import android.os.Bundle
import android.view.View
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCCustomerInfoInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.activity_quickie.*
import kotlinx.android.synthetic.main.quicky_item_layout.*
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.OrderData
import sslwireless.android.townhall.data.model.PaymentStatusResponse
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.data.model.recentTopUp.RecentOrderData
import sslwireless.android.townhall.data.model.recentTopUp.RecentTopUp
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.desco.TransactionSuccessfulDF
import sslwireless.android.townhall.view.adapter.QuickieRecentRecyclerAdapter
import sslwireless.android.townhall.view.adapter.QuickieRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*

class QuickieActivity : BaseActivity(), QuickieRecyclerAdapter.QuickieListener,
    SSLCTransactionResponseListener {
    private val pref by lazy { getDataManager().mPref }
    private val quickListFav = ArrayList<FavoriteModel>()
    private val hashMap = HashMap<String, TopUpModel>()
    private val activeHashMap = HashMap<String, Boolean>()
    private var operatorID = -1
    private val dialogFragment by lazy { TransactionSuccessfulDF()}
    private lateinit var sslCommerzIntegrationModel: SslCommerzIntegrationModel
    private val topUpModels = ArrayList<TopUpModel>()
    private var myNumber:String?=null
    private val favAdapter: QuickieRecyclerAdapter by lazy {
        QuickieRecyclerAdapter(
            quickListFav,
            this,
            getDataManager().mPref.prefGetLanguage()
        )
    }
    private val language: String by lazy {
        getDataManager().mPref.prefGetLanguage()
    }
    private val viewModel: QuickieViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(QuickieViewModel(getDataManager())))
            .get(QuickieViewModel::class.java)
    }
    //private val recentAdapter: QuickieRecyclerAdapter by lazy { QuickieRecyclerAdapter(quickListFav) }
    private var flag: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quickie)


        customTextView17.text = pref.prefGetCurrentUser().mobile
        if (pref.prefGetOperatorName().isNotEmpty() && pref.prefGetOperatorType().isNotEmpty()) {
            myNumber = pref.prefGetCurrentUser().mobile
            imageView17.operatorIcon(pref.prefGetOperatorName())
            operatorID = pref.prefGetOperatorName().operatorID(pref.prefGetOperatorType())
        } else {
            numberGroup.visibility = View.GONE
        }
        quickListFav.addAll(getDataManager().mPref.prefGetFavoriteList().filter { it.phone!=myNumber?:""})
        if (quickListFav.isNotEmpty()) {
            favGroup.makeVisible()
            favRecycler.adapter = favAdapter
        } else {
            favGroup.makeGone()
        }
        //recentRecycler.adapter = recentAdapter
    }

    override fun viewRelatedTask() {
        viewModel.recentTransactionService(this,this)
        backGroup.setOnClickListener {
            finish()
        }

        cardLayout1.setOnClickListener {
            if (!flag) {
                flag = true
                enterAmoutGroup.visibility = View.VISIBLE
                myNumberGroup.visibility = View.GONE
                view51.isSelected = false
                view50.isSelected = false
                view49.isSelected = false
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                onAmountRemove(number,false)

            }
        }

        cross.setOnClickListener {
            if (flag) {
                flag = false
                myNumberGroup.visibility = View.VISIBLE
                enterAmoutGroup.visibility = View.GONE
                customEdittext.setText("")
                view51.isSelected = false
                view50.isSelected = false
                view49.isSelected = false
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                onAmountRemove(number,false)

            }
        }

        view51.setOnClickListener {
            view51.isSelected = !view51.isSelected
            view50.isSelected = false
            view49.isSelected = false
            if (view51.isSelected) {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                val topUpModel = TopUpModel(50, operatorID, number, pref.prefGetOperatorType().toLowerCase())
                onAmountAdd(topUpModel)
            } else {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                onAmountRemove(number,false)
            }
        }

        view50.setOnClickListener {
            view51.isSelected = false
            view50.isSelected = !view50.isSelected
            view49.isSelected = false
            if (view50.isSelected) {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                val topUpModel = TopUpModel(100, operatorID, number, pref.prefGetOperatorType())
                onAmountAdd(topUpModel)
            } else {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                onAmountRemove(number,false)
            }
        }

        view49.setOnClickListener {
            view51.isSelected = false
            view50.isSelected = false
            view49.isSelected = !view49.isSelected
            if (view49.isSelected) {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                val topUpModel = TopUpModel(200, operatorID, number, pref.prefGetOperatorType())
                onAmountAdd(topUpModel)
            } else {
                val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
                onAmountRemove(number,false)
            }
        }

        customEdittext.doOnTextChanged { amount, start, before, count ->
            val number = pref.prefGetCurrentUser().mobile.substring(pref.prefGetCurrentUser().mobile.length - 11)
            if (amount.toString().trim().isNotEmpty() && amount.toString().trim().toInt()>9) {

                if (amount.toString().trim().toInt() < operatorID.getMinRechargeAmount(pref.prefGetOperatorType()) ||
                    amount.toString().trim().toInt() > operatorID.getMaxRechargeAmount(pref.prefGetOperatorType())) {
                    val minAmount = operatorID.getMinRechargeAmount(pref.prefGetOperatorType()).toString().digitChangeToBangla(language)
                    val maxAmount = operatorID.getMaxRechargeAmount(pref.prefGetOperatorType()).toString().digitChangeToBangla(language)
                    val msg = getString(R.string.recharge_range,minAmount,maxAmount,pref.prefGetOperatorType().operatorChangeToBangla(language))
                    showToast(this@QuickieActivity,msg)
                    onAmountRemove(number,true)
                    return@doOnTextChanged
                }
                val topUpModel = TopUpModel(amount.toString().trim().toInt(), operatorID, number, pref.prefGetOperatorType())
                onAmountAdd(topUpModel)
            } else {
                if (amount.toString().trim().isEmpty()){
                    onAmountRemove(number,false)
                } else {
                    onAmountRemove(number,true)
                }
            }
        }

        proceedToRecharge.setOnClickListener {
            topUpModels.clear()
            for (value in hashMap.values){
                topUpModels.add(value)
            }
            if (topUpModels.size>3) {
                showToast(this,getString(R.string.three_number_at_a_time))
                return@setOnClickListener
            }
            val topUpRequestModel = TopUpRequestModel(
                topUpModels,
                "",
                pref.prefGetCustomerToken()!!
            )
            viewModel.apiTopUpInit(topUpRequestModel, this, this)
        }
    }

    override fun onAmountAdd(item: TopUpModel) {
        activeHashMap[item.phone] = true
        hashMap[item.phone] = item
        proceedToRecharge.isEnabled = activeHashMap.size in 1..3 && !activeHashMap.containsValue(false)
        if (activeHashMap.size>3) {
            showToast(this,"Max 3 numbers")
        }
    }

    override fun onAmountRemove(phone: String,isNumberShowing:Boolean) {
        val number = phone.substring(phone.length - 11)
        if (isNumberShowing) {
            activeHashMap[number] = false
        } else {
            activeHashMap.remove(number)
        }
        hashMap.remove(number)
        proceedToRecharge.isEnabled = activeHashMap.size in 1..3 && !activeHashMap.containsValue(false)
        if (activeHashMap.size>3) {
            showToast(this,"Max 3 numbers")
        }
    }

    override fun transactionSuccess(p0: SSLCTransactionInfoModel?) {
        viewModel.apiPaymentStatus(sslCommerzIntegrationModel.transaction_id, this, this)
    }


    private fun handleSuccessDialog(holdData: OrderData?) {
        val args = Bundle()
        args.putBoolean("isSuccess", true)
        args.putBoolean("recharge", true)
        args.putBoolean("favorite", false)
        args.putSerializable("numbers", topUpModels)
        args.putSerializable("favNumbers", getDataManager().mPref.prefGetFavoriteList())
        args.putString("title", getString(R.string.transaction_successful))
        holdData?.let {
            args.putString(
                "holdText", getString(
                    R.string.bonus_text,
                    it.bonusAmount.toString(),
                    (it.amount - it.bonusAmount).toString(),
                    it.amount.toString(),
                    it.phone
                )
            )
            args.putString("bonus",it.bonusAmount.toString())
        }
        dialogFragment.setArguments(args)
        dialogFragment.show(supportFragmentManager, "dialog_submit_confirmation")
        dialogFragment.setOkClick(
            object : TransactionSuccessfulDF.OkClickListener {
                override fun onOkClick(rating:String) {
                    dialogFragment.dismiss()
                    finish()
                }

            }
        )
        dialogFragment.setTryAgainClick(
            object : TransactionSuccessfulDF.TryAgainClickListener {
                override fun onTryAgainClick() {
                    dialogFragment.dismiss()
                    finish()
                }

            }

        )

        dialogFragment.setOnProceed(
            object : TransactionSuccessfulDF.ProceedListener {
                override fun onProceed(isChecked:Boolean) {
                    viewModel.apiInitiateHoldUser(if (isChecked) "1" else "0",sslCommerzIntegrationModel.transaction_id,this@QuickieActivity,this@QuickieActivity)
                }

            }

        )
    }

    override fun transactionFail(p0: String?) {
        showToast(this, p0!!)
    }

    override fun closed(message: String?) {

    }

//    override fun merchantValidationError(p0: String?) {
//        showToast(this, p0!!)
//    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code==200) {
            if (key=="top_up") {
                val SSLCCustomerInfoInitializer =
                    SSLCCustomerInfoInitializer(
                        getDataManager().mPref.prefGetCurrentUser().name, getDataManager().mPref.prefGetCurrentUser().email,
                        getDataManager().mPref.prefGetCurrentUser().address, "", "", "Bangladesh", getDataManager().mPref.prefGetCurrentUser().mobile
                    )

                sslCommerzIntegrationModel =
                    result.data.data.fromResponse()!!
                sslCommerzIntegrationModel.let {
                    val sslCommerzInitialization = SSLCommerzInitialization(
                        sslCommerzIntegrationModel.store_id,
                        sslCommerzIntegrationModel.store_password,
                        sslCommerzIntegrationModel.amount.toString().toDouble(),
                        SSLCCurrencyType.BDT,
                        sslCommerzIntegrationModel.transaction_id,
                        "easy consumer",
                        if(BuildConfig.FLAVOR=="demo") SSLCSdkType.TESTBOX else SSLCSdkType.LIVE
                    )

                    sslCommerzInitialization.ipn_url = sslCommerzIntegrationModel.ipn_url
                    val additionalInitializer = SSLCAdditionalInitializer()
                    additionalInitializer.valueB = sslCommerzIntegrationModel.value_b
                    additionalInitializer.valueC = sslCommerzIntegrationModel.value_c

                    IntegrateSSLCommerz
                        .getInstance(this)
                        .addSSLCommerzInitialization(sslCommerzInitialization)
                        .addCustomerInfoInitializer(SSLCCustomerInfoInitializer)
                        .addAdditionalInitializer(additionalInitializer)
                        .buildApiCall(this)
                }
            } else if(key=="payment_status") {
                val paymentStatusResponse =
                    result.data.data.fromResponse<PaymentStatusResponse>()
                val orderData: ArrayList<OrderData> = Gson().fromJson(paymentStatusResponse?.orderData)
                val holdData = orderData.firstOrNull { it.status == "hold" }
                handleSuccessDialog(holdData)
            } else if (key=="payment_status") {
                dialogFragment.showPrimaryLayout()
            }

        }else if (key=="recent") {
            val recentTopUp =
                result.data?.data.fromResponse<List<RecentTopUp>>()?.filter { it.serviceTitle=="top-up" }
            val orderList = ArrayList<RecentOrderData>()
            recentTopUp?.forEach { recentItem ->
                val orderItems: ArrayList<RecentOrderData> =
                    Gson().fromJson(recentItem.orderData?.replace("&quot;", "\""))
                orderItems.forEach { item ->
                    item.time = recentItem.requestedAt
                    orderList.add(item)
                }
            }

            if (orderList.isNotEmpty()) {
                recentGroup.makeVisible()
                val recentList = orderList.distinctBy { it.phone }
                val favNumbers = quickListFav.map { it.phone }.toMutableList()
                myNumber?.let {
                    favNumbers.add(it)
                }
                val distinctList = recentList.filter { !favNumbers.contains(it.phone)}.distinctBy { it.phone }
                recentRecycler.adapter =
                    QuickieRecentRecyclerAdapter(
                        if (distinctList.size > 5) distinctList.take(5) as ArrayList<RecentOrderData>
                        else distinctList as ArrayList<RecentOrderData>,
                        this,
                        getDataManager().mPref.prefGetLanguage()
                    )
            } else {
                recentGroup.makeGone()
            }
        } else {
            showErrorToast(result.data.message)
        }
    }


}