package sslwireless.android.townhall.view.activity.quickie

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class QuickieViewModel(private val dataManager: DataManager) : BaseViewmodel(dataManager) {

    fun apiTopUpInit(
        topUpRequestModel: TopUpRequestModel,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiTopUpInit(
            topUpRequestModel,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "top_up"))
        )
    }

    fun recentTransactionService(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.recentTransactionService(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "recent")),
            dataManager.mPref.prefGetCustomerToken()?:""
        )
    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating,transactionId,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }

    fun apiInitiateHoldUser(
        allow_offer: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiInitiateHoldUser(
            allow_offer,transactionId,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "hold_user"))
        )
    }

    fun apiPaymentStatus(
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiPaymentStatus(
            transactionId,dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "payment_status"))
        )
    }
}