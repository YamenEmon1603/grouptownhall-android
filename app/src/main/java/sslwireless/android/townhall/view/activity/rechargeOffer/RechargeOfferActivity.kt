package sslwireless.android.townhall.view.activity.rechargeOffer

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.recharge_offer_layout.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.TopUp.RechargeActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.fragment.rechargeOfferFragment.RecommandedFragment
import sslwireless.android.townhall.view.utils.*
import sslwireless.android.townhall.view.viewpager.ViewPagerAdapter

class RechargeOfferActivity : BaseActivity(), RechargeSortingBottomSheet.BottomSheetListener {
    private var sortingType = SortType.ALL
    private var offerList = ArrayList<PopularPlan>()
    private var operatorType: String? = ""
    private var typeSelection: String? = ""
    private var isFromRecharge = false
    val viewModel: CommunicationViewModel by viewModels()
    private val rechargeOfferViewModel: RechargeOfferViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(RechargeOfferViewModel(getDataManager())))
            .get(RechargeOfferViewModel::class.java)
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
       result.data?.let {
           when (key) {
               "get_plans" -> {
                   if (result.data.code == 200) {
                       offerList = result.data.data.fromResponse<List<PopularPlan>>() as ArrayList<PopularPlan>
                       setupViewPager(viewPager)
                       rechargeTabs.setupWithViewPager(viewPager)
                   } else {
                       showErrorToast(result.data.message)
                   }
               }
           }
       }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activtity_recharge_offer)
    }

    override fun viewRelatedTask() {
        isFromRecharge = intent.extras?.getBoolean("isFromRecharge",false)?:false
        operatorType = intent.extras?.getString("operator", "All") ?: "All"
        typeSelection = intent.extras?.getString("type", "All")
        customTextView33.text = "For $operatorType"
        rechargeOfferViewModel.apiGetPlans(this, this)

        backGroup.setOnClickListener {
            finish()
        }


        sortingGroup.setOnClickListener {
            val bottomDialog = RechargeSortingBottomSheet(sortingType)
            bottomDialog.show(supportFragmentManager, "1234")
        }

        viewModel.planItemLiveData.observe(this, Observer {
            if (isFromRecharge) {
                val intent = Intent().apply {
                    putExtra("selectedPlan", it)
                }
                setResult(Activity.RESULT_OK, intent)
            } else {
                val intent = Intent(this,RechargeActivity::class.java).apply {
                    putExtra("selectedPlan", it)
                }
                startActivity(intent)
            }
            finish()
        })

    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        for (i in 0..2) {
            val type: String
            val planList: List<PopularPlan>
            when (i) {
                0 -> {
                    type = getString(R.string.internet)
                    planList = offerList.filter {
                        if (operatorType == "All") it.offer_type == "internet" else it.offer_type == "internet" && it.operator_name.contains(
                            operatorType!!,
                            true
                        ) && when (typeSelection) {
                            AppConstants.Operators.PREPAID -> it.is_for_prepaid == 1
                            AppConstants.Operators.POSTPAID -> it.is_for_postpaid == 1
                            else -> true
                        }
                    }
                }
                1 -> {
                    type = getString(R.string.voice)
                    planList = offerList.filter {
                        if (operatorType == "All") it.offer_type == "voice" else it.offer_type == "voice" && it.operator_name.contains(
                            operatorType!!,
                            true
                        ) && when (typeSelection) {
                            AppConstants.Operators.PREPAID -> it.is_for_prepaid == 1
                            AppConstants.Operators.POSTPAID -> it.is_for_postpaid == 1
                            else -> true
                        }
                    }
                }
                else -> {
                    type = getString(R.string.combo)
                    planList = offerList.filter {
                        if (operatorType == "All")
                            it.offer_type.contains("+")
                        else
                            it.offer_type.contains("+")
                                    &&
                                    it.operator_name.contains(operatorType!!, true)
                                    &&
                                    when (typeSelection) {
                                        AppConstants.Operators.PREPAID -> it.is_for_prepaid == 1
                                        AppConstants.Operators.POSTPAID -> it.is_for_postpaid == 1
                                        else -> true
                                    }
                    }
                }
            }
            val fragment = RecommandedFragment().apply {
                arguments = Bundle().apply {
                    putString("planList", Gson().toJson(planList))
                }
            }
            adapter.addFrag(fragment, type)
        }

        viewPager.adapter = adapter

        viewPager.offscreenPageLimit = 5

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {

            }
        })
    }

    override fun onClicked(sortingType: SortType) {
        this.sortingType = sortingType;
        viewModel.sortingLiveData.value = sortingType
    }
}