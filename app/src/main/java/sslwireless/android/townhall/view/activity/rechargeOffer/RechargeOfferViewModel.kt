package sslwireless.android.townhall.view.activity.rechargeOffer

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class RechargeOfferViewModel (private val dataManager: DataManager) : BaseViewmodel(dataManager){

    fun apiGetPlans(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetPlans(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "get_plans"))
        )
    }
}