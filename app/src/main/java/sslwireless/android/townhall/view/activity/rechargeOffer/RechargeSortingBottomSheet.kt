package sslwireless.android.townhall.view.activity.rechargeOffer


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import kotlinx.android.synthetic.main.recharge_offer_sorting_layout.*


class RechargeSortingBottomSheet(private val sortingType: SortType) : BottomSheetDialogFragment() {
    private var bottomSheetListener: BottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.recharge_offer_sorting_layout, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        popularRadio.visibility = View.GONE
        newRadio.visibility = View.GONE
        highestRadio.visibility = View.GONE
        maxRadio.visibility = View.GONE
        minRadio.visibility = View.GONE

        when (sortingType) {
            SortType.POPULAR -> {
                popularRadio.visibility = View.VISIBLE
            }
            SortType.NEW -> {
                newRadio.visibility = View.VISIBLE
            }
            SortType.HIGHEST_VALIDITY -> {
                highestRadio.visibility = View.VISIBLE
            }
            SortType.MAXIMUM_AMOUNT -> {
                maxRadio.visibility = View.VISIBLE
            }
            SortType.MINIMUM_AMOUNT -> {
                minRadio.visibility = View.VISIBLE
            }
        }

        customTextView3.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.ALL)
            dismiss()
        }
        popularLayout.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.POPULAR)
            dismiss()
        }
        newLayout.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.NEW)
            dismiss()
        }
        highestLayout.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.HIGHEST_VALIDITY)
            dismiss()
        }
        maxLayout.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.MAXIMUM_AMOUNT)
            dismiss()
        }
        minLayout.setOnClickListener {
            bottomSheetListener?.onClicked(SortType.MINIMUM_AMOUNT)
            dismiss()
        }
    }

    interface BottomSheetListener {
        fun onClicked(sortingType: SortType)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        bottomSheetListener = context as BottomSheetListener
    }


}
