package sslwireless.android.townhall.view.activity.rechargeRequest

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_recharge_request_received.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.recharge_request.Data
import sslwireless.android.townhall.data.model.recharge_request.RequestListItem
import sslwireless.android.townhall.databinding.ItemRechargeRequestHistoryBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.view.adapter.EmptyViewHolder
import sslwireless.android.townhall.view.base.*
import sslwireless.android.townhall.view.utils.AppConstants.RechargeRequest.RECHARGE_REQUEST_RELOAD

class ReceivedHistoryFragment(val listener: IRechargeRequestTabsListener) : BaseFragment() {
    private val viewModel: RechargeRequestListViewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(RechargeRequestListViewModel(getDataManager()))
        ).get(RechargeRequestListViewModel::class.java)
    }

    private lateinit var linearLayoutManager: LinearLayoutManager

    private var list: ArrayList<RequestListItem> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recharge_request_history, container, false)
    }

    override fun viewRelatedTask() {
        initRecycler()

        initList()

        itemsRV.addOnScrollListener(object :
            PaginationScrollListener(linearLayoutManager) {
            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                paginateToNextPage()
            }
        })

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            initList()
        }
    }

    override fun onResume() {
        super.onResume()
        if (RECHARGE_REQUEST_RELOAD) {
            initList()
            RECHARGE_REQUEST_RELOAD = false
        }
    }


    private fun initList() {
        isLastPage = false
        isLoading = false
        currentPage = 1
        list.clear()
        showList(currentPage, queryStr)
    }

    private fun showList(page: Int = 1, query: String = "") {
        if (isLastPage) return

        viewModel.rechargeRequestHistory(
            page = currentPage,
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    private fun paginateToNextPage() {
        isLoading = false
        currentPage++
        showList(page = currentPage, query = queryStr)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            RechargeRequestListViewModel.REQUEST_HISTORY -> {
                val baseModel = result.data

                when (baseModel?.code) {
                    200 -> {
                        val data = Gson().fromJson<Data>(
                            Gson().toJson(baseModel.data),
                            Data::class.java
                        )

                        isLastPage =
                            data?.paginator?.current_page ?: 1 >= data?.pagination_last_page ?: 1

                        val items = data.data as ArrayList<RequestListItem>
                        list.addAll(items)
                        itemsRV.adapter?.notifyDataSetChanged()
                    }
                    422 -> {
                        showToast(requireContext(), baseModel.message)
                    }
                    401 -> {
                        showToast(requireContext(), baseModel.message)

                        activity?.let { getDataManager().mPref.prefLogout(it) }
                    }
                }
            }
        }
    }

    private fun initRecycler() {
        linearLayoutManager = LinearLayoutManager(
            requireContext()
        )

        itemsRV.layoutManager = linearLayoutManager

        itemsRV.adapter =
            BaseRecyclerAdapter(requireContext(), object : IAdapterListener {
                override fun <T> clickListener(position: Int, model: T, view: View) {
                    val item = model as RequestListItem

                    when (view.id) {
                        R.id.itemLayout -> {
                            //showToast(requireContext(), "Item clicked")
                        }
                    }
                }

                override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {
                    return if (viewType > -1) {
                        ReceivedtHistoryViewHolder(
                            ItemRechargeRequestHistoryBinding.inflate(
                                LayoutInflater.from(parent.context),
                                parent,
                                false
                            ),
                            requireContext()
                        )
                    } else {
                        return EmptyViewHolder(
                            DataBindingUtil.inflate(
                                LayoutInflater.from(parent.context),
                                R.layout.empty_page,
                                parent,
                                false
                            ),
                            requireContext()
                        )
                    }
                }
            }, list)
    }

    companion object {
        private var queryStr = ""
        private var isLastPage: Boolean = false
        private var isLoading: Boolean = false
        private var currentPage = 1
        private const val SIZE = 10

        private const val PARAM_TAB_NAME = "_tab_name"

        @JvmStatic
        fun newInstance(tabTitle: String, listener: IRechargeRequestTabsListener) =
            ReceivedHistoryFragment(listener).apply {
                arguments = Bundle().apply {
                    putString(PARAM_TAB_NAME, tabTitle)
                }
            }

    }
}