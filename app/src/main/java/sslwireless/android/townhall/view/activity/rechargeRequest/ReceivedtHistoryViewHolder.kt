package sslwireless.android.townhall.view.activity.rechargeRequest

import android.content.Context
import androidx.viewbinding.ViewBinding
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.recharge_request.RequestListItem
import sslwireless.android.townhall.databinding.ItemRechargeRequestHistoryBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.dateChangeTo

class ReceivedtHistoryViewHolder(
    itemView: ViewBinding,
    context: Context
) : BaseViewHolder2(itemView.root) {

    var binding = itemView as ItemRechargeRequestHistoryBinding
    var mContext: Context = context

    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {
        itemModel as RequestListItem

        binding.nameTV.text = itemModel.request_body?.name

        binding.operatorTV.text =
            "${itemModel.request_body?.operator} | ${itemModel.request_body?.connection_type}"
        binding.dateTimeTV.text =
            itemModel.created_at?.dateChangeTo("yyyy-MM-dd HH:mm:ss", "EEE, d MMM yyyy")
        binding.statusTV.text = itemModel.status_value
        when (itemModel.status) {
            0 -> {
                binding.amountTV.text = ""+itemModel.request_body?.amount?.toInt()
                binding.statusBG.setBackgroundResource(R.drawable.bg_failed)
            }
            1 -> {
                try {
                    binding.amountTV.text =
                        itemModel.request_body?.rechargeAmount?.toDouble()?.toInt().toString()
                } catch (e:Exception) {
                    binding.amountTV.text = itemModel.request_body?.amount.toString()
                }
                binding.statusBG.setBackgroundResource(R.drawable.bg_success)
            }
            2 -> {
                binding.amountTV.text = itemModel.request_body?.amount.toString()
                binding.statusBG.setBackgroundResource(R.drawable.bg_processing)
            }
            else-> {
                binding.amountTV.text = itemModel.request_body?.amount.toString()
            }
        }

        binding.itemLayout.setOnClickListener {
            listener.clickListener(position, itemModel, binding.itemLayout)
        }
    }
}