package sslwireless.android.townhall.view.activity.rechargeRequest

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.widget.DatePicker
import kotlinx.android.synthetic.main.activity_recharge_request_list.*
import kotlinx.android.synthetic.main.activity_recharge_request_list.action_calendar
import kotlinx.android.synthetic.main.activity_transaction_history.tab_layout
import kotlinx.android.synthetic.main.activity_transaction_history.view_pager
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.TopUp.RechargeViewModel
import sslwireless.android.townhall.view.adapter.TabsPagerAdapter
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.showErrorToast
import java.util.*

class RechargeRequestListActivity : BaseActivity(), IRechargeRequestTabsListener, DatePickerDialog.OnDateSetListener {
    private val rechargeRequest : String? by lazy {
        intent.extras?.getString("RechargeRequest")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recharge_request_list)
    }

    override fun viewRelatedTask() {
        initTabs()

        action_calendar.setOnClickListener {
            showDatePickerDialog()
        }

        backIV.setOnClickListener {
            onBackPressed()
        }


    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        result.data?.let {
            when (key) {
                RechargeViewModel.CALL_RECHARGE_REQUEST -> {
                    if (result.data.code == 200) {

                    } else {
                        showErrorToast(result.data.message)
                    }
                }
            }
        }
    }

    private fun initTabs() {
        val mTabsPagerAdapter = TabsPagerAdapter(supportFragmentManager)
        mTabsPagerAdapter.addFragment(
            RequestReceivedFragment.newInstance(getString(R.string.request_received), this),
            getString(R.string.request_received)
        )
        mTabsPagerAdapter.addFragment(
            RequestSentFragment.newInstance(getString(R.string.request_sent), this),
            getString(R.string.request_sent)
        )
        mTabsPagerAdapter.addFragment(
            ReceivedHistoryFragment.newInstance(getString(R.string.received_history), this),
            getString(R.string.received_history)
        )
        view_pager.adapter = mTabsPagerAdapter
        view_pager.offscreenPageLimit = 3
        tab_layout.setupWithViewPager(view_pager)

    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            this,
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    override fun onBackPressed() {
        if (rechargeRequest!=null){
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }else {
            super.onBackPressed()
        }
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {

    }
}