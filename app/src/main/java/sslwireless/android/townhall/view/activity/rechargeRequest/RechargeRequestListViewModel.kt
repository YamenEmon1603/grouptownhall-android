package sslwireless.android.townhall.view.activity.rechargeRequest

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class RechargeRequestListViewModel(val dataManager: DataManager) : BaseViewmodel(dataManager) {
    fun rechargeRequestReceived(
        page: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequestList(
            page,
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, REQUEST_RECEIVED))
        )
    }

    fun rechargeRequestSent(
        page: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequestedList(
            page,
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, REQUEST_SENT))
        )
    }

    fun rechargeRequestHistory(
        page: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequestHistory(
            page,
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, REQUEST_HISTORY))
        )
    }

    fun rechargeRequestReject(
        requestId: Int,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequestReject(
            requestId,
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, REQUEST_REJECT))
        )
    }

    fun rechargeRequestDetails(
        ruid: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRechargeRequestInfo(
            ruid,
            dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, REQUEST_DETAILS))
        )
    }

    companion object {
        const val REQUEST_RECEIVED = "_request_received"
        const val REQUEST_SENT = "_request_sent"
        const val REQUEST_REJECT = "_request_reject"
        const val REQUEST_DETAILS = "_request_details"
        const val REQUEST_HISTORY = "_request_history"
    }
}