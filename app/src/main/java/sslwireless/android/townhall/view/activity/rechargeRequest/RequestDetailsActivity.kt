package sslwireless.android.townhall.view.activity.rechargeRequest

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.sslwireless.sslcommerzlibrary.model.response.SSLCTransactionInfoModel
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import kotlinx.android.synthetic.main.activity_request_details.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.PaymentStatusResponse
import sslwireless.android.townhall.data.model.recharge_request.RequestListItem
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.data.model.topup_models.TopUpRequestModel
import sslwireless.android.townhall.databinding.PaymentSuccessNFailedLayoutBinding
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.TopUp.RechargeViewModel
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*

class RequestDetailsActivity : BaseActivity(), SSLCTransactionResponseListener {
    private val viewModel: RechargeRequestListViewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(RechargeRequestListViewModel(getDataManager()))
        ).get(RechargeRequestListViewModel::class.java)
    }

    private val rechargeViewModel: RechargeViewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(RechargeViewModel(getDataManager()))
        ).get(RechargeViewModel::class.java)
    }

    private var topUpRequestData: RequestListItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_details)
    }

    override fun viewRelatedTask() {
        val data = intent.getParcelableExtra<RequestListItem>("data") ?: return

        backIV.setOnClickListener {
            onBackPressed()
        }

        viewModel.rechargeRequestDetails(
            ruid = data.ruid ?: return,
            lifecycleOwner = this,
            iObserverCallBack = this
        )

        proceedBTN.setOnClickListener {
            initRecharge()
        }
    }

    private fun initRecharge() {
        if (rechargeAmountET.text.toString().trim().isEmpty()) {
            rechargeAmountET.error = "Required"
            return
        }
        val amount = rechargeAmountET.text.toString().trim()
        val operator = topUpRequestData?.request_body?.operator_id ?: return
        val phone = topUpRequestData?.request_body?.msisdn ?: return
        val type = topUpRequestData?.request_body?.connection_type ?: return
        val customerToken = getDataManager().mPref.prefGetCustomerToken() ?: return
        val ruid = topUpRequestData?.ruid ?: return

        val topUpRequestModel = TopUpRequestModel(
            data =listOf(
                TopUpModel(
                    amount = amount.toInt(),
                    operator = operator,
                    phone = phone,
                    type = type
                )
            ),
            schedule_recharge = "",
            token = customerToken,
            is_recharge_request = true,
            ruid = ruid
        )

        rechargeViewModel.apiTopUpInit(topUpRequestModel, this, this)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            RechargeRequestListViewModel.REQUEST_DETAILS -> {
                val baseModel = result.data

                when (baseModel?.code) {
                    200 -> {
                        val data = Gson().fromJson<RequestListItem>(
                            Gson().toJson(baseModel.data),
                            RequestListItem::class.java
                        )
                        topUpRequestData = data

                        iconIV.loadImage("https://ui-avatars.com/api/?length=1&background=176EFF&color=ffffff&name=${data.request_body?.name}")
                        requestTitleTV.text = data.request_body?.name
                        phoneNumberTV.text = data.request_body?.msisdn
                        operatorNameTV.text = data.request_body?.operator
                        requestFrom.text = data.request_body?.requester_identifier
                        typeNameTV.text = data.request_body?.connection_type
                        dateValueTV.text = data.created_at?.dateChangeTo("yyyy-MM-dd HH:mm:ss", "EEE, d MMM yyyy")
                        data.request_body?.amount?.let {
                            rechargeAmountET.setText(""+it.toInt())
                        }
                    }
                    422 -> {
                        showToast(this, baseModel.message)
                    }
                    401 -> {
                        showToast(this, baseModel.message)

                        getDataManager().mPref.prefLogout(this)
                    }
                }
            }
            "top_up" -> {
                val baseModel = result.data
                when (baseModel?.code) {
                    200 -> {
                        val sslCommerzIntegrationModel: SslCommerzIntegrationModel =
                            baseModel.data?.fromResponse() ?: return

                        initSdk(this, sslCommerzIntegrationModel, getDataManager(), false)
                    }
                    422 -> {
                        showToast(this, baseModel.message)
                    }
                    401 -> {
                        showToast(this, baseModel.message)

                        getDataManager().mPref.prefLogout(this)
                    }
                }
            }
            "payment_status" -> {
                val baseModel = result.data
                when (baseModel?.code) {
                    200 -> {
                        val paymentStatusResponse =
                            baseModel.data.fromResponse<PaymentStatusResponse>()
                        paymentStatusResponse?.status?.let { showPaymentStatusDialog(true, it) }
                    }
                    else -> {
                        showPaymentStatusDialog(false, getString(R.string.something_went_wrong))
                    }
                }
            }
        }
    }

    override fun transactionSuccess(transactionInfo: SSLCTransactionInfoModel?) {
        transactionInfo?.tranId?.let { rechargeViewModel.apiPaymentStatus(it, this, this) }
    }

    override fun transactionFail(message: String?) {
        // showPaymentStatusDialog(false, message ?: getString(R.string.something_went_wrong))
    }

    override fun closed(message: String?) {

    }

    private fun showPaymentStatusDialog(status: Boolean, returnMessage: String) {
        val newDialog = Dialog(this, R.style.customDialog)
        newDialog.requestWindowFeature(android.view.Window.FEATURE_NO_TITLE)
        newDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val customDialogBinding: PaymentSuccessNFailedLayoutBinding = DataBindingUtil.inflate(
            LayoutInflater.from(this),
            R.layout.payment_success_n_failed_layout, null, false
        )

        if (status) {
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()

            customDialogBinding.lottieAnim.setAnimation("new_success_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.pay_successfull)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.newSuccessColor
                )
            )
        } else {
            customDialogBinding.lottieAnim.setAnimation("new_failed_lottie.json")
            customDialogBinding.lottieAnim.playAnimation()
            customDialogBinding.lottieAnim.loop(false)

            customDialogBinding.textView82.text = getString(R.string.fail_payment)
            customDialogBinding.textView83.text = returnMessage
            customDialogBinding.ratingText.makeGone()
            customDialogBinding.ratingBar.makeGone()
            customDialogBinding.textView86.makeVisible()

            customDialogBinding.textView84.text = getString(R.string.failed_text)
            customDialogBinding.textView86.text = getString(R.string.retry)

            customDialogBinding.textView82.setTextColor(
                ContextCompat.getColor(
                    this,
                    R.color.failedColor
                )
            )
        }

        customDialogBinding.textView84.setOnClickListener {
            newDialog.dismiss()
            if (status) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
            } else {
                onBackPressed()
            }
        }

        customDialogBinding.textView86.setOnClickListener {
            newDialog.dismiss()
            onBackPressed()
        }

        newDialog.setContentView(customDialogBinding.root)

        newDialog.show()
        newDialog.setCancelable(false)
    }
}


