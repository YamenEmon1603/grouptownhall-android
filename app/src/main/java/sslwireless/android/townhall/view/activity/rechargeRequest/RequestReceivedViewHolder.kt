package sslwireless.android.townhall.view.activity.rechargeRequest

import android.content.Context
import androidx.viewbinding.ViewBinding
import sslwireless.android.townhall.data.model.recharge_request.RequestListItem
import sslwireless.android.townhall.databinding.ItemRechargeRequestReceivedBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.dateChangeTo

class RequestReceivedViewHolder(
    itemView: ViewBinding,
    context: Context
) : BaseViewHolder2(itemView.root) {

    var binding = itemView as ItemRechargeRequestReceivedBinding
    var mContext: Context = context

    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {
        itemModel as RequestListItem

        binding.nameTV.text = itemModel.request_body?.name
        binding.amountTV.text = ""+itemModel.request_body?.amount?.toInt()
        binding.operatorTV.text =
            "${itemModel.request_body?.operator} | ${itemModel.request_body?.connection_type}"
        binding.dateTimeTV.text = itemModel.created_at?.dateChangeTo("yyyy-MM-dd HH:mm:ss", "EEE, d MMM yyyy")

        binding.itemLayout.setOnClickListener {
            listener.clickListener(position, itemModel, binding.itemLayout)
        }

        binding.acceptBTN.setOnClickListener {
            listener.clickListener(position, itemModel, binding.acceptBTN)
        }

        binding.rejectBTN.setOnClickListener {
            listener.clickListener(position, itemModel, binding.rejectBTN)
        }
    }


}