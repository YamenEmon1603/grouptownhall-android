package sslwireless.android.townhall.view.activity.rechargeRequest

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.session_logout_bottom_sheet_layout.view.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible

class RequestRejectBottomSheet(val listener: IBottomSheetDialogClicked) :
    BottomSheetDialogFragment() {

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.session_logout_bottom_sheet_layout, null)
        dialog.setContentView(contentView)
        val title = requireArguments().getString(KEY_TITLE, "")
        val subText = requireArguments().getString(KEY_DESC, "")
        val btn1 = requireArguments().getString(KEY_FIRST_BUTTON, "")
        val btn2 = requireArguments().getString(KEY_SECOND_BUTTON, "")
        val tag = requireArguments().getString(KEY_TAG, "")
        val itemId = requireArguments().getInt(KEY_ITEM_ID)

        if (subText.isEmpty()) {
            contentView.offer_title_tv_bottom_sheet.makeGone()
        } else {
            contentView.offer_title_tv_bottom_sheet.makeVisible()
        }
        contentView.customTextView54.text = title
        contentView.offer_title_tv_bottom_sheet.text = subText
        contentView.cancel_btn.text = btn1
        contentView.session_logout_btn.text = btn2

        contentView.cancel_btn.setOnClickListener {
            dismiss()
            listener.onCancelButtonClicked()
        }

        contentView.session_logout_btn.setOnClickListener {
            dismiss()
            listener.onOkButtonClicked(tag, itemId)
        }

    }


    companion object{
        const val KEY_TITLE = "_title"
        const val KEY_DESC = "_desc"
        const val KEY_FIRST_BUTTON = "_first_button"
        const val KEY_SECOND_BUTTON = "_second_button"
        const val KEY_TAG = "_tag"
        const val KEY_ITEM_ID = "_item_id"
    }

    interface IBottomSheetDialogClicked {
        fun onCancelButtonClicked()

        fun onOkButtonClicked(tag: String, itemId: Int)
    }
}