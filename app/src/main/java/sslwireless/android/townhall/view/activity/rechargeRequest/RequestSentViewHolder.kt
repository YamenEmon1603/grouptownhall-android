package sslwireless.android.townhall.view.activity.rechargeRequest

import android.content.Context
import androidx.viewbinding.ViewBinding
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.recharge_request.RequestListItem
import sslwireless.android.townhall.databinding.ItemRechargeRequestSentBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.dateChangeTo

class RequestSentViewHolder(
    itemView: ViewBinding,
    context: Context
) : BaseViewHolder2(itemView.root) {
    var binding = itemView as ItemRechargeRequestSentBinding
    var mContext: Context = context

    override fun <T> onBind(position: Int, itemModel: T, listener: IAdapterListener) {
        itemModel as RequestListItem

        binding.nameTV.text = itemModel.request_body?.requested_identifier
        binding.amountTV.text = ""+itemModel.request_body?.amount?.toInt()
        binding.dateTimeTV.text =
            itemModel.created_at?.dateChangeTo("yyyy-MM-dd HH:mm:ss", "EEE, d MMM yyyy")
        binding.statusTV.text = itemModel.status_value
        when (itemModel.status) {
            0 -> {
                binding.statusBG.setBackgroundResource(R.drawable.bg_failed)
            }
            1 -> {
                binding.statusBG.setBackgroundResource(R.drawable.bg_success)
            }
            2 -> {
                binding.statusBG.setBackgroundResource(R.drawable.bg_processing)
            }
        }
    }
}