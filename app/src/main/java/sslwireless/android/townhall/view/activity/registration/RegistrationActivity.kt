package sslwireless.android.townhall.view.activity.registration

import android.content.Intent
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.LocalValidator
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.RegistrationInfo
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.otp.OtpActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.activity_registration.*
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible

class RegistrationActivity : BaseActivity() {
    private var emailOrPhone: String? = null
    private var isSocialLogin: Boolean = false
    private var name: String = ""
    private var socialLoginId: String = ""
    private var provider: String = ""
    private lateinit var viewModel: RegistrationViewModel
    private lateinit var regInfo: RegistrationInfo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }

    override fun viewRelatedTask() {
        autoFillData()

        back.setOnClickListener {
            finish()
        }


        viewModel =
                ViewModelProviders.of(
                        this,
                        BaseViewmodelFactory(RegistrationViewModel(getDataManager()))
                )
                        .get(RegistrationViewModel::class.java)

        proceed_btn.setOnClickListener {
            callSubmitApi()
        }

        //editTextFocusEventHandler()

        nameInput.setPadding(30, 20, 30, 10)
        emailInput.setPadding(30, 20, 30, 10)
        mobileInput.setPadding(30, 20, 30, 10)
        passInput.setPadding(30, 20, 30, 10)
        retypePassInput.setPadding(30, 20, 30, 10)

        /*proceed.setOnClickListener {
            val intent = Intent(this, OtpActivity::class.java)
            startActivity(intent)
        }*/
    }

    private fun callSubmitApi() {

        if (name_et.text.isEmpty()) {
            name_et.error = getString(R.string.error_empty_register_name)
        } else if (!isSocialLogin && email.text.isEmpty()) {
            email.error = getString(R.string.error_empty_register_email)
        } else if (numberMobile.text.isEmpty()) {
            numberMobile.error = getString(R.string.error_empty_register_phone)
        } else if (!isSocialLogin && userPassword.text.isEmpty()) {
            userPassword.error = getString(R.string.error_empty_register_password)
        } else if (!isSocialLogin && userRetypePassword.text.isEmpty()) {
            userRetypePassword.error = getString(R.string.error_empty_confirm_password)
        } else {


            if (email.text.isNotEmpty() && !LocalValidator.isEmailValid(email.text.toString())) {
                email.error = getString(R.string.error_register_invalid_email)
                return
            }

            if (!LocalValidator.isPhoneValid(numberMobile.text.toString())) {
                numberMobile.error = getString(R.string.error_register_invalid_phone)
                return
            }

            if (!isSocialLogin && userPassword.text.toString() != userRetypePassword.text.toString()) {
                userRetypePassword.error = getString(R.string.error_register_confirm_password)
                return
            }

            regInfo = RegistrationInfo(
                    name = name_et.text.toString(),
                    email = email.text.toString(),
                    phone = numberMobile.text.toString(),
                    password = userPassword.text.toString(),
                    pass_confirmation = userRetypePassword.text.toString(),
                    device_key = getDataManager().mPref.prefGetDeviceKey()?.deviceKey ?: "",
                    referal_key = "",
                    isSocial = isSocialLogin,
                    social_login_id = socialLoginId,
                    provider = provider
            )

            //showProgressDialog(getString(R.string.progress_loading))
            viewModel.submitRegData(regInfo, this, this)
        }
    }

    private fun autoFillData() {
        emailOrPhone = intent.getStringExtra("emailorphone")
        name = intent.getStringExtra("name")?:""
        socialLoginId = intent.getStringExtra("social_login_id")?:""
        provider = intent.getStringExtra("provider")?:""
        isSocialLogin = socialLoginId.isNotEmpty()
        name_et.setText(name)
        if (isSocialLogin) {
            passGroup.makeGone()
        } else {
            passGroup.makeVisible()
        }

        if (emailOrPhone?.contains("@") == true) {
            email.setText(emailOrPhone)
        } else {
            numberMobile.setText(emailOrPhone)
        }

        if (isSocialLogin &&  email.text.toString().isNotEmpty()) {
            email.isFocusable = false
            email.isFocusableInTouchMode = false // user touches widget on phone with touch screen
            email.isClickable = false
        } else {
            email.isFocusable = true
            email.isFocusableInTouchMode = true
            email.isClickable = true
        }
    }

    private fun editTextFocusEventHandler() {
        nameInput.setPadding(30, 20, 30, 10)
        emailInput.setPadding(30, 20, 30, 10)
        mobileInput.setPadding(30, 20, 30, 10)
        passInput.setPadding(30, 20, 30, 10)
        retypePassInput.setPadding(30, 20, 30, 10)

        name_et.setOnFocusChangeListener { view, b ->
            if (b) {
                nameInput.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                emailInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                mobileInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                passInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                retypePassInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))

                nameInput.setPadding(30, 20, 30, 10)
                emailInput.setPadding(30, 20, 30, 10)
                mobileInput.setPadding(30, 20, 30, 10)
                passInput.setPadding(30, 20, 30, 10)
                retypePassInput.setPadding(30, 20, 30, 10)
            }
        }

        email.setOnFocusChangeListener { view, b ->
            if (b) {
                nameInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                emailInput.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                mobileInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                passInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                retypePassInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))

                nameInput.setPadding(30, 20, 30, 10)
                emailInput.setPadding(30, 20, 30, 10)
                mobileInput.setPadding(30, 20, 30, 10)
                passInput.setPadding(30, 20, 30, 10)
                retypePassInput.setPadding(30, 20, 30, 10)
            }
        }

        numberMobile.setOnFocusChangeListener { view, b ->
            if (b) {
                nameInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                emailInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                mobileInput.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                passInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                retypePassInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))

                nameInput.setPadding(30, 20, 30, 10)
                emailInput.setPadding(30, 20, 30, 10)
                mobileInput.setPadding(30, 20, 30, 10)
                passInput.setPadding(30, 20, 30, 10)
                retypePassInput.setPadding(30, 20, 30, 10)
            }
        }

        userPassword.setOnFocusChangeListener { view, b ->
            if (b) {
                userPassword.transformationMethod = PasswordTransformationMethod.getInstance()

                nameInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                emailInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                mobileInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                passInput.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                retypePassInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))

                nameInput.setPadding(30, 20, 30, 10)
                emailInput.setPadding(30, 20, 30, 10)
                mobileInput.setPadding(30, 20, 30, 10)
                passInput.setPadding(30, 20, 30, 10)
                retypePassInput.setPadding(30, 20, 30, 10)
            }
        }

        userRetypePassword.setOnFocusChangeListener { view, b ->
            if (b) {
                retypePassInput.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                emailInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                mobileInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                passInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))
                nameInput.setBackgroundDrawable(resources.getDrawable(R.drawable.normal_drawable))

                nameInput.setPadding(30, 20, 30, 10)
                emailInput.setPadding(30, 20, 30, 10)
                mobileInput.setPadding(30, 20, 30, 10)
                passInput.setPadding(30, 20, 30, 10)
                retypePassInput.setPadding(30, 20, 30, 10)
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        //hideProgressDialog()
        if (result.data?.code == 200) {
            val intent = Intent(this, OtpActivity::class.java)
            intent.putExtra(OtpActivity.BUNDLE_PHONE, numberMobile.text.toString())
            intent.putExtra(OtpActivity.BUNDLE_EMAIL, email.text.toString())
            intent.putExtra(OtpActivity.BUNDLE_REG_INFO, regInfo)
            intent.putExtra(OtpActivity.BUNDLE_IS_REGISTER, true)
            startActivity(intent)
        } else {
            showToast(
                    this,
                    result.data?.message ?: getString(R.string.error_common_something_went_wrong)
            )
        }
    }

}