package sslwireless.android.townhall.view.activity.registration

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.user.RegistrationInfo
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class RegistrationViewModel(val dataManager: DataManager) : BaseViewmodel(dataManager) {
    fun submitRegData(
        data: RegistrationInfo,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUserReg(
            data,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "userReg"))
        )
    }
}