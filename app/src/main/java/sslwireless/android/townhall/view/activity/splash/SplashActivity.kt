package sslwireless.android.townhall.view.activity.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.activity_splash.swipeRefresh
import kotlinx.android.synthetic.main.fragment_home_new.*
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.DeviceKey
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.utils.ShareInfo
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.activity.login.LoginStep1Activity
import sslwireless.android.townhall.view.activity.rechargeRequest.RechargeRequestListActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.dialog.NewUpdateDialog
import sslwireless.android.townhall.view.service.LocalFirebaseMessagingService
import sslwireless.android.townhall.view.utils.AppSignatureHashHelper
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.URL


class SplashActivity : BaseActivity(), NewUpdateDialog.OnNewUpdateItemClickListener {
    private lateinit var timer: Thread
    private lateinit var viewmodel: SplashViewmodel
    private var loadProgress: Int = 0
    private var ipAddress = "required"
    private val disposables: CompositeDisposable = CompositeDisposable()
    private lateinit var newUpdateDialog: NewUpdateDialog
    private var isLoaded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        viewmodel =
            ViewModelProviders.of(this, BaseViewmodelFactory(SplashViewmodel(getDataManager())))
                .get(SplashViewmodel::class.java)
        loadProgress = PROGRESS_MIN
    }

    override fun viewRelatedTask() {
        isLoaded = false

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false
            if (!isLoaded) {
                init()
            }
        }

        init()
    }

    private fun init() {
        val rightSwipe1 = AnimationUtils.loadAnimation(this, R.anim.anim_left)
        val rightSwipe2 = AnimationUtils.loadAnimation(this, R.anim.anim_left_2)
        layoutLogo.startAnimation(rightSwipe1)
        bg.startAnimation(rightSwipe2)

        versionIcon.text = getString(R.string.version, BuildConfig.VERSION_NAME)
        getToken()
    }

    private fun threadCaller() {

        /*val deviceInfo = DeviceInfo()

        deviceInfo.KEY_BROWSER = "required"
        deviceInfo.KEY_BROWSER_VERSION = "required"
        deviceInfo.KEY_DEVICE = "required"
        deviceInfo.KEY_DEVICE_KEY = ShareInfo.getInstance().getDeviceId()
        deviceInfo.KEY_OS = "required"
        deviceInfo.KEY_OS_VERSION = "required"
        deviceInfo.KEY_USER_AGENT = "required"
        deviceInfo.IP_ADDRESS = "172.0.0.1"*/
        val deviceInfo = ShareInfo.getInstance().getDeviceInfo()
        deviceInfo.FCM_TOKEN = getDataManager().mPref.prefGetFCMToken()!!
        deviceInfo.IP_ADDRESS = ipAddress
        if (getDataManager().mPref.prefGetOTPHashKey().isNullOrEmpty()) {
            try {
                val OTP_KEY = AppSignatureHashHelper(this).appSignatures[0]
                deviceInfo.KEY_USER_AGENT = "otp_key:" + OTP_KEY
                getDataManager().mPref.prefSetOTPHash(deviceInfo.OTP_KEY!!)
            } catch (e: Exception) {
            }
        } else {
            val OTP_KEY = getDataManager().mPref.prefGetOTPHashKey()
            deviceInfo.KEY_USER_AGENT = "otp_key:" + OTP_KEY
        }

        viewmodel.fetchGetDeviceKey(deviceInfo, this, this)
    }


    private fun getToken() {
        try {
            FirebaseMessaging.getInstance().token.addOnCompleteListener {
                if (it.isComplete) {
                    // getDataManager().mPref.prefSetFCMToken(it.result.toString())
                    getIpAddress()
                }
            }
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    private fun getIpAddress() {
        try {
            val observer = Single.create<String> { emitter ->
                val whatismyip = URL("http://checkip.amazonaws.com")
                var qwe: BufferedReader? = null
                try {
                    qwe = BufferedReader(
                        InputStreamReader(
                            whatismyip.openStream()
                        )
                    )
                } catch (e: Exception) {
                    emitter.onError(e)
                } finally {
                    if (qwe != null) {
                        try {
                            emitter.onSuccess(qwe.readLine())
                            qwe.close()
                        } catch (e: IOException) {
                            emitter.onError(e)
                        }
                    }
                }
            }

            observer
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ data ->
                    ipAddress = data
                    threadCaller()
                }, { error ->
                    threadCaller()
                }).let {
                    disposables.add(it)
                }

        } catch (e: Exception) {
            threadCaller()
        }

    }

    override fun onLoading(isLoader: Boolean) {
        super.onLoading(false)
        if (loadProgress < PROGRESS_MAX - PROGRESS_STEP) {
            loadProgress += PROGRESS_STEP
        }
        progressbar.progress = loadProgress
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }


    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200) {
            val deviceKey =
                Gson().fromJson<DeviceKey>(result.data.data.toString(), DeviceKey::class.java)
            getDataManager().mPref.prefSetDeviceKey(deviceKey!!)

            if (!deviceKey.appVersion.isNullOrEmpty() && !isLoaded) {
                isLoaded = true

                if (deviceKey.appVersion == BuildConfig.VERSION_NAME) {
                    goToNext()
                } else {
                    newUpdateDialog = NewUpdateDialog(this)
                    val args = Bundle()
                    args.putString(Enums.Common.value.name, deviceKey.appVersion)
                    newUpdateDialog.arguments = args
                    newUpdateDialog.show(this)
                }
            }

        } else {
            showToast(this, result.message ?: getString(R.string.something_went_wrong))
        }
    }

    private fun goToNext() {
        val deepLinkData: Uri? = intent.data
        val notificationData: String? =
            intent.getStringExtra(LocalFirebaseMessagingService.EXTRA_NOTIFICATION_TYPE)

        if (getDataManager().mPref.prefGetLoginMode()) {
            if (deepLinkData != null || notificationData == "RechargeRequest") {
                startActivity(
                    Intent(
                        this@SplashActivity,
                        RechargeRequestListActivity::class.java
                    ).apply {
                        putExtra("RechargeRequest", "RechargeRequest")
                    })
            } else {
                startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            }
        } else {
            startActivity(Intent(this@SplashActivity, LoginStep1Activity::class.java))
        }


        progressbar.progress = PROGRESS_MAX
        finishAffinity()
    }


    companion object {
        const val PROGRESS_MIN = 0
        const val PROGRESS_STEP = 25
        const val PROGRESS_MAX = 100
    }

    override fun onCancelClick() {
        finish()
    }
}