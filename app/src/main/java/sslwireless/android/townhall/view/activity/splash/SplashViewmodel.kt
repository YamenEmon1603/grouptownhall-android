package sslwireless.android.townhall.view.activity.splash

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.DeviceInfo
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class SplashViewmodel(val dataManager: DataManager) : BaseViewmodel(dataManager) {

    fun fetchGetDeviceKey(
        deviceInfo: DeviceInfo,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetDeviceKey(
            deviceInfo,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "deviceKey"))
        )
    }
}