package sslwireless.android.townhall.view.activity.townhall

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sslwireless.android.townhall.R

class TownhallActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_townhall)
    }
}