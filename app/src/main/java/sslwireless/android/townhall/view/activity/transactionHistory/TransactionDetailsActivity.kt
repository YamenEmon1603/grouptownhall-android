package sslwireless.android.townhall.view.activity.transactionHistory

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.view.fragment.transactionHistory.TransactionDetailsSuccessFragment
import sslwireless.android.townhall.view.fragment.transactionHistory.TransactionsDetailsFailedFragment
import sslwireless.android.townhall.view.utils.AppConstants
import kotlinx.android.synthetic.main.activity_transaction_details.*
import kotlinx.android.synthetic.main.activity_transaction_details.view.*
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity


class TransactionDetailsActivity : BaseActivity() {
    private var transaction: TransactionCategoryModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_details)

        transaction =
            intent.extras?.getParcelable<TransactionCategoryModel>(TransactionHistoryActivity.KEY_TRANSACTION)

        initUi()

    }

    override fun viewRelatedTask() {

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

    private fun initUi() {
        var toolbarColor = R.color.colorAccent
        var statusBarColor = R.color.darkish_blue
        var fragment: Fragment? = null

        when (transaction?.statusEnum) {
            AppConstants.TransactionStatus.SUCCESS -> {
                toolbarColor = R.color.colorAccent
                statusBarColor = R.color.darkish_blue
                fragment = TransactionDetailsSuccessFragment.newInstance(transaction)
            }
            AppConstants.TransactionStatus.FAILED -> {
                toolbarColor = R.color.brownish_purple
                statusBarColor = R.color.brownish_purple_two
                fragment = TransactionsDetailsFailedFragment.newInstance(transaction)
            }
        }

        initToolbar(toolbarColor)
        initStatusBar(statusBarColor)
        initFragment(fragment)
    }

    private fun initToolbar(toolbarColor: Int) {
        toolbar.setBackgroundColor(ContextCompat.getColor(this, toolbarColor))
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        toolbar.ivBack.setOnClickListener {
            super.onBackPressed()
            overridePendingTransition(R.anim.activity_in_back, R.anim.activity_out_back)
        }
    }

    private fun initStatusBar(statusBarColor: Int) {
        val window: Window = window
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, statusBarColor)
    }

    private fun initFragment(fragment: Fragment?) {
        if (fragment != null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment).commit()
        }

    }
}