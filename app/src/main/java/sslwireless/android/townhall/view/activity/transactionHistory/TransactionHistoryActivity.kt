package sslwireless.android.townhall.view.activity.transactionHistory

import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.DatePicker
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.TabsPagerAdapter
import sslwireless.android.townhall.view.fragment.transactionHistory.LastWeekTransactionHistoryFragment
import sslwireless.android.townhall.view.utils.AppConstants
import kotlinx.android.synthetic.main.activity_transaction_history.*
import kotlinx.android.synthetic.main.item_transaction_category_date.*
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.utils.dateChangeTo
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible
import java.util.*


class TransactionHistoryActivity : BaseFragment(),
    LastWeekTransactionHistoryFragment.OnLastWeekFragmentInteractionListener,
    DatePickerDialog.OnDateSetListener {

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    companion object {
        const val KEY_TRANSACTION = "_bundle_key_transaction"
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_transaction_history, container, false)
    }


    private fun initTabs() {
        val mTabsPagerAdapter = TabsPagerAdapter(childFragmentManager)
        mTabsPagerAdapter.addFragment(
            LastWeekTransactionHistoryFragment.newInstance(getString(R.string.tab_7_days),this),
            getString(R.string.tab_7_days)
        )
        mTabsPagerAdapter.addFragment(
            LastWeekTransactionHistoryFragment.newInstance(getString(R.string.tab_30_days),this),
            getString(R.string.tab_30_days)
        )
        mTabsPagerAdapter.addFragment(
            LastWeekTransactionHistoryFragment.newInstance(getString(R.string.tab_6_months),this),
            getString(R.string.tab_6_months)
        )
        mTabsPagerAdapter.addFragment(
            LastWeekTransactionHistoryFragment.newInstance(getString(R.string.tab_all_time),this),
            getString(R.string.tab_all_time)
        )
        view_pager.adapter = mTabsPagerAdapter
        tab_layout.setupWithViewPager(view_pager)
    }

    override fun viewRelatedTask() {
        action_calendar.setOnClickListener {
            showDatePickerDialog()
        }

        cancel.setOnClickListener {
            groupViewPager.makeVisible()
            container.makeGone()
            date.makeGone()
            cancel.makeGone()
        }

        initTabs()

    }

    override fun onFragmentInteraction(uri: Uri) {


    }

    override fun <T> onTransactionSelected(model: T) {
        val transactionCategoryModel = model as TransactionCategoryModel

        // TODO: details screen of processing pending for design
        if (transactionCategoryModel.statusEnum == AppConstants.TransactionStatus.SUCCESS || transactionCategoryModel.statusEnum == AppConstants.TransactionStatus.FAILED) {
            val intent = Intent(requireActivity(), TransactionDetailsActivity::class.java)
            intent.putExtra(KEY_TRANSACTION, transactionCategoryModel)
            startActivity(intent)
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        groupViewPager.makeGone()
        container.makeVisible()
        date.makeVisible()
        cancel.makeVisible()
        val date = if (p3 < 10) "0$p3" else p3.toString()
        val month = if (p2 + 1 < 10) "0${p2 + 1}" else (p2 + 1).toString()
        dateTextView.text = "$p1-$month-$date".dateChangeTo(
                AppConstants.DateTimeFormats.TRANSACTION_DATE,
        if (AppConstants.Language.CURRENT_LANGUAGE == AppConstants.Language.ENGLISH)
            AppConstants.DateTimeFormats.TRANSACTION_DATE_AND_DAY
        else
            AppConstants.DateTimeFormats.TRANSACTION_DATE_AND_DAY_BN)
        addFragment( R.id.container,
            LastWeekTransactionHistoryFragment.newInstance("$date-$month-$p1",this,true),true
        )
    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            requireActivity(),
            this,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    /*override fun onBackPressed() {
        if (groupViewPager.visibility==View.GONE) {
            groupViewPager.makeVisible()
            container.makeGone()
        } else {
            super.onBackPressed()
        }
    }*/
}
