package sslwireless.android.townhall.view.activity.uploadPicture

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_camera_handler.*
import kotlinx.android.synthetic.main.activity_upload_picture.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.CameraStep
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import sslwireless.android.townhall.view.utils.AppConstants.TownhallCamera
import java.io.File

class CameraHandlerActivity : BaseActivity(), CameraFragment.ICameraListener,
    UploadSubmitSuccessBottomSheet.IListener {
    private var isFromUpdate: Boolean = false

    private var stepCounter: Int = 0

    private val steps: MutableList<CameraStep> = mutableListOf()

    private val uploadPictureViewModel: UploadPictureViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(UploadPictureViewModel(getDataManager())))
            .get(UploadPictureViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera_handler)
    }

    override fun viewRelatedTask() {
        isFromUpdate = intent.getBooleanExtra(IS_UPDATE_PARAM, false)

        enableActionButtons(true)

        progressGroup.makeGone()

        initCameraSteps()

        handleIndicators(TownhallCamera.STRAIGHT)

        openCamera()

        backIV.setOnClickListener {
            onBackPressed()
        }

        continueBTN.setOnClickListener {
            handleContinue()
        }

        retakeBTN.setOnClickListener {
            openCamera()
        }
    }

    private fun handleContinue() {
        if (isFromUpdate) {
            goBackToUploadPicture(steps)
        } else {
            uploadCurrentPhoto()

            /* stepIncrement()

             checkLastCapture(stepCounter)

             handleIndicators(currentStep()?.type ?: return)

             openCamera()*/
        }
    }

    private fun stepIncrement(): Int = stepCounter++

    private fun currentStep(): CameraStep? =
        if (stepCounter < steps.size) steps[stepCounter] else null

    private fun updateIndicator(type: String, color: Int) {
        when (type) {
            TownhallCamera.STRAIGHT -> {
                updateColors(straightView, color)
            }
            TownhallCamera.RIGHT -> {
                updateColors(rightView, color)
            }
            TownhallCamera.LEFT -> {
                updateColors(leftView, color)
            }
            TownhallCamera.DOWNSIDE -> {
                updateColors(downsideView, color)
            }
            TownhallCamera.UPSIDE -> {
                updateColors(upsideView, color)
            }
        }
    }

    private fun updateColors(view: View, color: Int) {
        view.setBackgroundColor(
            ContextCompat.getColor(
                this,
                color
            )
        )
    }

    private fun openCamera() {
        if (isFinished(stepCounter)) return

        togglePreview(false)

        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.container,
                CameraFragment.newInstance(
                    stepCounter,
                    isFromUpdate,
                    this
                ),
                CameraFragment.TAG
            )
            .commit()
    }

    private fun closeCamera() {
        supportFragmentManager
            .beginTransaction()
            .remove(
                supportFragmentManager
                    .findFragmentByTag(CameraFragment.TAG)
                    ?: return
            )
            .commit()
    }

    private fun uploadCurrentPhoto() {
        val token = getDataManager().mPref.prefGetCustomerToken()?.getNormalBody() ?: return
        val mobile = intent.extras?.getString("mobile")?.getNormalBody() ?: return
        val serial = (stepCounter + 1).toString().getNormalBody()
        val userImage = steps[stepCounter].image?.getImageBody("user_image") ?: return

        uploadPictureViewModel.uploadPicture(
            token = token,
            mobileNo = mobile,
            imageSerial = serial,
            userImage = userImage,
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    private fun onImageUpload() {
        stepIncrement()

        checkLastCapture(stepCounter)

        handleIndicators(currentStep()?.type ?: return)

        openCamera()
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            UploadPictureViewModel.KEY_UPLOAD_PICTURE -> {
                if (result.data?.code == 200) {
                    onImageUpload()
                }

                showToast(this, result.data?.message ?: "Something went wrong!")
            }
        }
    }

    override fun onLoading(isLoader: Boolean) {
        if (isLoader) {
            enableActionButtons(false)

            progressGroup.makeVisible()
        } else {
            enableActionButtons(true)

            progressGroup.makeGone()
        }
    }

    private fun enableActionButtons(isEnabled: Boolean) {
        continueBTN.isEnabled = isEnabled
        retakeBTN.isEnabled = isEnabled
    }

    private fun handleCapture(image: File, step: Int) {
        updateCapturedListWithImage(image, step)
    }

    /**
     * update steps list with captured image
     */
    private fun updateCapturedListWithImage(image: File, step: Int) {
        if (step < 0 || step >= steps.size) {
            return
        }

        val capturedData = steps[step]
        capturedData.isCaptured = true
        capturedData.image = image
        steps[step] = capturedData
    }

    /**
     * checks last capture and goes back to upload
     */
    private fun checkLastCapture(step: Int) {
        if (isFinished(step)) {
            //goBackToUploadPicture(steps)

            updateLoginStatus()

            showSuccessDialog()
        }
    }

    private fun showSuccessDialog() {
        UploadSubmitSuccessBottomSheet(this).show(
            supportFragmentManager,
            UploadSubmitSuccessBottomSheet.TAG
        )
    }

    /**
     * change stepper indicator colors
     */
    private fun handleIndicators(currentStep: String) {
        steps.forEach {
            if (it.type == currentStep) {
                updateIndicator(it.type, R.color.pumpkin_orange)
                return@forEach
            }

            if (it.isCaptured) {
                updateIndicator(it.type, R.color.kelly_green)
            } else {
                updateIndicator(it.type, R.color.greyish_brown)
            }
        }
    }

    private fun isFinished(step: Int) = step > steps.lastIndex

    private fun initCameraSteps() {
        if (isFromUpdate) {
            stepperGroup.makeGone()

            preloadSteps()
        } else {
            stepperGroup.makeVisible()

            generateSteps()
        }
    }

    private fun generateSteps() {
        TownhallCamera.STEPS.forEachIndexed { index, item ->
            steps.add(
                CameraStep(
                    id = index + 1,
                    isCaptured = false,
                    type = item,
                    image = null
                )
            )
        }
    }

    private fun preloadSteps() {
        val initialSteps = intent.getStringExtra(IMAGE_LIST_PARAM)
        val imageList: MutableList<CameraStep> = Gson().fromJson(initialSteps)

        val selectedType = intent.getStringExtra(SELECTED_TYPE_PARAM)
        stepCounter = imageList.indexOf(imageList.findLast { it.type == selectedType })

        steps.clear()
        steps.addAll(imageList)
    }

    companion object {
        const val SELECTED_TYPE_PARAM = "_selected_type"
        const val IS_UPDATE_PARAM = "_is_update"
        const val IMAGE_LIST_PARAM = "_image_list"
    }


    private fun goBackToUploadPicture(capturedList: MutableList<CameraStep>) {
        val intent = Intent()
        intent.putExtra(UploadPictureActivity.CAMERA_CALLBACK_PARAM, Gson().toJson(capturedList))
        setResult(
            RESULT_OK,
            intent
        )
        finish()
    }

    private fun togglePreview(showPreview: Boolean) {
        if (showPreview) {
            cameraGroup.visibility = View.GONE
            previewGroup.visibility = View.VISIBLE
        } else {
            cameraGroup.visibility = View.VISIBLE
            previewGroup.visibility = View.GONE
        }

    }

    override fun onImageCapture(image: File, step: Int) {
        closeCamera()

        togglePreview(true)

        previewIV.loadImage(image)

        handleCapture(image, step)
    }

    override fun onGoHomeClick() {
        gotoHome(this@CameraHandlerActivity)
    }

    override fun onBackPressed() {
        showToast(this, "Please finish uploading photos!")
    }
}