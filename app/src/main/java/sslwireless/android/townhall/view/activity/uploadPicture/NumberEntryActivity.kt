package sslwireless.android.townhall.view.activity.uploadPicture

import android.content.Intent
import android.os.Bundle
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_login_step_2.*
import kotlinx.android.synthetic.main.activity_number_entry.*
import kotlinx.android.synthetic.main.activity_number_entry.back
import kotlinx.android.synthetic.main.activity_number_entry.email_or_phone_et
import kotlinx.android.synthetic.main.activity_number_entry.numEmail
import kotlinx.android.synthetic.main.activity_number_entry.proceed_btn
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.LocalValidator
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.UserCheckResponse
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.activity.webview.WebViewActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.fromResponse

class NumberEntryActivity : BaseActivity() {

    private var mobile: String? = null

    private val uploadPictureViewModel: UploadPictureViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(UploadPictureViewModel(getDataManager())))
            .get(UploadPictureViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_number_entry)
    }

    override fun viewRelatedTask() {
        back.setOnClickListener {
            finish()
        }
        email_or_phone_et.doAfterTextChanged {
            email_or_phone_et.error = null
        }

        proceed_btn.setOnClickListener {
            mobile = email_or_phone_et.text.trim().toString()
            handleProceed(mobile)
        }

        termsTV.setOnClickListener {
            val intent = Intent(this, WebViewActivity::class.java)
            intent.putExtra(Enums.Common.value.name, Enums.Common.condition.ordinal)
            startActivity(intent)
        }

        editTextFocusEventHandler()
    }

    private fun handleProceed(mobile: String?) {
        if (mobile.isNullOrEmpty()) {
            email_or_phone_et.error = getString(R.string.enter_phone_email)
            return
        }
        if (!LocalValidator.isNewPhnValid(mobile!!)) {
            email_or_phone_et.error = getString(R.string.phone_not_valid)
            return
        }

        if (getDataManager().mPref.prefGetDeviceKey() != null) {
            checkUserInfo(mobile)

        }
    }

    private fun checkUserInfo(mobile: String) {
        val token = getDataManager().mPref.prefGetCustomerToken() ?: return

        uploadPictureViewModel.userInfo(
            token = token,
            mobileNo = mobile,
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    private fun editTextFocusEventHandler() {
        email_or_phone_et.setOnFocusChangeListener { view, b ->
            if (b) {
                numEmail.setBackgroundDrawable(resources.getDrawable(R.drawable.mobile_number_drawable))
                numEmail.setPadding(30, 18, 30, 10)
            }
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            UploadPictureViewModel.KEY_USER_INFO -> {
                if (result.data?.code == 200) {
                    val userCheckData = result.data.data?.fromResponse<UserCheckResponse>()
                    if (isUserEligible(userCheckData)) {
                        mobile?.let { gotoUploadPicture(this, it) }
                    } else if (userCheckData?.image_upload_status == true) {
                        showToast(
                            this@NumberEntryActivity,
                            "Image already uploaded!"
                        )
                    } else if (userCheckData?.user_exist == false) {
                        showToast(
                            this@NumberEntryActivity,
                            "User not exists!"
                        )
                    } else {
                        showToast(
                            this@NumberEntryActivity,
                            "Something went wrong!"
                        )
                    }
                } else {
                    showToast(
                        this@NumberEntryActivity,
                        result.data?.message ?: "Something went wrong!"
                    )
                }


            }
        }
    }

    private fun isUserEligible(userCheckData: UserCheckResponse?) =
        userCheckData?.image_upload_status == false && userCheckData.user_exist == true

}