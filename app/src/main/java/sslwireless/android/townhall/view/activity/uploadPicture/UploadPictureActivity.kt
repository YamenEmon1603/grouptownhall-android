package sslwireless.android.townhall.view.activity.uploadPicture

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_camera_handler.*
import kotlinx.android.synthetic.main.activity_upload_picture.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.CameraStep
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import sslwireless.android.townhall.view.utils.AppConstants.TownhallCamera

class UploadPictureActivity : BaseActivity() {

    private var capturedList: MutableList<CameraStep>? = null

    private var selectedType: String = ""

    private var isCaptureCompleted: Boolean = false
    private val uploadPictureViewModel: UploadPictureViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(UploadPictureViewModel(getDataManager())))
            .get(UploadPictureViewModel::class.java)
    }
    private val mobile: String? by lazy {
        intent.extras?.getString("mobile")
    }

    private var forUpdate: Boolean = false

    private var callbackImages: String? = null

    private var uploadCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_picture)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun viewRelatedTask() {

        checkCaptureCompletion()

        captureStraightIV.setOnClickListener {
            if (isCaptureCompleted) {
                initRuntimePermission(TownhallCamera.STRAIGHT, true)
            }

        }

        captureRightIV.setOnClickListener {
            if (isCaptureCompleted) {
                initRuntimePermission(TownhallCamera.RIGHT, true)
            }
        }

        captureLeftIV.setOnClickListener {
            if (isCaptureCompleted) {
                initRuntimePermission(TownhallCamera.LEFT, true)
            }

        }

        captureDownsideIV.setOnClickListener {
            if (isCaptureCompleted) {
                initRuntimePermission(TownhallCamera.DOWNSIDE, true)
            }

        }

        captureUpsideIV.setOnClickListener {
            if (isCaptureCompleted) {
                initRuntimePermission(TownhallCamera.UPSIDE, true)
            }
        }

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }

        continueBtn.setOnClickListener {
            if (isCaptureCompleted) {
                uploadCount = 0

                attemptSend(capturedList)
            } else {
                initRuntimePermission(TownhallCamera.STRAIGHT)
            }
        }
    }

    @Deprecated(message = "this ui don't send image anymore")
    private fun attemptSend(capturedList: MutableList<CameraStep>?) {
        if (capturedList == null) {
            return
        }

        if (isUploadFinished(uploadCount)) {
            return
        }

        uploadPictureViewModel.uploadPicture(
            token = getDataManager().mPref.prefGetCustomerToken()?.getNormalBody() ?: return,
            mobileNo = if (mobile == null) getDataManager().mPref.prefGetCurrentUser().mobile.getNormalBody() else mobile!!.getNormalBody(),
            imageSerial = "".getNormalBody(),
            userImage = capturedList[uploadCount].image?.getImageBody("user_image") ?: return,
            lifecycleOwner = this,
            iObserverCallBack = this
        )
    }

    private fun gotoCameraView(forImage: String, isUpdate: Boolean = false) {
        val intent = Intent(this@UploadPictureActivity, CameraHandlerActivity::class.java)
        intent.putExtra("mobile", mobile)
        intent.putExtra(CameraHandlerActivity.SELECTED_TYPE_PARAM, forImage)
        intent.putExtra(CameraHandlerActivity.IS_UPDATE_PARAM, isUpdate)
        intent.putExtra(CameraHandlerActivity.IMAGE_LIST_PARAM, callbackImages)
        startActivityForResult(intent, REQUEST_CODE)
        finish()
    }

    private fun checkCaptureCompletion(capturedList: MutableList<CameraStep>? = null) {
        val hasPending = capturedList?.any { !it.isCaptured } ?: true

        if (!hasPending) {
            continueBtn.text = getString(R.string.submit_photo)
            updateIVGroup.makeVisible()
            isCaptureCompleted = true
        } else {
            continueBtn.text = getString(R.string.capture_photo)
            updateIVGroup.makeGone()
            isCaptureCompleted = false
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                callbackImages = data?.getStringExtra(CAMERA_CALLBACK_PARAM)

                capturedList = Gson().fromJson(callbackImages)

                updateImageThumbnails(capturedList)
            }
        }
    }


    private fun updateImageThumbnails(capturedList: MutableList<CameraStep>?) {
        if (capturedList == null) return

        capturedList.forEach {
            val image = it.image
            if (image == null) {
                showToast(this, "Invalid image!")
                return
            }

            when (it.type) {
                TownhallCamera.STRAIGHT -> {
                    captureStraightIV.loadImage(image)
                }
                TownhallCamera.RIGHT -> {
                    captureRightIV.loadImage(image)
                }
                TownhallCamera.LEFT -> {
                    captureLeftIV.loadImage(image)
                }
                TownhallCamera.DOWNSIDE -> {
                    captureDownsideIV.loadImage(image)
                }
                TownhallCamera.UPSIDE -> {
                    captureUpsideIV.loadImage(image)
                }
            }

        }

        checkCaptureCompletion(capturedList)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            UploadPictureViewModel.KEY_UPLOAD_PICTURE -> {
                if (result.data?.code == 200) {
                    uploadCount++

                    attemptSend(capturedList)
                } else {
                    uploadCount = 0

                    result.data?.message?.let { showToast(this@UploadPictureActivity, it) }
                }

                checkUpload(uploadCount)
            }
        }


    }

    override fun onError(err: Throwable) {
        super.onError(err)

        uploadCount = 0
    }

    private fun checkUpload(uploadCount: Int) {
        if (isUploadFinished(uploadCount)) {
            updateLoginStatus()

            gotoHome(this@UploadPictureActivity)
        }
    }

    private fun isUploadFinished(uploadCount: Int) = uploadCount == capturedList?.size

    companion object {
        const val CAMERA_CALLBACK_PARAM = "_camera_callback"
        const val REQUEST_CODE = 100
        const val PERMISSION_REQUEST_CODE = 500
    }


    private fun initRuntimePermission(selected: String, isUpdate: Boolean = false) {
        selectedType = selected
        forUpdate = isUpdate

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            gotoCameraView(selected, isUpdate)
            return
        }

        if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
            && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        ) {
            gotoCameraView(selected, isUpdate)
            return
        }

        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST_CODE) {
            handlePermissions(grantResults)
        }
    }

    private fun handlePermissions(grantResults: IntArray) {
        for (result in grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                showToast(this, "Permission denied!")
                return@handlePermissions
            }
        }


        gotoCameraView(selectedType, forUpdate)
    }

}