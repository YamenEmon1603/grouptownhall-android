package sslwireless.android.townhall.view.activity.uploadPicture

import androidx.lifecycle.LifecycleOwner
import okhttp3.MultipartBody
import okhttp3.RequestBody
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class UploadPictureViewModel(val dataManager: DataManager) : BaseViewmodel(dataManager) {
    fun uploadPicture(
        token: RequestBody,
        mobileNo: RequestBody,
        imageSerial: RequestBody,
        userImage: MultipartBody.Part,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiUploadPicture(
            token,
            mobileNo,
            imageSerial,
            userImage,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_UPLOAD_PICTURE))
        )
    }

    fun userInfo(
        token: String,
        mobileNo: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiGetUserInfo(
            token,
            mobileNo,
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, KEY_USER_INFO))
        )
    }


    companion object {
        const val KEY_UPLOAD_PICTURE = "_upload_picture"
        const val KEY_USER_INFO = "_user_info"
    }
}