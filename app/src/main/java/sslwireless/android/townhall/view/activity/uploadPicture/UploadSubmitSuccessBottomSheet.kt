package sslwireless.android.townhall.view.activity.uploadPicture

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.BottomsheetUploadPictureSuccessBinding

class UploadSubmitSuccessBottomSheet(val listener: IListener) : BottomSheetDialogFragment() {
    private lateinit var binding: BottomsheetUploadPictureSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.customDialog)
        isCancelable = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = BottomsheetUploadPictureSuccessBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.goHomeBTN.setOnClickListener {
            listener.onGoHomeClick()
        }
    }

    companion object {
        const val TAG = "UploadSubmitSuccessBottomSheet"
    }

    interface IListener {
        fun onGoHomeClick()
    }
}