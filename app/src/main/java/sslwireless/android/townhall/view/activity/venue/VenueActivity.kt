package sslwireless.android.townhall.view.activity.venue

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_venue.*
import kotlinx.android.synthetic.main.activity_venue.imgRecycler
import kotlinx.android.synthetic.main.activity_venue.sunTitle
import kotlinx.android.synthetic.main.fragment_venue.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.Townhall
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.utils.AppConstants.Venue


class VenueActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_venue)


    }

    override fun viewRelatedTask() {
        val townhallData = getDataManager().mPref.prefGetTownhallKey()
        titleTV.text = townhallData?.townhall?.vanue
        sunTitle.text = townhallData?.townhall?.address
        imgRecycler.adapter = VenueImageAdapter(Venue.imageList)

        initMap(townhallData?.townhall)

        locationDirectionIV.setOnClickListener {
            showDirection()
        }

        back_icon_iv.setOnClickListener {
            onBackPressed()
        }

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }


    private fun initMap(data: Townhall?) {
        val callback = OnMapReadyCallback { googleMap ->
            val location = LatLng(LATITUDE, LONGITUDE)
            googleMap.addMarker(
                MarkerOptions().position(location)
                    .title(data?.vanue)
            )?.showInfoWindow()
            val cameraPosition = CameraPosition.Builder().target(location).zoom(14f).build()
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            //googleMap.moveCamera(CameraUpdateFactory.newLatLng(location))
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

    }

    private fun showDirection() {
        //val uri = "https://goo.gl/maps/eKLpoCr5BVAuMJ3r5"
        val uri =
            "google.navigation:q=$LATITUDE,$LONGITUDE&mode=d"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        intent.setPackage("com.google.android.apps.maps")
        startActivity(intent)
    }


    companion object {
        const val LATITUDE = 23.827862576287487
        const val LONGITUDE = 90.42696481986577
    }
}