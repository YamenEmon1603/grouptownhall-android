package sslwireless.android.townhall.view.activity.venue

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_venue.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.data.model.townhall.Townhall
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.utils.AppConstants.Venue
import sslwireless.android.townhall.view.utils.CommunicationViewModel

class VenueFragment : BaseFragment() {
    private val cViewModel: CommunicationViewModel by activityViewModels()

    override fun viewRelatedTask() {
        cViewModel.townHallResponse.observe(this, Observer {
            it ?: return@Observer

            showData(it)
        })


        locationDirection.setOnClickListener {
            showDirection()
        }
    }

    private fun showData(townHallResponse: TownHallResponse) {
        title.text = townHallResponse.townhall?.vanue
        sunTitle.text = townHallResponse.townhall?.address
        imgRecycler.adapter = VenueImageAdapter(Venue.imageList)

        initMap(townHallResponse.townhall)
    }

    private fun initMap(data: Townhall?) {
        val callback = OnMapReadyCallback { googleMap ->
            val location = LatLng(LATITUDE, LONGITUDE)
            googleMap.addMarker(
                MarkerOptions().position(location)
                    .title(data?.vanue)
            )?.showInfoWindow()
            val cameraPosition = CameraPosition.Builder().target(location).zoom(14f).build()
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            //googleMap.moveCamera(CameraUpdateFactory.newLatLng(location))
        }

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

    }

    private fun showDirection() {
        //val uri = "https://goo.gl/maps/eKLpoCr5BVAuMJ3r5"
        val uri =
            "google.navigation:q=$LATITUDE,$LONGITUDE&mode=d"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        intent.setPackage("com.google.android.apps.maps")
        startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_venue, container, false)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

    companion object {
        const val LATITUDE = 23.827862576287487
        const val LONGITUDE = 90.42696481986577
    }

}