package sslwireless.android.townhall.view.activity.venue

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.VenueImageItemLayoutBinding
import sslwireless.android.townhall.view.utils.*

class VenueImageAdapter(
    private val list: ArrayList<Int>
) :
    RecyclerView.Adapter<VenueImageAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.venue_image_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        if (position==0) {
            holder.binding.leftMergin.makeVisible()
        } else {
            holder.binding.leftMergin.makeGone()
        }
        holder.binding.img.loadImage(item)

    }


    override fun getItemCount(): Int = list.size


    class ViewHolder(val binding: VenueImageItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }
}