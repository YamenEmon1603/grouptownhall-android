package sslwireless.android.townhall.view.activity.webview

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseActivity
import kotlinx.android.synthetic.main.activity_web_view.*
import kotlinx.android.synthetic.main.activity_web_view.progressBar
import kotlinx.android.synthetic.main.activity_web_view.webView
import sslwireless.android.townhall.data.model.TermCondition
import sslwireless.android.townhall.view.activity.desco.utils.Enums
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.fromResponse
import sslwireless.android.townhall.view.utils.showErrorToast


class WebViewActivity : BaseActivity() {

    private var lastBackPressTime: Long = 0
    lateinit var url: String
    var type: Int = -1
    var viewModel: WebViewViewModel? = null
    //lateinit var progressBarHandler: ProgressBarHandler

    companion object {
        const val BUNDLE_PARAM_URL = "_URL"
        const val TERMS_URL = "http://easy.publicdemo.xyz/terms"
        const val PRIVACY_URL = "http://easy.publicdemo.xyz/privacy"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        viewModel =
                ViewModelProviders.of(
                        this,
                        BaseViewmodelFactory(WebViewViewModel(getDataManager()))
                )
                        .get(WebViewViewModel::class.java)
    }

    override fun viewRelatedTask() {
        // setToolbar(this, toolbar, "Cart Details", true)
        //  val toolbar: Toolbar = findViewById(R.id.toolbar)
        // setSupportActionBar(toolbar)
        // supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        // supportActionBar!!.setHomeButtonEnabled(true)

        //progressBarHandler = ProgressBarHandler(this)
        type = intent.getIntExtra(Enums.Common.value.name, -1)

       // url = intent.getStringExtra(BUNDLE_PARAM_URL) ?: TERMS_URL
        //  showTheWebsite(url)
        progressBar.visibility = View.GONE

        if(type == Enums.Common.terms.ordinal ) {
            viewModel!!.getTerms(
                this,
                this
            )
        } else if(type==Enums.Common.condition.ordinal) {
            viewModel!!.getCondition(
                this,
                this
            )
        }else if(type == Enums.Common.privacy.ordinal) {
            viewModel!!.getPrivacy(
                this,
                this
            )
        }
    }

    private fun showTheWebsite(url: String) {
        val webViewClient: WebViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.e("log", "onPageFinished: $url")
                progressBar.visibility = View.GONE
            }

            @SuppressLint("NewApi")
            override fun onReceivedSslError(
                    view: WebView,
                    handler: SslErrorHandler,
                    error: SslError
            ) {
                Toast.makeText(this@WebViewActivity, error.primaryError, Toast.LENGTH_SHORT).show()
            }
        }
       /* webView.getSettings().setLoadsImagesAutomatically(true)
        webView.getSettings().setJavaScriptEnabled(true)
        webView.getSettings().setDomStorageEnabled(true)
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY)
        webView.setWebViewClient(webViewClient)
        webView.loadUrl(url)
        webView.goBack()
        webView.setWebChromeClient(object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progressBar.setProgress(newProgress)
            }
        })*/
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        val baseData = result.data ?: return
        if (baseData.code == 200) {
            if (key == "terms") {
                val termObj = result.data.data.fromResponse<TermCondition>()
                setToolbar(this, toolbar, getString(R.string.terms_of_service), true)
                if (getDataManager().mPref.prefGetLanguage() == AppConstants.Language.ENGLISH) {
                    webView.text=Html.fromHtml(termObj!!.description)
                }else{
                    webView.text=Html.fromHtml(termObj!!.description_bn)
                }
            }else if (key=="condition") {
                val termObj = result.data.data.fromResponse<TermCondition>()
                setToolbar(this, toolbar, getString(R.string.term_condition), true)
                if (getDataManager().mPref.prefGetLanguage() == AppConstants.Language.ENGLISH) {
                    webView.text=Html.fromHtml(termObj!!.description)
                }else{
                    webView.text=Html.fromHtml(termObj!!.description_bn)
                }
            } else if (key == "privacy") {
                val termObj = result.data.data.fromResponse<TermCondition>()
                if (getDataManager().mPref.prefGetLanguage() == AppConstants.Language.ENGLISH) {
                    setToolbar(this, toolbar, termObj!!.title, true)
                    webView.text=Html.fromHtml(termObj!!.description)
                }else{
                    setToolbar(this, toolbar, termObj!!.title_bn, true)
                    webView.text=Html.fromHtml(termObj!!.description_bn)
                }
            }
        } else if (baseData.code == 401) {
            showErrorToast(baseData.message)
        }
    }

    /*override fun onLoading(isLoader: Boolean) {
        if (isLoader) {
            progressBarHandler.show()
        } else {
            progressBarHandler.hide()
        }
    }

    override fun onError(err: Throwable) {
    }*/
}
