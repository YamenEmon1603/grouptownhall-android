package sslwireless.android.townhall.view.activity.webview

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack

class WebViewViewModel(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun getTerms(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getTerms(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "terms"))
        )
    }

    fun getCondition(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getTerms(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "condition"))
        )
    }
    fun getPrivacy(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getPrivacy(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "privacy"))
        )
    }
}