package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.AppliedPromoCodeItemLayoutBinding
import sslwireless.android.townhall.data.model.SampleModel.Promo

class AppliedPromoAdapter (
    mContext: Context,
    private val promo: ArrayList<Promo>
) :
    RecyclerView.Adapter<AppliedPromoAdapter.ViewHolder>() {

   private var binding : AppliedPromoCodeItemLayoutBinding ?= null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.applied_promo_code_item_layout, parent, false)

        val view: View = binding!!.root
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val promo = promo[position]

        binding!!.promoCodeTv.text = promo.promo_code
        binding!!.promoDetailsTv.text = promo.promo_details
        binding!!.promoValidityTv.text = promo.promo_validity

    }


    override fun getItemCount(): Int {
        return promo.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }
}