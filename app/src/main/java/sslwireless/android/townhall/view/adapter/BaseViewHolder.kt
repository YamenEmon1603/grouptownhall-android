package sslwireless.android.townhall.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var mCurrentPosition: Int = 0
    private lateinit var mCallback: IAdapterCallback


    abstract fun<T> onBind(position: Int, model:T, mCallback : IAdapterCallback)

}