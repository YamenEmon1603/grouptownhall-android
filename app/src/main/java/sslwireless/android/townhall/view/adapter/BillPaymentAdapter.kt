package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import kotlinx.android.synthetic.main.item_bill_payment.view.*

class BillPaymentAdapter(
    var mContext: Context,
    private val mTaskInfo:
    List<ServiceListsItem?>?
) :
    RecyclerView.Adapter<BillPaymentAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var clickListener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(inflater.context).inflate(
                R.layout.item_bill_payment,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datum = mTaskInfo!![position]

        holder.tvName.text = datum?.title
        Glide.with(mContext).load(datum?.logo).into(holder.mLogo)

        // mTaskInfo as MutableList<String>
        holder.layoutListItem.setOnClickListener {
            clickListener!!.itemClicked(holder.layoutListItem, position, "", mTaskInfo)
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return mTaskInfo!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    public interface ClickListener {
        fun itemClicked(
            v: View?, position: Int, tag: String,
            mTaskInfo: List<ServiceListsItem?>?
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val layoutListItem = view.layoutListItem
        val tvName = view.tvName
        val mLogo = view.imageView14
    }
}