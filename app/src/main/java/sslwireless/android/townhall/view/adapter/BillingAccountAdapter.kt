package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.descoModels.BillingAccount
import sslwireless.android.townhall.databinding.BillingAccountListItemBinding
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.DESCO
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity.Companion.WASA

class BillingAccountAdapter(
    mContext: Context,
    private val billing_accounts: ArrayList<BillingAccount>,
    var billingAccountActionListerner: BillingAccountActionListerner
) :
    RecyclerView.Adapter<BillingAccountAdapter.ViewHolder>() {

    private var binding: BillingAccountListItemBinding? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.billing_account_list_item,
                parent,
                false
            )

        val view: View = binding!!.root
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val billing_account = billing_accounts[position]
        binding!!.accountNumberTv.text = billing_account.account_no
        when (billing_account.type) {
            DESCO -> {
                binding!!.accountImageIv.setImageResource(R.drawable.ic_desco_bill)
                binding!!.accountTypeTv.text = billing_account.account_type
                binding!!.accountTypeTv.visibility = View.VISIBLE
            }
            WASA -> {
                binding!!.accountTypeTv.visibility = View.GONE
                binding!!.accountImageIv.setImageResource(R.drawable.ic_wasa_drop)
            }
        }


        binding!!.deleteIconIv.setOnClickListener {
            billingAccountActionListerner.onDeleteAccount(billing_accounts[position])
        }

        binding!!.editIconIv.setOnClickListener {
            billingAccountActionListerner.onEditAccount(billing_accounts[position])
        }

        binding!!.root.setOnClickListener {
            billingAccountActionListerner.onItemClicked(billing_accounts[position])
        }


    }


    override fun getItemCount(): Int {
        return billing_accounts.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }

    interface BillingAccountActionListerner{
        fun onDeleteAccount(billingAccount: BillingAccount)
        fun onEditAccount(billingAccount: BillingAccount)
        fun onItemClicked(billingAccount: BillingAccount)
    }

}