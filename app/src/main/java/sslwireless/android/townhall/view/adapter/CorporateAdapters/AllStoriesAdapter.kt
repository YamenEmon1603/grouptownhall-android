package sslwireless.android.townhall.view.adapter.CorporateAdapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R


internal class AllStoriesAdapter(
    private val ctx: FragmentActivity,
    private val storetList: MutableList<String>?
) : RecyclerView.Adapter<AllStoriesAdapter.StoriesViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoriesViewHolder {
        val inflater = LayoutInflater.from(ctx)
        val view = inflater.inflate(R.layout.story_list_items, null)
        return StoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: StoriesViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {

        return storetList!!.size
    }

    internal inner class StoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {

        }
    }
}