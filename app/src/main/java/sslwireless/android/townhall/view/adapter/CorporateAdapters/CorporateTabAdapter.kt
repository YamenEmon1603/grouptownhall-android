package sslwireless.android.townhall.view.adapter.CorporateAdapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import sslwireless.android.townhall.view.fragment.Corporate.AllStores

class CorporateTabAdapter(fm: FragmentManager, internal var totalTabs: Int) :
    FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {

            0 -> {
                return AllStores()
            }

            else -> return Fragment()
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

}