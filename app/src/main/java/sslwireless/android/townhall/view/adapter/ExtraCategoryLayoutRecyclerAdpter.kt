package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import kotlinx.android.synthetic.main.extra_categories_layout.view.*

class ExtraCategoryLayoutRecyclerAdpter(
    mContext: Context,
    private val mTaskInfo:
    IntArray
) :
    RecyclerView.Adapter<ExtraCategoryLayoutRecyclerAdpter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var clickListener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(inflater.context).inflate(
                R.layout.extra_categories_layout,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datum = mTaskInfo[position]

        holder.itemClick.setOnClickListener {
            clickListener!!.itemTopUpClicked(holder.itemClick, position, "", mTaskInfo)
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return mTaskInfo.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ClickListener {
        fun itemTopUpClicked(
            v: View?, position: Int, tag: String,
            mTaskInfo: IntArray
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemClick = view.itemClick
    }
}