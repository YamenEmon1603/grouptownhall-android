package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.databinding.FavoriteContactLayoutBinding
import sslwireless.android.townhall.view.utils.operatorIconByID

class FavoriteContactAdapter(
    private val listener: FavoriteListener
) :
    RecyclerView.Adapter<FavoriteContactAdapter.ViewHolder>() {
    var favList: ArrayList<FavoriteModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.favorite_contact_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = favList?.get(position)
        holder.binding.contactName.text = item?.name
        holder.binding.contactNumber.text = item?.phone
        holder.binding.optIcon.operatorIconByID(item?.operator_id ?: 0)

        holder.binding.contactEdit.setOnClickListener {
            item?.let { it1 -> listener.onNumberUpdate(position, it1) }
        }
        holder.binding.contactCancel.setOnClickListener {
            item?.phone?.let { it1 -> listener.onNumberDelete(position, it1) }
        }
    }

    override fun getItemCount(): Int {
        return favList?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface FavoriteListener {
        fun onNumberDelete(position: Int, phone: String)

        fun onNumberUpdate(position: Int, item: FavoriteModel)
    }


    class ViewHolder(val binding: FavoriteContactLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)
}