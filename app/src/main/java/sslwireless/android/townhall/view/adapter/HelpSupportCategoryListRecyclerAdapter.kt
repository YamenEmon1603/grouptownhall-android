package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import kotlin.collections.ArrayList
import androidx.databinding.DataBindingUtil
import sslwireless.android.townhall.data.model.help.ActiveCategoriesItem
import sslwireless.android.townhall.databinding.ItemHelpAndSupportCategoryBinding
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImageWithPlaceHolder


class HelpSupportCategoryListRecyclerAdapter(
   private val categoryItemClickListener : CategoryItemClickListener,private val lang:String
    ) :
    RecyclerView.Adapter<HelpSupportCategoryListRecyclerAdapter.ViewHolder>() {

    private var help_support_category : ArrayList<ActiveCategoriesItem> = ArrayList()
    private var binding :  ItemHelpAndSupportCategoryBinding ?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_help_and_support_category,
                parent,
                false
            )

        val view: View = binding!!.root
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datum = help_support_category[position]
        binding!!.categoryNameTv.text = if (lang==AppConstants.Language.ENGLISH) datum.title else datum.titleBn
        binding!!.categoryDetailsTv.text =if (lang==AppConstants.Language.ENGLISH) datum.subtitle else datum.subtitleBn
        binding!!.categoryImageIv.loadImageWithPlaceHolder(datum.banner?:"",R.drawable.ic_top_up_new)

        binding!!.helpAndSupportCategoryContainer.setOnClickListener {
            categoryItemClickListener.onCategoryItemClickListener(position)
        }

    }

    

    override fun getItemCount(): Int {
        return help_support_category.size
    }

    fun setData(help_support_category: ArrayList<ActiveCategoriesItem>){
        this.help_support_category = help_support_category
        notifyDataSetChanged()
    }


    interface CategoryItemClickListener{
        fun onCategoryItemClickListener(position: Int)
    }

    
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
      
    }
    
}