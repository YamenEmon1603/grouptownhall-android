package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import kotlin.collections.ArrayList
import androidx.databinding.DataBindingUtil
import sslwireless.android.townhall.data.model.help.ActivePopularTopicsItem
import sslwireless.android.townhall.databinding.ItemHelpAndSupportBinding
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImageWithPlaceHolder


class HelpSupportItemListRecyclerAdapter(
   private val helpItemClickListener: HelpItemClickListener,private val lang:String) :
    RecyclerView.Adapter<HelpSupportItemListRecyclerAdapter.ViewHolder>() {

    private var help_support_item : ArrayList<ActivePopularTopicsItem> = ArrayList()
    private var binding :  ItemHelpAndSupportBinding ?=null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_help_and_support,
                parent,
                false
            )

        val view: View = binding!!.root
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val datum = help_support_item[position]
        binding!!.helpSupportItemTitleTv.text = if (lang== AppConstants.Language.ENGLISH) datum.title else datum.titleBn
        binding!!.helpSupportItemShortDescTv.text = if (lang== AppConstants.Language.ENGLISH) datum.subtitle else datum.subtitleBn
        binding!!.iconView.loadImageWithPlaceHolder(datum.banner?:"",R.drawable.ic_top_up_new)

        binding!!.helpAndSupportItemContainer.setOnClickListener {
            helpItemClickListener.onHelpItemClickListener(position)
        }
    }

    

    override fun getItemCount(): Int {
        return help_support_item.size
    }

    fun setData(help_support_item: ArrayList<ActivePopularTopicsItem>){
        this.help_support_item = help_support_item
        notifyDataSetChanged()
    }


    interface HelpItemClickListener{
        fun onHelpItemClickListener(position: Int)
    }



    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
      
    }
    
}