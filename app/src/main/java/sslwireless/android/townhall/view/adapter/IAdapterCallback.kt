package sslwireless.android.townhall.view.adapter

import android.view.View

interface IAdapterCallback {
    fun <T> clickListener(position: Int, model: T, view: View)
    fun onRemoved(position: Int)

}