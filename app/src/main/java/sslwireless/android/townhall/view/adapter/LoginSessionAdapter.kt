package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.DeviceLocation
import sslwireless.android.townhall.databinding.LoginSessionListItemBinding
import sslwireless.android.townhall.data.model.SampleModel.LoginDevice
import sslwireless.android.townhall.view.utils.dateChangeTo
import sslwireless.android.townhall.view.utils.fromJson
import java.util.*
import kotlin.collections.ArrayList


class LoginSessionAdapter(
    private val loginDeviceList: ArrayList<LoginDevice>,
    private val clickListener: LogoutOptionClickListener, private val deviceKey: String
) :
        RecyclerView.Adapter<LoginSessionAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
                DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.login_session_list_item,
                        parent,
                        false
                )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = loginDeviceList[position]
        val deviceLocation: DeviceLocation = Gson().fromJson(item.deviceLocation)
        // val otp = Gson().fromJson<OTP>(item.deviceLocation, DeviceLocation::class.java)

        try {
            if (deviceLocation.status == "success") {
                holder.binding.deviceNameTv.text = item.device + " - " + deviceLocation.city + ", " + deviceLocation.country
            } else {
                holder.binding.deviceNameTv.text = item.device
            }
        }catch (e:Exception) {
            holder.binding.deviceNameTv.text = item.device
        }


        holder.binding.appNameTv.text = item.browser

        holder.binding.loginTimeTv.text =
                item.loggedTime.dateChangeTo("yyyy-MM-dd HH:mm:ss", "EEE, dd MMM ''yy")

        if (item.sessionKey == deviceKey) {
            holder.binding.loginTimeTv.text = holder.binding.loginTimeTv.context.getText(R.string.active_now)
            holder.binding.loginTimeTv.setTextColor(holder.binding.loginTimeTv.context.resources.getColor(R.color.true_green))
        } else {
            holder.binding.loginTimeTv.setTextColor(holder.binding.loginTimeTv.context.resources.getColor(R.color.get_started_text))
        }

        if (item.os.toLowerCase(Locale.ROOT)=="android" || item.os.toLowerCase(Locale.ROOT)=="ios") {
            holder.binding.deviceIconIv.setImageResource(R.drawable.ic_mobile)
        } else {
            holder.binding.deviceIconIv.setImageResource(R.drawable.ic_computer)
        }

        holder.binding.logoutIconIv.setOnClickListener {
            clickListener.onLogoutOptionClick(position)
        }

    }


    override fun getItemCount(): Int {
        return loginDeviceList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface LogoutOptionClickListener {
        fun onLogoutOptionClick(position: Int)
    }

    class ViewHolder(val binding: LoginSessionListItemBinding) :
            RecyclerView.ViewHolder(binding.root) {

    }
}