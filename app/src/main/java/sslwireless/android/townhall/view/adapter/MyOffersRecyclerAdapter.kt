package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.offer.ExclusiveOffer
import sslwireless.android.townhall.databinding.ListItemOffersNewBinding
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImageWithRoundCorner

class MyOffersRecyclerAdapter(
    private val mTaskInfo: ArrayList<ExclusiveOffer>, private val lang: String
) :
    RecyclerView.Adapter<MyOffersRecyclerAdapter.ViewHolder>() {
    private var clickListener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.list_item_offers_new, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mTaskInfo[position]

        holder.binding.tvTitle.text = if (lang== AppConstants.Language.ENGLISH) model.title else model.title_bn
        holder.binding.tvDetail.text = if (lang==AppConstants.Language.ENGLISH) model.description else model.description_bn
        holder.binding.offerImage.loadImageWithRoundCorner(model.banner, 20)

    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return mTaskInfo.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ClickListener {
        fun itemClicked(
            v: View?, position: Int, tag: String,
            mTaskInfo: MutableList<String>
        )
    }

    class ViewHolder(val binding: ListItemOffersNewBinding) : RecyclerView.ViewHolder(binding.root)
}