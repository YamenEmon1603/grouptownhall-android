package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.NotificationRecyclerItemLayoutBinding
import sslwireless.android.townhall.data.model.SampleModel.Notification
import sslwireless.android.townhall.view.utils.dateChangeTo


class NotificationRecyclerAdapter(private val notifications: ArrayList<Notification>, private val isToday:Boolean) :
    RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.notification_recycler_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val notification = notifications[position]
        //holder.binding.notificationIcon.loadImageWithPlaceHolder(notification.iconUrl, R.drawable.ic_any_notification)
        holder.binding.notificationTitleTv.text = notification.message
        if (isToday) {
            holder.binding.notificationTimeTv.text =
                notification.notifiedAt.dateChangeTo("yyyy-MM-dd HH:mm:ss","hh:mm aa")
        } else {
            holder.binding.notificationTimeTv.text =
                notification.notifiedAt.dateChangeTo("yyyy-MM-dd HH:mm:ss","dd MMM yyyy | hh:mm aa")
        }
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    class ViewHolder(val binding: NotificationRecyclerItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

}