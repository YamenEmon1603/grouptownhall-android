package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.databinding.PaymentSummaryLayoutBinding
import sslwireless.android.townhall.view.utils.operatorIconByID

class PaymentSummaryAdapter(private val topUpModelList :ArrayList<TopUpModel>,private val listener:SummaryListener):
    RecyclerView.Adapter<PaymentSummaryAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context)
                , R.layout.payment_summary_layout
                , parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val value = topUpModelList[position]

        holder.binding.imageView13.operatorIconByID(value.operator)
        holder.binding.number.text = value.phone
        holder.binding.amount.text = value.amount.toString()
        holder.binding.type.text = value.type

        holder.binding.close.setOnClickListener {
            listener.onItemDelete(position)
        }

    }

    override fun getItemCount(): Int {
        return topUpModelList.size
    }

    interface SummaryListener{
        fun onItemDelete(position:Int)
    }

    class ViewHolder(val binding: PaymentSummaryLayoutBinding) : RecyclerView.ViewHolder(binding.root)
}