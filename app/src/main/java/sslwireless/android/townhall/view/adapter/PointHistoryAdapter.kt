package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.SampleModel.PointHistory
import kotlinx.android.synthetic.main.point_history_item_layout.view.*

class PointHistoryAdapter(
    mContext: Context,
    private val point_history: ArrayList<PointHistory>
) :
    RecyclerView.Adapter<PointHistoryAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var clickListener: ClickListener? = null
    private val context: Context = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(inflater.context).inflate(
                R.layout.point_history_item_layout,
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val point_history = point_history[position]
        holder.point_history_title_tv.text = point_history.title
        holder.point_history_date_tv.text = point_history.date

        if (point_history.type.equals("redeem")) {
            holder.points_title_tv.setTextColor(ContextCompat.getColor(context, R.color.tomato))
            holder.point_tv.setTextColor(ContextCompat.getColor(context, R.color.tomato))
            holder.point_tv.text =
                String.format(context.getString(R.string.minus_point), point_history.point)

        } else {
            holder.points_title_tv.setTextColor(ContextCompat.getColor(context, R.color.true_green))
            holder.point_tv.setTextColor(ContextCompat.getColor(context, R.color.true_green))
            holder.point_tv.text =
                String.format(context.getString(R.string.plus_point), point_history.point)
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return point_history.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ClickListener {
        fun itemClicked(
            v: View?, position: Int, tag: String,
            mTaskInfo: MutableList<String>
        )
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var point_history_title_tv = view.point_history_title_tv
        var point_history_date_tv = view.point_history_date_tv
        var point_tv = view.point_tv
        var points_title_tv = view.points_title_tv
    }
}