package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.recentTopUp.RecentOrderData
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.databinding.QuickyItemLayoutBinding
import sslwireless.android.townhall.view.utils.*

class QuickieRecentRecyclerAdapter(
    private val quickieList: ArrayList<RecentOrderData>,
    private val listener: QuickieRecyclerAdapter.QuickieListener,private val language:String
) :
    RecyclerView.Adapter<QuickieRecentRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.quicky_item_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val value = quickieList[position]
        holder.binding.imageView17.operatorIconByID(value.operator!!)
        holder.binding.customTextView17.text = value.phone!!.replace("+88","").replace("-","").replace(" ","")
        holder.binding.layerImgOne.setOnClickListener {
            if (!holder.flag) {
                holder.flag = true
                holder.binding.myNumberGroup.visibility = View.GONE
                holder.binding.enterAmoutGroup.visibility = View.VISIBLE
                holder.binding.view51.isSelected = false
                holder.binding.view50.isSelected = false
                holder.binding.view49.isSelected = false
                holder.binding.customEdittext.setText("")
                val number = holder.binding.customTextView17.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.cross.setOnClickListener {
            if (holder.flag) {
                holder.flag = false
                holder.binding.myNumberGroup.visibility = View.VISIBLE
                holder.binding.enterAmoutGroup.visibility = View.GONE
                holder.binding.view51.isSelected = false
                holder.binding.view50.isSelected = false
                holder.binding.view49.isSelected = false
                holder.binding.customEdittext.setText("")
                val number = holder.binding.customTextView17.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view51.setOnClickListener {
            holder.binding.view51.isSelected = !holder.binding.view51.isSelected
            holder.binding.view50.isSelected = false
            holder.binding.view49.isSelected = false
            if (holder.binding.view51.isSelected) {
                val number = holder.binding.customTextView17.text.toString()
                val topUpModel = TopUpModel(50, value.operator, number, value.type!!.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.customTextView17.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view50.setOnClickListener {
            holder.binding.view51.isSelected = false
            holder.binding.view50.isSelected = !holder.binding.view50.isSelected
            holder.binding.view49.isSelected = false
            if (holder.binding.view50.isSelected) {
                val number = holder.binding.customTextView17.text.toString()
                val topUpModel = TopUpModel(100, value.operator, number, value.type!!.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.customTextView17.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view49.setOnClickListener {
            holder.binding.view51.isSelected = false
            holder.binding.view50.isSelected = false
            holder.binding.view49.isSelected = !holder.binding.view49.isSelected
            if (holder.binding.view49.isSelected) {
                val number = holder.binding.customTextView17.text.toString()
                val topUpModel = TopUpModel(200, value.operator, number, value.type!!.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.customTextView17.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.customEdittext.doOnTextChanged { amount, start, before, count ->
            val number = value.phone.substring(value.phone.length - 11)
            if (amount.toString().trim().isNotEmpty() && amount.toString().trim().toInt()>9) {
                if (amount.toString().trim().toInt() < value.operator.getMinRechargeAmount(value.type) ||
                    amount.toString().trim().toInt() > value.operator.getMaxRechargeAmount(value.type)) {
                    val minAmount = value.operator.getMinRechargeAmount(value.type).toString().digitChangeToBangla(language)
                    val maxAmount = value.operator.getMaxRechargeAmount(value.type).toString().digitChangeToBangla(language)
                    val msg = holder.binding.view51.context.getString(R.string.recharge_range,minAmount,maxAmount,value.type.operatorChangeToBangla(language))
                    listener.onAmountRemove(number,false)
                    holder.binding.view51.context.showToastMsg(msg)
                    return@doOnTextChanged
                }
                val topUpModel = TopUpModel(amount.toString().trim().toInt(), value.operator, number, value.type?.toLowerCase()?:"")
                listener.onAmountAdd(topUpModel)
            } else {
                if (amount.toString().trim().isEmpty()) {
                    listener.onAmountRemove(number,false)
                } else {
                    listener.onAmountRemove(number,true)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return quickieList.size
    }


    class ViewHolder(val binding: QuickyItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        var flag: Boolean = false
    }
}