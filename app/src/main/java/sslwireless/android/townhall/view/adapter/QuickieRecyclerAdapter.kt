package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.data.model.topup_models.TopUpModel
import sslwireless.android.townhall.databinding.QuickyItemFavLayoutBinding
import sslwireless.android.townhall.view.utils.*

class QuickieRecyclerAdapter(
    private val quickieList: ArrayList<FavoriteModel>,
    private val listener: QuickieListener,
    private val language:String
) :
    RecyclerView.Adapter<QuickieRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.quicky_item_fav_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val value = quickieList[position]
        holder.binding.imageView17.operatorIconByID(value.operator_id)
        holder.binding.favName.text = value.name
        holder.binding.favNumber.text = value.phone.replace("+88","").replace("-","").replace(" ","")
        holder.binding.layerImgOne.setOnClickListener {
            if (!holder.flag) {
                holder.flag = true
                holder.binding.myNumberGroup.visibility = View.GONE
                holder.binding.enterAmoutGroup.visibility = View.VISIBLE
                holder.binding.view51.isSelected = false
                holder.binding.view50.isSelected = false
                holder.binding.view49.isSelected = false
                holder.binding.customEdittext.setText("")
                val number = holder.binding.favNumber.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.cross.setOnClickListener {
            if (holder.flag) {
                holder.flag = false
                holder.binding.myNumberGroup.visibility = View.VISIBLE
                holder.binding.enterAmoutGroup.visibility = View.GONE
                holder.binding.view51.isSelected = false
                holder.binding.view50.isSelected = false
                holder.binding.view49.isSelected = false
                holder.binding.customEdittext.setText("")
                val number = holder.binding.favNumber.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view51.setOnClickListener {
            holder.binding.view51.isSelected = !holder.binding.view51.isSelected
            holder.binding.view50.isSelected = false
            holder.binding.view49.isSelected = false
            if (holder.binding.view51.isSelected) {
                val number = holder.binding.favNumber.text.toString()
                val topUpModel = TopUpModel(50, value.operator_id, number, value.connection_type.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.favNumber.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view50.setOnClickListener {
            holder.binding.view51.isSelected = false
            holder.binding.view50.isSelected = !holder.binding.view50.isSelected
            holder.binding.view49.isSelected = false
            if (holder.binding.view50.isSelected) {
                val number = holder.binding.favNumber.text.toString()
                val topUpModel = TopUpModel(100, value.operator_id, number, value.connection_type.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.favNumber.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.view49.setOnClickListener {
            holder.binding.view51.isSelected = false
            holder.binding.view50.isSelected = false
            holder.binding.view49.isSelected = !holder.binding.view49.isSelected
            if (holder.binding.view49.isSelected) {
                val number = holder.binding.favNumber.text.toString()
                val topUpModel = TopUpModel(200, value.operator_id, number, value.connection_type.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                val number = holder.binding.favNumber.text.toString()
                listener.onAmountRemove(number,false)
            }
        }

        holder.binding.customEdittext.doOnTextChanged { amount, start, before, count ->
            val number = holder.binding.favNumber.text.toString()
            if (amount.toString().trim().isNotEmpty() && amount.toString().trim().toInt()>9) {
                if (amount.toString().trim().toInt() < value.operator_id.getMinRechargeAmount(value.connection_type) ||
                    amount.toString().trim().toInt() > value.operator_id.getMaxRechargeAmount(value.connection_type)) {
                    val minAmount = value.operator_id.getMinRechargeAmount(value.connection_type).toString().digitChangeToBangla(language)
                    val maxAmount = value.operator_id.getMaxRechargeAmount(value.connection_type).toString().digitChangeToBangla(language)
                    val msg = holder.binding.favNumber.context.getString(R.string.recharge_range,minAmount,maxAmount,value.connection_type.operatorChangeToBangla(language))
                    listener.onAmountRemove(number,true)
                    holder.binding.favNumber.context.showToastMsg(msg)
                    return@doOnTextChanged
                }
                val topUpModel = TopUpModel(amount.toString().trim().toInt(), value.operator_id, number, value.connection_type.toLowerCase())
                listener.onAmountAdd(topUpModel)
            } else {
                if (amount.toString().trim().isEmpty()) {
                    listener.onAmountRemove(number,false)
                } else {
                    listener.onAmountRemove(number,true)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return quickieList.size
    }

    interface QuickieListener {
        fun onAmountAdd(item: TopUpModel)

        fun onAmountRemove(phone: String,isNumberShowing:Boolean)

    }

    class ViewHolder(val binding: QuickyItemFavLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        var flag: Boolean = false
    }
}