package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.recentTopUp.RecentOrderData
import sslwireless.android.townhall.databinding.RecentItemLayoutBinding
import sslwireless.android.townhall.view.utils.operatorIconByID
import sslwireless.android.townhall.view.utils.recentDayDif

class RecentTransactionAdapter(private val orderList: ArrayList<RecentOrderData>,private val listener:RecentNumberListener) :
    RecyclerView.Adapter<RecentTransactionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.recent_item_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = orderList[position]
        holder.binding.contactNumber.text= item.phone?:""
        holder.binding.optIcon.operatorIconByID(item.operator?:1)
        holder.binding.dayDifText.text = holder.binding.dayDifText.context.recentDayDif(item.time!!,"yyyy-MM-dd HH:mm:ss")
        holder.binding.rootLayout.setOnClickListener { listener.onRecentNumberListener(item) }
    }


    override fun getItemCount(): Int {
        return orderList.size
    }

    interface RecentNumberListener {
        fun onRecentNumberListener(item: RecentOrderData)
    }

    class ViewHolder(val binding: RecentItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)
}