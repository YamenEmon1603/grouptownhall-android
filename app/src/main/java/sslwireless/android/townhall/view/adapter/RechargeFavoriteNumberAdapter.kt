package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.View.GONE
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.favorite.FavoriteModel
import sslwireless.android.townhall.databinding.FavoriteContactLayoutBinding
import sslwireless.android.townhall.view.utils.operatorIconByID

class RechargeFavoriteNumberAdapter(private val favList: ArrayList<FavoriteModel>,private val listener:NumberListener) :
    RecyclerView.Adapter<RechargeFavoriteNumberAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context), R.layout.favorite_contact_layout, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = favList[position]
        holder.binding.contactName.text = item.name
        holder.binding.contactNumber.text = item.phone
        holder.binding.optIcon.operatorIconByID(item.operator_id)
        holder.binding.editGroup.visibility = GONE
        holder.binding.rootLayout.setOnClickListener {
            listener.onNumberListener(item)
        }
    }


    override fun getItemCount(): Int {
        return favList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface NumberListener {
        fun onNumberListener(item: FavoriteModel)
    }

    class ViewHolder(val binding: FavoriteContactLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)
}