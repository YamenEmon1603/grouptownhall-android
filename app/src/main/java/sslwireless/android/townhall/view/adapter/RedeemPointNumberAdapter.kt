package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
//import com.jakewharton.rxbinding3.view.clicks
import sslwireless.android.townhall.databinding.ItemRedeemPointNumberBinding

class RedeemPointNumberAdapter(
    mContext: Context,
    private val numbers: ArrayList<String>
) :
    RecyclerView.Adapter<RedeemPointNumberAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var clickListener: ClickListener? = null
    private val context: Context = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val bind = ItemRedeemPointNumberBinding.inflate(inflater, parent, false)
        return ViewHolder(bind)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(numbers[position])
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return numbers.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ClickListener {
        fun itemClicked(
            v: View?, position: Int, tag: String,
            mTaskInfo: MutableList<String>
        )
    }

    class ViewHolder(val bind: ItemRedeemPointNumberBinding) : RecyclerView.ViewHolder(bind.root) {

        fun onBind(s: String) {
            bind.numberRb.setOnCheckedChangeListener { compoundButton, isSelected ->
                bind.numberLayout.isPressed = isSelected
            }
        }
    }
}