package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.SampleModel.RedeemPointOffer
import kotlinx.android.synthetic.main.redeem_point_offers_item_layout.view.*

class RedeemPointOfferAdapter (
    mContext: Context,
    private val redeemPointOffers: ArrayList<RedeemPointOffer>
) :
    RecyclerView.Adapter<RedeemPointOfferAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private var clickListener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(inflater.context).inflate(
            R.layout.redeem_point_offers_item_layout,
            parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val redeemPointOffers = redeemPointOffers[position]
        holder.offer_title.text = redeemPointOffers.title
        holder.offer_validity.text = redeemPointOffers.validity
        holder.required_point_tv.text = redeemPointOffers.point
        holder.offer_image.setImageResource(redeemPointOffers.image!!)

        holder.itemView.setOnClickListener {
            clickListener!!.itemClicked(position)
        }
    }

    fun setClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    override fun getItemCount(): Int {
        return redeemPointOffers.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    interface ClickListener {
        fun itemClicked(position: Int)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var offer_title = view.offer_title_tv
        var offer_image = view.offer_image_iv
        var offer_validity = view.offer_validity_tv
        var required_point_tv = view.required_point_tv
    }
}