package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import kotlinx.android.synthetic.main.reffered_friends_item_layout.view.*

class ReferredFriendsListAdapter (
    mContext: Context,
    private val name: ArrayList<String>
) :
    RecyclerView.Adapter<ReferredFriendsListAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private val context: Context = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(inflater.context).inflate(
            R.layout.reffered_friends_item_layout,
            parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name_tv.text = name[position]
    }

    override fun getItemCount(): Int {
        return name.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name_tv = view.referred_friends_name_tv
    }
}