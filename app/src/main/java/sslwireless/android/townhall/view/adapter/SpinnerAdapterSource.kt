package com.sslwireless.crm.view.adapter

import android.content.Context
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import sslwireless.android.townhall.R


class SpinnerAdapterSource(context: Context,dates : ArrayList<String>) {

    var mContext = context
    var dates = dates

    fun getAdapter(): ArrayAdapter<String>
    {
        val spinnerAdapter = object : ArrayAdapter<String>(
            mContext, R.layout.spinner_item,dates
        ) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
              /*  tv.setPadding(0,0,0,0)
                tv.setTextSize(12f)*/
                if(position == 0){
                    tv.setTextColor(ContextCompat.getColor(mContext,R.color.white))
                }else{
                    tv.setTextColor(ContextCompat.getColor(mContext,R.color.white))
                }
                return view
            }
            override fun getDropDownView(
                position: Int, convertView: View?,
                parent: ViewGroup
            ): View {
                val view = super.getDropDownView(position, convertView, parent)
                val tv = view as TextView
                SetItemBackground(position, tv)
                return view
            }
        }
        spinnerAdapter!!.setDropDownViewResource(R.layout.spinner_dropdown_item)
        return spinnerAdapter
    }





    fun SetItemBackground(position:Int,tv:TextView )
    {
        if (position == 0) {
            tv.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        } else if (position % 2 == 1) {
            // Set the items background color
            tv.setBackgroundColor(ContextCompat.getColor(mContext,R.color.shadow_color));
        } else {
            // Set the alternate items background color
            tv.setBackgroundColor(ContextCompat.getColor(mContext,R.color.shadow_color));
        }
    }


}
