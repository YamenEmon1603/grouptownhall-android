package sslwireless.android.townhall.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.databinding.ItemTransactionCategoryBinding
import sslwireless.android.townhall.databinding.ItemTransactionCategoryDateBinding
import sslwireless.android.townhall.view.fragment.transactionHistory.LastWeekTransactionHistoryFragment
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.dateChangeTo

class TransactionCategoryAdapter : RecyclerView.Adapter<BaseViewHolder>() {

    var categories: List<TransactionCategoryModel>? = null
    lateinit var iCallBack: IAdapterCallback
    lateinit var con: Context

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): BaseViewHolder {
        con = parent.context
        return if (viewType == LastWeekTransactionHistoryFragment.LIST_TYPE_DATE) {
            DateViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.item_transaction_category_date, parent, false
                    )
            )
        } else {
            CategoryViewHolder(
                    DataBindingUtil.inflate(
                            LayoutInflater.from(parent.context),
                            R.layout.item_transaction_category, parent, false
                    )
            )
        }

    }

    override fun getItemCount(): Int {
        return categories?.size ?: 0
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, categories?.get(position), iCallBack)
        //holder.itemView.itemParent.animation = AnimationUtils.loadAnimation(con, R.anim.fade_transaction_animation)
    }

    public fun setCallback(iCallBack: IAdapterCallback) {
        this.iCallBack = iCallBack
    }

    override fun getItemViewType(position: Int): Int {
        return categories?.get(position)?.type ?: -1
    }

    class CategoryViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        var binding = itemView as ItemTransactionCategoryBinding

        override fun <T> onBind(position: Int, model: T, mCallback: IAdapterCallback) {
            model as TransactionCategoryModel
            binding.categoryName.text = model.categoryName
            binding.amount.text = model.amount
            binding.phoneOrUsername.text = model.user

            if (!model.status.isNullOrEmpty()) {
                when (model.statusEnum) {
                    AppConstants.TransactionStatus.SUCCESS -> {
                        binding.status.setBackgroundResource(R.drawable.bg_success)
                    }
                    AppConstants.TransactionStatus.FAILED -> {
                        binding.status.setBackgroundResource(R.drawable.bg_failed)
                    }
                    AppConstants.TransactionStatus.PROCESSING -> {
                        binding.status.setBackgroundResource(R.drawable.bg_processing)
                    }
                    AppConstants.TransactionStatus.SCHEDULED -> {
                        binding.status.setBackgroundResource(R.drawable.bg_scheduled)
                    }
                    else -> {
                        binding.status.setBackgroundResource(R.drawable.bg_default)
                    }
                }

                binding.status.text = model.status
            } else{
                binding.status.setBackgroundResource(R.drawable.bg_default)
            }

            binding.root.setOnClickListener {
                mCallback.clickListener(position, model, binding.root)
            }
        }

    }

    class DateViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        var binding = itemView as ItemTransactionCategoryDateBinding

        override fun <T> onBind(position: Int, model: T, mCallback: IAdapterCallback) {
            model as TransactionCategoryModel

            binding.dateTextView.text = model.date.dateChangeTo(
                    AppConstants.DateTimeFormats.TRANSACTION_DATE,
                    AppConstants.DateTimeFormats.TRANSACTION_DATE_AND_DAY
            )
        }

    }

}
