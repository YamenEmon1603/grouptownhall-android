package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import sslwireless.android.townhall.databinding.ItemUtilityBillsBinding
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImage

class UtilityBillsListAdapter(val listener: ItemListener, private val lang: String) :
    RecyclerView.Adapter<UtilityViewHolder>() {
    var list: List<ServiceListsItem?>? = null

    interface ItemListener {
        fun itemClicked(item: ServiceListsItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UtilityViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemUtilityBillsBinding.inflate(inflater, parent, false)
        return UtilityViewHolder(binding, lang)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: UtilityViewHolder, position: Int) {
        list?.get(position)?.let { holder.bind(it, listener) }
    }

}

class UtilityViewHolder(val binding: ItemUtilityBillsBinding, private val lang: String) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(
        item: ServiceListsItem,
        listener: UtilityBillsListAdapter.ItemListener
    ) {
        binding.serviceText.text = if (lang == AppConstants.Language.ENGLISH) item.title else item.bnTitle
        item.logo?.let { binding.iconImage.loadImage(it) }
        binding.billItemLayout.setOnClickListener {
            listener.itemClicked(item)
        }
    }
}