package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.ItemSavedBillingAccountBinding

class WasaAccountAdapter(accounts: ArrayList<String>) : RecyclerView.Adapter<BaseViewHolder>() {

    var accounts = accounts
    lateinit var mCallback: IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_saved_billing_account, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return accounts.size
    }

    fun setCallback(mCallback: IAdapterCallback):WasaAccountAdapter{
        this.mCallback =mCallback
        return this
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, accounts.get(position),mCallback)
    }


    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        var binding = itemView as ItemSavedBillingAccountBinding

        override fun <T> onBind(position: Int, model: T, mCallback : IAdapterCallback) {

            model as String

        }
    }
}
