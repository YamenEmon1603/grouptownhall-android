package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.ItemWasaAccountSearchBinding

class WasaAccountSearchAdapter (accounts: ArrayList<String>) : RecyclerView.Adapter<BaseViewHolder>() {

    lateinit var iAdapterCallback: IAdapterCallback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_wasa_account_search, parent, false
            ))
    }

    var accounts = accounts




    override fun getItemCount(): Int {
        return accounts.size
    }

    public fun setCallBack(iAdapterCallback: IAdapterCallback){
        this.iAdapterCallback = iAdapterCallback

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, accounts.get(position),iAdapterCallback)

        holder as ViewHolder

        holder.binding.root.setOnClickListener {
            iAdapterCallback.clickListener(position,accounts.get(position),holder.binding.root)

        }
    }



    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        val binding = itemView  as ItemWasaAccountSearchBinding

        override fun<T> onBind(position: Int, model:T, mCallback : IAdapterCallback) {

            model as String


        }


    }
}
