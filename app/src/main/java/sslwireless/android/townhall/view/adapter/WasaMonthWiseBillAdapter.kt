package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.ItemWasaMonthWiseBillBinding

class WasaMonthWiseBillAdapter (bills: ArrayList<String>) : RecyclerView.Adapter<BaseViewHolder>() {

    var bills = bills
    lateinit var iAdapterCallback: IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_wasa_month_wise_bill, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return bills.size
    }

    public fun setCallBack(iAdapterCallback: IAdapterCallback){
        this.iAdapterCallback = iAdapterCallback

    }


    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, bills.get(position),iAdapterCallback)

        holder as ViewHolder

        holder.binding.billAmountTv.setOnClickListener{
            iAdapterCallback.clickListener(position,bills.get(position),holder.binding.billAmountTv)
        }
        holder.binding.billDetailsTv.setOnClickListener{
            iAdapterCallback.clickListener(position,bills.get(position),holder.binding.billDetailsTv)
        }
        holder.binding.ivWasaBillNextStep.setOnClickListener{
            iAdapterCallback.clickListener(position,bills.get(position),holder.binding.ivWasaBillNextStep)
        }

    }



    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

        var binding = itemView  as ItemWasaMonthWiseBillBinding

        override fun<T> onBind(position: Int, model:T, mCallback : IAdapterCallback) {

            model as String

        }


    }

}
