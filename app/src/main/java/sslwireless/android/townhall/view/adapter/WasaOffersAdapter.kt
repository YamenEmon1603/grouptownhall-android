package sslwireless.android.townhall.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.ItemWasaOfferBinding

class WasaOffersAdapter(offers: ArrayList<Int>) : RecyclerView.Adapter<BaseViewHolder>() {

    var offers = offers
    lateinit var iCallBack : IAdapterCallback

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_wasa_offer, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return offers.size
    }

    fun setCallback(mCallback: IAdapterCallback):WasaOffersAdapter{
        this.iCallBack = mCallback
        return this
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position, offers.get(position), iCallBack)
    }



    class ViewHolder(itemView: ViewDataBinding) : BaseViewHolder(itemView.root) {

            var binding = itemView  as ItemWasaOfferBinding

        override fun<T> onBind(position: Int, model:T, mCallback : IAdapterCallback) {

            model as Int

            binding.ivOffer.setImageResource(model)
        }


    }

}
