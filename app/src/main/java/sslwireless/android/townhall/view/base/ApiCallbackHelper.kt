package sslwireless.android.townhall.view.base

import androidx.lifecycle.MutableLiveData
import sslwireless.android.townhall.data.model.BaseModel
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import sslwireless.android.townhall.utils.LiveDataResult

class ApiCallbackHelper(
    liveData: MutableLiveData<LiveDataResult<BaseModel<Any>>>
) :
    MaybeObserver<BaseModel<Any>> {

    val liveData = liveData
    override fun onSubscribe(d: Disposable) {
        liveData.postValue(LiveDataResult.loading())
    }

    override fun onError(e: Throwable) {

        liveData.postValue(LiveDataResult.error(e))
    }

    override fun onSuccess(t: BaseModel<Any>) {
        liveData.postValue(LiveDataResult.succes(t) as LiveDataResult<BaseModel<Any>>)
    }

    override fun onComplete() {
    }

}