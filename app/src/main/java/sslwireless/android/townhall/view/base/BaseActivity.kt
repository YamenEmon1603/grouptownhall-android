package sslwireless.android.townhall.view.base

import android.annotation.TargetApi
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.text.TextUtils
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.EasyApp
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.prefence.PreferencesHelper
import kotlinx.android.synthetic.main.toolbar.view.*
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.uploadPicture.UploadPictureActivity
import sslwireless.android.townhall.view.utils.ProgressBarHandler
import java.io.IOException
import java.util.*
import java.util.regex.Pattern

abstract class BaseActivity : AppCompatActivity(), IObserverCallBack {

    var displayMetrics = DisplayMetrics()
    private var dialogs: ProgressDialog? = null
    private var context: Context? = null
    private val listString = StringBuilder()
    lateinit var progressBarHandler: ProgressBarHandler

    val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        context = this

        progressBarHandler = ProgressBarHandler(context!!)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        viewRelatedTask()
    }

    abstract fun viewRelatedTask()

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        (context as Activity).overridePendingTransition(R.anim.activity_in, R.anim.activity_out)
    }

    fun startActivityLanguage(intent: Intent) {
        super.startActivity(intent)
        (context as Activity).overridePendingTransition(0, 0)
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        (context as Activity).overridePendingTransition(
            R.anim.activity_in_back,
            R.anim.activity_out_back
        )
    }

    fun customBackPressed() {
        (context as Activity).overridePendingTransition(
            R.anim.activity_in_back,
            R.anim.activity_out_back
        )
    }

    fun onBackPress(view: View) {
        super.onBackPressed()
        (context as Activity).overridePendingTransition(
            R.anim.activity_in_back,
            R.anim.activity_out_back
        )

    }


    fun showToast(context: Context, message: String) {
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_LONG
        toast.setGravity(Gravity.CENTER, 0, 0);

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_toast_layout, null)

        val toastText = view.findViewById<TextView>(R.id.toastText)
        toastText.text = message

        toast.view = view
        toast.show()
    }

    fun hideKeyboard() {
        // Check if no view has focus:
        val view = currentFocus
        if (view != null) {
            val inputManager = getSystemService(
                Context
                    .INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken, InputMethodManager
                    .HIDE_NOT_ALWAYS
            )
        }
    }

    fun showKeyboard() {
        val view = currentFocus
        if (view != null) {
            val inputManager = getSystemService(
                Context
                    .INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken, InputMethodManager
                    .SHOW_FORCED
            )
        }
    }

    fun showProgressDialog(message: String) {
        if (dialogs == null) {
            dialogs = ProgressDialog(context, R.style.MyAlertDialogStyle)
            dialogs!!.progress = ProgressDialog.STYLE_SPINNER
            dialogs!!.setCancelable(false)
        }

        if (TextUtils.isEmpty(message)) {
            dialogs!!.setMessage("")
        } else {
            dialogs!!.setMessage(message)
        }
        if (dialogs != null && !dialogs!!.isShowing) {
            dialogs!!.setCancelable(false)
            dialogs!!.show()
        }
    }

    fun hideProgressDialog() {
        if (!this.isFinishing && dialogs != null && dialogs!!.isShowing) {
            dialogs!!.dismiss()
        }
    }

    fun makeClipboard(key: String, value: String) {
        val sdk = Build.VERSION.SDK_INT
        if (sdk < Build.VERSION_CODES.HONEYCOMB) {
            val clipboard =
                this.getSystemService(Context.CLIPBOARD_SERVICE) as android.text.ClipboardManager
            clipboard.text = value
        } else {
            val clipboard =
                getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = android.content.ClipData.newPlainText(key, value)
            clipboard.setPrimaryClip(clip)
        }
    }

    fun isAppInstalled(uri: String): Boolean {
        val pm = packageManager
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
        }

        return false
    }

    fun saveUserType(type: String, context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString("type", type)
        editor.commit()
    }

    fun setToolbar(
        context: Context,
        view: View,
        title: String,
        isBackPressed: Boolean
    ) {
        view.toolbar.drawerTitle.setText(title)
        if (isBackPressed) {
            view.toolbar.drawerNavigationIcon.isVisible = true
            view.toolbar.drawerNavigationIcon.setImageResource(R.drawable.ic_back_black)
            view.toolbar.drawerNavigationIcon.setOnClickListener({ view -> onBackPressed() })
        } else {
            view.toolbar.drawerNavigationIcon.isVisible = false
            // view.toolbar.drawerNavigationIcon.setImageResource(R.drawable.menu)
            //view.toolbar.drawerNavigationIcon.setOnClickListener { view ->
        }

        val window = this.window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.colorPrimaryDark)
        }
    }

    fun addFragment(isReplace: Boolean, container: Int, fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        if (isReplace) {
            transaction.replace(container, fragment)
        } else {
            transaction.add(container, fragment)
        }
        transaction.commit()
    }

    fun fragmentShowHide(activeFragment: Fragment, inActiveFragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            hide(inActiveFragment)
            show(activeFragment)
            commit()
        }
    }

    fun addFragmentNew(isShow: Boolean, container: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            add(container, fragment, fragment.tag)
            if (isShow) show(fragment) else hide(fragment)
            commit()
        }
    }

    fun getUserType(context: Context): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString("type", "")
    }

    fun getDataManager(): DataManager {
        val application = applicationContext as EasyApp
        return application.getDataManager()
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(setLocale(newBase, PreferencesHelper(newBase).prefGetLanguage()))
    }

    private fun setLocale(con: Context, language: String): Context {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            updateResources(con, language)
        } else updateResourcesLegacy(con, language)
    }


    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResources(con: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = con.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        return con.createConfigurationContext(config)
    }

    private fun updateResourcesLegacy(con: Context, language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = con.resources
        val configuration = resources.configuration
        configuration.locale = locale
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLayoutDirection(locale)
        }
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return con
    }


    companion object {

        fun isEmailValid(email: String): Boolean {
            var isValid = false

            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"

            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(email)
            if (matcher.matches()) {
                isValid = true
            }
            return isValid
        }
    }

    override fun onLoading(isLoader: Boolean) {
        if (isLoader) {
            progressBarHandler.show()
            //showProgressDialog(getString(R.string.please_wait))
        } else {
            progressBarHandler.hide()
            // hideProgressDialog()
        }
    }

    override fun onError(err: Throwable) {
        if (err is IOException) {
            showToast(this, "No Internet!")
        } else {
            showToast(this, err.message.toString())
        }
    }

    fun returnMessage(message: List<String?>?): String? {
        listString.clear()

        for (item in message!!.iterator()) {
            listString.append(item)
        }

        return listString.toString()
    }

    fun gotoHome(activity: Activity) {
        if (!getDataManager().mPref.prefGetLoginMode()) {
            return
        }

        val intent = Intent(activity, MainActivity::class.java)
        startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK))
        finishAffinity()
    }

    fun updateLoginStatus() = getDataManager().mPref.prefLoginIsValid()

    fun gotoUploadPicture(activity: Activity, mobile: String) {
        val intent = Intent(activity, UploadPictureActivity::class.java)
        intent.putExtra("mobile", mobile)
        startActivity(intent)
        finish()
    }
}
