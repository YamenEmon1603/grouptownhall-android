package sslwireless.android.townhall.view.base

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.utils.ProgressBarHandler
import java.io.IOException


abstract class BaseFragment : Fragment(), IObserverCallBack{

    private val listString = StringBuilder()
    private var dialogs: ProgressDialog? = null
    lateinit var progressBarHandler: ProgressBarHandler

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        val activity = context as BaseActivity
        progressBarHandler = ProgressBarHandler(activity)

        viewRelatedTask()
    }

    abstract fun viewRelatedTask()

    fun showToast(context: Context, message: String) {
        val toast = Toast(context)
        toast.duration = Toast.LENGTH_LONG
        toast.setGravity(Gravity.CENTER, 0, 0)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.custom_toast_layout, null)

        val toastText = view.findViewById<TextView>(R.id.toastText)
        toastText.setText(message)

        toast.view = view
        toast.show()
    }

   /* fun showDialog(isCancelAble: Boolean,title:String) {
        (activity as BaseActivity).showDialog(isCancelAble, title)
    }
    fun hideDialog() {
        (activity as BaseActivity).hideDialog()
    }*/

   /* fun getDataManager(): DataManager {
        val application = context as BaseActivity
        return application.getDataManager()
    }*/

    fun hideKeyboard() {
        val application = context as BaseActivity
        return application.hideKeyboard()
    }

    fun getDataManager(): DataManager {
        val activity = context as BaseActivity
        return activity.getDataManager()
    }

    fun addFragment(container: Int, fragment: Fragment, isReplace: Boolean) {
        val transaction = childFragmentManager.beginTransaction()
        if (isReplace) {
            transaction.replace(container, fragment)
        } else {
            //transaction.add(container, fragment)
        }
        transaction.commit()
    }

    fun onBackPress(view: View){
        requireActivity().onBackPressed()
        (activity as Activity).overridePendingTransition(R.anim.activity_in_back, R.anim.activity_out_back)

    }

    override fun onLoading(isLoader: Boolean) {
        if (isLoader) {
            progressBarHandler.show()
            //showProgressDialog(getString(R.string.please_wait))
        }
        else {
            progressBarHandler.hide()
           // hideProgressDialog()
        }
    }

    override fun onError(err: Throwable) {
        if (err is IOException) {
            showToast(requireContext(), "No Internet!")
        } else {
            showToast(requireContext(), err.message.toString())
        }
    }

    fun showProgressDialog(message: String) {
        if (dialogs == null) {
            dialogs = ProgressDialog(context, R.style.MyAlertDialogStyle)
            dialogs!!.progress = ProgressDialog.STYLE_SPINNER
            dialogs!!.setCancelable(false)
        }

        if (TextUtils.isEmpty(message)) {
            dialogs!!.setMessage("")
        } else {
            dialogs!!.setMessage(message)
        }
        if (dialogs != null && !dialogs!!.isShowing) {
            dialogs!!.setCancelable(false)
            dialogs!!.show()
        }
    }

    fun hideProgressDialog() {
        if (!requireActivity().isFinishing && dialogs != null && dialogs!!.isShowing) {
            dialogs!!.dismiss()
        }
    }

    fun returnMessage(message: List<String?>?): String? {
        listString.clear()

        for (item in message!!.iterator()) {
            listString.append(item)
        }

        return listString.toString()
    }

}