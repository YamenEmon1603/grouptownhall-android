package sslwireless.android.townhall.view.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.view.IAdapterListener

abstract class BaseViewHolder2 (itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var mCurrentPosition: Int = 0
    private lateinit var mCallback: IAdapterListener


    abstract fun<T> onBind(position: Int, model:T, mCallback : IAdapterListener)

}