package sslwireless.android.townhall.view.base

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import monim.blackice.business.util.ObserverHelper

abstract class BaseViewmodel(dataManager: DataManager) : ViewModel() {

    private var dataManager = dataManager
    private val hashMap: HashMap<String, MutableLiveData<LiveDataResult<BaseModel<Any>>>> = HashMap()


    fun addLiveData(key: String): MutableLiveData<LiveDataResult<BaseModel<Any>>> {
        hashMap.put(key, MutableLiveData());
        return getLiveData(key)
    }

    fun getLiveData(key: String): MutableLiveData<LiveDataResult<BaseModel<Any>>> {
        return hashMap[key]!!
    }

    fun livedata(lifecycleOwner: LifecycleOwner,iObserverCallBack: IObserverCallBack,key: String): MutableLiveData<LiveDataResult<BaseModel<Any>>>{
        addLiveData(key).observe(
            lifecycleOwner,
            ObserverHelper(iObserverCallBack,key).baseObserver
        )
        return getLiveData(key)
    }
}