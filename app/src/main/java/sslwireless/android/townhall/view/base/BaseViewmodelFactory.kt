package sslwireless.android.townhall.view.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class BaseViewmodelFactory(
    private val baseViewModel: BaseViewmodel
): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return baseViewModel as T
    }
}