package sslwireless.android.townhall.view.base

import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult


interface IObserverCallBack {
    abstract fun onSuccess(result: LiveDataResult<BaseModel<Any>>,key:String)
    abstract fun onLoading(isLoader: Boolean)
    abstract fun onError(err: Throwable)
}