package sslwireless.android.townhall.view.custom

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet

import androidx.appcompat.widget.AppCompatTextView

import sslwireless.android.townhall.R

class CustomTextView : AppCompatTextView {

    constructor(context: Context) : super(context, null) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet) {
        if (!this.isInEditMode) { // used for preview while designing.
            val a = context.obtainStyledAttributes(attrs, R.styleable.custom)
            val type = a.getInteger(R.styleable.custom_textStyle, 0)
            if (type == 0) {
                UserTypeFace.SetLight(this) //Set Default Font if font is not defined in xml
                return
            }
            setStyle(type)
        } else {
            setTypeface(Typeface.DEFAULT, Typeface.NORMAL)
        }
    }


    private fun setStyle(type: Int) {
        when (type) {
            0 -> UserTypeFace.SetNormal(this)
            1 -> UserTypeFace.SetBold(this)
            2 -> UserTypeFace.SetItalic(this)
            3 -> UserTypeFace.SetLight(this)
            4 -> UserTypeFace.SetMedium(this)
            5 -> UserTypeFace.SetBlack(this)
            6 -> UserTypeFace.SetBlackItalic(this)
            else -> UserTypeFace.SetNormal(this)
        }

    }

    companion object {

        val ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android"
    }
}