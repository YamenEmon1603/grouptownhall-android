/*
package sslwireless.android.easyrevamp.view.custom;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

*/
/**
 * Author: Ferdous
 * Date: Dec 11 2015
 * Time: 22:35
 *//*

public class GridItemDecoration extends RecyclerView.ItemDecoration {

    private int mItemOffset;

    public GridItemDecoration(int itemOffset) {
        mItemOffset = itemOffset;
    }

    public GridItemDecoration(@NonNull Context context, @DimenRes int itemOffsetId) {
        this(context.getResources().getDimensionPixelSize(itemOffsetId));
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.set(mItemOffset, mItemOffset, mItemOffset, mItemOffset);
    }
}
*/
