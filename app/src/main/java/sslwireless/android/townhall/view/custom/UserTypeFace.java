package sslwireless.android.townhall.view.custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Hashtable;

/**
 * Created by ZAHID
 */

public class UserTypeFace {

    public static final String BOLD;
    public static final String LIGHT;
    public static final String NORMAL;
    public static final String ITALIC;
    public static final String MEDIUM;
    public static final String BLACK;
    public static final String BLACKITALIC;
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    static {
        NORMAL = "fonts/customfont/roboto_regular.ttf";
        LIGHT = "fonts/customfont/roboto_light.ttf";
        BOLD = "fonts/customfont/roboto_bold.ttf";
        MEDIUM = "fonts/customfont/roboto_medium.ttf";
        ITALIC = "fonts/customfont/roboto_italic.ttf";
        BLACK = "fonts/customfont/roboto_black.ttf";
        BLACKITALIC = "fonts/customfont/roboto_black_italic.ttf";
    }

    private static Typeface getTypeFace(Context context, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface typeFace = Typeface.createFromAsset(
                            context.getAssets(), assetPath);
                    cache.put(assetPath, typeFace);
                } catch (Exception e) {
                    Log.e("TypeFaces", "Typeface not loaded.");
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }

    public static void SetLight(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), LIGHT), Typeface.NORMAL);
    }

    public static void SetBold(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), BOLD), Typeface.BOLD);
    }

    public static void SetItalic(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), ITALIC), Typeface.ITALIC);
    }

    public static void SetNormal(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), NORMAL), Typeface.NORMAL);
    }

    public static void SetMedium(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), MEDIUM), Typeface.BOLD);
    }

    public static void SetBlack(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), BLACK), Typeface.NORMAL);
    }

    public static void SetBlackItalic(TextView obj) {
        obj.setTypeface(getTypeFace(obj.getContext(), BLACKITALIC), Typeface.ITALIC);
    }

    public static Typeface getNormal(View obj) {
        return getTypeFace(obj.getContext(), NORMAL);
    }

}