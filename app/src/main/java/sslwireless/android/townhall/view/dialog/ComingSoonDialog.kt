package sslwireless.android.townhall.view.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.ComingSoonDialogLayoutBinding
import kotlinx.android.synthetic.main.coming_soon_dialog_layout.*

class ComingSoonDialog() :
    DialogFragment() {

    private lateinit var bind: ComingSoonDialogLayoutBinding
    private val layoutRes: Int = R.layout.coming_soon_dialog_layout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.customDialog2)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        bind.lifecycleOwner = this
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doneButton.setOnClickListener {
            hide()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun show(activity: AppCompatActivity) {
        show(activity.supportFragmentManager, activity.toString())
    }

    fun show(fragment: Fragment) {
        show(fragment.childFragmentManager, fragment.tag)
    }

    fun hide() {
        dismissAllowingStateLoss()
    }

}