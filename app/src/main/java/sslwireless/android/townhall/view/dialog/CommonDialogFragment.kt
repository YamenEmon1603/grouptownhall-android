package sslwireless.android.townhall.view.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.DialogCommonBinding
import kotlinx.android.synthetic.main.dialog_common.*

class CommonDialogFragment(private val type: String, private val listener: OnItemClickListener) :
    DialogFragment() {

    private lateinit var bind: DialogCommonBinding
    private val layoutRes: Int = R.layout.dialog_common

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.customDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        bind.lifecycleOwner = this
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (type) {
            REDEEM_SUCCESS -> {
                bind.iconImage.setAnimation("new_success_lottie.json")
                bind.favButton.visibility = View.GONE
                bind.headerText.text = getString(R.string.transaction_successful)
                bind.descText.text = String.format(getString(R.string.redeem_successful_desc), 120, "0171750158")
                bind.rateLayout.visibility = View.VISIBLE
            }
        }

        doneButton.setOnClickListener {
            listener.onDoneClick()
        }

        favButton.setOnClickListener {
            listener.onFavClick()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun show(activity: AppCompatActivity) {
        show(activity.supportFragmentManager, activity.toString())
    }

    fun show(fragment: Fragment) {
        show(fragment.childFragmentManager, fragment.tag)
    }

    fun hide() {
        dismissAllowingStateLoss()
    }

    interface OnItemClickListener {
        fun onDoneClick()

        fun onFavClick()
    }

    companion object {
        const val REDEEM_SUCCESS = "REDEEM_SUCCESS"
    }
}