package sslwireless.android.townhall.view.dialog

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.DialogNewUpdateBinding
import sslwireless.android.townhall.view.activity.desco.utils.Enums

class NewUpdateDialog(private val listener: OnNewUpdateItemClickListener) : DialogFragment() {
    private lateinit var bind: DialogNewUpdateBinding
    private val layoutRes: Int = R.layout.dialog_new_update

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.customDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = DataBindingUtil.inflate(inflater, layoutRes, container, false)
        bind.lifecycleOwner = this
        isCancelable = false

        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var serverVersion = requireArguments().getString(Enums.Common.value.name)

        bind.subTitleTV.text =
            getString(R.string.the_version_of_this_application_is_no_longer_supported, BuildConfig.VERSION_NAME, serverVersion)
            //"A new version (v${version}) of this app is available, Please update to get latest features and best experience."

        bind.updateBTN.setOnClickListener {
            var packageName = requireActivity().getPackageName()
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                )
            )
            //hide()
            //listener.onUpdateClick()
        }

        bind.closeTV.setOnClickListener {
            hide()
            listener.onCancelClick()
        }
    }

    fun show(activity: AppCompatActivity) {
        show(activity.supportFragmentManager, activity.toString())
    }

    fun show(fragment: Fragment) {
        show(fragment.childFragmentManager, fragment.tag)
    }

    fun hide() {
        dismissAllowingStateLoss()
    }

    interface OnNewUpdateItemClickListener {
        //fun onUpdateClick()
        fun onCancelClick()
    }
}