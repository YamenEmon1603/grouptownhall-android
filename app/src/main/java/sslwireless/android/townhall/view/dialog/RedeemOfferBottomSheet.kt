package sslwireless.android.townhall.view.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import android.view.WindowManager
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.SampleModel.RedeemPointOffer
import sslwireless.android.townhall.view.adapter.RedeemPointNumberAdapter
import kotlinx.android.synthetic.main.redeem_offer_bottom_sheet_layout.view.*
import kotlinx.android.synthetic.main.redeem_point_offers_item_layout.view.*


/**
 * Created by ruhul on 06,October,2019
 */

class RedeemOfferBottomSheet(redeemPointOffer: RedeemPointOffer) : BottomSheetDialogFragment() {

    private var position: Int = 0
    var listener: IBottomSheetDialogClicked? = null
    var redeemPointOffer = redeemPointOffer

    private val mBottomSheetBehaviorCallback = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onStateChanged(bottomSheet: View, newState: Int) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss()
            }

        }

        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
    }


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)

        val contentView =
            View.inflate(getContext(), R.layout.redeem_offer_bottom_sheet_layout, null)
        dialog.setContentView(contentView)


        dialog.setOnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet =
                d.findViewById(R.id.design_bottom_sheet) as FrameLayout?

            bottomSheet?.let { it ->
                val behaviour = BottomSheetBehavior.from(it)
                setupFullHeight(it)
                behaviour.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }

        contentView.offer_title_tv.text = redeemPointOffer.title
        contentView.offer_validity_tv.text = redeemPointOffer.validity
        contentView.offer_image_iv.setImageResource(redeemPointOffer.image)
        contentView.required_point_tv.text = redeemPointOffer.point


        contentView.redeem_point_number_rec_view.layoutManager = LinearLayoutManager(context)
        contentView.redeem_point_number_rec_view.setHasFixedSize(true)

        var numbers = ArrayList<String>();
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        numbers.add("1")
        var redeemPointNumberAdapter = RedeemPointNumberAdapter(context!!, numbers)
        contentView.redeem_point_number_rec_view.adapter = redeemPointNumberAdapter
        redeemPointNumberAdapter.notifyDataSetChanged()

        contentView.cancel.setOnClickListener(View.OnClickListener {
            listener!!.onCancelButtonClicked()
        })

        contentView.redeem_btn_bottom_sheet.setOnClickListener {
            listener!!.onRedeemButtonClicked()
        }
    }

    fun setBottomDialogListener(listener: IBottomSheetDialogClicked) {
        this.listener = listener
    }

    private fun setupFullHeight(bottomSheet: View) {
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        bottomSheet.layoutParams = layoutParams
    }

    interface IBottomSheetDialogClicked {
        fun onCancelButtonClicked()
        fun onRedeemButtonClicked()
    }

}