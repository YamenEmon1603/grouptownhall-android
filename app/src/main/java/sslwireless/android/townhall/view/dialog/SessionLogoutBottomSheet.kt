package sslwireless.android.townhall.view.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sslwireless.android.townhall.R
import kotlinx.android.synthetic.main.session_logout_bottom_sheet_layout.view.*
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible

/**
 * Created by ruhul on 06,October,2019
 */

class SessionLogoutBottomSheet() : BottomSheetDialogFragment() {

    var listener: IBottomSheetDialogClicked? = null


    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.session_logout_bottom_sheet_layout, null)
        dialog.setContentView(contentView)
        val title = requireArguments().getString("title","")
        val subText = requireArguments().getString("subText","")
        val btn1 = requireArguments().getString("btn1","")
        val btn2 = requireArguments().getString("btn2","")
        val tag = requireArguments().getString("tag","")

        if (subText.isEmpty()) {
            contentView.offer_title_tv_bottom_sheet.makeGone()
        } else {
            contentView.offer_title_tv_bottom_sheet.makeVisible()
        }
        contentView.customTextView54.text = title
        contentView.offer_title_tv_bottom_sheet.text = subText
        contentView.cancel_btn.text = btn1
        contentView.session_logout_btn.text = btn2

        contentView.cancel_btn.setOnClickListener {
            dismiss()
            listener!!.onCancelButtonClicked()
        }

        contentView.session_logout_btn.setOnClickListener {
            dismiss()
            listener!!.onSessionLogoutButtonClicked(tag)
        }


    }


    fun setBottomDialogListener(listener: IBottomSheetDialogClicked) {
        this.listener = listener
    }

    interface IBottomSheetDialogClicked {
        fun onCancelButtonClicked()

        fun onSessionLogoutButtonClicked(tag:String)
    }

}