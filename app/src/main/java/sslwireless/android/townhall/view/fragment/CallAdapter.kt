package sslwireless.android.townhall.view.fragment

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.databinding.HelpCallItemLayoutBinding

class CallAdapter(
    private val list: ArrayList<String>,private val listener:(String)->Unit) :
    RecyclerView.Adapter<CallAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.help_call_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val item = list[position]
        holder.binding.numberText.text = item
        holder.binding.root.setOnClickListener {
            listener(item)
        }
    }


    override fun getItemCount(): Int = list.size


    class ViewHolder(val binding: HelpCallItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }
}