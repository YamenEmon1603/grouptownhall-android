package sslwireless.android.townhall.view.fragment.Corporate

import android.content.Context
import androidx.databinding.ViewDataBinding
import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.databinding.StoryListItemsBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2

class AllStoreViewholder (itemView: ViewDataBinding, context: Context) : BaseViewHolder2(itemView.root) {

    var binding = itemView as StoryListItemsBinding
    var mContext: Context = context
    override fun<T> onBind(position: Int,itemModel : T, listener: IAdapterListener) {
        itemModel as String

        binding.root.setOnClickListener {
            listener.clickListener(position,itemModel,binding.root)
            binding.root.isEnabled =  false
        }

    }
}