package sslwireless.android.townhall.view.fragment.Corporate

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import sslwireless.android.townhall.view.IAdapterListener

import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.adapter.CorporateAdapters.AllStoriesAdapter
import sslwireless.android.townhall.view.base.BaseRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseViewHolder2
import kotlinx.android.synthetic.main.fragment_all_stories.view.*


class AllStores : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_all_stories, container, false)

        val storetList  = ArrayList<String>()
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")
        storetList.add("Open")

        val adapter = AllStoriesAdapter(activity!!,storetList)
        view.storeList.layoutManager =  GridLayoutManager(this!!.activity!!,1)

//        view.storeList.adapter = adapter
        view.storeList.adapter = BaseRecyclerAdapter(activity,object : IAdapterListener {
            override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {
                return AllStoreViewholder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context)
                        ,R.layout.story_list_items
                        ,parent,false)

                    ,this@AllStores.context!!)

            }
            override fun <T> clickListener(position: Int, model: T, view: View) {
            }
        }, storetList)


        view.storeList.adapter!!.notifyDataSetChanged()
        return view
    }

}
