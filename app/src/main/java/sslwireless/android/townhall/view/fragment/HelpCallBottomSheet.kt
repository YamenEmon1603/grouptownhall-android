package sslwireless.android.townhall.view.fragment


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.help_call_bottom_sheet.*
import sslwireless.android.townhall.R


class HelpCallBottomSheet(private val phoneList: ArrayList<String>) : BottomSheetDialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.help_call_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerData.adapter = CallAdapter(list = phoneList, listener = {
            dismiss()
            val intent = Intent(
                Intent.ACTION_DIAL,
                Uri.fromParts("tel", it, null)
            )
            startActivity(intent)
        })
    }

    /* override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setStyle(DialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialog)
     }*/


}
