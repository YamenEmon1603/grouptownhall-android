package sslwireless.android.townhall.view.fragment


import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.BillingAccount.BillingAccountActivity
import sslwireless.android.townhall.view.activity.bill_alert.BillingAlertsActivity
import sslwireless.android.townhall.view.activity.profile.ProfileActivity
import sslwireless.android.townhall.view.activity.recurring_payment.RecurringPaymentsActivity
import sslwireless.android.townhall.view.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_more.*
import kotlinx.android.synthetic.main.fragment_more.change_password_layout
import kotlinx.android.synthetic.main.fragment_more.where_your_logged_in_layout
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.view.activity.ChangePassword.ChangePasswordActivity
import sslwireless.android.townhall.view.activity.ShowQRActivity
import sslwireless.android.townhall.view.activity.programScheduling.ScheduleActivity
import sslwireless.android.townhall.view.activity.uploadPicture.NumberEntryActivity
import sslwireless.android.townhall.view.activity.venue.VenueActivity
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.dialog.SessionLogoutBottomSheet
import sslwireless.android.townhall.view.utils.*


class MoreFragment : BaseFragment(), SessionLogoutBottomSheet.IBottomSheetDialogClicked {

    lateinit var userData: UserData
    private val cViewModel by activityViewModels<CommunicationViewModel>()
    private val viewModel: MoreViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(MoreViewModel(getDataManager())))
            .get(MoreViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_more, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = requireActivity().window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.faded_black)
        }
    }


    override fun viewRelatedTask() {
        cViewModel.townHallResponse.observe(this, Observer {
            it ?: return@Observer

            loadCurrentUserDataOnUI(it)
        })

        initializeViewClickListeners()
    }

    override fun onResume() {
        super.onResume()
        loadCurrentUserDataOnUI()
    }

    private fun initializeViewClickListeners() {
        profile_info_layout_cl.setOnClickListener {
            val intent = Intent(activity, ProfileActivity::class.java)
            requireActivity().startActivity(intent)

        }

        recurring_payments_menu.setOnClickListener {
            requireActivity().startActivity(Intent(activity, RecurringPaymentsActivity::class.java))
        }
        billing_alert_and_notification_menu.setOnClickListener {
            requireActivity().startActivity(Intent(activity, BillingAlertsActivity::class.java))
        }

        billing_accounts_menu.setOnClickListener {
            requireActivity().startActivity(Intent(activity, BillingAccountActivity::class.java))
        }

        recharge_request_menu_layout.setOnClickListener {
            requireActivity().startActivity(
                Intent(
                    activity,
                    ScheduleActivity::class.java
                )
            )
        }

        /*if (getDataManager().mPref.prefGetCurrentUser().qr_pin) {
            change_pin_layout.makeVisible()
            div.makeVisible()
        } else {
            change_pin_layout.makeGone()
            div.makeGone()
        }*/


        change_password_layout.setOnClickListener {
            startActivity(
                Intent(
                    requireActivity(),
                    ChangePasswordActivity::class.java
                ).apply {
                    putExtra(
                        ChangePasswordActivity.FROM_ACTION,
                        ChangePasswordActivity.FROM_CHANGE_PASS
                    )
                })
        }


        where_your_logged_in_layout.setOnClickListener {
            startActivity(
                Intent(requireActivity(), NumberEntryActivity::class.java)
            )
        }

        help_and_support_menu.setOnClickListener {
            val contentData = getDataManager().mPref.prefGetTownhallKey()
            contentData?.let {
                HelpCallBottomSheet((it.townhall?.phone as ArrayList<String>)).show(
                    childFragmentManager,
                    "asdasd"
                )
            }
            //requireActivity().startActivity(Intent(activity, HelpAndSupportActivity::class.java))
        }

        show_qr_layout.setOnClickListener {
            startActivity(
                Intent(requireActivity(), ShowQRActivity::class.java)
            )
        }

        logout_menu.setOnClickListener {
            val con = requireContext().getLocale(getDataManager().mPref.prefGetLanguage())
            val bundle = Bundle()
            bundle.putString("title", con.resources.getString(R.string.sign_out_main_title))
            bundle.putString("subText", "")
            bundle.putString("btn1", con.resources.getString(R.string.cancel))
            bundle.putString("btn2", con.resources.getString(R.string.log_out))
            val sessionLogoutBottomSheet = SessionLogoutBottomSheet()
            sessionLogoutBottomSheet.arguments = bundle
            sessionLogoutBottomSheet.show(
                requireActivity().supportFragmentManager,
                sessionLogoutBottomSheet.tag
            )
            sessionLogoutBottomSheet.setBottomDialogListener(this)
        }

        favorite_contacts_menu.setOnClickListener {
            val intent = Intent(context, VenueActivity::class.java)
            requireActivity().startActivity(intent)
        }

    }


    private fun loadCurrentUserDataOnUI() {
        userData = getDataManager().mPref.prefGetCurrentUser()
        user_name_tv.text = userData.name
        mbl.text = userData.mobile
        profile_image_iv.loadImageProfile(userData.profile_picture_url)
        val contentData = getDataManager().mPref.prefGetTownhallKey()
        contentData?.let {
            val phones = it.townhall?.phone ?: arrayListOf()
            phoneNumbers.text = phones.toString().replace("[", "").replace("]", "")
        }
    }

    private fun loadCurrentUserDataOnUI(townHallResponse: TownHallResponse) {
        userData = getDataManager().mPref.prefGetCurrentUser()
        user_name_tv.text = userData.name
        mbl.text = userData.mobile
        profile_image_iv.loadImageProfile(userData.profile_picture_url)


        townHallResponse.let {
            val phones = it.townhall?.phone ?: arrayListOf()
            phoneNumbers.text = phones.toString().replace("[", "").replace("]", "")
        }
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data?.code == 200 || result.data?.code == 401) {
            getDataManager().mPref.prefLogout(requireActivity())
        } else {
            showToast(
                requireContext(),
                result.data?.message ?: getString(R.string.something_went_wrong)
            )
        }
    }

    override fun onCancelButtonClicked() {
        // sessionLogoutBottomSheet.dismiss()
    }

    override fun onSessionLogoutButtonClicked(tag: String) {
        //sessionLogoutBottomSheet.dismiss()
        viewModel.apiLogout(this, this)
    }
}
