package sslwireless.android.townhall.view.fragment

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class MoreViewModel @Inject constructor(private val dataManager: DataManager) : BaseViewmodel(dataManager) {

    fun apiLogout(
            lifecycleOwner: LifecycleOwner,
            iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiLogoutSingleDevice(
                dataManager.mPref.prefGetCustomerToken()?:"",
                dataManager.mPref.prefGetDeviceKey()?.deviceKey?:"",
                ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "logout_single_device"))
        )
    }
}