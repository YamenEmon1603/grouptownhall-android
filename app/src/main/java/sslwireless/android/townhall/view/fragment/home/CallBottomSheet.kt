package sslwireless.android.townhall.view.fragment.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.call_bottom_sheet.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.townhall.Bot
import sslwireless.android.townhall.view.utils.openSocialUrl
import sslwireless.android.townhall.view.utils.openTelegram

class CallBottomSheet(private val dataManager: DataManager, private val bot: Bot) :
    BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.call_bottom_sheet, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cardLayout1.setOnClickListener {
            val isOpened = requireContext().openSocialUrl(bot.whatsapp)

            if (isOpened) {
                dataManager.mPref.prefSetIsBotOpened(isOpened)
            }

            dismiss()
        }

        cardLayout2.setOnClickListener {
            val isOpened = requireContext().openSocialUrl(bot.messenger)

            if (isOpened) {
                dataManager.mPref.prefSetIsBotOpened(isOpened)
            }

            dismiss()

        }

        cardLayout3.setOnClickListener {
            val isOpened = requireContext().openTelegram(bot.telegram)

            if (isOpened) {
                dataManager.mPref.prefSetIsBotOpened(isOpened)
            }

            dismiss()
        }
    }
}
