package sslwireless.android.townhall.view.fragment.home


import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import sslwireless.android.townhall.view.IAdapterListener
import kotlinx.android.synthetic.main.activity_main.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.offer.ExclusiveOffer
import sslwireless.android.townhall.data.model.offer.OfferResponse
import sslwireless.android.townhall.data.model.utility.ServiceListsItem
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.TopUp.RechargeActivity
import sslwireless.android.townhall.view.activity.UtilityBillsActivity
import sslwireless.android.townhall.view.activity.notification.NotificationsActivity
import sslwireless.android.townhall.view.activity.otherServices.OtherServicesViewModel
import sslwireless.android.townhall.view.activity.otherServices.dynamic_services_form_box.DynamicCommonApiActivity
import sslwireless.android.townhall.view.activity.profile.ProfileActivity
import sslwireless.android.townhall.view.adapter.BillPaymentAdapter
import sslwireless.android.townhall.view.adapter.ExtraCategoryLayoutRecyclerAdpter
import sslwireless.android.townhall.view.adapter.MyOffersRecyclerAdapter
import sslwireless.android.townhall.view.base.*
import sslwireless.android.townhall.view.dialog.ComingSoonDialog
import sslwireless.android.townhall.view.viewpager.OffersViewHolder
import kotlinx.android.synthetic.main.fragment_home.*
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.view.utils.*


class HomeFragment : BaseFragment(), BillPaymentAdapter.ClickListener,
    ExtraCategoryLayoutRecyclerAdpter.ClickListener, IObserverCallBack {

    private lateinit var homeViewModel: HomeViewModel
    var offerResponse: ArrayList<OfferResponse> = ArrayList()
    var exclusiveOffers: ArrayList<ExclusiveOffer> = ArrayList()
    //private val comingSoonDialog: ComingSoonDialog by lazy { ComingSoonDialog() }
    private lateinit var viewModel: OtherServicesViewModel

    private lateinit var mContext: Context

    private lateinit var offerAdapter: MyOffersRecyclerAdapter
    private lateinit var extraCategory: ExtraCategoryLayoutRecyclerAdpter
    private var flag: Boolean = false
    private var flag2: Boolean = false

    override fun itemTopUpClicked(v: View?, position: Int, tag: String, mTaskInfo: IntArray) {
        if (position == 0) {
            (activity as MainActivity).startActivity(Intent(activity, RechargeActivity::class.java))
        }
    }

    override fun itemClicked(
        v: View?,
        position: Int,
        tag: String,
        mTaskInfo: List<ServiceListsItem?>?
    ) {

        val intent =
            Intent(mContext, DynamicCommonApiActivity::class.java)
        intent.putExtra("modelData", mTaskInfo!![position])
        startActivity(intent)
    }


    override fun viewRelatedTask() {
        homeViewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(HomeViewModel(getDataManager())))
                .get(HomeViewModel::class.java)
        homeViewModel.getOffers(this, this)
        homeViewModel.getProfile(this,this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = requireActivity().applicationContext
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = requireActivity().window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.qr_color)
        }

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(OtherServicesViewModel(getDataManager()))
            )
                .get(OtherServicesViewModel::class.java)

        layoutTopup.setOnClickListener {
            (activity as MainActivity).startActivity(Intent(activity, RechargeActivity::class.java))
        }

        layoutUtility.setOnClickListener {
            val intent = Intent(mContext, UtilityBillsActivity::class.java)
            startActivity(intent)
        }

        verification_capture_image.setOnClickListener {
            val intent = Intent(mContext, ProfileActivity::class.java)
            startActivity(intent)
        }

        layoutRewardPoints.setOnClickListener {
            val comingSoonDialog= ComingSoonDialog()
            comingSoonDialog.show(this)
        }
        layoutMoreServices.setOnClickListener {
            val comingSoonDialog= ComingSoonDialog()
            comingSoonDialog.show(this)
        }

       /* viewAll.setOnClickListener {
            (activity as MainActivity).startActivity(
                Intent(
                    activity,
                    OffersActivity::class.java
                ).putExtra("flag", "1")
            )
        }*/

        viewAll.setOnClickListener {
            (activity as MainActivity).bottomNavigation.selectedItemId = R.id.menu_offer
            /*(activity as MainActivity).startActivity(
                Intent(
                    activity,
                    OffersActivity::class.java
                ).putExtra("flag", "1")
            )*/
        }

        notification_icon_iv.setOnClickListener {
            requireActivity().startActivity(Intent(activity, NotificationsActivity::class.java))
        }

        search_service_btn.setOnClickListener {
            //requireActivity().startActivity(Intent(activity, SearchActivity::class.java))
        }

        val userData = getDataManager().mPref.prefGetCurrentUser()
        verification_capture_image.loadImageProfile(userData.profile_picture_url)
        tvName.text = userData.name
    }

    private fun initSubCategoryRecycler() {
        myOffersRecycler.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        myOffersRecycler.adapter = BaseRecyclerAdapter(activity, object : IAdapterListener {
            override fun <T> clickListener(position: Int, model: T, view: View) {
                model as ExclusiveOffer

                //val intent = Intent(this@SearchActivity, ProductDetailsActivity::class.java)
                // intent.putExtra("sku", model.sku)
                // intent.putExtra(Enums.Common.source.name, "Search")
                // startActivityForResult(intent, Enums.Common.viewPager.ordinal)
            }

            override fun getViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder2 {

                return OffersViewHolder(
                    DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.list_item_offers_new,
                        parent,
                        false
                    ), getDataManager().mPref.prefGetLanguage()
                )
            }

        }, exclusiveOffers as ArrayList)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (result.data?.code) {
            200 -> {
                when (key) {
                    "offers" -> {
                        offerResponse =
                            result.data?.data.fromResponse<List<OfferResponse>>() as ArrayList<OfferResponse>
                        if (offerResponse.isEmpty()) {
                            viewAll.setColor(R.color.bluey_grey)
                            viewAll.isClickable = false
                            offerGroup.makeVisible()
                            return
                        } else {
                            viewAll.setColor(R.color.colorAccent)
                            viewAll.isClickable = true
                            offerGroup.makeGone()
                        }
                        offerResponse.map { it.exclusive_offers.firstOrNull() }.forEach {
                            it?.let { exclusiveOffers.add(it) }
                        }
                        initSubCategoryRecycler()
                    }
                    "profile"->{
                        val newData = result.data.data?.fromResponse<UserData>()
                        getDataManager().mPref.prefUserData(newData!!)
                        verification_capture_image.loadImageProfile(newData.profile_picture_url)
                        tvName.text = newData.name
                    }
                }
            }
            401 -> {
                showToast(mContext,result.data.message)
                getDataManager().mPref.prefLogout(requireActivity())
            }
            else -> {
                showToast(mContext,result.data!!.message)
            }
        }
    }



    override fun onLoading(isLoader: Boolean) {
    }

    override fun onError(err: Throwable) {

    }

}
