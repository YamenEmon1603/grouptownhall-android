package sslwireless.android.townhall.view.fragment.home


import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.fragment_home_new.*
import org.joda.time.DateTime
import org.joda.time.Duration
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.data.model.user.UserData
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.MainActivity
import sslwireless.android.townhall.view.activity.experience_zone.ExperienceZoneActivity
import sslwireless.android.townhall.view.activity.game.GameQRActivity
import sslwireless.android.townhall.view.activity.instructions.InstructionsActivity
import sslwireless.android.townhall.view.activity.notification.NotificationsActivity
import sslwireless.android.townhall.view.activity.otherServices.OtherServicesViewModel
import sslwireless.android.townhall.view.activity.profile.ProfileActivity
import sslwireless.android.townhall.view.base.BaseActivity
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.CommunicationViewModel
import sslwireless.android.townhall.view.utils.dateChangeTo
import sslwireless.android.townhall.view.utils.fromResponse
import sslwireless.android.townhall.view.utils.loadImageProfile
import java.util.concurrent.TimeUnit


class HomeFragmentNew : BaseFragment() {
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var viewModel: OtherServicesViewModel
    private lateinit var mContext: Context
    private var countDownTimer: CountDownTimer? = null
    private val cViewModel: CommunicationViewModel by activityViewModels()
    private var isLoading = false

    override fun viewRelatedTask() {
        isLoading = false

        homeViewModel =
            ViewModelProviders.of(this, BaseViewmodelFactory(HomeViewModel(getDataManager())))
                .get(HomeViewModel::class.java)

        loadData()

        swipeRefresh.setOnRefreshListener {
            swipeRefresh.isRefreshing = false

            if (!isLoading) {
                loadData()
            }
        }

        //textTop.spanText(getString(R.string.explore_easy_services_n_amp_make_life_easy),AppConstants.Language.CURRENT_LANGUAGE)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(OtherServicesViewModel(getDataManager()))
            )
                .get(OtherServicesViewModel::class.java)

        notification_icon_iv.setOnClickListener {
            requireActivity().startActivity(Intent(activity, NotificationsActivity::class.java))
        }

        instructionLayout.setOnClickListener {
            (activity as MainActivity).startActivity(
                Intent(
                    activity,
                    InstructionsActivity::class.java
                )
            )
        }

        connectLayout.setOnClickListener {
            /* val intent = Intent(mContext, UtilityBillsActivity::class.java)
             startActivity(intent)*/
            val bot = getDataManager().mPref.prefGetTownhallKey()?.townhall?.bot
            bot?.let {
                CallBottomSheet(getDataManager(), it).show(childFragmentManager, "123456")
            }
        }

        playGameLayout.setOnClickListener {
            startActivity(
                Intent(
                    requireContext(),
                    GameQRActivity::class.java
                )
            )
        }

        verification_capture_image.setOnClickListener {
            val intent = Intent(mContext, ProfileActivity::class.java)
            startActivity(intent)
        }

        topUpOffer.setOnClickListener {
            val intent = Intent(mContext, ExperienceZoneActivity::class.java)
            startActivity(intent)
        }

        /*cViewModel.onHomeScreenShow.observe(viewLifecycleOwner, Observer {
            if (it) {
                //homeViewModel.getProfile(this, this)
            }
        })*/
    }

    private fun loadData() {
        homeViewModel.getProfile(this, this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mContext = requireActivity().applicationContext
        return inflater.inflate(R.layout.fragment_home_new, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = requireActivity().window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.home_gradient_start)
        }
    }

    override fun onResume() {
        super.onResume()
        verification_capture_image.loadImageProfile(getDataManager().mPref.prefGetCurrentUser().profile_picture_url)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (result.data?.code) {
            200 -> {
                when (key) {
                    "profile" -> {
                        val newData = result.data.data?.fromResponse<UserData>()
                        getDataManager().mPref.prefUserData(newData!!)
                        verification_capture_image.loadImageProfile(newData.profile_picture_url)

                        homeViewModel.getTownhallContent(this, this)
                    }
                    "townhall_content" -> {
                        try {
                            val townHallResponse =
                                result.data.data?.fromResponse<TownHallResponse>()
                                    ?: throw Exception()

                            handleResponse(townHallResponse)

                        } catch (e: Exception) {
                            e.printStackTrace()
                            showToast(requireContext(), "Something went wrong!")
                        }

                    }
                }
            }
            401 -> {
                showToast(mContext, result.data.message)
                getDataManager().mPref.prefLogout(requireActivity())
            }
            else -> {
                showToast(mContext, result.data!!.message)
            }
        }
    }

    private fun handleResponse(townHallResponse: TownHallResponse) {
        getDataManager().mPref.prefSetTownHallResponseKey(townHallResponse)

        cViewModel.townHallResponse.value = townHallResponse

        showHomeData(townHallResponse)

    }

    override fun onLoading(isLoader: Boolean) {
        super.onLoading(isLoader)

        isLoading = isLoader
    }

    override fun onError(err: Throwable) {
        super.onError(err)

        showToast(requireContext(), "Please swipe to reload!")
    }

    override fun onDestroy() {
        super.onDestroy()
        countDownTimer?.onFinish()
    }

    private fun showHomeData(townHallResponse: TownHallResponse) {
        if (townHallResponse.imageUploadStatus == 0) {
            (activity as BaseActivity).gotoUploadPicture(
                activity ?: return,
                getDataManager().mPref.prefGetCurrentUser().mobile
            )
        }

        val dates =
            townHallResponse.townhall?.date.dateChangeTo("dd MMMM yyyy", "dd")
        val months =
            townHallResponse.townhall?.date.dateChangeTo("dd MMMM yyyy", "MM")
        val month2 =
            townHallResponse.townhall?.date.dateChangeTo("dd MMMM yyyy", "MMM")
        val dayOfTheWeeks =
            townHallResponse.townhall?.date.dateChangeTo("dd MMMM yyyy", "EEEE")
        val years =
            townHallResponse.townhall?.date.dateChangeTo("dd MMMM yyyy", "yyyy")

        val times = townHallResponse.townhall?.time?.split("-")?.get(0)

        val hours = times.dateChangeTo("hh:mma", "HH")
        val mins = times.dateChangeTo("hh:mma", "mm")

        title.text = townHallResponse.townhall?.title
        month.text = month2
        eventDate.text = dates
        dayOfTheWeek.text = dayOfTheWeeks
        venue.text = "Venue: " + townHallResponse.townhall?.vanue

        //val end = DateTime(year.toInt(), month.toInt(), date.toInt(), hours.toInt(), mins.toInt(), 0, 0)

        val today = DateTime.now()
        val end = DateTime(
            years.toInt(),
            months.toInt(),
            dates.toInt(),
            hours.toInt(),
            mins.toInt(),
            0,
            0
        )
        //val oks = Days.daysBetween(today.toLocalDate(), end.toLocalDate())
        val oks = Duration(today, end).millis
        object : CountDownTimer(oks, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    val millis: Long = millisUntilFinished
                    day.text = "" + TimeUnit.MILLISECONDS.toDays(millis)
                    val h =
                        TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(
                            TimeUnit.MILLISECONDS.toDays(millis)
                        )
                    if (h < 10) {
                        hour.text = "0$h"
                    } else {
                        hour.text = "$h"
                    }

                    val m =
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(
                            TimeUnit.MILLISECONDS.toHours(millis)
                        )
                    if (m < 10) {
                        min.text = "0$m"
                    } else {
                        min.text = "$m"
                    }
                    val s =
                        (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(
                            TimeUnit.MILLISECONDS.toMinutes(millis)
                        ))

                    if (s < 10) {
                        sec.text = "0$s"
                    } else {
                        sec.text = "$s"
                    }
                } catch (e: Exception) {

                }
            }

            override fun onFinish() {

            }

        }.start()
    }
}
