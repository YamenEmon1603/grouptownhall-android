package sslwireless.android.townhall.view.fragment.home

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class HomeViewModel @Inject constructor(private val dataManager: DataManager) : BaseViewmodel(dataManager) {

    fun getOffers(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getOffers(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "offers"))
        )
    }

    fun getProfile(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getProfile(dataManager.mPref.prefGetCustomerToken()?:"",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "profile"))
        )
    }

    fun getTownhallContent(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getTownhallContent(dataManager.mPref.prefGetCustomerToken()?:"","1",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "townhall_content"))
        )
    }
}