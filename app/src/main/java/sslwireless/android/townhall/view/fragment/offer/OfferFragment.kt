package sslwireless.android.townhall.view.fragment.offer


import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.FilterModel
import sslwireless.android.townhall.data.model.offer.ExclusiveOffer
import sslwireless.android.townhall.data.model.offer.OfferResponse
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.offer.OfferBottomSheet
import sslwireless.android.townhall.view.adapter.MyOffersRecyclerAdapter
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import kotlinx.android.synthetic.main.fragment_offer.*
import sslwireless.android.townhall.view.utils.*

class OfferFragment : BaseFragment(), OfferBottomSheet.OfferFilterListener,
    SwipeRefreshLayout.OnRefreshListener {

    private val offerViewModel: OfferViewModel by lazy {
        ViewModelProviders.of(this, BaseViewmodelFactory(OfferViewModel(getDataManager())))
            .get(OfferViewModel::class.java)
    }
    private var offerResponse: ArrayList<OfferResponse> = ArrayList()
    private var exclusiveOffers: ArrayList<ExclusiveOffer> = ArrayList()
    private val categoryList = ArrayList<FilterModel>()
    private val offerAdapter: MyOffersRecyclerAdapter by lazy {
        MyOffersRecyclerAdapter(exclusiveOffers, getDataManager().mPref.prefGetLanguage())
    }
    private val lang by lazy { getDataManager().mPref.prefGetLanguage() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_offer, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (Build.VERSION.SDK_INT >= 21) {
            val window: Window = requireActivity().window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = resources.getColor(R.color.faded_black)
        }
    }

    override fun viewRelatedTask() {
        swipeRefresh.setOnRefreshListener(this)
        swipeRefresh.setColorSchemeResources(R.color.colorAccent)
        onRefresh()
        filterGroup.setOnClickListener {
            val bottomSheet = OfferBottomSheet(this).apply {
                arguments = Bundle().apply {
                    putSerializable("categoryList", categoryList)
                    putString("language", getDataManager().mPref.prefGetLanguage())
                }
            }
            bottomSheet.show(requireActivity().supportFragmentManager, "12348")
        }

    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        if (result.data!!.code == 200) {
            if (key == "offers") {
                offerResponse =
                    result.data.data.fromResponse<List<OfferResponse>>() as ArrayList<OfferResponse>
                if (offerResponse.isEmpty()) {
                    offerGroup.makeGone()
                    noOfferText.makeVisible()
                    return
                }

                exclusiveOffers.addAll(offerResponse.flatMap { it.exclusive_offers })
                offerRecycler.apply {
                    layoutManager =
                        LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
                    adapter = offerAdapter
                }

                categoryList.clear()
                offerResponse.map {
                    if (lang == AppConstants.Language.ENGLISH)
                        it.title else it.title_bn
                }
                    .forEach {
                        categoryList.add(FilterModel(it))
                    }
            }
        } else {
            requireContext().showErrorToast(result.data.message)
        }
    }

    override fun onPause() {
        super.onPause()
        super.onLoading(false)
    }

    override fun onLoading(isLoader: Boolean) {
        swipeRefresh.isRefreshing = isLoader
    }


    override fun onApplyFilter(sortBy: String, filterCategoryList: List<String>) {
        exclusiveOffers.clear()
        if (filterCategoryList.isEmpty()) {
            exclusiveOffers.addAll(offerResponse.flatMap { it.exclusive_offers })
        } else {
            exclusiveOffers.addAll(offerResponse.filter {
                filterCategoryList
                    .contains(if (lang == AppConstants.Language.ENGLISH) it.title else it.title_bn)
            }
                .flatMap { it.exclusive_offers })
        }
        offerAdapter.notifyDataSetChanged()
    }

    override fun onRefresh() {
        offerViewModel.getOffers(this, this)
    }
}
