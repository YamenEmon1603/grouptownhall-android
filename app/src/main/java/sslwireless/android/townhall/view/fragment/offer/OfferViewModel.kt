package sslwireless.android.townhall.view.fragment.offer

import androidx.lifecycle.LifecycleOwner
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import javax.inject.Inject

class OfferViewModel @Inject constructor(dataManager: DataManager) : BaseViewmodel(dataManager) {
    val dataManager = dataManager

    fun getOffers(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.getOffers(
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "offers"))
        )
    }
}