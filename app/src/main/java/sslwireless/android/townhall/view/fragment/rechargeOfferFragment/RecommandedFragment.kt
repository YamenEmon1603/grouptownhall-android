package sslwireless.android.townhall.view.fragment.rechargeOfferFragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.gson.Gson
import sslwireless.android.townhall.R
import sslwireless.android.townhall.view.adapter.MostPopularOffersRecyclerAdapter
import sslwireless.android.townhall.view.utils.fromJson
import kotlinx.android.synthetic.main.fragment_recommanded.*
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.activity.rechargeOffer.SortType
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.utils.CommunicationViewModel
import sslwireless.android.townhall.view.utils.makeGone
import sslwireless.android.townhall.view.utils.makeVisible

class RecommandedFragment() : BaseFragment() {
    private val viewModel by activityViewModels<CommunicationViewModel>()
    private val planList = ArrayList<PopularPlan>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_recommanded, container, false)
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {

    }

    override fun viewRelatedTask() {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val planListString = arguments?.getString("planList")
        planList.clear()
        planListString?.let {
            planList.addAll(Gson().fromJson(it))
            if (planListString.isNotEmpty()) {
                noOffer.makeGone()
            } else {
                noOffer.makeVisible()
            }
            rechargeOfferRecycler.adapter =
                MostPopularOffersRecyclerAdapter(planList, viewModel)

        }

        viewModel.sortingLiveData.observe(viewLifecycleOwner, Observer {
            val filterList = when (it) {
                SortType.POPULAR -> planList.filter { item -> item.is_popular == 1 }
                SortType.MAXIMUM_AMOUNT -> planList.sortedByDescending { item -> item.amount_max }.toList()
                else -> planList
            }
            if (filterList.isNotEmpty()) {
                noOffer.makeGone()
            } else {
                noOffer.makeVisible()
            }

            rechargeOfferRecycler.adapter = MostPopularOffersRecyclerAdapter(
                filterList as ArrayList<PopularPlan>,
                viewModel
            )

        })

    }
}
