package sslwireless.android.townhall.view.fragment.transactionHistory

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_last_week_transaction_history.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.data.model.transaction.GraphData
import sslwireless.android.townhall.data.model.transaction.Transaction
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import sslwireless.android.townhall.view.adapter.TransactionCategoryAdapter
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*
import java.io.IOException
import java.util.*


class LastWeekTransactionHistoryFragment(private var listener: OnLastWeekFragmentInteractionListener?) :
    BaseFragment(),
    SwipeRefreshLayout.OnRefreshListener, IAdapterCallback {

    private lateinit var categoryAdapter: TransactionCategoryAdapter
    private lateinit var viewModel: TransactionHistoryViewModel
    private var date: Date? = null
    private var isMonthWise = false
    private var isWeek = false
    private val mDisposable = CompositeDisposable()
    private lateinit var toDate: String
    private lateinit var fromDate: String
    private var isFilter=false
    private val transactionAdapter by lazy {
        TransactionAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel =
            ViewModelProviders.of(
                this,
                BaseViewmodelFactory(TransactionHistoryViewModel(getDataManager()))
            ).get(TransactionHistoryViewModel::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_last_week_transaction_history, container, false)
    }

    @ExperimentalCoroutinesApi
    override fun viewRelatedTask() {
        swipeRefresh.setOnRefreshListener(this)
        swipeRefresh.setColorSchemeResources(R.color.colorAccent)
        rootLayout.makeVisible()
        isMonthWise = false
        isWeek = false
        isFilter = arguments?.getBoolean("isFilter")?:false
        when (arguments?.getString(PARAM_TAB_TYPE)) {
            getString(R.string.tab_7_days) -> {
                date = DateTime().minusWeeks(1).toDate()
                isWeek = true
                toDate = DateTime().toDate()
                    .formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME)
                    ?: ""
            }
            getString(R.string.tab_30_days) -> {
                date = DateTime().minusMonths(1).toDate()
                toDate = DateTime().toDate()
                    .formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME)
                    ?: ""
            }
            getString(R.string.tab_6_months) -> {
                date = DateTime().minusMonths(6).toDate()
                isMonthWise = true
                toDate = DateTime().toDate()
                    .formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME)
                    ?: ""
            }
            getString(R.string.tab_all_time) -> {
                date = DateTime().minusYears(2).toDate()
                isMonthWise = true
                toDate = DateTime().toDate()
                    .formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME)
                    ?: ""
            }
            else -> {
                val formatter: DateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy HH:mm:ss")
                date = formatter.parseDateTime(arguments?.getString(PARAM_TAB_TYPE) + " 00:00:00")
                    .toDate()
                val date2 =
                    formatter.parseDateTime(arguments?.getString(PARAM_TAB_TYPE) + " 23:59:59")
                        .toDate()
                toDate = date2.formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME)
                    ?: ""
            }
        }

        fromDate = date.formattedDate(AppConstants.DateTimeFormats.TRANSACTION_DATETIME) ?: ""

        transactionCategoryRecycler.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false);
            adapter = transactionAdapter
        }

        onRefresh()

        transactionAdapter.addLoadStateListener { loadState ->
            if (loadState.refresh is LoadState.Loading) {
                onLoading(true)
            } else {
                onLoading(false)
                val error = when {
                    loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                    loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                    loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                    else -> null
                }
                error?.let {
                    if (it.error is IOException) {
                        showToast(requireContext(), "No Internet")
                    } else {
                        showToast(requireContext(), it.error.message.toString())
                    }
                    return@addLoadStateListener
                }
                try {
                    if (transactionAdapter.itemCount == 0) {
                        rootLayout.makeGone()
                        noTransaction.makeVisible()
                    } else {
                        rootLayout.makeVisible()
                        noTransaction.makeGone()
                        totalAmount.text = AppConstants.TransactionAmount.amount.toDouble().toDecimalTwo()
                    }
                } catch (e: Exception) {
                }
            }
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnLastWeekFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)

        fun <T> onTransactionSelected(model: T)
    }

    companion object {
        private const val PARAM_TAB_TYPE = "_param_tab_type"

        @JvmStatic
        fun newInstance(tabType: String, lis: OnLastWeekFragmentInteractionListener,isFilter:Boolean=false) =
            LastWeekTransactionHistoryFragment(lis).apply {
                arguments = Bundle().apply {
                    putString(PARAM_TAB_TYPE, tabType)
                    putBoolean("isFilter", isFilter)
                }
            }

        const val LIST_TYPE_DATE = 0
        const val LIST_TYPE_CATEGORY = 1
    }

    override fun <T> clickListener(position: Int, model: T, view: View) {
        listener?.onTransactionSelected(model)
    }

    override fun onRemoved(position: Int) {

    }


    private fun generateBarChart(graphData: List<GraphData>) {
        if (graphData.isEmpty()) return

        // setBarChartData(x = graphData.size, graphData = graphData)
    }

    /* private fun setBarChartData(x: Int, y: Int = 14000, graphData: List<GraphData>) {
         chart1.axisLeft.isEnabled = false
         chart1.axisRight.isEnabled = false
         chart1.description.isEnabled = false

         val values = ArrayList<BarEntry>()
         val xValues: ArrayList<String> = ArrayList()

         for (i in 0 until x) {
             values.add(BarEntry(i.toFloat(), (graphData[i].amount ?: 0).toFloat()))

             when {
                 isMonthWise -> {
                     xValues.add(graphData[i].month ?: "")
                 }
                 isWeek -> {
                     xValues.add(
                         graphData[i].day.dateChangeTo(
                             AppConstants.DateTimeFormats.TRANSACTION_DATE,
                             AppConstants.DateTimeFormats.TRANSACTION_DATE_DAY
                         )
                     )
                 }
                 else -> {
                     xValues.add(graphData[i].day ?: "")
                 }
             }
         }

         val set1: BarDataSet
         if (chart1.data != null && chart1.data.dataSetCount > 0) {
             set1 = chart1.data.getDataSetByIndex(0) as BarDataSet
             set1.values = values
             chart1.data.notifyDataChanged()
             chart1.notifyDataSetChanged()
         } else {
             set1 = BarDataSet(values, "")
             set1.color = ContextCompat.getColor(requireActivity(), R.color.colorAccent)
             set1.highLightColor = ContextCompat.getColor(requireActivity(), R.color.chart_blue)

             val dataSets = ArrayList<IBarDataSet>()
             dataSets.add(set1)

             val data = BarData(dataSets)
             chart1.data = data
             chart1.barData.barWidth = .50f
             chart1.setFitBars(true)

             val xAxis = chart1.xAxis
             xAxis.position = XAxis.XAxisPosition.BOTTOM
             xAxis.setDrawGridLines(false)
             xAxis.axisMinLabels = 1
             xAxis.labelCount = graphData.size
             xAxis.valueFormatter = IndexAxisValueFormatter(xValues.toTypedArray())
         }

         chart1.animateY(1500)
         chart1.legend.isEnabled = false

         chart1.invalidate()

         chart1.setOnChartValueSelectedListener(this)
     }

     override fun onNothingSelected() {
     }

     override fun onValueSelected(e: Entry?, h: Highlight?) {
     }*/

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        when (key) {
            TransactionHistoryViewModel.KEY_TRANSACTION_HISTORY_SERVICE -> {
                val baseModel = result.data

                when (baseModel?.code) {
                    200 -> {
                        val data = Gson().fromJson<Transaction>(
                            Gson().toJson(baseModel.data),
                            Transaction::class.java
                        )
                        // processData(data)
                    }
                    422 -> {
                        showToast(requireContext(), baseModel.message)
                    }
                    401 -> {
                        showToast(requireContext(), baseModel.message)

                        activity?.let { getDataManager().mPref.prefLogout(it) }
                    }
                }
            }
        }
    }

    private fun processData(data: Transaction?) {
        /*val graphData = data?.graph_data
        graphData?.let { generateBarChart(it) }*/

        val list = viewModel.transactionHistoryList(data)
        list?.let {
            if (it.isEmpty()) {
                rootLayout.makeGone()
                noTransaction.makeVisible()
            } else {
                rootLayout.makeVisible()
                noTransaction.makeGone()
                categoryAdapter.categories = list
                categoryAdapter.notifyDataSetChanged()
                //totalAmount.text = viewModel.transactionTotal(list).toString()
            }
        }

    }

    override fun onLoading(isLoader: Boolean) {
        if (swipeRefresh != null) {
            swipeRefresh.isRefreshing = isLoader

        }
    }

    override fun onError(err: Throwable) {
        requireContext().showErrorToast(err.message!!)
    }


    override fun onPause() {
        super.onPause()
        mDisposable.clear()
    }

    override fun onRefresh() {
        mDisposable.add(
            viewModel.transactionHistoryServiceNew(
                fromDate,
                toDate,
                if (isMonthWise) "1" else "0",
                isFilter
            ).subscribe {
                transactionAdapter.submitData(lifecycle, it)
            })
    }
}
