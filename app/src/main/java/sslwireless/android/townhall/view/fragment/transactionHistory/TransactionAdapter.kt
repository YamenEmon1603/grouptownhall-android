package sslwireless.android.townhall.view.fragment.transactionHistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.databinding.ItemTransactionCategoryBinding
import sslwireless.android.townhall.databinding.ItemTransactionCategoryDateBinding
import sslwireless.android.townhall.view.adapter.IAdapterCallback
import sslwireless.android.townhall.view.utils.*


class TransactionAdapter(private val mCallback: IAdapterCallback) : PagingDataAdapter<TransactionCategoryModel, RecyclerView.ViewHolder>(DataDifferntiator) {

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            when(it.type){
                LastWeekTransactionHistoryFragment.LIST_TYPE_DATE->{
                   val header = holder as ViewHolderHeader
                    header.binding.dateTextView.text = it.date.dateChangeTo(
                        AppConstants.DateTimeFormats.TRANSACTION_DATE,
                        if (AppConstants.Language.CURRENT_LANGUAGE == AppConstants.Language.ENGLISH)
                            AppConstants.DateTimeFormats.TRANSACTION_DATE_AND_DAY
                        else
                            AppConstants.DateTimeFormats.TRANSACTION_DATE_AND_DAY_BN,
                        AppConstants.Language.CURRENT_LANGUAGE
                    ).digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
                }
                else->{
                    val mViewHolder = holder as ViewHolder
                    mViewHolder.binding.categoryName.text = it.categoryName
                    mViewHolder.binding.amount.text = it.amount?.toDouble()?.toDecimalTwo()
                    mViewHolder.binding.phoneOrUsername.text = it.user?.digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)

                    if (!it.status.isNullOrEmpty()) {
                        when (it.statusEnum) {
                            AppConstants.TransactionStatus.SUCCESS -> {
                               mViewHolder.binding.statusBg.loadImage(R.drawable.bg_success)
                            }
                            AppConstants.TransactionStatus.FAILED -> {
                                mViewHolder.binding.statusBg.loadImage(R.drawable.bg_failed)
                            }
                            AppConstants.TransactionStatus.CANCELED -> {
                                mViewHolder.binding.statusBg.loadImage(R.drawable.bg_failed)
                            }
                            AppConstants.TransactionStatus.PROCESSING -> {
                                mViewHolder.binding.statusBg.loadImage(R.drawable.bg_processing)
                            }
                            AppConstants.TransactionStatus.SCHEDULED -> {
                                mViewHolder.binding.statusBg.loadImage(R.drawable.bg_scheduled)
                            }
                            else -> {
                                mViewHolder.binding.statusBg.loadImage(R.drawable.bg_default)
                            }
                        }

                        mViewHolder.binding.status.text = it.status
                    } else{
                        mViewHolder.binding.statusBg.loadImage(R.drawable.bg_default)
                    }

                    mViewHolder.binding.root.setOnClickListener {view->
                        mCallback.clickListener(position, it, view.rootView)
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == LastWeekTransactionHistoryFragment.LIST_TYPE_DATE) {
            ViewHolderHeader(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.item_transaction_category_date, parent, false))
        } else {
            ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context), R.layout.item_transaction_category, parent, false))
        }
    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.type ?: -1
    }


    class ViewHolder(val binding: ItemTransactionCategoryBinding) : RecyclerView.ViewHolder(binding.root)

    class ViewHolderHeader(val binding: ItemTransactionCategoryDateBinding) : RecyclerView.ViewHolder(binding.root)

    object DataDifferntiator : DiffUtil.ItemCallback<TransactionCategoryModel>() {
        override fun areItemsTheSame(
            oldItem: TransactionCategoryModel,
            newItem: TransactionCategoryModel
        ): Boolean {
            return oldItem.date == newItem.date
        }

        override fun areContentsTheSame(
            oldItem: TransactionCategoryModel,
            newItem: TransactionCategoryModel
        ): Boolean {
            return oldItem==newItem
        }


    }
}