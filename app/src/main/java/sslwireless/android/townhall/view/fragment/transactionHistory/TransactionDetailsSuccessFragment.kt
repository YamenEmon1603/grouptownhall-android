package sslwireless.android.townhall.view.fragment.transactionHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import kotlinx.android.synthetic.main.fragment_transaction_details_success.*
import sslwireless.android.townhall.data.model.BaseModel
import sslwireless.android.townhall.utils.LiveDataResult
import sslwireless.android.townhall.view.base.BaseFragment
import sslwireless.android.townhall.view.base.BaseViewmodelFactory
import sslwireless.android.townhall.view.utils.*

class TransactionDetailsSuccessFragment : BaseFragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            BaseViewmodelFactory(TransactionHistoryViewModel(getDataManager()))
        ).get(TransactionHistoryViewModel::class.java)
    }

    override fun viewRelatedTask() {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transaction_details_success, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    override fun onSuccess(result: LiveDataResult<BaseModel<Any>>, key: String) {
        showToast(
            requireContext(),
            result.data?.message ?: getString(R.string.something_went_wrong)
        )
        if (result.data?.code == 200) {
            ratingBar.isClickable = false
            ratingBar.isScrollable = false
            ratingBar.isClearRatingEnabled = false
            rateTitleText.text = requireContext().getString(R.string.your_rating)
            rateSubtitleText.text = requireContext().getString(R.string.subtext_rating)
            submitBtn.makeGone()
        }
    }

    companion object {
        private const val ARG_DETAILS = "_arg_details"

        @JvmStatic
        fun newInstance(details: TransactionCategoryModel?) =
            TransactionDetailsSuccessFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_DETAILS, details)
                }
            }
    }

    private fun initUi() {
        val transaction = arguments?.getParcelable<TransactionCategoryModel>(ARG_DETAILS)

        transaction?.let {
            dateTimeText.text = it.date.dateChangeTo(
                AppConstants.DateTimeFormats.TRANSACTION_DATETIME,
                if (AppConstants.Language.CURRENT_LANGUAGE == AppConstants.Language.ENGLISH)
                    AppConstants.DateTimeFormats.TRANSACTION_DATETIME_AND_DAY
                else
                    AppConstants.DateTimeFormats.TRANSACTION_DATETIME_AND_DAY_BN,
                AppConstants.Language.CURRENT_LANGUAGE
            ).digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
            submitBtn.makeGone()
            if (it.token==null) {
                tokenIdText.makeGone()
            } else {
                tokenIdText.makeVisible()
                tokenIdText.text = resources.getString(R.string.token,it.token.toString().digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE))
            }
            if (it.rating == 0) {
                ratingBar.isClickable = true
                rateTitleText.text = requireContext().getString(R.string.rate_your_experience)
                rateSubtitleText.text =
                    requireContext().getString(R.string.how_satisfied_are_you_with_the_transaction)
            } else {
                ratingBar.rating = it.rating.toFloat()
                ratingBar.isClickable = false
                rateTitleText.text = requireContext().getString(R.string.your_rating)
                rateSubtitleText.text = requireContext().getString(R.string.subtext_rating)
            }
            purposeText.text = it.categoryName
            nameOrPhoneText.text =
                it.user?.digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
            totalAmountText.text = it.amount?.toDouble()?.toDecimalTwo()
            transactionMethodText.text = it.paymentMethod
            statusText.text = it.sub_bill_status
            transactionIdText.text =
                String.format(getString(R.string.transaction_id), it.transactionId);
        }
        ratingBar.setOnRatingChangeListener { ratingBar, rating, fromUser ->
            if (submitBtn.visibility == GONE) {
                submitBtn.makeVisible()
            }
        }
        submitBtn.setOnClickListener {
            viewModel.apiRating(
                ratingBar.rating.toInt().toString(),
                transaction?.transactionId ?: "",
                this,
                this
            )
        }

    }
}