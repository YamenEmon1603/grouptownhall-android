package sslwireless.android.townhall.view.fragment.transactionHistory

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import androidx.paging.rxjava2.flowable
import io.reactivex.Flowable
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.data.model.transaction.Transaction
import sslwireless.android.townhall.view.base.ApiCallbackHelper
import sslwireless.android.townhall.view.base.BaseViewmodel
import sslwireless.android.townhall.view.base.IObserverCallBack
import sslwireless.android.townhall.view.fragment.transactionHistory.source.TransactionPagingSource
import sslwireless.android.townhall.view.utils.AppConstants
import java.lang.Exception
import javax.inject.Inject

class TransactionHistoryViewModel @Inject constructor(val dataManager: DataManager) :
    BaseViewmodel(dataManager) {
    var amount=0

    companion object {
        const val KEY_TRANSACTION_HISTORY_SERVICE = "transactionHistoryService"
    }

    fun transactionHistoryService(
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack,
        fromDate: String,
        toDate: String,
        isMonthWise: String
    ) {
        dataManager.apiHelper.transactionHistoryService(
            ApiCallbackHelper(
                livedata(
                    lifecycleOwner,
                    iObserverCallBack,
                    KEY_TRANSACTION_HISTORY_SERVICE
                )
            ),
            dataManager.mPref.prefGetCustomerToken() ?: "",
            fromDate,
            toDate,
            isMonthWise
        )
    }

    fun transactionHistoryServiceNew(
        fromDate: String,
        toDate: String,
        isMonthWise: String,
        isFilter:Boolean
    ): Flowable<PagingData<TransactionCategoryModel>> {
        return Pager(PagingConfig(pageSize = 10)) {
            TransactionPagingSource(dataManager,fromDate,toDate,isMonthWise,isFilter)
        }.flowable.cachedIn(viewModelScope)
    }


    fun transactionHistoryList(data: Transaction?): ArrayList<TransactionCategoryModel>? {
        try {
            val transactionData = data?.transaction_data
            val groupByDate = transactionData?.groupBy {
                it.transaction_at.split(" ")[0]
            }

            val categories: ArrayList<TransactionCategoryModel> = ArrayList()
            groupByDate?.forEach {
                categories.add(
                    TransactionCategoryModel(
                        date = it.key,
                        type = LastWeekTransactionHistoryFragment.LIST_TYPE_DATE
                    )
                )
                it.value.forEach { transactionData ->
                    categories.add(
                        TransactionCategoryModel(
                            "",
                            transactionData.service_title,
                            transactionData.amount.toString(),
                            transactionData.transaction_id,
                            transactionData.payment_method,
                            transactionData.status,
                            transactionData.status_enum,
                            transactionData.transaction_at,
                            null,
                            LastWeekTransactionHistoryFragment.LIST_TYPE_CATEGORY,
                            transactionData.rating,
                            transactionData.sub_bill_status
                        )
                    )
                }
            }
            return categories
        } catch (e: Exception) {
            return null
        }
    }

    fun transactionTotal(data: ArrayList<TransactionCategoryModel>?): Int {
        return try {
            data?.sumBy {
                if (it.statusEnum == AppConstants.TransactionStatus.SUCCESS) it.amount?.toInt()
                    ?: 0 else 0
            } ?: 0
        } catch (ex: Exception) {
            0
        }
    }

    fun apiRating(
        rating: String,
        transactionId: String,
        lifecycleOwner: LifecycleOwner,
        iObserverCallBack: IObserverCallBack
    ) {
        dataManager.apiHelper.apiRating(
            rating, transactionId, dataManager.mPref.prefGetCustomerToken() ?: "",
            ApiCallbackHelper(livedata(lifecycleOwner, iObserverCallBack, "rating"))
        )
    }
}