package sslwireless.android.townhall.view.fragment.transactionHistory

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_transactions_details_failed.*
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.dateChangeTo
import sslwireless.android.townhall.view.utils.digitChangeToBangla
import sslwireless.android.townhall.view.utils.toDecimalTwo

class TransactionsDetailsFailedFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transactions_details_failed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    companion object {
        private const val ARG_DETAILS = "_arg_details"

        @JvmStatic
        fun newInstance(details: TransactionCategoryModel?) =
            TransactionsDetailsFailedFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_DETAILS, details)
                }
            }
    }

    private fun initUi() {
        val transaction = arguments?.getParcelable<TransactionCategoryModel>(
            ARG_DETAILS
        )

        dateTimeText.text = transaction?.date.dateChangeTo(
            AppConstants.DateTimeFormats.TRANSACTION_DATETIME,
            if (AppConstants.Language.CURRENT_LANGUAGE == AppConstants.Language.ENGLISH)
                AppConstants.DateTimeFormats.TRANSACTION_DATETIME_AND_DAY
            else
                AppConstants.DateTimeFormats.TRANSACTION_DATETIME_AND_DAY_BN,
            AppConstants.Language.CURRENT_LANGUAGE
        ).digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)

        phoneText.text = transaction?.user?.digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
        purposeText.text = transaction?.categoryName
        nameOrPhoneText.text = transaction?.user?.digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
        totalAmountText.text = transaction?.amount?.toDouble()?.toDecimalTwo()
        transactionIdText.text = String.format(getString(R.string.transaction_id), transaction?.transactionId)
    }
}