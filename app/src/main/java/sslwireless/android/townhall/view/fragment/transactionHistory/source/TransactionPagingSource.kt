package sslwireless.android.townhall.view.fragment.transactionHistory.source

import androidx.paging.rxjava2.RxPagingSource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.TransactionCategoryModel
import sslwireless.android.townhall.data.model.transaction.TransactionData
import sslwireless.android.townhall.view.fragment.transactionHistory.LastWeekTransactionHistoryFragment
import sslwireless.android.townhall.view.utils.AppConstants

class TransactionPagingSource(
    private val mDataManager: DataManager,
    private val fromDate: String,
    private val toDate: String,
    private val isMonthWise: String,
    private val isFilter:Boolean=false
) : RxPagingSource<Int, TransactionCategoryModel>() {


    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, TransactionCategoryModel>> {
        var nextPage: Int? = params.key ?: 0
        val hashMap = HashMap<String, String>()
        hashMap["token"] = mDataManager.mPref.prefGetCustomerToken() ?: ""
        hashMap["from_date"] = fromDate
        hashMap["to_date"] = toDate
        hashMap["is_month_wise"] = isMonthWise
        hashMap["item_per_page"] = "10"
        hashMap["page_no"] = nextPage.toString()
        return mDataManager.apiHelper.apiService.transactionHistoryService(hashMap)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map {
                if (it.code!=200) {
                    throw Throwable(it.message)
                }
                AppConstants.TransactionAmount.amount= it.data?.sum_amount?:"0"
                it.data?.transaction_data ?: emptyList() }
            .map {
                if (it.isEmpty()) {
                    nextPage = null
                }
                transactionHistoryList(it)
            }
            .map { toLoadResult(it, nextPage) }
            .onErrorReturn {
                LoadResult.Error(it)
            }

    }

    private fun toLoadResult(
        data: ArrayList<TransactionCategoryModel>?,
        nextPage: Int?
    ): LoadResult<Int, TransactionCategoryModel> {
        return LoadResult.Page(
            data = data ?: emptyList(),
            prevKey = if (nextPage == null || nextPage == 0) null else nextPage - 1,
            nextKey = nextPage?.plus(1)
        )
    }

    private fun transactionHistoryList(transactionData: List<TransactionData>?): ArrayList<TransactionCategoryModel>? {
        try {
            // val transactionData = data?.transaction_data
            val groupByDate = transactionData?.groupBy {
                it.transaction_at.split(" ")[0]
            }

            val categories: ArrayList<TransactionCategoryModel> = ArrayList()
            groupByDate?.forEach {
                if (!isFilter) {
                    categories.add(
                        TransactionCategoryModel(
                            date = it.key,
                            type = LastWeekTransactionHistoryFragment.LIST_TYPE_DATE
                        )
                    )
                }
                it.value.forEach { transactionData ->
                    categories.add(
                        TransactionCategoryModel(
                            if (transactionData.order_data.dada!=null) transactionData.order_data.dada[0].value else "",
                            transactionData.service_title,
                            transactionData.amount.toString(),
                            transactionData.transaction_id,
                            transactionData.payment_method,
                            transactionData.status,
                            transactionData.status_enum,
                            transactionData.transaction_at,
                            transactionData.token,
                            LastWeekTransactionHistoryFragment.LIST_TYPE_CATEGORY,
                            transactionData.rating,
                            transactionData.sub_bill_status
                        )
                    )
                }
            }
            return categories
        } catch (e: java.lang.Exception) {
            return null
        }
    }

    class TestException(message:String): Exception(message)


}