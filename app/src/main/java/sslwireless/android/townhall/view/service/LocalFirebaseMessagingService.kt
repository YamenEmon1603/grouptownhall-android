package sslwireless.android.townhall.view.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.EasyApp
import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.prefence.PreferencesHelper
import sslwireless.android.townhall.view.activity.splash.SplashActivity

class LocalFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Refreshed token: $token")
        }

        val application = applicationContext as EasyApp
        application.getDataManager().mPref.prefSetFCMToken(token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        //val notification = remoteMessage.notification ?: return
        val params = remoteMessage.data
        val title = params["title"]?:""
        val body = params["body"]?:""
        val mPref = PreferencesHelper(applicationContext)
        val pattern = longArrayOf(0, 1000, 500, 1000)

        val notificationIntent = Intent(this, SplashActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        notificationIntent.putExtra(EXTRA_NOTIFICATION_TYPE, params["notificationType"])
        val intent = PendingIntent.getActivity(
            this,
            0,
            notificationIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val mNotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID, "Your Notifications",
                NotificationManager.IMPORTANCE_HIGH
            )

            notificationChannel.description = ""
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = pattern
            notificationChannel.enableVibration(true)
            mNotificationManager.createNotificationChannel(notificationChannel)
        }

        // to diaplay notification in DND Mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = mNotificationManager.getNotificationChannel(NOTIFICATION_CHANNEL_ID)
            channel.canBypassDnd()
        }

        val notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        if (title.equals("logout", true) && mPref.prefGetLoginMode()) {
            mPref.prefLogout(applicationContext)
        }
        val bigTextStyle = NotificationCompat.BigTextStyle()
        bigTextStyle.setBigContentTitle(title)
        bigTextStyle.bigText(body)


        notificationBuilder.setAutoCancel(true)
            .setContentTitle(title)
            .setContentText(body)
            .setStyle(bigTextStyle)
            .setColor(ContextCompat.getColor(this, R.color.colorAccent))
            .setDefaults(Notification.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(intent)
            .setAutoCancel(true)

        mNotificationManager.notify(1000, notificationBuilder.build())
    }

    companion object {
        private const val TAG = "Firebase"
        private const val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        const val EXTRA_NOTIFICATION_TYPE = "_extra_notification_type"
    }
}