package sslwireless.android.townhall.view.utils

import sslwireless.android.townhall.R
import sslwireless.android.townhall.data.model.operator.OperatorModel

class AppConstants {
    object RechargeRequest {
        var RECHARGE_REQUEST_RELOAD = false
    }

    object DateTimeFormats {
        const val TRANSACTION_DATETIME = "yyyy-MM-dd HH:mm:ss"
        const val TRANSACTION_DATE = "yyyy-MM-dd"
        const val TRANSACTION_DATE_DAY = "E"
        const val TRANSACTION_DATE_AND_DAY = "E, d MMM yyyy"
        const val TRANSACTION_DATE_AND_DAY_BN = "EEEE, d MMMM yyyy"
        const val TRANSACTION_DATETIME_AND_DAY = "E, d MMM yyyy | hh.mm aa"
        const val TRANSACTION_DATETIME_AND_DAY_BN = "EEEE, d MMMM yyyy | hh.mm aa"
    }

    object TransactionStatus {
        const val SCHEDULED = 3
        const val PROCESSING = 2
        const val SUCCESS = 1
        const val FAILED = 0
        const val CANCELED = 4
    }

    object Operators {
        const val GRAMEENPHONE = "Grameenphone"
        const val PREPAID = "prepaid"
        const val POSTPAID = "postpaid"
        const val SKITTO = "skitto"
        const val SKITTO_ID = 13

        val OPERATOR_LIST = ArrayList<OperatorModel>()
    }

    object Language {
        const val ENGLISH = "en"
        const val BANGLA = "bn"
        var CURRENT_LANGUAGE = ENGLISH
    }

    object Social {
        const val FACEBOOK = "facebook"
        const val GOOGLE = "google"
    }

    object TransactionAmount {
        var amount = "0"
    }

    object IntegrationModel {
        const val TAG = "sslCommerzIntegrationModel"
        const val TAG_AUTO_DEBIT = "TAG_AUTO_DEBIT"
    }

    object ChannelType {
        const val CARD = "card"
        const val OTHERS = "others"
    }

    object TownhallCamera {
        const val STRAIGHT = "Capture Straight"
        const val LEFT = "Capture Left"
        const val RIGHT = "Capture Right"
        const val DOWNSIDE = "Capture Downside"
        const val UPSIDE = "Capture Upside"

        val STEPS = arrayOf(
            STRAIGHT,
            RIGHT,
            LEFT,
            DOWNSIDE,
            UPSIDE
        )
    }

    object Venue{
        val imageList = arrayListOf(
            R.drawable.mask_group_57,
            R.drawable.mask_group_58,
            R.drawable.mask_group_59,
            R.drawable.mask_group_60
        )
    }
}