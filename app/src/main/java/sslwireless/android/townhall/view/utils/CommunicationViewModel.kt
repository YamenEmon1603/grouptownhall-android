package sslwireless.android.townhall.view.utils

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import sslwireless.android.townhall.data.model.populerPlan.PopularPlan
import sslwireless.android.townhall.data.model.townhall.TownHallResponse
import sslwireless.android.townhall.view.activity.rechargeOffer.SortType

class CommunicationViewModel : ViewModel() {
    val planItemLiveData = MutableLiveData<PopularPlan>()

    val sortingLiveData = MutableLiveData<SortType>()

    val onLanguageChange = MutableLiveData<String>()

    val onHomeScreenShow = MutableLiveData<Boolean>()

    val townHallResponse = MutableLiveData<TownHallResponse>()
}