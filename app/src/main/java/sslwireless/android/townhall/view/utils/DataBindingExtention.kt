package sslwireless.android.townhall.view.utils

import android.graphics.Paint
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter


@BindingAdapter("android:strikeText")
fun AppCompatTextView.strikeText(text: String?) {
    this.apply {
        paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        setText(text)
    }
}

@BindingAdapter("android:loadImage")
fun ImageView.loadImageBinder(url: String) {
    this.loadImageProfile(url)
}