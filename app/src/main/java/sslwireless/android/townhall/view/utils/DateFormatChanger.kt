package sslwireless.android.townhall.view.utils

import android.content.Context
import org.joda.time.DateTime
import org.joda.time.Days
import org.joda.time.Hours
import org.joda.time.Minutes
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import sslwireless.android.townhall.R
import java.text.SimpleDateFormat
import java.util.*


fun String?.dateChangeTo(inputType: String, outputType: String,language:String=AppConstants.Language.ENGLISH): String {
    val finalDate: String?
    val input = SimpleDateFormat(inputType, Locale.US)
    val output = SimpleDateFormat(outputType,Locale.forLanguageTag(language))

    finalDate = try {
        val inputDate = input.parse(this) // parse input
        output.format(inputDate)
    } catch (e: Exception) {
        ""
    }
    return finalDate
}

fun String.isToday(formatString: String): Boolean {
    val formatter: DateTimeFormatter = DateTimeFormat.forPattern(formatString).withLocale(Locale.US)
    val start = formatter.parseDateTime(this)
    val end = DateTime()
    return Days.daysBetween(start,end).days==0
}
fun String.isYesterday(formatString: String): Boolean {
    val formatter: DateTimeFormatter = DateTimeFormat.forPattern(formatString).withLocale(Locale.US)
    val start = formatter.parseDateTime(this)
    val end = DateTime().minusDays(1)
    return Days.daysBetween(start,end).days==0
}


fun String.dayDif(formatString: String): Int {
    val formatter: DateTimeFormatter = DateTimeFormat.forPattern(formatString).withLocale(Locale.US)
    val start = formatter.parseDateTime(this)
    val end = DateTime()
    return Days.daysBetween(start,end).days
}

fun Context.recentDayDif(inputString:String,formatString: String): String {
    val formatter: DateTimeFormatter = DateTimeFormat.forPattern(formatString).withLocale(Locale.US)
    val start = formatter.parseDateTime(inputString)
    val end = DateTime()
    val days =Days.daysBetween(start,end).days
    return if (days>0) {
        ""+days+" "+getString(R.string.days_ago)
    } else {
        val hours =Hours.hoursBetween(start,end).hours
        if (hours>0) {
            ""+hours+" "+getString(R.string.hr_ago)
        } else {
            val min= Minutes.minutesBetween(start, end).minutes
            return ""+min+" "+getString(R.string.min_ago)
        }
    }

}

fun Date?.formattedDate(format: String): String? {
    if (this == null) return null
    return try {
        SimpleDateFormat(format,Locale.US).format(this)
    } catch (e: java.lang.Exception) {
        null
    }
}


