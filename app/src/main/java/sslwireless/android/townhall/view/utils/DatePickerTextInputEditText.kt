package sslwireless.android.townhall.view.utils

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Rect
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import com.google.android.material.textfield.TextInputEditText
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class DatePickerTextInputEditText : TextInputEditText, DatePickerDialog.OnDateSetListener, DialogInterface.OnCancelListener {
    private var isPopup = false
    private var mDatePickerDialog: DatePickerDialog? = null
    private var invalidDate = false
    private var dateFormat:String?=null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        val mCalendar = Calendar.getInstance(TimeZone.getDefault())
        mDatePickerDialog = DatePickerDialog(
            getContext(),
            this,
            mCalendar.get(Calendar.YEAR),
            mCalendar.get(Calendar.MONTH),
            mCalendar.get(Calendar.DAY_OF_MONTH)
        )
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            val mInputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mInputMethodManager.hideSoftInputFromWindow(windowToken, 0)
            keyListener = null
            if (!isPopup) {
                 //showDropDown();
            } else {
                dismissDropDown()
            }
        } else {
            isPopup = false
        }
    }




    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_UP -> {
                if (isPopup) {
                    dismissDropDown()
                } else {
                    requestFocus()
                    showDropDown()
                }
            }
        }
        return super.onTouchEvent(event)
    }

    fun showDropDown() {
        if (!invalidDate) {
            mDatePickerDialog?.show()
            isPopup = false
        } else {
            //Toast.makeText(context, "Booking date has been expire", Toast.LENGTH_SHORT).show()
        }
    }

    fun dismissDropDown() {
        mDatePickerDialog?.dismiss()
        isPopup = false
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        val mFormat: NumberFormat = DecimalFormat("00")
        val mMonthFormat = mFormat.format(month + 1.toLong())
        val mDateFormat = mFormat.format(dayOfMonth.toLong())
        val mDate = "$mDateFormat-$mMonthFormat-$year"
        setText(mDate.dateChangeTo("dd-MM-yyyy",dateFormat?:"dd-MM-yyyy"))
    }


    fun outputDateFormat(format:String?){
        dateFormat = format
    }

    fun dialogClose() {
        invalidDate = true
    }

    override fun onCancel(dialogInterface: DialogInterface) {}
}