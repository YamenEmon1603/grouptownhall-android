package sslwireless.android.townhall.view.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.webkit.URLUtil
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import sslwireless.android.townhall.R
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import sslwireless.android.townhall.data.LocalValidator
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.lang.reflect.Type
import java.util.*
import kotlin.collections.HashMap

fun Activity.isPermissionGranted(permission: String, requestCode: Int): Boolean {
    return if (Build.VERSION.SDK_INT >= 23) {
        if (ActivityCompat.checkSelfPermission(this, permission)
            == PackageManager.PERMISSION_GRANTED
        ) {
            Log.v("TAG", "Permission is granted")
            true
        } else {
            Log.v("TAG", "Permission is revoked")
            requestPermissions(arrayOf(permission), requestCode)
            false
        }
    } else { //permission is automatically granted on sdk<23 upon installation
        Log.v("TAG", "Permission is granted")
        true
    }
}

fun String.capitalizeFirstCharacter(): String {
    return if (this.isNotEmpty()) substring(0, 1).toUpperCase() + substring(1) else ""
}

fun Context.showToastMsg(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    val toast = Toast(this)
    toast.duration = duration
    toast.setGravity(Gravity.CENTER, 0, 0);
    val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val view = inflater.inflate(R.layout.custom_toast_layout, null)

    val toastText = view.findViewById<TextView>(R.id.toastText)
    toastText.text = text

    toast.view = view
    toast.show()
}

fun Context.showErrorToast(text: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    val toast = Toast(this)
    toast.duration = duration
    toast.setGravity(Gravity.CENTER, 0, 0);
    val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val view = inflater.inflate(R.layout.custom_toast_layout, null)

    val toastText = view.findViewById<TextView>(R.id.toastText)
    toastText.text = text

    toast.view = view
    toast.show()
}

fun ImageView.loadImage(url: Any) {
    if (url is String && url.toString().contains("svg")) {
        GlideToVectorYou
            .init()
            .with(context)
            .load(Uri.parse(url), this)
        return
    }
    Glide.with(context).load(url).into(this)
}

fun ImageView.loadImageWithoutCache(url: Any) {
    Glide
        .with(context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.NONE)
        .skipMemoryCache(true)
        .into(this)
}

fun ImageView.loadImageProfile(url: Any) {
    Glide.with(context)
        .load(url).apply(
            RequestOptions.placeholderOf(R.drawable.ic_profile_image_placeholder)
                .error(R.drawable.ic_profile_image_placeholder)
        )
        .into(this)
}

fun ImageView.loadImageWithPlaceHolder(url: Any, placeholder: Int) {
    if (url is String && url.toString().contains("svg", true)) {
        GlideToVectorYou
            .init()
            .with(context)
            .setPlaceHolder(placeholder, placeholder)
            .load(Uri.parse(url), this)
        return
    }
    Glide.with(context)
        .load(url).apply(
            RequestOptions.placeholderOf(placeholder)
                .error(placeholder)
        )
        .into(this)
}

fun ImageView.loadImageWithRoundCorner(url: String?, radius: Int) {
    val options = RequestOptions()
        .centerCrop()
        .transform(CenterCrop(), RoundedCorners(radius))
        .placeholder(R.drawable.offer_placeholder)
        .error(R.drawable.offer_placeholder)

    Glide.with(context)
        .load(url)
        .apply(options)
        .into(this)
}

fun ImageView.operatorIcon(operatorName: String) {
    val value = AppConstants.Operators.OPERATOR_LIST.find { it.operatorName == operatorName }
    value?.let { loadImage(it.imageUrl) }
}

fun String.operatorID(operatorType: String): Int {
    val value = AppConstants.Operators.OPERATOR_LIST.find { it.operatorName == this }
    return if (value != null) {
        if (operatorType.equals(
                AppConstants.Operators.SKITTO,
                true
            ) && value.operatorName == AppConstants.Operators.GRAMEENPHONE
        ) {
            return AppConstants.Operators.SKITTO_ID
        }
        value.operatorId
    } else {
        -1
    }
}

fun Int.getMinRechargeAmount(operatorType: String?): Int {
    return if (this == AppConstants.Operators.SKITTO_ID) {
        10  //https://www.skitto.com/faq/290-what-is-the-maximum-amount-i-can-reload
    } else {
        val operator = AppConstants.Operators.OPERATOR_LIST.find { it.operatorId == this }
        if (operatorType.equals(AppConstants.Operators.POSTPAID, true)) operator?.postpaidLowLimit
            ?: 10 else operator?.prepaidLowLimit ?: 10
    }
}

fun Int.getMaxRechargeAmount(operatorType: String?): Int {
    return if (this == AppConstants.Operators.SKITTO_ID) {
        1000  //https://www.skitto.com/faq/290-what-is-the-maximum-amount-i-can-reload
    } else {
        val operator = AppConstants.Operators.OPERATOR_LIST.find { it.operatorId == this }
        if (operatorType.equals(AppConstants.Operators.POSTPAID, true)) operator?.postpaidHighLimit
            ?: 2500 else operator?.prepaidHighLimit ?: 1000
    }
}

fun Int.operatorName(): String {
    val value = AppConstants.Operators.OPERATOR_LIST.find { it.operatorId == this }
    return value?.operatorName ?: AppConstants.Operators.GRAMEENPHONE
}

fun ImageView.operatorIconByID(operatorID: Int) {
    val value = if (operatorID == AppConstants.Operators.SKITTO_ID) {
        AppConstants.Operators.OPERATOR_LIST.find { it.operatorName == AppConstants.Operators.GRAMEENPHONE }
    } else {
        AppConstants.Operators.OPERATOR_LIST.find { it.operatorId == operatorID }
    }

    value?.let { loadImage(value.imageUrl) }
}

fun ImageView.operatorIconByShotName(shotName: String) {
    val value = AppConstants.Operators.OPERATOR_LIST.find { it.operatorShortName == shotName }
    value?.let { loadImage(value.imageUrl) }
}

fun String.getNormalBody(): RequestBody = RequestBody.create(MediaType.parse("text/plain"), this)

fun File.getImageBody(key: String): MultipartBody.Part {
    val body = RequestBody.create(MediaType.parse("image/*"), this)
    return MultipartBody.Part.createFormData(key, name, body)
}

inline fun <reified T : Any> Any?.fromResponse(): T? {
    val listType: Type = object : TypeToken<T>() {}.type
    val moshi = Moshi.Builder().build()
    val adapter: JsonAdapter<T> = moshi.adapter(listType)
    return adapter.fromJsonValue(this)
}

fun View.makeVisible() {
    visibility = VISIBLE
}

fun View.makeGone() {
    visibility = GONE
}

fun View.makeInvisible() {
    visibility = INVISIBLE
}

fun TextView.setColor(@ColorRes color: Int) {
    setTextColor(ContextCompat.getColor(context, color))
}

fun View.setBackgroundExtension(@DrawableRes drawableRes: Int) {
    background = ContextCompat.getDrawable(context, drawableRes)
}

fun String.digitChangeToBangla(language: String): String {
    if (language == AppConstants.Language.ENGLISH) {
        return this
    }
    val hash = HashMap<Int, String>()
    hash[0] = "০"
    hash[1] = "১"
    hash[2] = "২"
    hash[3] = "৩"
    hash[4] = "৪"
    hash[5] = "৫"
    hash[6] = "৬"
    hash[7] = "৭"
    hash[8] = "৮"
    hash[9] = "৯"
    var banglaDate = ""
    this.forEach {
        try {
            val digit = it.toString().toInt()
            banglaDate += hash[digit]
        } catch (e: Exception) {
            banglaDate += it
        }
    }
    return banglaDate
}

fun String?.operatorChangeToBangla(language: String): String {
    if (language == AppConstants.Language.ENGLISH) {
        return this ?: ""
    }
    val hash = HashMap<String, String>()
    hash[AppConstants.Operators.POSTPAID.toLowerCase()] = "পোস্টপেইড"
    hash[AppConstants.Operators.PREPAID.toLowerCase()] = "প্রিপেইড"
    hash[AppConstants.Operators.SKITTO.toLowerCase()] = "স্কিটো"
    return hash[this?.toLowerCase()] ?: ""
}

inline fun <reified T> Gson.fromJson(json: String?): T =
    fromJson<T>(json, object : TypeToken<T>() {}.type)

fun Context.getLocale(language: String): Context {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        createConfigurationContext(config)
    } else {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        this
    }
}

fun TextView.spanText(text: String, lang: String) {
    val sb = SpannableStringBuilder(text);
    val bss = StyleSpan(Typeface.BOLD)
    if (lang == AppConstants.Language.ENGLISH) {
        val startIndex = text.indexOfFirst { it == '\n' }
        sb.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.text_second_color)),
            0,
            startIndex,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
        sb.setSpan(bss, startIndex, text.length, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    } else {
        sb.setSpan(bss, 0, 8, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        sb.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.text_second_color)),
            9,
            text.length,
            Spannable.SPAN_INCLUSIVE_INCLUSIVE
        )
    }
    this.text = sb
}

fun Double.toDecimalTwo(): String {
    return String.format(
        "%.02f",
        this
    ).digitChangeToBangla(AppConstants.Language.CURRENT_LANGUAGE)
}

fun String.isEmailOrMobileValid(): Boolean {
    if (contains("@")) {
        if (!LocalValidator.isEmailValid(this)) {
            return false
        }
    } else if (!LocalValidator.isNewPhnValid(this)) {
        return false
    }

    return true
}

fun String.getTelegramUri(context: Context): Uri {
    try {
        context.packageManager.getPackageInfo("org.telegram.messenger", 0)

        return if (URLUtil.isValidUrl(this)) {
            val roomId = Uri.parse(this).lastPathSegment
            if (roomId.isNullOrEmpty()) {
                Uri.parse(this)
            } else {
                Uri.parse("tg://resolve?domain=${roomId}")
            }
        } else {
            Uri.parse("tg://resolve?domain=${this}")
        }
    } catch (e: PackageManager.NameNotFoundException) {
        return if (URLUtil.isValidUrl(this)) {
            Uri.parse(this)
        } else {
            Uri.parse("https://t.me/$this")
        }
    } catch (e: Exception) {
        return Uri.parse("https://t.me/$this")
    }
}

fun Context.openTelegram(url: String?): Boolean {
    try {
        if (url.isNullOrEmpty()) {
            showErrorToast(getString(R.string.unable_to_open_telegram), Toast.LENGTH_SHORT)
            return false
        }

        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = url.getTelegramUri(this)
        startActivity(intent)
    } catch (exception: Exception) {
        showErrorToast(getString(R.string.unable_to_open_telegram), Toast.LENGTH_SHORT)
        return false
    }

    return true
}


fun Context.openSocialUrl(url: String?): Boolean {
    try {
        if (url.isNullOrEmpty()) {
            showErrorToast(getString(R.string.unable_to_open), Toast.LENGTH_SHORT)
            return false
        }

        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        this.startActivity(intent)
    } catch (exception: Exception) {
        showErrorToast(getString(R.string.unable_to_open), Toast.LENGTH_SHORT)
        return false
    }

    return true
}

fun String.setSpanColor(color: Int, startIndex: Int, endIndex: Int): SpannableString {
    val spannableString = SpannableString(this)
    spannableString.setSpan(
        ForegroundColorSpan(color),
        startIndex,
        endIndex,
        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
    )

    return spannableString
}

fun File.reduceFileSize(requiredSize: Int = 75): File? {
    // BitmapFactory options to downsize the image
    Log.d("IMAGE SIZE", "Actual size ---> ${this.sizeInKb()}")

    val options: BitmapFactory.Options = BitmapFactory.Options()
    options.inJustDecodeBounds = true
    options.inSampleSize = 6
    var inputStream = FileInputStream(this)
    BitmapFactory.decodeStream(inputStream, null, options)
    inputStream.close()

    var scale = 1
    while (options.outWidth / scale / 2 >= requiredSize &&
        options.outHeight / scale / 2 >= requiredSize
    ) {
        scale *= 2
    }

    val options2: BitmapFactory.Options = BitmapFactory.Options()
    options2.inSampleSize = scale
    inputStream = FileInputStream(this)
    val selectedBitmap: Bitmap =
        BitmapFactory.decodeStream(inputStream, null, options2) ?: return null
    inputStream.close()

    this.createNewFile()

    val outputStream = FileOutputStream(this)
    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)

    Log.d("IMAGE SIZE", "Reduced size ---> ${this.sizeInKb()}")
    return this
}

fun File.sizeInKb(): Double {
    return if (!exists()) {
        0.0
    } else {
        length().toDouble() / 1024
    }
}

fun File.reduceUntilSize(size: Int, counter: Int = 5): File {
    val file = this.reduceFileSize() ?: return this

    if (file.sizeInKb() <= size || counter <= 0) return file

    return file.reduceUntilSize(size, counter - 1)
}

fun String.getQrCodeBitmap(size: Int = 512): Bitmap {
    val bits = QRCodeWriter().encode(this, BarcodeFormat.QR_CODE, size, size)
    return Bitmap.createBitmap(bits.width, bits.height, Bitmap.Config.RGB_565).also {
        for (x in 0 until size) {
            for (y in 0 until size) {
                it.setPixel(x, y, if (bits[x, y]) Color.BLACK else Color.WHITE)
            }
        }
    }
}


