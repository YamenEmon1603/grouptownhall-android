package sslwireless.android.townhall.view.utils

import android.app.Activity
import android.content.Context
import android.graphics.PorterDuffColorFilter
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import sslwireless.android.townhall.R

class ProgressBarHandler {
    private var mProgressBar: ProgressBar? = null
    private var mContext: Context? = null
    internal var color: Int = 0

    constructor(context: Context) {
        mContext = context

        initialize("")
    }

    constructor(context: Context, color: Int) {
        mContext = context
        this.color = color

        initialize("")
    }

    constructor(context: Context, text: String) {
        mContext = context

        initialize(text)
    }

    private fun initialize(text: String) {
        val layout = (mContext as AppCompatActivity).findViewById<View>(android.R.id.content).rootView as ViewGroup

        mProgressBar = ProgressBar(mContext, null, android.R.attr.progressBarStyleLarge)
        mProgressBar!!.isIndeterminate = true

//         if (!text.isEmpty()) {
//           mProgressBar..setText(text);
//         }
        //mProgressBar.setProgressDrawable(context.getDrawable(R.mipmap.p));
        val colorr = if (color != 0) color else mContext!!.resources.getColor(R.color.colorAccent)
        mProgressBar!!.indeterminateDrawable!!.colorFilter = PorterDuffColorFilter(colorr, android.graphics.PorterDuff.Mode.SRC_IN) //(colorr, android.graphics.PorterDuff.Mode.SRC_IN)

        val params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT
        )

        val rl = RelativeLayout(mContext)
        rl.gravity = Gravity.CENTER
        rl.addView(mProgressBar)

        layout.addView(rl, params)

        hide()
    }

    fun show() {
        mProgressBar!!.visibility = View.VISIBLE
        (mContext as Activity).window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
    }

    fun hide() {
        mProgressBar!!.visibility = View.INVISIBLE
        (mContext as Activity).window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }
}
