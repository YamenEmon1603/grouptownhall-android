package sslwireless.android.townhall.view.utils

import android.content.Context
import android.content.Intent
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCAdditionalInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCCustomerInfoInitializer
import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization
import com.sslwireless.sslcommerzlibrary.model.util.SSLCCurrencyType
import com.sslwireless.sslcommerzlibrary.model.util.SSLCLanguage
import com.sslwireless.sslcommerzlibrary.model.util.SSLCSdkType
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.SSLCTransactionResponseListener
import sslwireless.android.townhall.BuildConfig
import sslwireless.android.townhall.data.DataManager
import sslwireless.android.townhall.data.model.sslCommerzIntegrationModel.SslCommerzIntegrationModel
import sslwireless.android.townhall.view.activity.paymentMethod.PaymentMethodActivity

fun Context.initSdk(
    listener: SSLCTransactionResponseListener,
    sslCommerzIntegrationModel: SslCommerzIntegrationModel,
    dataManager: DataManager, checkSaveCard: Boolean
) {
    if (checkSaveCard && sslCommerzIntegrationModel.pay_with_save_card) {
        startActivity(Intent(this, PaymentMethodActivity::class.java).apply {
            putExtra(AppConstants.IntegrationModel.TAG, sslCommerzIntegrationModel)
        })
        return
    }

    val SSLCCustomerInfoInitializer =
        SSLCCustomerInfoInitializer(
            dataManager.mPref.prefGetCurrentUser().name,
            dataManager.mPref.prefGetCurrentUser().email,
            dataManager.mPref.prefGetCurrentUser().address,
            "",
            "",
            "Bangladesh",
            dataManager.mPref.prefGetCurrentUser().mobile
        )

    val sslCommerzInitialization = SSLCommerzInitialization(
        sslCommerzIntegrationModel.store_id,
        sslCommerzIntegrationModel.store_password,
        sslCommerzIntegrationModel.amount.toString().toDouble(),
        SSLCCurrencyType.BDT,
        sslCommerzIntegrationModel.transaction_id,
        "easy consumer",
        if (BuildConfig.FLAVOR=="demo") SSLCSdkType.TESTBOX else SSLCSdkType.LIVE,
        if (dataManager.mPref.prefGetLanguage() == AppConstants.Language.ENGLISH) SSLCLanguage.English else SSLCLanguage.Bangla
    )

    sslCommerzInitialization.ipn_url = sslCommerzIntegrationModel.ipn_url
    val additionalInitializer = SSLCAdditionalInitializer()
    additionalInitializer.valueB = sslCommerzIntegrationModel.value_b
    additionalInitializer.valueC = sslCommerzIntegrationModel.value_c
    sslCommerzIntegrationModel.user_refer?.let {
        additionalInitializer.user_refer = it
    }
    IntegrateSSLCommerz
        .getInstance(this)
        .addSSLCommerzInitialization(sslCommerzInitialization)
        .addCustomerInfoInitializer(SSLCCustomerInfoInitializer)
        .addAdditionalInitializer(additionalInitializer)
        .buildApiCall(listener)
}