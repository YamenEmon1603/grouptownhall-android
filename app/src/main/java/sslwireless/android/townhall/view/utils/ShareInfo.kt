package sslwireless.android.townhall.utils

import android.Manifest
import android.animation.ValueAnimator
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings.Secure
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import com.google.android.gms.ads.identifier.AdvertisingIdClient
import sslwireless.android.townhall.EasyApp
import sslwireless.android.townhall.data.model.DeviceInfo
import org.json.JSONObject
import sslwireless.android.townhall.BuildConfig
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class ShareInfo {

    companion object {
        private var shareInfo: ShareInfo = ShareInfo()
        fun getInstance(): ShareInfo {
            return shareInfo
        }

    }

    fun convertServerDate(dte: String?, tag: String): String? {
        var convertedDate = ""
        var fromFormat = SimpleDateFormat()
        try {
            if (tag == "halkhata") {
                fromFormat = SimpleDateFormat("yyyy-MM-dd")
            } else if (tag == "customerDetails") {
                fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            }

            //val toFormat = SimpleDateFormat("EE, dd, MMM-yyyy, HH:mm")
            val toFormat = SimpleDateFormat("EEEE, dd MMM")
            val date1: Date = fromFormat?.parse(dte)

            val calendar = Calendar.getInstance()
            calendar.time = date1
            convertedDate = toFormat.format(calendar.time)

        } catch (e: Exception) {
        }
        return convertedDate
    }

    fun convertServerTime(dte: String?): String? {
        var convertedDate = ""
        try {
            //val fromFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val fromFormat = SimpleDateFormat("HH:mm:ss")
            val toFormat = SimpleDateFormat("hh:mm aa")
            val date1: Date = fromFormat?.parse(dte)

            val calendar = Calendar.getInstance()
            calendar.time = date1
            convertedDate = toFormat.format(calendar.time)

        } catch (e: Exception) {
        }
        return convertedDate
    }

    fun call(context: Context, phoneNumber : String? ){
        val intent = Intent(
            Intent.ACTION_DIAL,
            Uri.fromParts("tel", phoneNumber, null)
        )
        context.startActivity(intent)
    }

    private val dialogs: ProgressDialog? = null

    val requestId: String
        get() {
            val random = Random(10000)
            return (System.currentTimeMillis() + random.nextInt()).toString()
        }

    fun getPxToDp(px: Int): Int {
        return (px / Resources.getSystem().displayMetrics.density).toInt()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }

    fun spToPx(sp: Float): Int {
        return (sp * Resources.getSystem().displayMetrics.scaledDensity).toInt()
    }

    fun getDeviceId(): String {
        var androidId: String? = Secure.getString(EasyApp.context.contentResolver, Secure.ANDROID_ID)

        if(androidId != null){
            return androidId
        } else{
            val adInfo =
                AdvertisingIdClient.getAdvertisingIdInfo(EasyApp.context)
            val adId = adInfo?.id
            if (adId != null) {
                return androidId!!
            }
        }

        return ""
    }

    fun getDeviceData(): JSONObject? {
        val params = JSONObject()
        params.put("model", Build.MODEL)
        try {
            params.put("id", Build.ID)
            params.put("manufacture", Build.MANUFACTURER)
            params.put("brand", Build.BRAND)
            params.put("type", Build.TYPE)
            params.put("user", Build.USER)
            params.put("version_code", Build.VERSION.RELEASE)
        } catch (e: java.lang.Exception) {
        }
        return params
    }

    fun getDeviceInfo(): DeviceInfo{
        val deviceInfo = DeviceInfo()
        deviceInfo.KEY_DEVICE_KEY = getDeviceId()
        try {
            deviceInfo.KEY_OS_VERSION = Build.VERSION.RELEASE
            deviceInfo.KEY_OS = "Android"
            deviceInfo.KEY_DEVICE = Build.DEVICE//getDeviceData().toString()
            deviceInfo.KEY_USER_AGENT = Build.USER
            deviceInfo.KEY_BROWSER = "Android App"
            deviceInfo.KEY_BROWSER_VERSION = BuildConfig.VERSION_NAME.toString()
            deviceInfo.IP_ADDRESS = "required"
            deviceInfo.app_android_version = BuildConfig.VERSION_CODE

        } catch (e: java.lang.Exception) {
        }
        return deviceInfo
    }

    fun getDeviceName(context: Context): String {
        val manufacturer = Build.MANUFACTURER
        val model = Build.MODEL
        return if (model.startsWith(manufacturer)) {
            capitalize(model)
        } else capitalize(manufacturer) + " " + model
    }

    private fun capitalize(str: String): String {
        if (TextUtils.isEmpty(str)) {
            return str
        }
        val arr = str.toCharArray()
        var capitalizeNext = true

        val phrase = StringBuilder()
        for (c in arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c))
                capitalizeNext = false
                continue
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true
            }
            phrase.append(c)
        }

        return phrase.toString()
    }

    fun getCurrentDate(): String {

        return SimpleDateFormat("yyyy-MM-dd").format(Date())
    }

    fun get12HourFormatTime(hourOfDay: Int, selectedMinute: Int): String{
        if (hourOfDay == 0) {
            return (hourOfDay + 12).toString()+":"+selectedMinute+" AM";
        }
        else if (hourOfDay == 12) {
            return hourOfDay.toString()+":"+selectedMinute+" PM";
        }
        else if (hourOfDay > 12) {
            return (hourOfDay - 12).toString()+":"+selectedMinute+" PM"

        }
        else {
            return "00"+":"+selectedMinute+" AM";
        }
    }
    fun isPhoneNumberValid(phoneNumber: String): Boolean {
        var isValid = false

        val expression = "^(?:\\+?88)?01[15-9]\\d{8}$"

        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(phoneNumber)
        if (matcher.matches()) {
            isValid = true
        }
        return isValid
    }

    fun getDigitFromString(str: String): String {
        // str..matches(".*\\d.*")  // is string?
        var value: String? = str.replace("\\D+".toRegex(), "")
        if (value == null) {
            value = ""
        }

        return value
    }

    fun isValid(cardNumber: String): Boolean {
        var sum = 0
        var alternate = false
        for (i in cardNumber.length - 1 downTo 0) {
            var n = Integer.parseInt(cardNumber.substring(i, i + 1))
            if (alternate) {
                n *= 2
                if (n > 9) {
                    n = n % 10 + 1
                }
            }
            sum += n
            alternate = !alternate
        }
        return sum % 10 == 0
    }

    fun isURLString(url: String): Boolean {
        val URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$"

        val p = Pattern.compile(URL_REGEX)
        val m = p.matcher(url)//replace with string to compare
        return if (m.find()) {
            true
        } else false

    }

    fun convertListToArrayList(arrayList: List<String>): ArrayList<String> {

        val datas = ArrayList<String>()
        for (str in arrayList) {
            datas.add(str)
        }

        return datas
    }

    fun showProgressDialog(context: Context?): ProgressDialog? {
        val pDialog = ProgressDialog(context)
        try {
            if (context != null) {
                pDialog.setMessage("Downloading file. Please wait...")
                pDialog.isIndeterminate = false
                pDialog.max = 100
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
                pDialog.setCancelable(true)
                pDialog.show()
                return pDialog
            }
        } catch (e: Exception) {
            pDialog?.dismiss()
        }

        return null
    }

    fun isWIFIOn(context: Context): Boolean {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getBoolean("wifi_on", false)
    }

    fun setWIFIOn(loginStatus: Boolean, context: Context) {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putBoolean("wifi_on", loginStatus)
        editor.commit()
    }

    fun isNetworkAvailable(ctx: Context): Boolean {
        val networkStatePermission = ctx.checkCallingOrSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)

        if (networkStatePermission == PackageManager.PERMISSION_GRANTED) {

            val mConnectivity = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            // Skip if no connection, or background data disabled
            val info = mConnectivity.activeNetworkInfo ?: return false
// Only update if WiFi
            val netType = info.type
            // int netSubtype = info.getSubtype();
            return if (netType == ConnectivityManager.TYPE_WIFI || netType == ConnectivityManager.TYPE_MOBILE) {
                info.isConnected
            } else {
                false
            }
        } else {
            return true
        }
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun slideAnimator(start: Int, end: Int, view: View): ValueAnimator {

        val animator = ValueAnimator.ofInt(start, end)
        animator.addUpdateListener { valueAnimator ->
            //Update Height
            val value = valueAnimator.animatedValue as Int
            val layoutParams = view.layoutParams
            layoutParams.height = value
            view.layoutParams = layoutParams
        }
        return animator
    }

    fun expand(view: View) {
        val matchParentMeasureSpec =
            View.MeasureSpec.makeMeasureSpec((view.parent as View).width, View.MeasureSpec.EXACTLY)
        val wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        view.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
        val targetHeight = view.measuredHeight

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.layoutParams.height = 1
        view.visibility = View.VISIBLE
        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                view.layoutParams.height = if (interpolatedTime == 1f)
                    ViewGroup.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                view.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (targetHeight / view.context.resources.displayMetrics.density).toInt().toLong()
        view.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun hasPermissions(context: Context?, vararg permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            Log.e("log per", "granted 1")
            for (permission  in permissions[0]) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    Log.e("log per", "granted 2")
                    return false
                }
            }
        }
        return true
    }

    fun getAppVersionName(context: Context): String {
        try {
            val pInfo = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_META_DATA)
            return pInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            return ""
        }

    }
}


