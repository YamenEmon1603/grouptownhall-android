package sslwireless.android.townhall.view.utils

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager

class SharedPrefHeaderSingleton {
    companion object {
        val instance = SharedPrefHeaderSingleton()
        private lateinit var mContext: Context

        private var mSharedPref: SharedPreferences? = null
        const val deviceId = "deviceId"
        const val simId = "simId"
        const val googleAdId = "googleAdId"
        const val subscriptionId = "subscriptionId"
        const val imeiNo = "imeiNo"
        const val sessionId = "sessionId"
        const val fcmToken = "fcmToken"
        const val networkConnectivtyCheck = "networkConnectivtyCheck"
        const val terminalCheckerFlag = "terminalCheckerFlag"
        const val storeUid = "storeUid"
        const val terminalID = "terminalID"
        const val skipStatus = "skipStatus"
        const val dropDown = "dropDown"
        const val recyclerItemNotify = "recyclerItemNotify"
        const val savePosition = "savePosition"
        const val dopDownCacheTimeStamp = "dopDownCacheTimeStamp"
        const val offerAmount = "offerAmount"
        const val spinnerHeaderName = "spinnerHeaderName"
        const val splashNetworkChecker = "splashNetworkChecker"
        const val noNetworkChecker = "noNetworkChecker"
        const val langChangeTracker = "langChangeTracker"
        const val payDistributorCheck = "payDistributorCheck"
        const val walletEnabled = "walletEnabled"
    }

    fun init(context: Context) {
        mContext = context
        if (mSharedPref == null) mSharedPref =
            context.getSharedPreferences(context.packageName, Activity.MODE_PRIVATE)
    }


    fun getData(key: String?, defValue: String?): String? {
        return mSharedPref!!.getString(key, defValue)
    }

    fun setData(key: String?, value: String?) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putString(key, value)
        prefsEditor.commit()
    }

    fun getBoolData(key: String?, defValue: Boolean): Boolean {
        return mSharedPref!!.getBoolean(key, defValue)
    }

    fun setBoolData(key: String?, value: Boolean) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putBoolean(key, value)
        prefsEditor.commit()
    }

    fun getIntData(key: String?, defValue: Int): Int? {
        return mSharedPref!!.getInt(key, defValue)
    }

    fun setIntData(key: String?, value: Int?) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putInt(key, value!!).commit()
    }

    fun getLongData(key: String?, defValue: Long): Long? {
        return mSharedPref!!.getLong(key, defValue)
    }

    fun setLongData(key: String?, value: Long?) {
        val prefsEditor = mSharedPref!!.edit()
        prefsEditor.putLong(key, value!!).commit()
    }

    fun isNetworkAvailable(): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}