package sslwireless.android.townhall.view.viewpager

import sslwireless.android.townhall.view.IAdapterListener
import sslwireless.android.townhall.data.model.offer.ExclusiveOffer
import sslwireless.android.townhall.databinding.ListItemOffersNewBinding
import sslwireless.android.townhall.view.base.BaseViewHolder2
import sslwireless.android.townhall.view.utils.AppConstants
import sslwireless.android.townhall.view.utils.loadImageWithRoundCorner

class OffersViewHolder(
    private val binding: ListItemOffersNewBinding, private val lang: String
) :
    BaseViewHolder2(binding.root) {


    override fun <T> onBind(position: Int, model: T, listener: IAdapterListener) {
        model as ExclusiveOffer

        binding.tvTitle.text = if (lang == AppConstants.Language.ENGLISH) model.title else model.title_bn
        binding.tvDetail.text = if (lang == AppConstants.Language.ENGLISH) model.description else model.description_bn
        binding.offerImage.loadImageWithRoundCorner(model.banner, 20)

        binding.root.setOnClickListener {
            listener.clickListener(position, model, binding.root)
        }
    }
}