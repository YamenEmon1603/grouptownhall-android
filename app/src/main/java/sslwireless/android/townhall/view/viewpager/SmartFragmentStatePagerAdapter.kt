package sslwireless.android.townhall.view.viewpager

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

abstract class SmartFragmentStatePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private val mFragments = SparseArray<Fragment>()
    override fun instantiateItem(mContainer: ViewGroup, mPosition: Int): Any {
        val mFragment = super.instantiateItem(mContainer, mPosition) as Fragment
        mFragments.put(mPosition, mFragment)
        return mFragment
    }

    override fun destroyItem(mContainer: ViewGroup, mPosition: Int, mObject: Any) {
        mFragments.remove(mPosition)
        super.destroyItem(mContainer, mPosition, mObject)
    }
}