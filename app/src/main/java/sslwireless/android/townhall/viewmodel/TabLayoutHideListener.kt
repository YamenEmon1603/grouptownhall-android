package sslwireless.android.townhall.viewmodel

interface TabLayoutHideListener {
    fun onTabLayoutHide(flag: Boolean)
}